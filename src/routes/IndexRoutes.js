import React, { useEffect } from 'react';
import {BrowserRouter,Switch,Route,useLocation, useHistory,Link,withRoute, withRouter} from 'react-router-dom';
import Login from '../bundles/common/components/Auth/Login';
import Signup from '../bundles/common/components/Auth/Signup';
import VerifyEmail from '../bundles/common/components/Auth/VerifyEmail';
import ResetPassword from '../bundles/common/components/Auth/ResetPassword';
import AdminApprove from '../bundles/company/components/UI/AdminApprove';
import StudentSignup from '../bundles/student/components/Auth/Signup';
import ActivationPage from '../bundles/common/components/UI/ActivationPage';
import CompanySignup from '../bundles/company/components/Signup/Signup';
import ForgotPassword from '../bundles/common/components/Auth/ForgotPassword';
import EmailConfirmation from '../bundles/company/components/UI/EmailConfirmation';
import UserApproved from '../bundles/common/components/UI/UserApproved';
import StudentHome from '../bundles/common/components/Dashboard/StudentHome';
import StudentHomeComponent from '../bundles/student/components/Home/StudentHome';
import CompanyHome from '../bundles/common/components/Dashboard/CompanyHome';
import CompanyHomeComponent from '../bundles/company/components/Home/CompanyHome';
import ViewJob from '../bundles/job/ViewJob';
import NotFound from '../bundles/common/components/UI/NotFound';
import Maintainence from '../bundles/common/components/UI/Maintainence';
import Profile from '../StudentSection/Page/Profile/Index'
import Layout from '../CollegeSection/layout/Layout';
import OpenJob from '../CollegeSection/page/OpenJob/OpenJob';
import CloseJob from '../CollegeSection/page/ClosedJob/CloseJob';
import NewJob from '../CollegeSection/page/NewJob/NewJob';
import CompanyCard from '../CollegeSection/components/companyCard/CompanyCard';
import Invite from '../CollegeSection/page/InviteJob/Invite';
import CompanyConnection from '../CollegeSection/page/CompanyConnection/CompanyConnection';
import TrackCollege  from '../CollegeSection/page/Track/Track';
// student


import AppliedJob from '../StudentSection/Page/AppliedJob/Index'
import HiddenJob from '../StudentSection/Page/HiddenJob/Index'
import SavedJob from '../StudentSection/Page/SavedJob/Index'
import OpenJobStudent from '../StudentSection/Page/OpenJob/NewIndex'
import InterviewJob from '../StudentSection/Page/InterviewJob/Index'
import StudentDashboard from '../StudentSection/Page/StudentDashboard/Index'
import StudentBlog from '../StudentSection/Page/Blog/Index'
import StudentSingleBlog from '../StudentSection/Page/singleblog/SingleBlog'
import PublicProfile from '../StudentSection/Page/publicprofile/Index' 
import ClosedJob from '../StudentSection/Page/ClosedJob/Index' 
import InvitedJob from '../StudentSection/Page/Invited/Index' 
import ScheduledJob from '../StudentSection/Page/scheduled/Index'
import ConnectionJob from '../StudentSection/Page/connection/Connection'
import CompanyDashboard from '../bundles/company/components/Home/Dashboard/Dashboard';
import Applicants from '../bundles/company/components/Home/Jobs/Applicants';
import PostJob from '../bundles/company/components/Home/Jobs/PostJob/PostJob';
import { Interview } from '../bundles/company/components/Home/Interview/Interview';
import { Track } from '../bundles/company/components/Home/Track/Track';



const IndexRoutes=()=>{
   
    return(
        <>
        <BrowserRouter>
            <Switch>
            <Route exact path="/login" component={Login} />
            <Route exact path="/login1" component={Login} />
                <Route path="/signup" component={Signup} /> 
                <Route path="/confirm-email" component={EmailConfirmation} />
                <Route path="/forgot-password" component={ForgotPassword} />
                <Route path="/verify-email/:token" component={VerifyEmail} />
                <Route path="/reset-password/:token" component={ResetPassword} />
                <Route exact path="/company/admin-approve" component={AdminApprove} />
                <Route exact path="/student/register" component={StudentSignup} />
                <Route path="/activation-link/:token" component={ActivationPage} />
                <Route exact path="/company/register" component={CompanySignup} />
                <Route  path="/user-approved/:token" component={UserApproved} />
                <Route exact path="/student-home" component={withRouter(StudentHome)} />
                <Route    path="/company-home" component={withRouter(CompanyHome)} />
                <Route  path="/student" component={withRouter(StudentHomeComponent)} />
                <Route path="/company" component={withRouter(CompanyHomeComponent)} />
                <Route path="/job/:token" component={ViewJob} />
                <Route path="/maintainence" component={Maintainence}/>
                <Route exact path='/' component={CheckUser}/>
                    {/* college route */}
                <Route exact path='/college/open-job' component={OpenJob}/>
                <Route exact path='/college/close-job' component={CloseJob}/>
                <Route exact path='/college/new-job' component={NewJob}/>
                <Route exact path='/college/invite' component={Invite}/>
                <Route exact path='/college/company-connection' component={CompanyConnection}/>
                <Route exact path='/college/track' component={TrackCollege}/>
            
                



                {/* Akshay's route */}
                {/* Student */}
          {/* <Route path="/student/profile" component={Profile}/>
        <Route exact path="/student/applied" component={AppliedJob}/>
        <Route exact path="/student/hidden" component={HiddenJob}/>
        <Route  exact path="/student/saved" component={SavedJob}/>
        <Route exact path="/student/open" component={OpenJob}/>
        <Route exact path="/student/interview" component={InterviewJob}/>
        <Route exact path='/student/blog' component={StudentBlog}/>
        <Route exact path='/student/singleblog/:id' component={StudentSingleBlog}/>
        <Route exact path='/student/dashboard' component={StudentDashboard}/>
        <Route exact path='/student/publicprofile' component={PublicProfile}/>
        <Route  exact path='/student/closed' component={ClosedJob}/>
        <Route exact path='/student/invited' component={InvitedJob}/>
        <Route  exact path='/student/scheduled' component={ScheduledJob}/>
        <Route exact path='/login' component={Login}/>
        <Route exact path='/student/connection' component={ConnectionJob}/> */}

        {/* Company */}

        {/* <Route path="/company/dashboard">
                        <CompanyDashboard/>
                    </Route>
                    <Route path="/company/job-applicants" >
                        <Applicants/>
                    </Route>
                    <Route path="/company/post-job">
                        <PostJob/>
                    </Route>
                    <Route path="/company/interview">
                        <Interview/>
                    </Route>
                    <Route path="/company/track">
                        <Track/>
                    </Route> */}
                    <Route component={NotFound} />


         
        {/* <Route exact path='/' component={StudentDashboard}/> */}
            </Switch>


        </BrowserRouter>

        </>
    )
}

export default IndexRoutes;


const CheckUser=()=>{

    const history=useHistory();
    let location = useLocation()
    useEffect(()=>{
        const data=localStorage.getItem('user_type')
        if(data==='Company'){
            history.push('/company/dashboard')
        }else if(data==='Student'){
            history.push('/student/dashboard')
        }
    },[])
    return(
        null
    )
}