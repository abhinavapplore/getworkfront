import React,{useEffect} from 'react';
import {Link,BrowserRouter,Route,NavLink,Switch} from 'react-router-dom';
// import StudentDashboard from '../bundles/student/components/Home/Dashboard/Dashboard';
import StudentProfile from '../bundles/student/components/Home/Profile/StudentProfile';
import ChangePassword from '../bundles/student/components/Auth/ChangePassword';
import NotFound from '../bundles/common/components/UI/NotFound';
import Profile from '../StudentSection/Page/Profile/Index'
import AppliedJob from '../StudentSection/Page/AppliedJob/Index'
import HiddenJob from '../StudentSection/Page/HiddenJob/Index'
import SavedJob from '../StudentSection/Page/SavedJob/Index'
import OpenJob from '../StudentSection/Page/OpenJob/NewIndex'
import InterviewJob from '../StudentSection/Page/InterviewJob/Index'
import StudentDashboard from '../StudentSection/Page/StudentDashboard/Index'
import StudentBlog from '../StudentSection/Page/Blog/Index'
import StudentSingleBlog from '../StudentSection/Page/singleblog/SingleBlog'
import PublicProfile from '../StudentSection/Page/publicprofile/Index' 
import ClosedJob from '../StudentSection/Page/ClosedJob/Index' 
import InvitedJob from '../StudentSection/Page/Invited/Index' 
import ScheduledJob from '../StudentSection/Page/scheduled/Index'
import ConnectionJob from '../StudentSection/Page/connection/Connection'
import Login from '../bundles/common/components/Auth/Login'
const StudentRoutes=({studentData})=>{
    //console.log('inside here from routes :) ');
    // useEffect(()=>{
    //     console.log(studentData);
    // },[])
    return(
        <>

            <Switch>
                <Route path="/student/profile" 
                    render={(props)=><StudentProfile {...props} studentData={studentData} />}
                 />
                {/* <Route path="/student/dashboard" component={StudentDashboard} />
                <Route path="/student/update-password" component={ChangePassword} /> */}
                {/* Akhsay's route */}
        {/* <Route path="/student/profile" component={Profile}/> */}
        <Route path="/student/applied" component={AppliedJob}/>
        <Route path="/student/hidden" component={HiddenJob}/>
        <Route path="/student/saved" component={SavedJob}/>
        <Route path="/student/open" component={OpenJob}/>
        <Route path="/student/interview" component={InterviewJob}/>
        <Route path='/student/blog' component={StudentBlog}/>
        <Route path='/student/singleblog/:id' component={StudentSingleBlog}/>
        <Route path='/student/dashboard' component={StudentDashboard}/>
        <Route path='/student/publicprofile' component={PublicProfile}/>
        <Route path='/student/closed' component={ClosedJob}/>
        <Route path='/student/invited' component={InvitedJob}/>
        <Route path='/student/scheduled' component={ScheduledJob}/>
        <Route path='/login' component={Login}/>
        <Route path='/student/connection' component={ConnectionJob}/>
        <Route path="/student/update-password" component={ChangePassword} />

        

                {/* <Route component={NotFound}/> */}
            </Switch>
       
        </>
    )
}

export default StudentRoutes;