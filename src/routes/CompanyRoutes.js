import React, { useEffect, useState } from "react";
import { Link, BrowserRouter, Route, NavLink, Switch } from "react-router-dom";
import CompanyDashboard from "../bundles/company/components/Home/Dashboard/Dashboard";
import Applicants from "../bundles/company/components/Home/Jobs/Applicants";
import PostJob from "../bundles/company/components/Home/Jobs/PostJob/PostJob";
import { Interview } from "../bundles/company/components/Home/Interview/Interview";
import { Track } from "../bundles/company/components/Home/Track/Track";

const CompanyRoutes = ({ companyData }) => {
  useEffect(() => {
    console.log(companyData);
  }, [companyData]);
  return (
    <>
      <Switch>
        <Route exact path="/company/dashboard">
          <CompanyDashboard />
        </Route>
        <Route exact path="/company/job-applicants">
          <Applicants />
        </Route>
        <Route exact path="/company/post-job">
          <PostJob />
        </Route>
        <Route exact path="/company/interview">
          <Interview />
        </Route>
        <Route exact path="/company/track">
          <Track />
        </Route>
      </Switch>
    </>
  );
};

export default CompanyRoutes;
