import React from 'react';

const ScrollableRadioList=({state,setState,listData})=>{
const scrollableListStyle={
    border: '1px solid #ced4da',
    maxHeight: '100px',
    overflowY: 'scroll'
}
let tempData=['Option 1','Option 2','Option 3','Option 4','Option 5','Option 6','Option 7']
const handleChange=(e)=>{
    //console.log(e.target.value);
    setState({[e.target.id]:e.target.value})
}
let list=[];
//console.log(listData)
if(listData){
   // console.log(listData, typeof(listData), listData[0])
    listData.forEach(data=>{
    list.push(

        
        <div className="form-check" key={data.id}>
                   
                        <input className="form-check-input" onChange={handleChange} key={data.id} type="radio" id={data.id} value={data.name} checked={Object.values(state)[0]==data.name}/>
                    {/* <span className="fs-14 ml-2 list-item"></span> */}
                    <label for={data.id} className="fs-14 ml-2 list-item">{data.name}
                    </label>
                </div>
   

    )
    })
}
    return(
        <> 
        <form class="form-horizontal checkbox-list pl-2 shadow_1-lightest my-2"  style={scrollableListStyle}>
        {list}
        </form>

        </>
    )
}

export default ScrollableRadioList;