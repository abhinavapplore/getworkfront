import React,{useState, useEffect} from 'react';
import {Link,useLocation, useHistory} from 'react-router-dom';
import './Cards.css';


const SignupOptions=({userid,email,isemailVerified,token})=>{
    // const [userID,setUserID]=useState(null);
     let location=useLocation();
    const cardBackGround={
        background:'#F8F9FA'
    }

     let history=useHistory();

    useEffect(()=>{
        console.log(userid);
        console.log(email)
        console.log(isemailVerified)
        console.log(token)
    },[])

    const navigateSignup=(userType)=>{
        if(userType==1)
        history.push({
            pathname:'/student/register',
            userID:userid,
            email,
            token
        });
        else if(userType==5){
            if(isemailVerified){
                history.push({
                    pathname:'/company/register',
                    userID:userid,
                    email,
                    token
                })
            }
            else{
                history.push({
                    pathname:'/confirm-email',
                    email
                })
            }

        }
    }


      
        return (
            <>
            <h1 className="mb-4 login-heading text-left">Signup as a</h1>

            <div className="row text-left">
                <div className="large-list">
                    <ul>
                        <li className="my-4 shadow_1" onClick={()=>{navigateSignup(1)}}>
                            {/* <i className="px-4  fas fa-user-graduate fa-3x "></i> */}
                            <span className="fw-700 fs-34 py-3 px-3">Student</span></li>
                        <li  className="my-4 shadow_1" onClick={()=>{navigateSignup(5)}}>
                            {/* <i className="px-4  fas fa-building fa-3x"></i> */}
                            <span className="fw-700 fs-34 py-3 px-3">Company</span></li>
                    </ul>
                </div>
            </div>


            </>
         
        )
      

}

export default SignupOptions;