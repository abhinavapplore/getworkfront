import React from 'react';
import { functionDeclaration } from '@babel/types';

const ScrollableCheckboxList=({state,setState,listData})=>{
const scrollableListStyle={
    border: '1px solid #ced4da',
    maxHeight: '100px',
    overflowY: 'scroll'
}
const found=(arr,item)=>{
    let ind=0;
    let isPresent=false;
    console.log('found func called')
    arr.forEach(a=>{
        
        if(Object.values(a)[0]==item){
            console.log('found item, return from here')
            isPresent=true;
             return;
        }
        ind++;
    })
if(isPresent) return ind;
return -1;
}
let tempData=['Option 1','Option 2','Option 3','Option 4','Option 5','Option 6','Option 7']
const handleChange=(e)=>{
    //console.log(e.target.value);
    const val=e.target.value;
    const ind=found(state,val)
    console.log(ind);
    if(ind!=-1){
    console.log('found so remove')
            //remove it from state
    setState(state.splice(ind,1))
    console.log(state)
       
    }
    else
    setState([...state,{[e.target.id]:e.target.value}])
}
let list=[];
//console.log(listData)
if(listData && listData.length>0)
listData.forEach(data=>{
    list.push(
                <div className="checkbox text-left" key={data.id}>
                    <label>
                        <input onChange={handleChange} key={data.id} type="checkbox" id={data.id} defaultValue={data.name} />
                    <span className="fs-14 ml-2 list-item">{data.name}</span>
                    </label>
                </div>
   

    )
})
    return(
        <> 
        <form class="form-horizontal checkbox-list pl-2 shadow_1-lightest my-2"  style={scrollableListStyle}>
        {list}
        </form>

        </>
    )
}

export default ScrollableCheckboxList;