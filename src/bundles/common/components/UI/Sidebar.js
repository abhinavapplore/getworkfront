import React from 'react';
import getworkLogo from '../../../../assets/images/getwork_new.png';
import {Link, useHistory} from 'react-router-dom';
import campus_icon from '../../../../assets/images/icons/campus-icon.png'
import home_icon from '../../../../assets/images/icons/home-icon.png'
import job_icon from '../../../../assets/images/icons/job-icon.png'
import manage_icon from '../../../../assets/images/icons/manage-icon.png'
import relationships_icon from '../../../../assets/images/icons/relationships-icon.png'
import plus_icon from '../../../../assets/images/icons/plus-icon.png'
import rectangle_27 from '../../../../assets/images/Rectangle 27.png'
import gw_logo_2 from '../../../../assets/images/getwork_new.png'

const LinkIconStyles={
  position: 'absolute',
  right: '20px',
  'marginTop':'0.25rem'
}

const SidebarStyles={
  boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
  zIndex:'100001',

}

const SidebarLinkItemStyles={
  position: 'absolute',
  left: '20%'
}



const Sidebar=({userType})=>{
  const history=useHistory()
  
  return(
    <>

<nav style={SidebarStyles} className="pt-1 navbar-nav bg-gradient-primary sidebar bg-white sidebar-dark accordion" style={{width:'17%', marginTop:'35px'}} id="accordionSidebar">
        <div className="row ">
          <div className="col">
          <Link to="/">
            <img  src={gw_logo_2} style={{width:'99px',paddingLeft:'26px'}} alt=""/>
          </Link>
          </div>
          <div className="col">
          <img className="mt-2 pt-1 pr-3" src={rectangle_27} alt=""/>
          </div>


      
        </div>

    
  <div className="links text-left my-4">


{
  userType==="STUDENT" && (
    <>
    <ul className="pl-2 my-3" >
      
      <li className="nav-item ">
        <a className="nav-link collapsed sidebar-link pl-2" href="/student/dashboard" >
        <i className="fas fa-columns" />
          <span className="ml-2">Dashboard</span>
        </a>

      </li>

    </ul>
    <ul className="pl-2 my-3" >

        <span className="fs-12">Open Jobs</span>

            <li className="nav-item sidebar-section-link my-1">
              <a className="nav-link collapsed sidebar-link pl-3" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
              <i className="fas fa-users" />
                <span className="ml-2">Off Campus</span>
              </a>
            </li>

            <li className="nav-item sidebar-link  sidebar-section-link my-1">
              <a className="nav-link collapsed sidebar-link  pl-3" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
              <i className="fas fa-user-friends" />
                <span className="ml-2">On Campus</span>
              </a>

            </li>
        </ul>
        <ul className="pl-2 my-3" >
          <span className="fs-12">My Jobs</span>

            <li className="nav-item sidebar-link sidebar-section-link">
              <a className="nav-link collapsed sidebar-link  pl-3" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
              <i class="fas fa-save"></i><span className="ml-2">Saved Jobs</span>
              </a>

            </li>

            <li className="nav-item sidebar-link sidebar-section-link ">
              <a className="nav-link collapsed sidebar-link pl-3" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
              <i className="fas fa-laptop-house" /><span className="ml-2">Applied Jobs</span>
              </a>

            </li>

            <li className="nav-item sidebar-link sidebar-section-link ">
              <a className="nav-link collapsed sidebar-link pl-3" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
              <i className="fas fa-history" /><span className="ml-2">Past/History</span>
              </a>

            </li>
        </ul>

      <ul className="pl-2 my-3" >
        <span className="fs-12">Resume</span>


            <li className="nav-item sidebar-link  sidebar-section-link ">
              <a className="nav-link collapsed sidebar-link pl-3" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
              <i className="far fa-file" /><span className="ml-2">Update Resume</span>
              </a>

            </li>
      </ul>

    </>
  )
}
{
  userType=="COMPANY" && (
    <>
    <div className="row my-5 w-100 mx-2" >
    
       <button className="ml-4 btn btn-sm btn-main btn-main fw-500 fs-18" onClick={()=>history.push('/company/post-job')}>Create Job<span><img className="ml-3" src={plus_icon} alt=""/></span></button>

    </div>
    <ul className="px-2 px-xl-4 my-3" >
      
      <li className="nav-item ">
        <Link  activeClassName='link-active' className="fs-18 nav-link collapsed sidebar-link pl-2" to="/company/dashboard" >
        <img src={home_icon} alt=""/>
          <span className="ml-3" style={SidebarLinkItemStyles}>Home</span>
        </Link>

      </li>

    </ul>
    <ul className="px-2 px-xl-4 my-3">

        <a className="fs-18 sidebar-link " data-toggle="collapse" href="#jobs" role="button" aria-expanded="false" aria-controls="collapseExample">
            <img src={job_icon} /><span className="ml-3" style={SidebarLinkItemStyles}>Jobs</span>     
            <i style={LinkIconStyles} className="fas fa-angle-right"></i>
     
         </a>

        <div class="collapse ml-4" id="jobs">

        <li className="nav-item sidebar-section-link my-1">
              <Link className="nav-link collapsed sidebar-link pl-3" to="/company/job-applicants" >
              {/* <i className="fas fa-users" /> */}
                <span className="ml-4 fs-16">Applicants</span>
              </Link>
            </li>

           

            <li className="nav-item sidebar-link  sidebar-section-link my-1">
              {/* <i className="fas fa-user-friends" /> */}
              <Link className="nav-link collapsed sidebar-link pl-3" to="/company/track" >
              {/* <i className="fas fa-users" /> */}
                <span className="ml-4 fs-16">Track</span>
              </Link>
            </li>


            <li className="nav-item sidebar-link  sidebar-section-link my-1">
              {/* <i className="fas fa-user-friends" /> */}
              <Link className="nav-link collapsed sidebar-link pl-3" to="/company/interview" >
              {/* <i className="fas fa-users" /> */}
                <span className="ml-4 fs-16">Interview</span>
              </Link>
            </li>

        </div>
          
        </ul>
    <ul className="px-2 px-xl-4 my-3" >

        <a className="fs-18 sidebar-link" data-toggle="collapse" href="#relationships" role="button" aria-expanded="false" aria-controls="collapseExample">
            <img src={relationships_icon} /><span className="ml-3" style={SidebarLinkItemStyles}>Relationships</span>    
            <i style={LinkIconStyles} className="fas fa-angle-right"></i>
      
         </a>


        <div class="collapse ml-4" id="relationships">
        <li className="nav-item sidebar-section-link my-1">
              <a className="nav-link collapsed sidebar-link pl-3" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
              {/* <i className="fas fa-users" /> */}
                <span className="ml-4 fs-16 ">Applicants</span>
              </a>
            </li>

            <li className="nav-item sidebar-link  sidebar-section-link my-1">
              <a className="nav-link collapsed sidebar-link  pl-3" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
              {/* <i className="fas fa-user-friends" /> */}
                <span className="ml-4 fs-16">Track</span>
              </a>

            </li>

            <li className="nav-item sidebar-link  sidebar-section-link my-1">
              <a className="nav-link collapsed sidebar-link  pl-3" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
              {/* <i className="fas fa-user-friends" /> */}
                <span className="ml-4 fs-16 ">Source</span>
              </a>

            </li>
        </div>
          
        </ul>
    <ul className="px-2 px-xl-4 my-3" >

        <a className="fs-18 sidebar-link" data-toggle="collapse" href="#campus" role="button" aria-expanded="false" aria-controls="collapseExample">
            <img src={campus_icon} /><span className="ml-3" style={SidebarLinkItemStyles}>Campus</span>          
            <i style={LinkIconStyles} className="fas fa-angle-right"></i>
         
         </a>


        <div class="collapse ml-4" id="campus">
        <li className="nav-item sidebar-section-link my-1">
              <a className="nav-link collapsed sidebar-link pl-3" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
              {/* <i className="fas fa-users" /> */}
                <span className="ml-4 fs-16">Applicants</span>
              </a>
            </li>

            <li className="nav-item sidebar-link  sidebar-section-link my-1">
              <a className="nav-link collapsed sidebar-link  pl-3" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
              {/* <i className="fas fa-user-friends" /> */}
                <span className="ml-4 fs-16">Track</span>
              </a>

            </li>

            <li className="nav-item sidebar-link  sidebar-section-link my-1">
              <a className="nav-link collapsed sidebar-link  pl-3" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
              {/* <i className="fas fa-user-friends" /> */}
                <span className="ml-4 fs-16">Source</span>
              </a>

            </li>
        </div>
          
        </ul>
    <ul className="px-2 px-xl-4 my-3" >

        <a className="fs-18 sidebar-link" data-toggle="collapse" href="#manage" role="button" aria-expanded="false" aria-controls="collapseExample">
            <img src={manage_icon} /><span className="ml-3" style={SidebarLinkItemStyles}>Manage</span>
            <i style={LinkIconStyles} className="fas fa-angle-right"></i>
          
         </a>


        <div class="collapse ml-4" id="manage">
        <li className="nav-item sidebar-section-link my-1">
              <a className="nav-link collapsed sidebar-link pl-3" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
              {/* <i className="fas fa-users" /> */}
                <span className="ml-4 fs-16">Applicants</span>
              </a>
            </li>

            <li className="nav-item sidebar-link  sidebar-section-link my-1">
              <a className="nav-link collapsed sidebar-link  pl-3" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
              {/* <i className="fas fa-user-friends" /> */}
                <span className="ml-4 fs-16">Track</span>
              </a>

            </li>

            <li className="nav-item sidebar-link  sidebar-section-link my-1">
              <a className="nav-link collapsed sidebar-link  pl-3" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
              {/* <i className="fas fa-user-friends" /> */}
                <span className="ml-4 fs-16">Source</span>
              </a>

            </li>
        </div>
          
        </ul>
 
    </>
  )
}

  <div class="dropdown-divider mt-5"></div>
      <ul className="pl-2 my-1" >
      <li className="nav-item">
        <a className="nav-link collapsed sidebar-link pl-2" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
        <i className="far fa-address-book" /><span className="ml-2">Settings</span>
        </a>

      </li>

    </ul>

  <ul className="pl-2 my-1" >


      <li className="nav-item">
        <a className="nav-link collapsed sidebar-link pl-2" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
        <i className="fas fa-question-circle" /><span className="ml-2">Help </span>
        </a>

      </li>

    </ul>

  </div>
 
   
</nav>





    </>
  )

}

export default Sidebar;