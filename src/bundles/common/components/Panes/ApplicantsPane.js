import React,{useState, useEffect} from 'react';
import Axios from 'axios';

const PaneStyles={
    background: '#FFF',
    boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
    width:'83vw',
    height:'75px',
    top:'auto'
}
const ApplicantsPane=({jobRoles,selectedJobRole,setSelectedJobRole,status,setStatus,sortBy,setSortBy})=>{
    const [statuses,setStatuses]=useState(['All','Applied','Review'])
    const sortParams=['All','Apply Date','Best Match','Colleges'];


    const getAllStatuses=()=>{
        Axios.get('http://54.162.60.38/job/status/')
            .then(res=>{
                console.log(res)
                if(res.data.success){
                    let temp=statuses;
                    res.data.data.forEach(status=>{
                        temp.push(status.name)
                    })

                    console.log(temp)

                    setStatuses(temp);
                }
                else{

                }
            })
            .catch(err=>{
                console.log(err)
            })
    }

    useEffect(()=>{
        // getAllStatuses();
    },[])

    return(
        <>
            <nav style={PaneStyles} className="fs-18 navbar small-nav navbar-expand navbar-light topbar fixed-top shadow-main ml-auto">
                <div className="row">
                    {/* Separate this as a component */}
                        <p className="my-auto pl-5">Profile</p>
                        <div className="dropdown ml-4">
                            <div  className="dropdown-sort dropdown-toggle-custom" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style={{whiteSpace:'nowrap',overflow:'hidden',textOverflow:'ellipsis'}}>
                                {selectedJobRole}
                            </div>
                            <div className="dropdown-menu" style={{height:'50vh',overflowY:'scroll'}} aria-labelledby="dropdownMenuButton">
                                {
                                    jobRoles && jobRoles.map(job_role=>{
                                        return(
                                            <button onClick={()=>{setSelectedJobRole(job_role)}}  className="dropdown-item fs-18" value={job_role} >{job_role}</button>
                                        )   
                                    })
                                }
                              
                            </div>
                        </div>
                    {/* Separate this as a component */}
                    { 
                      
                        <>
                        <p className="my-auto pl-4">Status</p>
                        <div className="dropdown ml-4">
                            <div  className="dropdown-sort dropdown-toggle-custom fs-18" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               {status=='Applied' ? ('Applied'):status}
                            </div>
                            <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                {
                                    statuses && statuses.map((current_status,ind)=>{
                                        if(ind<=3)
                                        return(
                                            <button onClick={()=>{setStatus(current_status)}}  className="dropdown-item fs-18" value="All Jobs"  >{current_status==='Applied' ? ('Applied'):current_status}</button>

                                        )
                                    })
                                }
                                
                            </div>
                        </div>
                     {/* Separate this as a component */}
                                           <p className="my-auto pl-4">Sort by</p>
                        <div className="dropdown ml-4">
                            <div  className="dropdown-sort dropdown-toggle-custom" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span className="fs-18">{sortBy}</span>
                            </div>
                            <div className="dropdown-menu fs-18" aria-labelledby="dropdownMenuButton">
                                {
                                    sortParams && sortParams.map(param=>{
                                        return(
                                            <button onClick={()=>{setSortBy(param)}}  className="dropdown-item" value="Apply Date"  >{param}</button>

                                        )
                                    })
                                }

                            </div>
                        </div>
                        </>

                      
                    }
                        
 
                </div>

            </nav>
        </>
    )
}

export default ApplicantsPane;