import React,{useState} from 'react';
import './Panes.css';

// const PaneStyles={

// }
const JobPane=({setShow, setSearched})=>{
    const [selected,setSelected]=useState('All Jobs')
    const handleSelect=(e)=>{
        console.log('inside change: ',e.target.value)
        setSelected(e.target.value)
        setShow(e.target.value)
    }
    return(
        <>
            <nav  className="job-pane fs-18 fixed-top navbar small-nav navbar-expand navbar-light topbar mb-4 shadow-main">
                <div className="row">
                    {/* Separate this as a component */}
                        
                        <p className="my-auto ml-5 pl-5">Sort by</p>
                            <div className="dropdown ml-3">
                                <div  className="dropdown-sort dropdown-toggle-custom" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span className="fs-18 gray-2">{selected}</span>
                                </div>
                                <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <button onClick={handleSelect}  className="dropdown-item gray-2" value="All Jobs"  >All Jobs</button>
                                    <button onClick={handleSelect} className="dropdown-item gray-2" value="Open Jobs" href="#" >Open Jobs</button>
                                    <button onClick={handleSelect} className="dropdown-item gray-2" value="Drafted Jobs" href="#"  >Drafted Jobs</button>
                                    <button onClick={handleSelect} className="dropdown-item gray-2" value="Closed Jobs" href="#"  >Closed Jobs</button>
                                </div>
                            </div>
                      </div>
                       
                            <div className="gw-input-container ml-auto mr-5">
                                <i class="fas fa-search"></i>
                                <input onChange={(e)=>{setSearched(e.target.value)}} type="text" className="ml-5 form-control input-secondary input-small fs-18" placeholder="Search by Job Name"></input>
                            </div>
                       

                        {/* </div> */}

             
                {/* Separate this as a component */}

            </nav>
        </>
    )
}

export default JobPane;