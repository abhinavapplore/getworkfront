import React,{useState, useEffect, useRef} from 'react';
import './Login.css';
import '../../../../icons.css';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'
import {Link, useHistory} from 'react-router-dom';
import GoogleLogin from 'react-google-login';
import axios from 'axios';
import {useAlert} from 'react-alert';
import {emailRegex, EndPointPrefix} from '../../../../constants/constants';
import SideImage from '../UI/SideImage';
import AuthHeader from '../UI/AuthHeader';
import { Tooltip } from 'reactstrap';
import AdminApprove from '../../../company/components/UI/AdminApprove';
import Loader from '../UI/Loader';


const Login=()=>{




    //axios.defaults.withCredentials=true;
    const alert=useAlert();
    const history=useHistory();

    //if(localStorage.getItem('gw_token')) history.push('/home')


    //prevent first render calls
    const firstRender_1=useRef(true);
    const firstRender_2=useRef(true);
    const firstRender_3=useRef(true);

    
    //state variables
    const [loading,setLoading]=useState(false);
    const [tooltipOpen, setTooltipOpen] = useState(false);
    const toggle = () => setTooltipOpen(!tooltipOpen);
    const [showPassword,setShowPassword]=useState(false);
    let passwordFieldType= showPassword?'text':'password';
    let tooltipText= showPassword?'Hide Password':'Show Password';
    const [showApprovalPage,setShowApprovalPage]=useState({
        show:false,
        company:null
    })
    const [Email,setEmail]=useState('');
    const [Password,setPassword]=useState('');
    const [authToken,setAuthToken]=useState('');
    const [fbAccessToken,setfbAccessToken]=useState('');
    const [googleAccessToken,setgoogleAccessToken]=useState('');
    const [errors,setErrors]=useState(
        {
            emailError:'',
            passwordError:'',
            disabledClass:'disabled',
            isError:true
           
        }
    )
    
    const validate=(state,val)=>{

        switch(state){
        case 'Email':
        if(!val) 
        {
            
            setErrors({...errors,emailError:'Email cannot be left blank', isError:true})
      
        }
        else if(val && !emailRegex.test(val)){
            
            setErrors({...errors,emailError:'Please enter a valid email',isError:true})
      
        } 
        else{ 
            setErrors({...errors,emailError:''}) 
        }
        break;
        case 'Password':
        if(!val) {
            setErrors({...errors,passwordError:'Password cannot be left blank',isError:true})
      
        }
        else if(val && val.length<6){ 
            setErrors({...errors,passwordError:'Password must be at least 6 characters long',isError:true})
      
        }
        else{ 
            setErrors({...errors,passwordError:''})
      
        }
        break;
        }
      
      }
    useEffect(()=>{
     
        let accessToken='';
        if(firstRender_1.current){
            firstRender_1.current=false;
            return;
        }

        //Facebook Auth API calls
        const fbGraphAPICalls=async()=>{
            
        
            axios.get(`https://graph.facebook.com/v6.0/oauth/access_token?grant_type=fb_exchange_token&client_id=3527199753979643&client_secret=985184d9659e8d8d4e829cd6cbca1b6a&fb_exchange_token=${fbAccessToken}`)
            .then(res=>{
                console.log('res: ',res);
                //setLongAccessToken(res.data.access_token)
                accessToken=res.data.access_token;
                // console.log('sending access token: ',accessToken)
            })
                .then(()=>{
                    axios.post(EndPointPrefix+'/oauth/login/',{
                        headers: {
                            'Content-Type': 'application/json',
                            "Connection": "keep-alive"
                        },
                        
                                "provider": "facebook",
                                "access_token": accessToken
        
                    })
                        .then(res=>{
                            console.log('res: ',res);

                            if(!res.data.profile_complete){
                                //send to student signup
                                let userdata=res.data;
                                let data={
                                    "FirstName":userdata.first_name,
                                    "LastName":userdata.last_name,
                                    "StudentID":userdata.id,
                                    "Email":userdata.email
                                }
                                alert.show('New user, will be navigated to signup')
                                history.push({
                                    pathname:'/student/register',
                                    data
                                })
                            }else{
                                setAuthToken(res.data.token)
                                localStorage.setItem('gw_token',res.data.token);
                                history.push('/home');
                                alert.show('existing user, proceed to dashboard')
                            }
                        })
                        .catch(err=>{
                            console.log('err: ',err)
                            alert.error('Error occured!')
                        })
                })
                .catch(err=>{
                    console.log(err);
                })

 
        } 
            
        fbGraphAPICalls();
        
    },[fbAccessToken])

    useEffect(()=>{
        setLoading(false)
    },[])

    useEffect(()=>{
        let token='';
        if(firstRender_2.current){
            firstRender_2.current=false;
            return;
        }
        if(googleAccessToken)
        axios.post(EndPointPrefix+'/oauth/login/',{
            headers: {
                'Content-Type': 'application/json',
                "Connection": "keep-alive"
            },
            
                    "provider": "google-oauth2",
                    "access_token": googleAccessToken

        })
            .then(res=>{
                console.log(googleAccessToken);
                console.log('res: ',res);
                token=res.data.token;
                console.log(token)
                localStorage.setItem('gw_token',token)
                setLoading(true)
                checkUser(token);
 
            })
            .catch(err=>{
                console.log('err: ',err);
                alert.error('Error')
            })
        console.log('fired!');

    },[googleAccessToken])
    
    useEffect(()=>{
       
        if(firstRender_3.current){
            firstRender_3.current=false;
            return;
        }
        //disable and enable sign in button on form error
        if(Email && Password && emailRegex.test(Email) )
        setErrors({...errors,isError:false, disabledClass:''});
        else
        setErrors({...errors,isError:true, disabledClass:'disabled'});

    
    },[Email,Password])
   

    const fbAuth=(data)=>{
        console.log('data',data);
    }

    //access short life token from facebook popup response
    const responseFacebook = (response) => {
        console.log(response.accessToken);
        setfbAccessToken(response.accessToken);
      }

    //access short life token from google popup response
    const responseGoogle=(response)=>{
        console.log(response);
        setgoogleAccessToken(response.accessToken);
    }

    const handleChange=(e)=>{
        e.preventDefault();
        //console.log(e.target.id);
        let field=e.target.id;
        if(field==='Email') setEmail(e.target.value);
        else                setPassword(e.target.value);

        validate(e.target.id,e.target.value,errors,setErrors);

    }

    const checkUser=(token)=>{
        axios.get(EndPointPrefix+'/profile/details/',{
            headers:{
                "Authorization":'Token '+token
            }
        })
            .then(res=>{
               
                console.log('user details: ',res)
                    //CASE 1: user type does not exist => profile is also not completed
                    if(res.data.user_type.length==0){

                        if(!res.data.email_verified){
                         //move to signup options component
                        history.push({
                            pathname:'/signup',
                            step:2,
                            userid:res.data.id,
                            email:res.data.email,
                            token,
                            isemailVerified:false
                        })
                        }
                       else{
                         //move to signup options component
                        history.push({
                            pathname:'/signup',
                            step:2,
                            userid:res.data.id,
                            token,
                            email:res.data.email,
                            isemailVerified:true
                        })
                        }


                }else{
                    localStorage.setItem('user_type',res.data.user_type[0].main_user)
                    if(res.data.email_verified){
                        localStorage.setItem('gw_token',token);
                        //check if its student or company
                        if(res.data.user_type[0].type==1){
                            //student with verified email, go to home
                            console.log('from #1')
                            history.push('/student/dashboard')
                        }
                        else{
                            //company user with verified email, check if admin approved
                            if(!res.data.company_details.admin_approved){
                                //alert.show('Please wait while admin approves you');
                                history.push({
                                    pathname:'/company/admin-approve',
                                    company:res.data.company_details.company_name
                                })
                            }
                            else{
                                console.log(res.data);
                                // console.log(res.data.user_type[0]);
                                // console.log(res.data.user_type[0]);
                                // localStorage.setItem('user',JSON.stringify(res.data.user_type[0].main_user))
                                localStorage.setItem('company',JSON.stringify(res.data.company_details))
                                history.push('/company/dashboard')
                            }
                           
                        }
                    }
                    else{
                        //check if its student or company
                        if(res.data.user_type[0].type==1){
                            localStorage.setItem('gw_token',token);
                            //student without verified email, go to home
                            console.log('from #2')
                            history.push('/student/dashboard')
                        }
                        else{
                            //company user without verified email
                            history.push({
                                pathname:'/confirm-email',
                                email:res.data.data.email
                          })
                        }
                    }

                }
                

                    //CASE 2: 
            })
            .catch(err=>{
                setLoading(false)
                console.log(err);
                alert.error('Oop! There is some error, try again later.')
            })
        //  history.push('/home');
         //getUserInfo(res.data.token);

    }
    const loginUser=()=>{
        setLoading(true);
        let token='';
        axios.post(EndPointPrefix+'/login/',{
            "email":Email,
            "password":Password
        })
        .then(res=>{
            //setLoading(false)
            console.log(res);
            if(res.data.success)
                alert.success(res.data.data.message)
            else{
                alert.error(res.data.error)
                setLoading(false)
                return;
            }
            console.log('res from login api: ',res.data.data.token);
            
            token=res.data.data.token;
             setAuthToken(token)
             localStorage.setItem('gw_token',res.data.token);
             checkUser(token)
        })
        .catch(err=>{
            alert.error('Error');
            console.log(err);
        })
    }

    const handleSubmit=(e)=>{
        e.preventDefault();
        //console.log(errors);
        console.log(`checking auth for user ${Email}`)
        loginUser();
        
    }


    

    return loading ? <Loader/> :(
        <>

        <div className="container-fluid">
        <div className="row no-gutter full-page-view">

          <SideImage/>
          <div style={{background:'#ffff'}} className="col-md-8  col-lg-8">
            <div className="row">
  
                <AuthHeader/>
            
            </div>
            <div className="login d-flex align-items-center py-5 mx-md-0 mx-3 px-md-0 px-2">
              <div className="container">
                <div className="row">
                  <div className="col-md-9 col-lg-5 mx-auto">

                      {
                          showApprovalPage.show?
                          ( <> <AdminApprove company={showApprovalPage.company}/> </> ) 
                          :
                          (<>
                          
                            <h1 className="mb-4 login-heading text-left">Sign In
                            
                        </h1>
                        <form onChange={handleChange} onSubmit={handleSubmit} className="my-md-0 my-2"> 
                        <div className="form-group text-left">
                                <label htmlFor="Email" className="fw-500">Enter your Email address</label>
                                <input type="email" className="form-control mt-2 mb-1 shadow_1-lightest" id="Email" aria-describedby="emailHelp" placeholder="" required autoFocus/>


                        </div>
                        <div className="form-group text-left pt-md-2">
                                <label htmlFor="Password" className="fw-500">Enter your Password</label>
                                <div className="row no-gutters">
                                    <div className="col">
                                    <input type={passwordFieldType} className="form-control mt-2 mb-1 shadow_1-lightest" id="Password" placeholder="" required  />
                                    </div>
                                    <div className="col-auto">
                                        <i onClick={()=>{setShowPassword(!showPassword)}} id="toggleShowPassword" className="fas fa-eye show-password-icon"></i>
                                    </div>
                                    <Tooltip className="fs-13" placement="right" isOpen={tooltipOpen} target="toggleShowPassword" toggle={toggle}>
                                    {tooltipText}
                                    </Tooltip>
                                </div>

                        </div>

                        <div className="row my-1 pt-md-2 pt-3">
                            <div className="col-6 text-left">

                            <button className={errors.disabledClass+" shadow_1 btn btn-lg btn-main btn-login text-uppercase font-weight-bold mb-2" } type="submit">Sign in</button>

                            </div>
                        <div className="col-6 text-center my-1">
                            <Link className="fs-14 link-text_2" to="/forgot-password">Forgot password?</Link>
                        </div>
                        </div>
                            <div className="row justify-content-center mt-md-3 mt-5 pt-2">
                                <p className="text-muted">or connect with</p>
                            </div>
                            <div className="row mt-md-0 mt-4">
                                <div className="col-4 text-right-small">
                                <FacebookLogin
                                    appId="3527199753979643"
                                    callback={responseFacebook}
                                    
                                    render={renderProps => (
                                        <a  onClick={renderProps.onClick} className="btn shadow_1 azm-social azm-size-48 azm-circle azm-long-shadow azm-facebook text-light"><i className="fa fa-facebook"></i></a>                            

                                                                            
                                    )}
                                />
                                    
                            </div>
                                <div className="col-4">
                                <GoogleLogin
                                    clientId="468384875560-0abm3b0a8mniumht5t5fv1br7flqkbg7.apps.googleusercontent.com"
                                    render={renderProps => (
                                        <a  href="" onClick={renderProps.onClick} className="btn shadow_1 azm-social azm-size-48 azm-circle azm-long-shadow azm-google-plus"><i className="fa fa-google-plus"></i></a>                            
                                    )}
                                    onSuccess={responseGoogle}
                                    onFailure={responseGoogle}
                    
                                />
                                </div>
                                <div className="col-4 text-left-small">
                                <a href="" className="disabled btn shadow_1 azm-social azm-size-48 azm-circle azm-long-shadow azm-linkedin"><i className="fa fa-linkedin"></i></a>                            {/* <a href="#" class="btn azm-social azm-size-64 azm-circle azm-long-shadow azm-linkedin"><i class="fa fa-linkedin"></i></a> */}
                                </div>

                            </div>
                        </form>
         </>)
                      }

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

        </>
    )
}

export default Login;