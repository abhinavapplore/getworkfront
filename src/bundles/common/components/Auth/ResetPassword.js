import React,{useState, useEffect, useRef} from 'react';
import {useLocation, Link} from 'react-router-dom';
import axios from 'axios';
import {useAlert} from 'react-alert';
import SideImage from '../UI/SideImage';
import AuthHeader from '../UI/AuthHeader';
import { Tooltip } from 'reactstrap';
import { EndPointPrefix } from '../../../../constants/constants';
import Loader from '../UI/Loader';


const ResetPassword=()=>{

    
    //axios.defaults.withCredentials=true;
    const [loading,setLoading]=useState(false);
    const location=useLocation();
    const alert=useAlert();
    const showErrorText=useRef();
    const disableButton=useRef();
    
    //UI state variables
    const [tooltipOpen, setTooltipOpen] = useState(false);
    const toggle = () => setTooltipOpen(!tooltipOpen);

    //state variables
    const [step,setStep]=useState(1);
    const [Password,setPassword]=useState('');
    const [confirmPassword,setConfirmPassword]=useState('');
    const [uid,setUid]=useState('');
    const [token,setToken]=useState('');
    
    useEffect(()=>{
        const temp=location.pathname.split('/')
        setUid(temp[2]);
        setToken(temp[3])
        console.log(temp);
        if(step==1){
            showErrorText.current.style.display="none";
            disableButton.current.classList.add('disabled')
        }

    },[])

    useEffect(()=>{
        if(step==1)
        validate();
    },[Password,confirmPassword])

    const validate=()=>{
        //setConfirmPassword(e.target.value)
        if(Password!==confirmPassword){
        showErrorText.current.style.display="block";
        disableButton.current.classList.add('disabled')
        }
        else{ 
        showErrorText.current.style.display="none";
        disableButton.current.classList.remove('disabled')
        }
        //console.log(showErrorText.current);
    }

    const handleSubmit=(e)=>{
        setLoading(true);
        e.preventDefault();
        axios.post(EndPointPrefix+'/email/reset',{
            "uid":uid,
            "token":token,
            "password":Password
        })
            .then(res=>{
                setLoading(false);
                console.log(res);
                if(res.data.success)
                alert.success(res.data.data.message)
                else{
                    alert.error(res.data.error)
                    return;
                }
                setStep(step+1)
            })
            .catch(err=>{
                alert.error('Oops! There is some error, try again later');
            })
    }

    return loading ? <Loader/>
    :
    (
        <>

        <div className="container-fluid">
        <div className="row no-gutter full-page-view">

          <SideImage/>
          <div className="col-md-8 col-lg-8">
            <div className="row">
  
                <AuthHeader/>
            
            </div>
            <div className="login d-flex align-items-center py-5 mx-md-0 mx-3 px-md-0 px-2">
              <div className="container">
                <div className="row">
                  <div className="col-md-9 col-lg-5 mx-auto">
                    {step==1 && (
                        <>
                        <h1 className="mb-4 login-heading text-left">
                            Hi user, set your new password
                        </h1>

                        <form  onSubmit={handleSubmit} className="my-md-0 my-2"> 
                        <div className="form-group text-left">
                                <label htmlFor="Password" className="fw-500"> New Password</label>
                                <small id="emailHelp" className="form-text text-muted">Password must be atleast 8 characters long and contain at least one uppercase, one lowercase and one digit</small>
                                <input  onChange={(e)=>{setPassword(e.target.value)}} type="password" className="form-control mt-2 mb-1 shadow_1-lightest" id="Password" value={Password} aria-describedby="emailHelp" placeholder="" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" autoFocus/>
                        </div>
                        <div className="form-group text-left">
                                <label htmlFor="ConfirmPassword" className="fw-500"> Confirm Password</label>
                                <input  onChange={(e)=>{setConfirmPassword(e.target.value)}} type="password" className="form-control mt-2 mb-1 shadow_1-lightest" id="ConfirmPassword" value={confirmPassword} aria-describedby="emailHelp" placeholder="" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" required />
                                <small ref={showErrorText} className="err-text">Password's do not match</small>

                        </div>



                        <div className="row my-1 pt-md-3 pt-3">
                            <div className="col-6 text-left">

                            <button ref={disableButton} className=" shadow_1 btn btn-lg btn-main btn-login text-uppercase font-weight-bold mb-2"  type="submit">Change Password  <i className="fas fa-pen"></i></button>

                            </div>
    
                        </div>

                    </form>
                        </>
                    )}
                    {step==2 && (
                        <>

                        <div className="row justify-content-center mx-auto my-2">
                        <i class="far fa-check-circle fa-5x"></i>
                        </div>
                        <div className="row justify-content-center mx-auto my-2">
                            <p className="fs-18 fw-500">
                            Bingo! You've successfully changed your password.
                            </p>
                            <p className="fs-14">
                             You can login with your new credentials <Link to="/login">here</Link>

                            </p>
                        </div>
                        </>
                    )}

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

        </>
    )
}

export default ResetPassword;