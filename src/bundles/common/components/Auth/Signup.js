import React,{useState, useEffect, useRef} from 'react';
import {useHistory,useLocation} from 'react-router-dom';
import {useAlert} from 'react-alert';
import { Tooltip } from 'reactstrap';
import axios from 'axios';
import {EndPointPrefix} from '../../../../constants/constants';
import SideImage from '../UI/SideImage';
import AuthHeader from '../UI/AuthHeader';
import SignupOptions from '../UI/SignupOptions';
import Loader from '../UI/Loader';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'
import GoogleLogin from 'react-google-login';

const Signup=()=>{
        //prevent first render calls
        const firstRender_1=useRef(true);
        const firstRender_2=useRef(true);
        const firstRender_3=useRef(true);

    //refs
    const disableButton=useRef();

    const [step,setStep]=useState(1);
    const [loading,setLoading]=useState(false);
    //UI state variables
    
    const history=useHistory();
    const location=useLocation();
    
    const alert=useAlert();
    const [tooltipOpen, setTooltipOpen] = useState(false);
    const toggle = () => setTooltipOpen(!tooltipOpen);
    const [showPassword,setShowPassword]=useState(false);
    const [fbAccessToken,setfbAccessToken]=useState('');
    const [googleAccessToken,setgoogleAccessToken]=useState('');
    let passwordFieldType= showPassword?'text':'password';
    let tooltipText= showPassword?'Hide Password':'Show Password';

    //state variables 
    const [userDetails,setUserDetails]=useState({
        Flag:false,
        UserID:null,
        FirstName:'',
        LastName:'',
        PhoneNo:0,
        Email:'',
        Password:'',
        Token:null,
        isemailVerified:false
    })

    const handleChange=e=>{ setUserDetails({...userDetails,[e.target.id]:e.target.value}) }

    useEffect(()=>{
        setLoading(false)
        if(location.step){
        console.log(location.token)
        setUserDetails({...userDetails,UserID:location.userid,Email:location.email, isemailVerified:location.isemailVerified, Token:location.token})
        setStep(location.step)
        }
        //disable next button
        if(!loading && step==1)
        disableButton.current.classList.add('disabled');
        
    },[])

    
    useEffect(()=>{
        if(!loading && step==1){
        const {FirstName,LastName,PhoneNo,Password,Email}=userDetails
        if(FirstName && LastName && Email && Password)
            disableButton.current.classList.remove('disabled')
        else
            disableButton.current.classList.add('disabled');
        }
    },[userDetails])

    const createUserProfile=()=>{
        setLoading(true);
        axios.post(EndPointPrefix+'/profile/create/',{
            "email"     :     userDetails.Email,
            "first_name":     userDetails.FirstName,
            "last_name" :     userDetails.LastName,
            "password"  :     userDetails.Password,
            "phone"     :     userDetails.PhoneNo
                
        })
            .then(res=>{
                setLoading(false)
                console.log(res);
                if(res.data.success)
                    alert.success(res.data.data.message)
                else{
                    alert.error(res.data.error)
                    return;
                }

                let data=res.data.data;
                console.log(data.id);
                setUserDetails({...userDetails,UserID:data.id,Token:data.token})
                alert.show('User create call made');
                    setStep(step+1)
                
             
            })
            .catch(err=>{
                console.log(err);
                alert.error('Oops, there is some error. Try again later');
            })
    }
    const checkUser=(token)=>{
        axios.get(EndPointPrefix+'/profile/details/',{
            headers:{
                "Authorization":'Token '+token
            }
        })
            .then(res=>{
               
                console.log('user details: ',res)
                    //CASE 1: user type does not exist => profile is also not completed
                    if(res.data.user_type.length==0){

                        if(!res.data.email_verified){
                         //move to signup options component
                        history.push({
                            pathname:'/signup',
                            step:2,
                            userid:res.data.id,
                            email:res.data.email,
                            token,
                            isemailVerified:false
                        })
                        }
                       else{
                         //move to signup options component
                        history.push({
                            pathname:'/signup',
                            step:2,
                            userid:res.data.id,
                            token,
                            email:res.data.email,
                            isemailVerified:true
                        })
                        }


                }else{
                    localStorage.setItem('user_type',res.data.user_type[0].main_user)
                    if(res.data.email_verified){
                        localStorage.setItem('gw_token',token);
                        //check if its student or company
                        if(res.data.user_type[0].user_id==1){
                            //student with verified email, go to home
                            console.log('from #1')
                            history.push('/student/dashboard')
                        }
                        else{
                            //company user with verified email, check if admin approved
                            if(!res.data.company_details.admin_approved){
                                //alert.show('Please wait while admin approves you');
                                history.push({
                                    pathname:'/company/admin-approve',
                                    company:res.data.company_details.company_name
                                })
                            }
                            else
                            history.push('/company/dashboard')
                        }
                    }
                    else{
                        //check if its student or company
                        if(res.data.user_type[0].type==1){
                            localStorage.setItem('gw_token',token);
                            //student without verified email, go to home
                            console.log('from #2')
                            history.push('/student/dashboard')
                        }
                        else{
                            //company user without verified email
                            history.push({
                                pathname:'/confirm-email',
                                email:res.data.data.email
                          })
                        }
                    }

                }
                

                    //CASE 2: 
            })
            .catch(err=>{
                setLoading(false)
                console.log(err);
                alert.error('Oop! There is some error, try again later.')
            })
        //  history.push('/home');
         //getUserInfo(res.data.token);

    }
    const handleSubmit=e=>{ 
        e.preventDefault(); 
        console.log(userDetails);
        //setStep(step+1)
        createUserProfile();
    }
    const responseGoogle=(response)=>{
        console.log(response);
        setgoogleAccessToken(response.accessToken);
    }
    const responseFacebook = (response) => {
        console.log(response.accessToken);
        setfbAccessToken(response.accessToken);
      }

    useEffect(()=>{
        let token='';
        if(firstRender_2.current){
            firstRender_2.current=false;
            return;
        }
        if(googleAccessToken)
        axios.post(EndPointPrefix+'/oauth/login/',{
            headers: {
                'Content-Type': 'application/json',
                "Connection": "keep-alive"
            },
            
                    "provider": "google-oauth2",
                    "access_token": googleAccessToken

        })
            .then(res=>{
                console.log(googleAccessToken);
                console.log('res: ',res);
                token=res.data.token;
                console.log(token)
                localStorage.setItem('gw_token',token)
                setLoading(true)
                checkUser(token);
 
            })
            .catch(err=>{
                console.log('err: ',err);
                alert.error('Error')
            })
        console.log('fired!');

    },[googleAccessToken])

    return loading ? <Loader/>
    :
    (
        <>
        <div className="container-fluid">
        <div className="row no-gutter full-page-view">

          <SideImage/>
          <div style={{background:'#ffff'}} className="col-md-8 col-lg-8 y-scroll full-page-view">
            <div className="row">
   
                <AuthHeader/>
            
            </div>
            <div  className="login d-flex align-items-center py-5 mx-md-0 mx-3 px-md-0 px-2">
              <div className="container">
                <div className="row">
                  <div className="col-md-9 col-lg-6 mx-auto">

                         {step==1 && (
                             <> 
                             <div className="row pl-3">
                                <h1 className="mb-4 login-heading text-left">Enter your Personal Details</h1>
                             </div>

                             <form onChange={handleChange} onSubmit={handleSubmit} className="my-md-0 my-2"> 
                                <div className="row">
                                        <div className="col-6">
                                        <div className="form-group required text-left">
                                        <label htmlFor="FirstName" className="fw-500 control-label">First Name</label>
                                        <input type="text" className="form-control mt-2 mb-1 shadow_1-lightest" id="FirstName" aria-describedby="emailHelp" placeholder="" required autoFocus/>

                                        </div>
                                        </div>
                                        <div className="col-6">
                                        <div className="form-group required text-left">
                                        <label htmlFor="LastName" className="fw-500 control-label">Last Name</label>
                                        <input type="text" className="form-control mt-2 mb-1 shadow_1-lightest" id="LastName" aria-describedby="emailHelp" placeholder="" required />


                                </div>
                                        </div>
                                    </div>
                                <div className="form-group text-left">
                                        <label htmlFor="PhoneNo" className="fw-500">Phone No</label>
                                        <input type="number" className="form-control mt-2 mb-1 shadow_1-lightest" id="PhoneNo" aria-describedby="emailHelp" placeholder=""/>
                                </div>
                                <div className="form-group required text-left">
                                        <label htmlFor="Email" className="fw-500 control-label">Enter your Email address</label>
                                        <input type="email" className="form-control mt-2 mb-1 shadow_1-lightest" id="Email" aria-describedby="emailHelp" placeholder="" required />
                                </div>
                                <div className="form-group required text-left">
                        
                                        <label htmlFor="Password" className="fw-500 control-label">Create your Password</label>
                                        <small id="emailHelp" className="form-text text-muted">Passworld must be atleast 8 characters long and contain at least one uppercase, one lowercase and one digit</small>
                                        <div className="row no-gutters">
                                            <div className="col">
                                            <input type={passwordFieldType} className="form-control mt-2 mb-1 shadow_1-lightest" id="Password" placeholder="" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"/>
                                            </div>
                                            <div className="col-auto">
                                                <i onClick={()=>{setShowPassword(!showPassword)}} id="toggleShowPassword" className="fas fa-eye show-password-icon"></i>
                                            </div>
                                            <Tooltip className="fs-13" placement="right" isOpen={tooltipOpen} target="toggleShowPassword" toggle={toggle}>
                                        {tooltipText}
                                            </Tooltip>
                                        </div>

                                    


                                </div>

                                <div className="row my-1 pt-3">
                                    <div className="col-6 text-left">
                    
                                    <button ref={disableButton} className=" shadow_1 btn btn-lg btn-main btn-login btn-next text-uppercase font-weight-bold mb-2"  type="submit"><span>Next <i className="fas fa-angle-double-right"/></span></button>

                                    </div>
                        

                                    </div>
                                </form>
                                <div className="row justify-content-center mt-md-3 mt-5 pt-2">
                                <p className="text-muted">or connect with</p>
                            </div>
                                <div className="row mt-md-0 mt-4">
                                <div className="col-4 text-right-small">
                                <FacebookLogin
                                    appId="3527199753979643"
                                    callback={responseFacebook}
                                    
                                    render={renderProps => (
                                        <a  onClick={renderProps.onClick} className="btn shadow_1 azm-social azm-size-48 azm-circle azm-long-shadow azm-facebook text-light"><i className="fa fa-facebook"></i></a>                            

                                                                            
                                    )}
                                />
                                    
                            </div>
                                <div className="col-4">
                                <GoogleLogin
                                    clientId="468384875560-0abm3b0a8mniumht5t5fv1br7flqkbg7.apps.googleusercontent.com"
                                    render={renderProps => (
                                        <a  href="" onClick={renderProps.onClick} className="btn shadow_1 azm-social azm-size-48 azm-circle azm-long-shadow azm-google-plus"><i className="fa fa-google-plus"></i></a>                            
                                    )}
                                    onSuccess={responseGoogle}
                                    onFailure={responseGoogle}
                    
                                />
                                </div>
                                <div className="col-4 text-left-small">
                                <a href="" className="disabled btn shadow_1 azm-social azm-size-48 azm-circle azm-long-shadow azm-linkedin"><i className="fa fa-linkedin"></i></a>                            {/* <a href="#" class="btn azm-social azm-size-64 azm-circle azm-long-shadow azm-linkedin"><i class="fa fa-linkedin"></i></a> */}
                                </div>

                            </div>
                             </>
                        )}   
                        { step==2 && (
                            <> 
                                <SignupOptions userid={userDetails.UserID} email={userDetails.Email} isemailVerified={userDetails.isemailVerified} token={userDetails.Token}/>
                            </>
                        )}



                 </div>
                </div>
              </div>
 
            </div>

          </div>
  
        </div>
        
      </div>
        </>
    )
}

export default Signup;