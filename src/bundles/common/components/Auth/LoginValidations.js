import {emailRegex as emailRegex} from '../../../../constants/constants';
const validate=({state,val,errors,setErrors})=>{

  switch(state){
  case 'Email':
  if(!val) 
  {
      
      setErrors({...errors,emailError:'Email cannot be left blank', isError:true})

  }
  else if(val && !emailRegex.test(val)){
      
      setErrors({...errors,emailError:'Please enter a valid email',isError:true})

  } 
  else{ 
      setErrors({...errors,emailError:''}) 
  }
  break;
  case 'Password':
  if(!val) {
      setErrors({...errors,passwordError:'Password cannot be left blank',isError:true})

  }
  else if(val && val.length<6){ 
      setErrors({...errors,passwordError:'Password must be at least 6 characters long',isError:true})

  }
  else{ 
      setErrors({...errors,passwordError:''})

  }
  break;
  }
  return errors;

}

export default validate;