import React,{useEffect,useState} from 'react';
import axios from 'axios';
import {useHistory,useLocation} from 'react-router-dom'
import { EndPointPrefix} from '../../../../constants/constants';

const CompanyHome=()=>{

    const location=useLocation();
    const history=useHistory();
    let token='';
    const [userDetails,setUserDetails]=useState({})

    useEffect(()=>{

        token=localStorage.getItem('gw_token');
        getUserInfo(token)
    },[])

    useEffect(()=>{
        console.log('user details: ',userDetails)
    },[userDetails])

    const getUserInfo=(token)=>{
        axios.get(EndPointPrefix+'/profile/details/',{
            headers:{
                "Authorization":'Token '+token
            }
        })
            .then(res=>{
                console.log('user details: ',res.data.data[0])
                setUserDetails(res.data.data[0]);
            })
            .catch(err=>{
                console.log(err);
            })
    }

    const logout=()=>{
        localStorage.clear();
        axios.get(EndPointPrefix+'/logout/',{
            headers:{
                "Authorization":'Token '+token
            }
        })
            .then(res=>{
                console.log('logout!',res);
            })
            .catch(err=>{
                console.log(err);
            })
        history.push('/login')
    }
    if(!userDetails) return (<>Loading...</>)
    else
    return(
        <>
            <h1>This is dummy home page for company</h1>
            {userDetails && (
                <>  
                <h2>User Name: {userDetails.first_name} {userDetails.last_name}</h2>
                <p>User Email: {userDetails.email}</p>
                <h4>Company Name: {userDetails.company_name}</h4>
                <p>Industry: {userDetails.company_industry}</p>
                </>
            )}
           
           
            <button onClick={logout} className=" shadow_1 btn btn-lg btn-main btn-login text-uppercase font-weight-bold mb-2" >Logout</button>

        </>
    )
}

export default CompanyHome;