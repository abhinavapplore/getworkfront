import React,{useState,useEffect} from 'react'
import axios from 'axios';
import { EndPointPrefix } from '../../../../constants/constants';
import Skills from '../../../company/components/UI/Skills';
import Ratings from '../../../company/components/UI/Ratings';

const SkillsForm=({type, newSkill,setNewSkill})=>{
    
    const [newSkillDetails,setNewSkillDetails]=useState({
        skill_name:'',
        skill_id:null
    })

    const [newRatingDetails,setNewRatingDetails]=useState({
        rating_name:'',
        rating_value:null
    })

    useEffect(()=>{
        //console.log(type,newSkill,setNewSkill)
       
    },[])

    useEffect(()=>{
        if(newSkillDetails && newRatingDetails){
            setNewSkill({
                skill_name:newSkillDetails.skill_name,
                skill_id:newSkillDetails.skill_id,
                rating_name:newRatingDetails.rating_name,
                rating_value:newRatingDetails.rating_value,
                type: type=='Technical Skills' ? 2 : 1
            })
        }
    },[newSkillDetails,newRatingDetails])

    return(
        <>
        <div className="form-group text-left w-100">
                  <label  htmlFor="skill" className="fw-500 control-label fs-14">Select Skill</label>
                  <Skills type={type} newSkill={newSkillDetails} setNewSkill={setNewSkillDetails}/>

        </div>
        <div className="form-group text-left w-100">
        
                  <label  htmlFor="level" className="fw-500 control-label fs-14">Select Level</label>
                  <Ratings type={type} newRating={newRatingDetails} setNewRating={setNewRatingDetails}/>

        </div>



        </>
    )
}

export default SkillsForm;