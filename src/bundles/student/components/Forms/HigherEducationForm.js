import React,{useState,useEffect} from 'react';
import {EndPointPrefix, collegeList,collegeLocationList,highestQualifications,courseList,branchList,years} from '../../../../constants/constants';
import SelectSearch from 'react-select-search'

const HigherEducationForm=()=>{
    const [stateList,setStateList]=useState([])
    let   [cityList,setCityList]=useState([])
    const [collegeList,setCollegeList]=useState('')
    const [courseList,setCourseList]=useState('')
    const [branchList,setBranchList]=useState('');

    const [college,setCollege]=useState('');
    const [collegeState,setCollegeState]=useState('');
    const [collegeCity,setCollegeCity]=useState('');
    const [qualification,setQualification]=useState('');
    const [course,setCourse]=useState('');
    const [branch,setBranch]=useState('');
    const [year,setYear]=useState('');

    const [showField,setShowField]=useState({
        College:false,
        CollegeState:false,
        CollegeCity:false,
        Qualification:false,
        Course:false,
        Branch:false
    });
    const QualificationDropdown = () => (
        
        <SelectSearch
            value={qualification}
            onChange={setQualification}
            options={highestQualifications}
            placeholder="Select your ongoing/recent acquired degree"
            
        />
     );
    const YearDropdown= () =>(
        <SelectSearch
            value={year}
            options={years}
            placeholder="Select"
            onChange={setYear}
        />
    )
     const TypeAndSearchCollege = () => (
        <SelectSearch
            options={collegeList}
            value={college}
            onChange={setCollege}
            search
            placeholder="Type and Search your college from the list"
            
        />
     );

     const TypeAndSearchState = () => (
        <SelectSearch
            options={stateList}
            value={collegeState}
            onChange={setCollegeState}
            search
            placeholder="Search State"
            
        />
     );

     const TypeAndSearchCity = () => (
        <SelectSearch
            options={cityList}
            value={collegeCity}
            onChange={setCollegeCity}
            search
            placeholder="Search City"
            
        />
     );

     const TypeAndSearchCourse = () => (
        <SelectSearch
            options={courseList}
            search
            value={course}
            onChange={setCourse}
            placeholder="Type and Search your course from the list"
        />
     );

     const TypeAndSearchBranch = () => (
        <SelectSearch
            options={branchList}
            search
            value={branch}
            onChange={setBranch}
            placeholder="Type and Search your branch from the list"
        />
     );
    const handleSubmit=()=>{
        console.log('inside submit');
    }
    const handleChange=(e)=>{
        //console.log(e.target.id)

        let val=e.target.value;
        switch(e.target.id){
            case 'CollegeName'         :    setCollege(val);            break;
            case 'Course'              :    setCourse(val);             break;
            case 'HighestQualification':    setQualification(val);      break;
            case 'Branch'              :    setBranch(val);             break;
            case 'Year'                :    setYear(val);               break; 
        }
        //console.log(studentDetails)
        //console.log('inside handle change');

    }
    return(
        <>
            
        <form onSubmit={handleSubmit} className="my-md-0 my-2 py-2"> 
                    <div className="form-group required text-left my-2">
                           
                            {showField.College && (
                            <>
                            <label htmlFor="CollegeName" className="fw-500 control-label fs-14">Enter your College Name </label>
                            <input type="text" className="form-control mt-2 mb-1 shadow_1-lightest" id="CollegeName" aria-describedby="emailHelp" placeholder="" onChange={handleChange} required autoFocus/>
                            
                            {/* {showField.CollegeState && ( */}
                                <div className="form-group text-left mb-2 mt-3">
                                <label htmlFor="CollegeLocation" className="fw-500 control-label fs-14">Choose your college Location </label>
                                <div className="row">
                                    <div className="col-6">
                                    <TypeAndSearchState className="form-control shadow_1-lightest" />
                                    </div>
                                    
                                    <div className="col-6">
                                    <TypeAndSearchCity className="form-control shadow_1-lightest" />
                                    </div>
                                    


                                </div>
                               
                            </div>
                            </>
                            ) 
                        }
                        { !showField.College && (
                            <>
                            <label htmlFor="CollegeName" className="fw-500 control-label fs-14">Choose your College </label>
                            <TypeAndSearchCollege className="form-control shadow_1-lightest"/>
                            </>
                            )}

                        { 
                        <div className="form-check mb-3">
                        <input type="checkbox" checked={showField.college} onChange={()=>{setShowField({...showField,College:!showField.College})}} className="form-check-input my-2" id="college-checkbox" />
                        <label className="form-check-label" htmlFor="college-checkbox">
                        <small id="emailHelp" className="form-text my-1 fw-500">My college is not in the list</small>
                        </label>
                        </div> 
                        }


                    </div>
                    <div className="form-group required text-left my-2">
                        {
                            college && showField.Qualification && (
                                <>
                                <label htmlFor="Qualification" className="fw-500 control-label fs-14">Mention your Highest Qualification</label>
                                <input type="text" className="form-control mt-2 mb-1 shadow_1-lightest" id="HighestQualification" aria-describedby="emailHelp" placeholder="" onChange={handleChange} required autoFocus/>

                                 </>
                            )
                        }
                        {
                            college && !showField.Qualification && (
                                <>
                                <label htmlFor="Qualification" className="fw-500 control-label fs-14">Select your Highest Qualification</label>
                                <QualificationDropdown className="form-control shadow_1-lightest"/>
   
                                </>

                            )
 
                        }

                    </div>
                    <div className="form-group required text-left my-2">
                        {
                            qualification && showField.Course && (
                                <>
                                <label htmlFor="Course" className="fw-500 control-label fs-14">Mention your Course</label>
                                <input type="text" className="form-control mt-2 mb-1 shadow_1-lightest" id="Course" aria-describedby="emailHelp" placeholder="" onChange={handleChange} required autoFocus/>

                                </>
                            )
                        }
                        {
                            qualification && !showField.Course && (
                                <>
                                <label htmlFor="Course" className="fw-500 control-label fs-14">Choose your Course</label>
                                <TypeAndSearchCourse className="form-control shadow_1-lightest" />
                                </>
                            )
                        }
                        {
                             qualification && (
                                <> 
                                <div className="form-check mb-3">
                                <input type="checkbox" checked={showField.Course} onChange={()=>{setShowField({...showField,Course:!showField.Course})}} className="form-check-input my-2" id="course-checkbox" />
                                <label className="form-check-label" htmlFor="course-checkbox">
                                <small id="emailHelp" className="form-text my-1 fw-500">My Course is not in the list</small>
                                </label> 
                                </div>
                                </>
                            )
                        }


                    </div>
                    <div className="form-group required text-left my-2">
                        {
                            course  && showField.Branch && (
                                <>
                                <label htmlFor="Branch" className="fw-500 control-label fs-14">Mention your Branch</label>
                                <input type="text" className="form-control mt-2 mb-1 shadow_1-lightest" id="Branch" aria-describedby="emailHelp" placeholder="" onChange={handleChange} required />
                                </>
                            )
                        }
                        {
                            course && !showField.Branch && (
                                <>
                                <label htmlFor="Branch" className="fw-500 control-label fs-14">Choose your Branch</label>
                                <TypeAndSearchBranch className="form-control shadow_1-lightest" />
                                </>
                            )
                        }
                        {
                             course && (
                                <>
                                <div className="form-check mb-3">
                                <input type="checkbox" checked={showField.Branch} onChange={()=>{setShowField({...showField,Branch:!showField.Branch})}} className="form-check-input my-2" id="branch-checkbox" />
                                <label className="form-check-label" htmlFor="branch-checkbox">
                                <small id="emailHelp" className="form-text my-1 fw-500">My Branch is not in the list</small>
                                </label> 
                                </div>
                                </>
                            )
                        }


                    </div>
                    <div className="form-group required text-left my-2">
                        {
                             branch && (
                                <>
                                <label htmlFor="Year" className="fw-500 control-label fs-14">Select your current year</label>
                                <YearDropdown className="form-control shadow_1-lightest" /> 
                                </>
                            )
                        }


                    </div>


                    </form>
        </>
    )
}

export default HigherEducationForm;