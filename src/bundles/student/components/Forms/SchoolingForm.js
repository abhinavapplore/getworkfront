import React,{useState,useEffect} from 'react';
import axios from 'axios';
import SelectSearch from 'react-select-search'
import { EndPointPrefix } from '../../../../constants/constants';

const SchoolingForm=()=>{

    const [boardList,setBoardList]=useState({})

    const boards=Object.keys(boardList).map((board)=>{return {'name':board,'value':board}})

    const BoardDropdown=()=>(
          <SelectSearch
        options={boards}
        search
        placeholder="Type and Search your education board"
        />
    )

    const getBoards=()=>{
        axios.get(EndPointPrefix+'/education/board')
            .then(res=>{
                console.log('res from board list api :',res);
                let temp={};
                res.data.data.board_list.forEach(board=>{
                    temp[board.name]=board.id;
                })
            console.log(temp)
            setBoardList({...temp});
            })
            .catch(err=>{
                console.log(err);
            })
    }

    useEffect(()=>{
        getBoards();
    },[])
    return(
        <>
        
        <div className="m-2">
        <div className="row mx-auto">
                <h6 className="py-2 color-blue-dark">Class 10th equivalent or Senior Secondary Information</h6>

            </div>            
            <div className="form-group text-left w-100">
                  <label  htmlFor="board" className="fw-500 control-label fs-14">Select Board</label>
                <BoardDropdown/>
            </div>

          <div className="form-group text-left w-100">
            <label  htmlFor="marks" className="fs-14 fw-500 mb-1">Add Marks</label>
            <div className="row">
                <div className="col-4">
                <div>
                    <div className="form-check">
                        <input className="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" defaultValue="option1" defaultChecked />
                        <label className="form-check-label fs-14" htmlFor="exampleRadios1">
                        CGPA
                        </label>
                    </div>
                    <div className="form-check">
                        <input className="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" defaultValue="option2" />
                        <label className="form-check-label fs-14" htmlFor="exampleRadios2">
                        Percentage
                        </label>
                    </div>
                    </div>

                </div>
                <div className="col-8">
                <input type="text"  class="form-control shadow_1-lightest profile-field " id="marks" aria-describedby="emailHelp" placeholder="CGPA, Percentage" />                            

                </div>  
            </div>
          </div>
       
        </div>
        <div className="m-2">
            <div className="row mx-auto">
                <h6 className="py-2 color-blue-dark">Class 12th equiavlent or Higher Secondary Information</h6>

            </div>

            <div className="form-group text-left w-100">
                  <label  htmlFor="board" className="fw-500 control-label fs-14">Select Board</label>
                <BoardDropdown/>
            </div>
        
          <div className="form-group text-left w-100">
            <label  htmlFor="marks" className="fs-14 fw-500 mb-1">Add Marks</label>
            <div className="row">
                <div className="col-4">
                <div>
                    <div className="form-check">
                        <input className="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" defaultValue="option1" defaultChecked />
                        <label className="form-check-label fs-14" htmlFor="exampleRadios1">
                        CGPA
                        </label>
                    </div>
                    <div className="form-check">
                        <input className="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" defaultValue="option2" />
                        <label className="form-check-label fs-14" htmlFor="exampleRadios2">
                        Percentage
                        </label>
                    </div>
                    </div>

                </div>
                <div className="col-8">
                <input type="text"  class="form-control shadow_1-lightest profile-field " id="marks" aria-describedby="emailHelp" placeholder="CGPA, Percentage" />                            

                </div>  
            </div>          </div>
       
        </div>

        </>
    )
}

export default SchoolingForm;