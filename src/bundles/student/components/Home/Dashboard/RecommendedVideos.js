import React from 'react';

const RecommendedVideos=()=>{
    return(
        <>
        <div className="row mx-auto w-100">
    <div className="col">
      <div className="card shadow mb-4">
        <div className="card-header py-3 d-flex flex-row align-items-center justify-content-between">
          <h6 className="m-0 font-weight-bold color-blue">Recommended Videos</h6>
          
          <button className="d-none d-sm-inline-block btn btn-main btn-sm shadow-main">See More</button>

        </div>
        <div className="card-body">
                <div className="container">
        <div className="row">
            <div id="carousel" className="carousel slide" data-ride="carousel">
            <ol className="carousel-indicators">
                <li data-target="#carousel" data-slide-to={0} className="active" />
                <li data-target="#carousel" data-slide-to={1} />
            </ol>
            <div className="carousel-inner">
                <div className="carousel-item active">
                <div className="d-none  d-md-block d-lg-block">
                    <div className="slide-box">
                    <iframe width="200" height="150" src="https://www.youtube.com/embed/GNS2pNqkt6g" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <iframe width="200" height="150" src="https://www.youtube.com/embed/bG1Zn2uLAyk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <iframe width="200" height="150" src="https://www.youtube.com/embed/8UOMw3XxQfg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <iframe width="200" height="150" src="https://www.youtube.com/embed/Yw5FB70nD6E" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    {/* <img src="https://picsum.photos/285/200/?image=0&random" alt="First slide" />
                    <img src="https://picsum.photos/285/200/?image=1&random" alt="First slide" />
                    <img src="https://picsum.photos/285/200/?image=2&random" alt="First slide" />
                    <img src="https://picsum.photos/285/200/?image=3&random" alt="First slide" /> */}
                    </div>
                </div>
                <div className="d-none d-md-block d-lg-none">
                    <div className="slide-box">

                    <img src="https://picsum.photos/240/200/?image=0&random" alt="First slide" />
                    <img src="https://picsum.photos/240/200/?image=1&random" alt="First slide" />
                    <img src="https://picsum.photos/240/200/?image=2&random" alt="First slide" />
                    </div>
                </div>
                <div className="d-none d-sm-block d-md-none">
                    <div className="slide-box">
                    <img src="https://picsum.photos/270/200/?image=0&random" alt="First slide" />
                    <img src="https://picsum.photos/270/200/?image=1&random" alt="First slide" />
                    </div>
                </div>
                <div className="d-block d-sm-none">
                    <img className="d-block w-100" src="https://picsum.photos/600/400/?image=0&random" alt="First slide" />
                </div>
                </div>
                <div className="carousel-item">
                <div className="d-none d-lg-block">
                    <div className="slide-box">
                    {/* <img src="https://picsum.photos/285/200/?image=4&random" alt="Second slide" />
                    <img src="https://picsum.photos/285/200/?image=5&random" alt="Second slide" />
                    <img src="https://picsum.photos/285/200/?image=6&random" alt="Second slide" />
                    <img src="https://picsum.photos/285/200/?image=7&random" alt="Second slide" /> */}
                    <iframe width="200" height="150" src="https://www.youtube.com/embed/uVtNmR9IKE0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <iframe width="200" height="150" src="https://www.youtube.com/embed/uVtNmR9IKE0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <iframe width="200" height="150" src="https://www.youtube.com/embed/uVtNmR9IKE0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <iframe width="200" height="150" src="https://www.youtube.com/embed/uVtNmR9IKE0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
                <div className="d-none d-md-block d-lg-none">
                    <div className="slide-box">
                    <img src="https://picsum.photos/240/200/?image=3&random" alt="Second slide" />
                    <img src="https://picsum.photos/240/200/?image=4&random" alt="Second slide" />
                    <img src="https://picsum.photos/240/200/?image=5&random" alt="Second slide" />
                    </div>
                </div>
                <div className="d-none d-sm-block d-md-none">
                    <div className="slide-box">
                    <img src="https://picsum.photos/270/200/?image=2&random" alt="Second slide" />
                    <img src="https://picsum.photos/270/200/?image=3&random" alt="Second slide" />
                    </div>
                </div>
                <div className="d-block d-sm-none">
                    <img className="d-block w-100" src="https://picsum.photos/600/400/?image=1&random" alt="Second slide" />
                </div>
                </div>
            </div>
            <a className="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                <span className="carousel-control-prev-icon" aria-hidden="true" />
                <span className="sr-only">Previous</span>
            </a>
            <a className="carousel-control-next" href="#carousel" role="button" data-slide="next">
                <span className="carousel-control-next-icon" aria-hidden="true" />
                <span className="sr-only">Next</span>
            </a>
            </div>
        </div>
        </div>





     

        </div>
      </div>
      </div>
  </div>
        </>
    )
}

export default RecommendedVideos;
