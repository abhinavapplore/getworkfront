import React from 'react';
import StudentHeader from './Header';
import NewJobsPanel from './NewJobsPanel';
import RecommendedBlogs from './RecommendedBlogs';
import ProfileSummary from './ProfileSummary';
import CollegeNotices from './CollegeNotices';
import RecommendedVideos from './RecommendedVideos';

const StudentDashboard=()=>{
    return(
        <>
<div className=" container pl-md-5 pl-0">
  <StudentHeader/>

  <NewJobsPanel/>
  <div className="row">
    <RecommendedBlogs/>
    <div className="col-xl-4 col-lg-5">
      <ProfileSummary/>
      <CollegeNotices/>

    </div>
  </div>
  <div className="row">
    <RecommendedVideos/>
  </div>

</div>


    

        </>
    )
}

export default StudentDashboard;