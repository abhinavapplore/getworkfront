import React,{useState,useEffect} from 'react';

const StudentHeader=()=>{
    const [firstName,setFirstName]=useState(JSON.parse(localStorage.getItem('user_details')).first_name)
    return(
        <>
        <div className="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 className="h3 mb-0 text-gray-800 mb-md-0 mb-2">Hi, {firstName}!</h1>
    <button className="btn btn-main btn-login shadow-main"><i className="fas fa-download fa-sm text-white-50" /> Download My Resume</button>
  </div>
  <div className="row">
    <div className="col-xl-3 col-md-6 mb-4">
      <div className="card border-left-primary shadow h-100 py-2">
        <div className="card-body">
          <div className="row no-gutters align-items-center">
            <div className="col mr-2">
              <div className="text-xs font-weight-bold color-blue text-uppercase mb-1">Open Jobs</div>
              <div className="h5 mb-0 font-weight-bold text-gray-800">47</div>
            </div>
            <div className="col-auto">
              <i className="fas fa-calendar fa-2x text-gray-300 color-blue-light" />
            </div>
          </div>
          <div className="view-btn-pos">
          <button className=" btn btn-main btn-sm shadow-main">View</button>

          </div>
        </div>
      </div>
    </div>
    <div className="col-xl-3 col-md-6 mb-4">
      <div className="card border-left-primary shadow h-100 py-2">
        <div className="card-body">
          <div className="row no-gutters align-items-center">
            <div className="col mr-2">
              <div className="text-xs font-weight-bold color-blue text-uppercase mb-1">Applied Jobs</div>
              <div className="h5 mb-0 font-weight-bold text-gray-800">4</div>
            </div>
            <div className="col-auto">
            <i className="fas fa-clipboard-check fa-2x color-blue-light"></i>
            </div>
          </div>
          <div className="view-btn-pos">
          <button className="btn btn-main btn-sm shadow-main">View</button>

          </div>
        </div>
      </div>
    </div>
    <div className="col-xl-3 col-md-6 mb-4">
      <div className="card border-left-primary shadow h-100 py-2">
        <div className="card-body">
          <div className="row no-gutters align-items-center">
            <div className="col mr-2">
              <div className="text-xs font-weight-bold color-blue text-uppercase mb-1">Placement Status</div>
              <div className="row no-gutters align-items-center">
                <div className="col-auto">
                  <div className="h5 mb-0 mr-3 font-weight-bold text-gray-800"></div>
                </div>
                <div className="col">
                <i className="fas fa-times-circle fa-2x color-red-light"></i>
                </div>
              </div>

            </div>
            <div className="col-auto">
              <i className="fas fa-clipboard-list fa-2x text-gray-300 color-blue-light" />
            </div>
          </div>
          <div className="view-btn-pos">
                  <button className="btn btn-main btn-sm shadow-main">View</button>

          </div>
        </div>
      </div>
    </div>
    <div className="col-xl-3 col-md-6 mb-4">
      <div className="card border-left-primary shadow h-100 py-2">
        <div className="card-body">
          <div className="row no-gutters align-items-center">
            <div className="col mr-2">
              <div className="text-xs font-weight-bold color-blue text-uppercase mb-1">New Jobs</div>
              <div className="h5 mb-0 font-weight-bold text-gray-800">18</div>
            </div>
            <div className="col-auto">
            <i className="fas fa-calendar-plus fa-2x color-blue-light"></i>
            </div>
          </div>
          <div className="view-btn-pos">
          <button className="btn btn-main btn-sm shadow-main">View</button>

          </div>
        </div>
      </div>
    </div>
  </div>
        </>
    )
}

export default StudentHeader;