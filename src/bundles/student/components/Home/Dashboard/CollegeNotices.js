import React from 'react';

const CollegeNotices=()=>{
    return(
        <>
        <div className="card shadow mb-4">
        <div className="card-header py-3 d-flex flex-row align-items-center justify-content-between">
          <h6 className="m-0 font-weight-bold color-blue">Notices from College</h6>
          <button className="d-none d-sm-inline-block btn btn-main btn-sm shadow-main">View All</button>
          {/* <div className="col-md-3 col-sm-6"> */}

              {/* </div> */}

          {/* <div className="dropdown no-arrow">
            <a className="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i className="fas fa-ellipsis-v fa-sm fa-fw text-gray-400" />
            </a>
            <div className="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
              <div className="dropdown-header">Dropdown Header:</div>
              <a className="dropdown-item" href="#">Action</a>
              <a className="dropdown-item" href="#">Another action</a>
              <div className="dropdown-divider" />
              <a className="dropdown-item" href="#">Something else here</a>
            </div>
          </div> */}
        </div>
        <div className="card-body" style={{padding:'0'}}>
        <main>
          <div className="">
            <div className="carousel slide" id="main-carousel" data-ride="carousel">
              <ol className="carousel-indicators">
                <li data-target="#main-carousel" data-slide-to={0} className="active" />
                <li data-target="#main-carousel" data-slide-to={1} />
                <li data-target="#main-carousel" data-slide-to={2} />
                <li data-target="#main-carousel" data-slide-to={3} />
              </ol>
              <div className="carousel-inner">
                <div className="carousel-item active">
                  <img className="d-block img-fluid" src="https://s19.postimg.cc/qzj5uncgj/slide1.jpg" alt="" />
                  <div className="carousel-caption d-none d-md-block">
                    <h1>Mountain</h1>
                  </div>
                </div>
                <div className="carousel-item">
                  <img className="d-block img-fluid" src="https://s19.postimg.cc/lmubh3h0j/slide2.jpg" alt="" />
                  <div className="carousel-caption d-none d-md-block">
                    <h1>Mountain</h1>
                  </div>
                </div>
                <div className="carousel-item">
                  <img className="d-block img-fluid" src="https://s19.postimg.cc/99hh9lr5v/slide3.jpg" alt="" />
                  <div className="carousel-caption d-none d-md-block">
                    <h1>Mountain</h1>
                  </div>
                </div>
                <div className="carousel-item">
                  <img src="https://s19.postimg.cc/nenabzsnn/slide4.jpg" alt="" className="d-block img-fluid" />
                  <div className="carousel-caption d-none d-md-block">
                    <h1>Mountain</h1>
                  </div>
                </div>
              </div>
              <a href="#main-carousel" className="carousel-control-prev" data-slide="prev">
                <span className="carousel-control-prev-icon" />
                <span className="sr-only" aria-hidden="true">Prev</span>
              </a>
              <a href="#main-carousel" className="carousel-control-next" data-slide="next">
                <span className="carousel-control-next-icon" />
                <span className="sr-only" aria-hidden="true">Next</span>
              </a>
            </div>
          </div>
        </main>

        </div>
      </div>
        </>
    )
}

export default CollegeNotices;