import React,{useState} from 'react';
import { Tooltip } from 'reactstrap';


const NewJobsPanel=()=>{
  const [tooltipOpen, setTooltipOpen] = useState({
    job1:false,job2:false
  });
  const toggle = (job) => setTooltipOpen({
    ...tooltipOpen,
    [job]:!tooltipOpen[job]
  });
  const tooltipText='Save this job!'
    return(
        <>
        <div className="row">
    <div className="col">
      <div className="card shadow mb-4">
        <div className="card-header py-3 d-flex flex-row align-items-center justify-content-between">
          <h6 className="m-0 font-weight-bold color-blue">Top New Jobs For You</h6>
          {/* <div className="dropdown no-arrow">
            <a className="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i className="fas fa-ellipsis-v fa-sm fa-fw text-gray-400" />
            </a>
            <div className="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
              <div className="dropdown-header">Dropdown Header:</div>
              <a className="dropdown-item" href="#">Action</a>
              <a className="dropdown-item" href="#">Another action</a>
              <div className="dropdown-divider" />
              <a className="dropdown-item" href="#">Something else here</a>
            </div>
          </div> */}
          <button className="btn btn-main btn-sm shadow-main">See All Jobs</button>

        </div>
        <div className="card-body">
          <div className="row">
            <div className="col-md-6 col-sm-12">
                <div className="card job-card-main">
                <div className="card-body">
                  <div className="row my-3">
                              <div className="col-md-5 col-sm-12">
                                <img className="supporting-image" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxEPEhUSEBISExISEBsYFxgVEhUWFxcXFxgbGBUWGBcYHSggGBslGxYYITEhJSkrLy4uFyAzODMsNygvLi0BCgoKDg0OGxAPGzMlHR83LzUzMjc3MzcsNy4uLTcvLS8tNzU1NzctMi83MSw3NSstMjUtNS03LS0tLi01LSs1K//AABEIALgBEgMBIgACEQEDEQH/xAAcAAEAAgIDAQAAAAAAAAAAAAAABwgFBgIDBAH/xABHEAABAwICBAoFCQcCBwAAAAABAAIDBBEFEgYHITETIjVBUWFxdIGxCDKRsrMUFSNCUnJzodEkNFSCk8HCJfAzQ1NjZJLS/8QAGQEBAAMBAQAAAAAAAAAAAAAAAAMEBQEC/8QAKREBAAIBAgMHBQEAAAAAAAAAAAEDAgQRBSExEhQyQVGRoRMiQmHRM//aAAwDAQACEQMRAD8AnFERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQERY3GcfpKJuaqnihB3Z3AE9g3lBkkWq0usbCJXBjK6HMd1y5o9rgAtojeHAFpBBFwQbgjpBQckXVU1DIml8jmsaN7nEADxK6KXFIJjlimikda9mSNcbdNgUHsRYXEtLKClfwc9VBG/7Lni47RzLK01SyVofG5r2OFw5pBBHSCEHaiw+KaUUNI7JUVUMT/sueAfEcyyNFWRzsEkL2SMO5zHBwPiEHeiLw4tjFPRtz1M0cLel7gL9g5/BB7kWpw6ycIe4NFdDcmwvmA9pFltEE7ZGhzHNc1wuHNIII6QRvQdiLhNK1gLnENaBckmwA6STuXg+f6P8Aiqf+sz9UGSRY35/o/wCKp/6zP1XfSYnBMcsU0UhAuQyRriB02BQetEWGxPSugpXZJ6qCN/2XSC47RzIMyi8eG4pBVNz08screljg63bbcvYgIvDPjNNG4tfUQtc3eHSsBHaCdi7KrEYYY+FkkYyK187nANsd23cg9SLEYVpRQ1bslPUwyvH1WvBd7N5WXQEREBERAREQEREBERBrOsTScYVRSVNgZNjI2ncZHbr9QAJPYqo1tXUV0xfI6SeeV3QXOcTuAA8grRazdCjjUEcTZuBMUucEtzNPFLdov1rXdW2qx2EVT6momiltFljIaRlLjxncbdsFvEoIAxXBamkIFTBLCXC7c7C2/YTvUn6g9MJY6kYfK8uhmBMQcb5JGi9m9AIB2dIW36+q2lkw4sEsTpmTsLWh7S8bbOIG/cVDuql1sWoz/wB/za4IJu1/8ku7xH5lQBonj78OmdPF/wATgJGMPQ57cod4Xv4Kf9f/ACSe8R+ZUCaBYQyuxCmp5PUkl41udrQXOHiG28UGDlkLyXOJc5xJJJuSTtJJ5yt00D1hT4VDUwtJcJYjwQ3iOa9s9jzWJJ62hSdrw0Ro4sN4eCCOF9PIwAxtDbscchabb94PgoP0Vwv5bWU9MTYTTNafuk8a3Xa6DH1E7pHufI4ue5xLnONySd5JW/alNJpaPEI4Mx4Cqdkey+zMRxHgcxvYdh7FJ2tXQihiwmV0FPHE+lY1zHNaA6wc0ODnb3XBO9QRoYbV9J3uL3wgthpfjzMOo5qp+0RM2D7TycrG+LiFUjHMYqMRndNUPdJLI7YNuy52MY3mG3YFanWFoqcWpDTCUw/SNdfLcHLzEdHP4LR9A9UD8NrWVVRPDMyJri0BrmkPIsHEHZsBP5IINxTAaqka11RTyxNf6pewtB6rnn6lvWpDS6WkrY6Rziaapdlyk7GSH1XN6LnYe1SrrkrKSTC6iJ8sRkDWuY3O3Nma8EWF73tdV30NNq+k75F8RqCz+tQH5prLfw58xdVHV4nNB2HaCsNpFV0tBTS1M0cYZEwu9RtyfqtHWTYeKCm6k/0eY3HE3OANm0j7nmF3Mtf2KPcaxJ9XPJUSWzSyFxA3C+5o6gLDwVk9S+iPzdRCSRtqiqAe++9rP+WzqsDc9ZQY3Xdp2/D4m0lK7LUTtJc8HbHHu2dDnbQDzAFV8oqCeqcRDHLM/wBZ2Rrnu6ybea2DWnihqsUqnk3DJjE37sXE8wT4qa9QGFsiwwTAceolcXHnIY4saOzYT4oK/wCj2O1OGVAmp3OjkY6zmnc4A8Zj2neObqVtdFMejxGliqot0rdo52uGx7T2EFV81+YY2DEy9jcoqIWvNud4u1x/ILcfRtxUuiqqVx2RvZIzsfcP/No9qCJNPT/qVZ3uT3iuek2k0lXFS0+Y8DSUzGNbfYX247yOnmHUF16e8pVne5PeKmbUdobSuoPlVRCyWSoe4DhGhwEbTlsAd1yCUEA0tQ+J7ZI3Fj2ODmuabEEbQQVbvV/jxxHD6epd672Wf99hLHnxLSfFVh1hYSyixGpp4haNkt2Doa4BwHYM1vBTzqCdfCW9VRJ53/ugkdERAREQEREBERAREQa5p9pXHhFI6oeMzycsbL2zvO4dgAJPUFWLHNKcQxaW0sskhkdZsTLhlydjWxjf43Kkn0lal3CUcV+Jkkfbru0A+y/tWG9HekjfiMj3gF0VM4svzEua0kddiR4oNWxrV9iNDTfKqqERRhwFi9pdd2wcUEr7qs5Wo+8DyKmj0g65keGiInjzVDco6mXc4+XtUJatJ2x4pRucbD5S0X7bgfmQgnTX/wAknvMfmVDmpdoOM0l+mT4MimPX/wAku7zH5lQ5qW5ZpO2X4EiCbdejiMHntzviB/qtUC6reVqLvA8ip517cjz/AIkXxGqBtVvK1F3geRQWM1sckVn4H+TVWLQ39/pO9xe+FZ3WxyRWfgf5NVYtDf3+k73F74QWy0t0giw2lkqpblsY2NG9zjsa0dpKq7pRpvX4rIeFleGOdZsMZIYLnY3KPWO7epW9JOpcKaljBOV873EdOVoA94rQNRtJHLi0XCAHg43vaD9sN2dtrk+CDGVerjE4KZ9XNBwUUbQ453tD7GwByg3G/cVitDv3+k75F8RqsfrtxBsOETgkZpiyNoPOS8E27GgnwVbNGJxHWUz3bmVUZPYHtugueq/ekDpdw0zcPhdxIeNNbnkPqtP3Rt7XdSmDTvSNuGUMtVsLmttGOmR2xg7Lm56gVUOqqHSvdJIcz5Hlziedzjdx9pQbxqb0S+cq4PkF6elIkk6HOv8ARs8SLnqBVpAFXHV3rTp8IphT/I3OJeXSSNkGZ7jz2I5hYAX5lM2h2ntDiwtTPIlAu6KQZZAOm20EdYJQVX0kP7XU33/KpfiOVndTnI9L9x3xHKuesTDzTYnVxnZ+0vcPuyHO38nBT1qFrGyYSxgN3QzSNd1XdnH5OQaJ6SY/aqU/+O731x9G1x+V1I5jTC/g/YvD6Q1eJMRZEDfgaZoPUXkut7LHxWe9GqgN6yc+raOMdZ4znezi+1BFunvKNZ3uT3irJanh/o9J+G74j1W3T3lKs73J7xVk9T/I9J+G74jkEB65OWKv7zPhsUzagOSW94k8woZ1ycsVf3mfDYpm1Ackt7xJ5hBJCIiAiIgIiICIiAiIghv0jcDfJBBVsBIgc5j7czZLFrj1Zhb+ZQlo/jc+HztqKZ+SRtwDa4IIsQQd4VzaiBsjSx7Q5rhYhwuCDvBCj+r1MYRI/OI5WAm5ayUhvsNyPBBB1RU4npJUgG88rIzYNAaxjBtOwbBc+JNlruHUM00zIYWOdM59mtGx2YeVreFlcDR3Rqkw6Pg6SFsTSbki5c49LnHaV0UOilFS1MtbHC1s8o47ujncWjc0nntvQaPrkZM3Ao21Ja6cPhEhbuLwOMR4qKtS3LNJ2yfAkVksVw2kxWDgpmiaFzgbBxG1puNoNwsbgervDKGZtRT0+SVl8ri97rZgWnYTbcT7VzHKMo3h2YmJ2lite3I8/wCJF8VqgXVdytRd4HkVavHMHgroXU9SzPE+123I3EEG42jaAtfwnVphdJMyeCnyyxOu0mR5sbWvYm3OuuOWtjkis/A/yaqxaG/v9J3uL3wrf4rh0VVC+CdueKVuVzbkXHaNy1eg1XYTBIyWOms+N4c0mR5s4bQbE2O1BgvSAwN9TQNmjBJpJc7gPsOGVx8DY9l1XjCMTlpJmT07iyWJ12uHNsse0EEjxV1XsDgQQCCLEHcQd4K0HE9TuETyGTgnxEm5bFIWtP8ALtt4IILxDGMT0jqIoXHhpNoYxrQ1jftPIG7ZvJ6FrTqCUSmDI4yiUx5ALnODlLbdN9it5oxojRYY0tpIQwu9ZxJc93a47bdS+s0Som1hrxC35SW2L/yzZd2a2zNvsgjfWdT1EWjlPHWEGoY+EP232gGwJ5yBYE9IUTYDoDiVeA+npXlh3PdZjT2F1rq2GKwwPj/aGsdG0h1ngEXabtNjzgrBVWlzGm0UZcBsuTlHgFWv1dVH+mWyanT2XeCN0AV2qTGIW5vkwfbmjkY4+y917dStJJDjcUcrHRvbHKC1wII+jdzFTZHpkb8aIW6nbVsVPBDMWVIjbwmTivLRnDXbxfeuafWU3zMVzvs9XaW2nbtxtuh70gtEHvLcRhaXBrMk9t4A9SS3RtIJ7FFeimmFZhTnmkkDOEADmuaHNNtxsecXO3rVwZGBwLXAEEWIIuCDvBCjzGNTGFVDy9rZYCTciJ4DdvQ1wIHgrSurjI+or6i5zzVFRJ2ue9x/32K12rzRoYXQxU5sZAM0hHPI7a63UNw7Fw0S0BoMLOami+lIsZHnM+3OAfq+FltCCnenvKVZ3uT3irJan+R6T8N3xHr7iOrDCqmV80tNeSV5c4iR4u47zYGwWy4RhkVHCyCBuSKNtmtuTYXvvO/aSgq7rk5Yq/vM+G1TNqA5Jb3iTzCzmN6ucMrZnVFRT5pZLZnB723sABsBtuAWa0fwKnw+EQUrODiDibXJ2uNybnagySIiAiIgIiICIiAiIgIiIC+OF19RBp+K4dLRvM9MSGH1m7wPDoXtwrSmOSzZfo3dP1T48y2JzbrU8c0Yvd9ONu8s/wDn9Fj30X6aZs03PHzx/jSqtqvjsX8p8sv62tjwRcG46lyCjGmr56Y2a5zCDtad3iCs9R6YOGyWO/W39ClPGacuVn2z8O28Ltx54fdDcEWGp9JaZ/18p6HCy98eIRO9WRh/mC0cNTVn4colRypsw8WMw9SLrE7T9Ye0LrfWRt3vaO1wUn1MY80fZn0ehFi6jH6Zm+Rp7NvksVVaYMGyJhd1u2BV7dfp6/FnCxXpLs/DjLYK2kZM3LIMzb3t1hYepkoICGlkeYm1g3NbtWtVeN1FQcuYgH6rNl/7lZnAdGbEST794Z/d36LN753qzaiuJ9cphdnS93w3uz2/UPJphRxRGMxtDS4G4G7Za3mttwmPJDG07xG3yWs4uz5TWsiHqx2v2DjO/sFt7RZT6GuO8W2YxtHT26odVnP0K8Jnn19+jkiItZniIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAvll9RBj8SwmKoHHbt+0NhHitWr9FJWXMREg6Nzv0K3lFR1PD6L+eUc/WFqjW3U8sZ5eiK6ilfHsexzT1hdKlhzAd4uvHLhMDt8TP/VZNnAcvwz92nhxmPzw9kaXXwqR/mGm/wCk3812xYTA31YmexRxwO6euUfL3PGK/LGUc09K+Q2YxzuwLN0Gikr9shEY6N7v0W7sYBuFguSu08Eqx52Tv8Qq28Wty5YRsx2G4PDT+o3jfaO0+1ZCy+otiuvGvHs4RtDLzzyznfKd5Y3C8LELnvJzPkcST1XuAFkkRK68a8ezj0M85znfIREXt5EREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQf/2Q==" alt=""/>
                                <p><b>Amazon India</b></p>
                                <div className="row justify-content-center">
                                  <i className="fas fa-map-marker text-secondary"></i>
                                  <p className="text-secondary fs-14 fw-500 pl-2">
                                      Bengaluru, IN
                                  </p>
                                </div>

                              </div>
                              <div className="col-md-7 col-sm-12 text-left">
                                <div className="row">
                                  <div className="col text-left">
                                    <span className="fs-16 fw-500">Senior Software Engineer</span><br/>
                                  </div>
                                  <div className="col-auto text-left">
                                    <div className="save-container" id="saveJob1">
                                      <i  className="far fa-bookmark"></i>
                                      <Tooltip className="fs-13" placement="top" isOpen={tooltipOpen.job1} target="saveJob1" toggle={()=>{toggle('job1')}}>
                                    {tooltipText}
                                    </Tooltip>
                                    </div>  
                                  </div>
                                </div>
                                <span className="fs-14 text-secondary"> Job Type: Full Time</span><br/>
                                <span className="fs-14 text-secondary"> CTC: 16 LPA</span><br/>
                                <div className="row my-3 pl-2">
                                <span className="badge badge-light bg-blue-light p-1 mx-2 my-1">Python</span>
                                <span className="badge badge-light bg-blue-light p-1 mx-2 my-1">Data Structures</span>
                                <span className="badge badge-light bg-blue-light p-1 mx-2 my-1">Web Development</span>
                                <span className="badge badge-light bg-blue-light p-1 mx-2 my-1">3 More</span> 
                                </div>

                              </div>
                    </div>
                  <div className="row mx-auto">
                      <div className="col-md-5 col-sm-12 text-center">
                        <p className="text-muted fs-12">Posted 4 days ago</p>
                      </div>

                      <div className="col-md-7 col-sm-12">
                        <div className="row">
                          <div className="col-md-6 col-sm-12">
                          <p className="text-muted fs-12">Apply by: 25/7/2020</p>

                          </div>
                          <div className="col-md-6 col-sm-12">
                          <button className=" btn btn-block btn-main btn-sm shadow-main">Apply Now!</button>

                          </div>

                        </div>


                      </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-md-6 col-sm-12">
                <div className="card job-card-main">
                <div className="card-body">
                  <div className="row my-3">
                              <div className="col-md-5 col-sm-12">
                                <img className="supporting-image" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATYAAACjCAMAAAA3vsLfAAABF1BMVEX///9ChfT7vAXqQzU0qFP7uAA1f/QkpEixyfo7qlhIiPSx2rr7ugAzqkNBiOjqQTPqPi/yPh78wQAuif3pOSnpLxsre/M9gvT7vxf//PPsuhDpMiCuYJEqevPpODfZ5f3w9f7F1/tTj/X/9+bsWU7rUEP73Nr86ej619XucGf0qaXpMB280PvrSj3uYixwn/aYuPj+8tf92o7j7P391Hf8xTn+6bz8z2TQ3vy73sP1s6/ympTwgnvvc2r5zMntYVb+9POnWJHzoZuErPfxj4jwf3jtIwD4xMBrnPbsWDDwdif1lBz4qhDziiD93pv2uLT8yU/3oBb+5K+Qs/j8x0TpxET914RdtnOOypzk8udtvICg0qxRs2ppvczkAAAGbElEQVR4nO2cbVfaSBSAAZtoNQq7mxBZVkUUENSqra9VgSp20XVdV1v7+v9/xw5vq8C9k0wMGWe4zzk9px8wzjznztyZe4OxGEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQPihV3x+flFcLm5uF1fLJ8ftqSfaIXjzFyumEk7XtVCrZJmXbdtaZOK0UZY/s5VI9STp2KjkxBLPnJBtV2eN7iZSOU+uQskd168ljWq79rJUdm+Osa852ymuyR/qCWKs5KS9nHVJOjcR1KJX9SuuIO6WlyjgTkdYRdyZ7zNIpFrKee9ogyWxhzM8jFdFQ6wVcRfbIZXK6Lhxq3YBbv5A9dmnUC3YwaS3sQl32+OVQtAMt0B6/fFiWPQMZFIMu0K61P35bGENvRfEMOmAtHl/YkD2LqAnDWjyeHrN4q/Nu7b6txePmkuyZRErhedmgZ415y8meSoSUn3Hy6LMWd/Oy5xIdZ9mwrLFwO5c9m6gofvRU066GM4AtsN8aS6fjkhYm+OkgaWedwmnjrFI5a5QLTra/fjloja3TnOwJRUKDt7ElU07huFp//HS92th0HoNu2Frc3ZI2lQgpOtxAOwFqQmsXWRu1xk5vh9HPInJq+Nkj5TTq8A+VTtoVJtDaWGTTKh5s2Rqn3F2qZRFrLJtORTd+SRSwfJD0Kj1WPiLWGNGMXR5/rmML1PZsSRXzLmLN1P1Ov4rsbKnNuo+fbpqIt+lRj1sua4872+Li4sTlmxaX7P+rdV8/v4V4M/U+816kusour/66tn7tYV3v7md8PQBZp3qf3ertiu7i5d/XzFXiKZZh7N2seD8hh6zSdG7kg5fHP1kWaFfXA8oezd3uez5iaQFepTqfQWqpxSsDdNYzN+8pbhvc3nQ+8tadNzxpHXF7Ox5PGbtVWvzXQ1pH3C7/KctguJnb0cwhenbmvaW1MOb5AZeHdzdN65V3huVPGwu4O96DNuDDm5nPRTSTKHlr+JTWDri3vEchdyzXfRfVZCJjV8Qa88bb4M4xb9r1/wSt8b0dYlfTuKlXvAmt0K43fJ3mUG2uVo2FO3FrzBueF9AKklbH3p0g1hJWAn3gPapNp3PIvN+TR5+1ebwmMoWuUo36psLpwMsaclHordPoZjZKAi1RrjVOKm0t0/vIpjZK9gIsUb612FKaoy2e1uEUsh8g2DyseWjTotQbIB94WcNqlRqFGy/YLMMw2v8ErcXe5fNpEz+EuOofQm6xYLOMxMN+q3uwsv+QMISstTk8N/E7Vm60kxo5K1iwWYmnl4C7hCVojZG7x7Y45TsLN4i2oZv6gSFqjbEUR/p/zVAnET2f4DVq3Ax9snPbF7LGAm4a9qb4Ks3AwQZY63gTtMa8IdrUvmHBedQ6AD98awlbw84irtoNGbjOZsEfzhji1pBSr+L1owNoawOXaIubANZiOTCdms8ZtXTAK4IR7u/YgsJN7W4ztEaRnS0wYBVJ6WYMmEj5TVBxwM6C0qkUvCMYXi95iAJqU/m1VFibj9fYhJiGtKl8vSJtgYhGG5RJNVyk3q9MCqFfSoAzKXbaDQjYjzGV/h4WdNq19sL9HWCvWe26ONi1Cnlzg1+uDPVXRM0uGG6hXhPgDr3aX4uB35kJ88AL3+QVb8LAHXkrEaTUAdME65RKH9sYkDXm7VNYzz+Hu1dqZwSk4NbKpuHEG/bVNbW3Nry7bFkhHHoPkQaMBl9SgK218sLnmec9ebmJvgmi+hqNxR6Qrrz1+6vJyS9fZzhMcdjeMvG3GZRvk6JteWatxSSH2TkTx8XfANHj75OBSaFrjcvsHMcMD8XbVh2gcPNjLbg2HYINenXXl7XA2jTY2VpkBlepP2uBtamfRjsMnN18WguqTf0zW48DI4C1gNq0yAddnjTnfVsLqE3tdnw/K/+/nuvfWjBtC0oXwwfpFZAErAXSptsfMO6kBRFrQbQtKF5mG2afrVMhawG0pTWLtRY7lpg1YW2uHreDQTKfJ0epzY1rcswd4oeQNzFtaR2+aIXw7aeAOBFtptIvL3hz9Mq3OP/azLQ2FyqUmdc+xfnU5prmfU72pKLg6PukH3N+tLlmOq/dWQ0lc/S9XQ/30MYrfLssytJuc0rX9Inx7ejH1y8/X+PMzk3j5Jvn2xsqvw1OEARBEARBEARBEARBEARBEARBEARBEARBEARBEOHzH7i4qvbg2+HjAAAAAElFTkSuQmCC" alt=""/>
                                <p><b>Google India</b></p>
                                <div className="row justify-content-center">
                                  <i className="fas fa-map-marker text-secondary"></i>
                                  <p className="text-secondary fs-14 fw-500 pl-2">
                                      Bengaluru, IN
                                  </p>
                                </div>

                              </div>
                              <div className="col-md-7 col-sm-12 text-left">
                                <div className="row">
                                  <div className="col text-left">
                                    <span className="fs-16 fw-500">Google Cloud Associate</span><br/>
                                  </div>
                                  <div className="col-auto text-left">
                                    <div id="saveJob2" className="save-container">
                                      <i  className="far fa-bookmark"></i>
                                      <Tooltip className="fs-13" placement="top" isOpen={tooltipOpen.job2} target="saveJob2" toggle={()=>{toggle('job2')}}>
                                    {tooltipText}
                                    </Tooltip>
                                    </div>  
                                  </div>
                                </div>
                                <span className="fs-14 text-secondary"> Job Type: Internship</span><br/>
                                <span className="fs-14 text-secondary"> Stipend: 40,000 per month</span><br/>
                                <div className="row my-3 pl-2">
                                <span className="badge badge-light bg-blue-light p-1 mx-2 my-1">Google Cloud Services</span>
                                <span className="badge badge-light bg-blue-light p-1 mx-2 my-1">Programming</span>
                                <span className="badge badge-light bg-blue-light p-1 mx-2 my-1">Analytics</span>
                                <span className="badge badge-light bg-blue-light p-1 mx-2 my-1">3 More</span> 
                                </div>

                              </div>
                    </div>
                  <div className="row mx-auto">
                      <div className="col-md-3 col-sm-6">
                        <p className="text-muted fs-12">Posted 4 days ago</p>
                      </div>
                      <div className="col-md-3 col-sm-6">
                        <p className="text-muted fs-12">Apply by 25th July, 2020</p>
                      </div>
                      <div className="col-md-6 col-sm-12">
                      <button className="btn btn-block btn-main btn-sm shadow-main">Apply Now!</button>

                      </div>
                  </div>
                </div>
              </div>
            </div>


          </div>

     

        </div>
      </div>
      </div>
  </div>
        </>
    )
}

export default NewJobsPanel;