import React from 'react';

const RecommendedBlogs=()=>{
    return(
        <>  
        <div className="col-xl-8 col-lg-7">
      <div className="card shadow mb-4">
        <div className="card-header py-3 d-flex flex-row align-items-center justify-content-between">
          <h6 className="m-0 font-weight-bold color-blue">Recommended For You</h6>
          <button className="btn btn-main btn-sm shadow-main">View All Blogs</button>

          {/* <div className="dropdown no-arrow">
            <a className="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i className="fas fa-ellipsis-v fa-sm fa-fw text-gray-400" />
            </a>
            <div className="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
              <div className="dropdown-header">Dropdown Header:</div>
              <a className="dropdown-item" href="#">Action</a>
              <a className="dropdown-item" href="#">Another action</a>
              <div className="dropdown-divider" />
              <a className="dropdown-item" href="#">Something else here</a>
            </div>
          </div> */}
        </div>
        <div className="card-body">

        <div className="row my-3">
                    <div className="col-md-2 col-4">
                      <img className="supporting-image" src="https://images.unsplash.com/photo-1506702315536-dd8b83e2dcf9?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80" alt=""/>
                    </div>
                    <div className="col-md-10 col-8 text-left py-auto">
                      <div className="row">
                        <div className="col-md-8 col-12">
                          <h6>5 Job Search Mistakes </h6>
                        </div>
                        <div className="col-md-4 col-12 text-md-right text-left">
                            <button className="btn btn-main btn-sm shadow-main">Read More</button>

                        </div>
                      </div>
                      <div className="row mx-auto d-none d-md-block">
                      <p className="text-muted fs-14 pt-2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus a quos sed dolore, quia hic labore laudantium rem fugiat ipsam </p>

                      </div>
                    </div>
        </div>
        <div className="row my-3">
                    <div className="col-md-2 col-4">
                      <img className="supporting-image" src="https://images.unsplash.com/photo-1586281380349-632531db7ed4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80" alt=""/>
                    </div>
                    <div className="col-md-10 col-8 text-left py-auto">
                      <div className="row">
                        <div className="col-md-8 col-12">
                          <h6>10 Awesome Resume Tips </h6>
                        </div>
                        <div className="col-md-4 col-12 text-md-right text-left">
                            <button className="btn btn-main btn-sm shadow-main">Read More</button>

                        </div>
                      </div>
                      <div className="row mx-auto d-none d-md-block">
                      <p className="text-muted fs-14 pt-2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus a quos sed dolore, quia hic labore laudantium rem fugiat ipsam </p>

                      </div>
                    </div>
        </div>
        <div className="row my-3">
                    <div className="col-md-2 col-4">
                      <img className="supporting-image" src="https://images.unsplash.com/photo-1504639725590-34d0984388bd?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=667&q=80" alt=""/>
                    </div>
                    <div className="col-md-10 col-8 text-left py-auto">
                      <div className="row">
                        <div className="col-md-8 col-12">
                          <h6>Tech Trends You Should Know </h6>
                        </div>
                        <div className="col-md-4 col-12 text-md-right text-left">
                            <button className="btn btn-main btn-sm shadow-main">Read More</button>

                        </div>
                      </div>
                      <div className="row mx-auto d-none d-md-block">
                      <p className="text-muted fs-14 pt-2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus a quos sed dolore, quia hic labore laudantium rem fugiat ipsam </p>

                      </div>
                    </div>
        </div>
        <div className="row my-3">
                    <div className="col-md-2 col-4">
                      <img className="supporting-image" src="https://images.unsplash.com/photo-1593574239502-64f0b64ba1e4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80" alt=""/>
                    </div>
                    <div className="col-md-10 col-8 text-left py-auto">
                      <div className="row">
                        <div className="col-md-8 col-12">
                          <h6>Take Up That Part-Time Gig!</h6>
                        </div>
                        <div className="col-md-4 col-12 text-md-right text-left">
                            <button className="btn btn-main btn-sm shadow-main">Read More</button>

                        </div>
                      </div>
                      <div className="row mx-auto d-none d-md-block">
                      <p className="text-muted fs-14 pt-2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus a quos sed dolore, quia hic labore laudantium rem fugiat ipsam </p>

                      </div>
                    </div>
        </div>
       

        </div>
      </div>
    </div>
        </>
    )
}

export default RecommendedBlogs;
