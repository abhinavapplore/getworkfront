import React,{useEffect, useState} from 'react';
import Sidebar from '../../../common/components/UI/Sidebar';
import StudentNavbar from '../UI/StudentNavbar';
import '../UI/UI.css';
import Dashboard from './Dashboard/Dashboard';
import {BrowserRouter, Switch, Route, useHistory, useLocation} from 'react-router-dom';
import StudentRoutes from '../../../../routes/StudentRoutes';
import axios from 'axios';
import { EndPointPrefix } from '../../../../constants/constants';
import Loader from '../../../common/components/UI/Loader';


const  StudentHomeComponent =()=>{
    const [loading,setLoading]=useState(true);
    const history=useHistory();
    const location=useLocation();
    const [userDetails,setUserDetails]=useState({})
    const [educationDetails,setEducationDetails]=useState({})
    
    const getStudentDetails=(token)=>{
        //console.log('token: ',token)
      axios.get(EndPointPrefix+'/shared/user/?user_type=Student',{
        headers:{
            "Authorization":'Token '+token
        }
    })
        .then(res=>{
            
          //fire an action here to save details in student context store 
            console.log('user details: ',res)
            localStorage.setItem('user_details',JSON.stringify(res.data.data))
            localStorage.setItem('email_verified',res.data.data.email_verified)
            setUserDetails(res.data.data);

        })
        .catch(err=>{
            console.log(err);
        })
    }
    useEffect(()=>{
      let token=null;
      token=localStorage.getItem('gw_token');
      console.log('token:',token);
      //if(Object.keys(userDetails).length==0)
      getStudentDetails(token);
      // if(location.pathname=='/student')
      // history.push('/student/dashboard')
    },[])

    useEffect(()=>{
        if(Object.keys(userDetails).length>0)
        setLoading(false);
    },[userDetails])
    return(
        loading ?
            <>
      <Loader/>
      </>
        :
      <>
      <div className="">
      <div className="">
        <div className="">
        {/* <Sidebar userType={"STUDENT"}/> */}

        </div>
        <div className="">
                {/* <StudentNavbar name={userDetails.first_name+' '+userDetails.last_name} dp={userDetails.profile_picture}/> */}
           {/* { userDetails && (<StudentRoutes studentData={userDetails}/>)} */}
           <StudentRoutes studentData={userDetails}/>
            
        </div>
      </div>
      </div>

      </> 
  
      )
  
  

}

export default StudentHomeComponent;