import React,{useState, useEffect} from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import '../../UI/UI.css';
import StudentProfilePersonal from './StudentProfilePersonal';
import StudentProfileEducation from './StudentProfileEducation';
import StudentProfileWorkExperience from './StudentProfileWorkExperience';
import StudentProfileSkills from './StudentProfileSkills';
import SudentProfileProjects from './SudentProfileProjects';
import StudentProfileLinks from './StudentProfileLinks';
import Navbar from '../../../../../StudentSection/Components/Navbar/Navbar';
import { BreakpointProvider, Breakpoint } from 'react-socks';
import ChangePassword from '../../Auth/ChangePassword'


const StudentTabs= () => (
<>
  <BreakpointProvider>
    <Breakpoint large up>

   <Tabs defaultIndex={1} onSelect={index => {}}>
    <TabList>
      <Tab className="d-md-none d-block"><span className="fs-14 text-uppercase fw-500">Personal</span></Tab>
      <Tab ><span className="fs-14 text-uppercase fw-500 ">Education</span></Tab>
      <Tab ><span className="fs-14 text-uppercase fw-500 ">Work Experience</span></Tab>
      <Tab ><span className="fs-14 text-uppercase fw-500 ">Projects</span></Tab>
      <Tab ><span className="fs-14 text-uppercase fw-500 ">Skills</span></Tab>
      <Tab ><span className="fs-14 text-uppercase fw-500 ">Links</span></Tab>
    </TabList>

    <TabPanel className="d-md-none d-block"><StudentProfilePersonal/></TabPanel>

    <TabPanel><StudentProfileEducation/></TabPanel>

    <TabPanel><StudentProfileWorkExperience/></TabPanel>

    <TabPanel><SudentProfileProjects/></TabPanel>

    <TabPanel><StudentProfileSkills/></TabPanel>

    <TabPanel><StudentProfileLinks/></TabPanel>

  </Tabs>
    </Breakpoint>
  </BreakpointProvider>
  <BreakpointProvider>
    <Breakpoint medium down>

   <Tabs>
    <TabList>
      <Tab className="d-md-none d-block"><span className="fs-14 text-uppercase fw-500">Personal</span></Tab>
      <Tab ><span className="fs-14 text-uppercase fw-500 ">Education</span></Tab>
      <Tab ><span className="fs-14 text-uppercase fw-500 ">Work Experience</span></Tab>
      <Tab ><span className="fs-14 text-uppercase fw-500 ">Projects</span></Tab>
      <Tab ><span className="fs-14 text-uppercase fw-500 ">Skills</span></Tab>
      <Tab ><span className="fs-14 text-uppercase fw-500 ">Links</span></Tab>
    </TabList>

    <TabPanel className="d-md-none d-block"><StudentProfilePersonal/></TabPanel>

    <TabPanel><StudentProfileEducation/></TabPanel>

    <TabPanel><StudentProfileWorkExperience/></TabPanel>

    <TabPanel><SudentProfileProjects/></TabPanel>

    <TabPanel><StudentProfileSkills/></TabPanel>

    <TabPanel><StudentProfileLinks/></TabPanel>

  </Tabs>
    </Breakpoint>
  </BreakpointProvider>
  </>
    
);
 



const StudentProfile=({studentData})=>{
  const [studentDetails,setStudentDetails]=useState()

  useEffect(()=>{
      setStudentDetails(JSON.parse(localStorage.getItem('user_details')))
  },[])

  useEffect(()=>{
    if(studentDetails)
    console.log(studentDetails)
  },[studentDetails])

    //console.log('inside student profile')
    return(
        <>
        <Navbar/>
            <div className="row profile-card-holder" style={{marginTop:'2.5rem'}}>
              <div className="col-lg-3 col-sm-12 ml-2 ml-md-0 d-none d-lg-block ">
                  <StudentProfilePersonal />
                </div>
                <div className="col-lg-8 col-sm-12 ml-2 ml-md-0">
                    <div className="card">
                        <div className="card-body profile-section">
                          <StudentTabs/>
                        </div>
                    </div>
                </div>
            </div>      

        </>
    )
}

export default StudentProfile;