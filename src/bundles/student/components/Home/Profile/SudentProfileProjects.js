import React, { useState, useEffect } from 'react'
import AddDetails from './AddDetails';
import Axios from 'axios';
import { EndPointPrefix } from '../../../../../constants/constants';
import {useAlert} from 'react-alert';
import moment from 'moment';

const StudentProfileProjects=()=>{

  const alert=useAlert();
  const [loading,setLoading]=useState(true);
  const addProject="ADD_PROJECT";
  const editProject="EDIT_PROJECT";
  const [type,setType]=useState('')
  const [showProjectForm,setShowProjectForm]=useState(false);
  const [projectDetails,setProjectDetails]=useState([])
  const [selectedProject,setSelectedProject]=useState()
  const [newProject,setNewProject]=useState(
    {
      id:null,
      title:'',
      links:null,
      description:'',
      start_date:null,
      end_date:null,
      skills:[]
    }
  )

  const getAllProjects=()=>{
    setLoading(true);
    Axios.get(EndPointPrefix+'/education/user_project',{
      
      headers: {
        "Authorization":'Token '+localStorage.getItem('gw_token')
      }
    })
    .then(res=>{
      setLoading(false)
      console.log(res);
        if(!res.data.success){
          alert.error(res.data.error)
        }
        else{
          if(res.data.data.length>0){
            //popualte state with projects from backend
            let temp=[];
            res.data.data.forEach(project=>{
              temp.push(project)
            })
            setProjectDetails(temp);
          }
         //alert.success('fetched project details succeffully')
        }

        
    })
    .catch(err=>{
      console.log(err);
      alert.error('Error fetching project details')
    })
  }

  useEffect(()=>{
    getAllProjects();
  },[])

  const saveProject=()=>{
    let data=newProject
    console.log('sending data: ',data);
    Axios.post(EndPointPrefix+'/education/user_project',data,{
      
        headers: {
          "Authorization":'Token '+localStorage.getItem('gw_token')
        }
    })
      .then(res=>{
        console.log(res);
        if(res.data.success){
          let temp=[...projectDetails]
          setNewProject({...newProject,id:res.data.data.project_id})          
          temp.push(newProject);
          // setProjectDetails(temp);
          alert.success(res.data.data.message)
          //console.log(projectDetails)
        }
        else
        res.error(res.data.error)
      })
      .catch(err=>{
        console.log(err);
        alert.error('There was some error adding a project. Please try again later')
      })
  }
  useEffect(()=>{
    if(newProject.id){
      setProjectDetails([...projectDetails,newProject])
    }
  },[newProject])
  const AddProject=()=>{
    console.log(newProject)
    let isValid=true;
    Object.values(newProject).forEach((val,ind)=>{
      console.log(ind,val)
      if(!val || (val && val.length==0)){
        isValid=false;
        return;
      }
    })
    if(isValid)
    saveProject();
    else
    alert.error('Please fill all required details')

  }

  const ChangeProject=()=>{
    // console.log('changed project, updated one is: ',selectedProject)
    let temp=[...projectDetails]
    temp.forEach((project,ind)=>{
      if(project.id==selectedProject.id){
        //send a patch request to update details
        console.log('changed project, updated one is: ',selectedProject)
        let res=updateProject(selectedProject)
        res.then((response)=>{
          console.log('here: ',res)
          temp[ind]=selectedProject;
          setProjectDetails(temp);
        })
      }
    })
   
  }

  const DeleteProject=(id)=>{
    Axios.delete(EndPointPrefix+`/education/user_project/${id}/`,{
      headers: {
        "Authorization":'Token '+localStorage.getItem('gw_token')
      }
    })
      .then(res=>{
        console.log(res);
        if(res.data.success){
          alert.success(res.data.data.message)
              let temp=projectDetails.filter((project)=>{
                return project.id!=id;
              })
              setProjectDetails(temp)
        }
        else{
          alert.error(res.data.error)
        }
      })
      .catch(err=>{
        console.log(err);
        alert.error('There was some error deleting this project. Try again later')
      })

  }

  const updateProject=async(project)=>{
    let result=Axios.patch(EndPointPrefix+`/education/user_project/${project.id}/`,project,{
      headers: {
        "Authorization":'Token '+localStorage.getItem('gw_token')
      }
    })
      .then(res=>{
        console.log(res);
        if(res.data.success)
        alert.success(res.data.data.message)
        else
        alert.error(res.data.error)
      })
      .catch(err=>{
        console.log(err);
      })
    return result;
  }

  const EditProject=(project)=>{
    setSelectedProject(project)
    openProjectForm("EDIT_PROJECT");
  }

  const openProjectForm=(project_type)=>{
    setType(project_type)
    setShowProjectForm(true)
  }

  useEffect(()=>{
    if(projectDetails.length>0)
    console.log(projectDetails)
  },[projectDetails])

    return(
        <>

        { showProjectForm && (
          <AddDetails 
          appendToParentState={type=="ADD_PROJECT" ? AddProject : ChangeProject} 
          formState={ type=="ADD_PROJECT" ? newProject : selectedProject } 
          setFormState={ type=="ADD_PROJECT" ? setNewProject : setSelectedProject } 
          detailType={type} showForm={showProjectForm} setShowForm={setShowProjectForm}/>)
          
        }

        <div className="row my-3 justify-content-center">
        <button onClick={()=>{openProjectForm("ADD_PROJECT")}} className="add ripple"><span>+</span></button>
      </div>

      { 
        loading ? <div>Loading...</div>
        :
        projectDetails.length>0
        ?
        projectDetails.map((project,ind)=>{

        return(
          <div className="card job-card-main" key={project.id}>
        <div className="card-body">
          <div className="row">
            <div className="col-md-8 col-6 text-left">
              <p className="fs-12 text-muted">Project {ind+1} </p> 
            </div>
            <div className="col-md-4 col-6 text-right">
              <i onClick={()=>{EditProject(project)}} className="fas fa-edit edit-icon mx-2"></i>
              <i onClick={()=>{DeleteProject(project.id)}} className="fas fa-trash edit-icon mx-2"></i>
            </div>
          </div>
          <div className="row my-2">
            <div className="col-md-1 col-12 text-center">
              <img src="https://getwork.org/wp-content/uploads/2019/12/getwork_new.png" alt="" style={{height:'50px',width:'50px'}}/>
            </div>
            <div className="col-7 text-left">
              <p className="fs-14">
               <b className="fs-16"> {project.title}</b> <br/>  <span className="fw-500 link-text">{project.links}</span>
              </p>             
            </div>
            <div className="col-md-4 col-sm-12 text-md-right text-left">
              <span className="fs-12 text-muted"><i class="far fa-calendar"></i> {moment(project.end_date).diff(moment(project.start_date),'days')} days</span>
              <br/>
              <span className="fs-13 text-muted"> {moment(project.start_date).format('MMM DD')} - {moment(project.end_date).format('MMM DD')}  </span>
         
            </div>
          </div>
          <div className="row my-2">
            <div className="col-md-8 col-sm-12 text-left">
              <p className="fs-12">
                {project.description}
              </p>
            </div>
            <div className="col-md-4 col-sm-12 text-left pl-md-5 pl-2">
              {project.skills && project.skills.map((projSkill)=>{
                return(
                  <span key={projSkill.skill_id} className="badge badge-light bg-blue-light p-1 mx-2 my-1">{projSkill.skill_name}</span>

                )

              })
              
             
              }

            </div>
          </div>
          
        </div>
      </div>
        )
       
      })
      :
       
      <div>No projects yet, but you can add them right away! :)</div>
      
      }

      {/* <div className="row mt-4 pt-2 justify-content-center">
            <button className="btn btn-main btn-sm shadow-main update-btn">Update Details</button>
        </div> */}
        </>
    )
}

export default StudentProfileProjects;