import React,{useState, useEffect, useRef} from 'react'
import Axios from 'axios';
import { EndPointPrefix } from '../../../../../constants/constants';
import {useAlert} from 'react-alert';
import Skeleton from 'react-loading-skeleton';
import moment from 'moment';
import EducationForm from '../../Forms/EducationForm';
import { continueStatement } from '@babel/types';

const StudentProfileEducation=()=>{
      const disableCurrentEducationButton=useRef()
      const alert=useAlert();
      const [showEducationForm,setShowEducationForm]=useState(false);
      const [currentEducationDetails,setCurrentEducationDetails]=useState({})
      const [newEducationDetails,setNewEducationDetails]=useState({})
      const [allEducationDetails,setAllEducationDetails]=useState([])

      const [showField,setShowField]=useState({
            Percentage : false,
            Start_Date : false,
            End_Date : false,
      })
      const addEducation="ADD_EDUCATION"
      const openEducationForm=()=>{
            setShowEducationForm(true);
      }

      const updateCurrentEducation=()=>{
            if(!currentEducationDetails.percentage && !currentEducationDetails.start_date && !currentEducationDetails.end_date){
                  alert.error('Please fill any detail first');
                  return;
            }

            Axios.patch(EndPointPrefix+`/education/user_education/${currentEducationDetails.education_id}/`,{
                  'percentage': currentEducationDetails.percentage ? currentEducationDetails.percentage : null,
                  'start_date': currentEducationDetails.start_date ? currentEducationDetails.start_date :  null,
                  'end_date'  : currentEducationDetails.end_date ? currentEducationDetails.end_date : null
            },{
                  headers: {
                        "Authorization":'Token '+localStorage.getItem('gw_token')
                      }
            })
               .then(res=>{
                     console.log(res);
                     if(res.data.success){
                           alert.success(res.data.data.message)
                           setShowField({
                              Percentage : false,
                              Start_Date : false,
                              End_Date : false,
                           })
                           
                     }
                     else
                     alert.error(res.data.error)

               })
               .catch(err=>{
                     console.log(err);
                     alert.error('There was some error updating your details. Try again later');
               })
      }     

      const handleCurrentEducationChange=(e)=>{
            setCurrentEducationDetails({...currentEducationDetails,[e.target.id]:e.target.value})
      }

      const SaveNewEducation=async()=>{
       
        let result= await  Axios.post(EndPointPrefix+`/education/user_education`,{
                  'college'   :     typeof(newEducationDetails.college)=='string' ? null: newEducationDetails.college,
                  'college_name'   : typeof(newEducationDetails.college)=='number' ? null: newEducationDetails.college,
                  'education' : newEducationDetails.education,
                  'percentage': newEducationDetails.percentage,
                  'board'     : newEducationDetails.board,
                  'end_date'  : newEducationDetails.end_date 
            },{
                  headers: {
                        "Authorization":'Token '+localStorage.getItem('gw_token')
                      }
            })
               .then(res=>{
                     console.log(res);
                     if(res.data.success){
                           //setNewEducationDetails({...newEducationDetails,education_id:res.data.data.education_id})
                           alert.success(res.data.data.message)
                           setAllEducationDetails([...allEducationDetails,res.data.data.data])
                           setNewEducationDetails({})
                           setShowEducationForm(false)

                     }
                     else
                     alert.error(res.data.error)

               })
               .catch(err=>{
                     console.log(err);
                     alert.error('There was some error updating your details. Try again later');
               })
      }

      const AddNewEducationDetails=()=>{
            let temp=[...allEducationDetails]
            console.log(newEducationDetails)
            temp.push(newEducationDetails);
         
            let result=SaveNewEducation();
            // result.then(()=>{
            //       setAllEducationDetails([...allEducationDetails,newEducationDetails])
            // })
            
            
            //setAllEducationDetails(temp);
      }

      const appendToParentState=()=>{

      }

      useEffect(()=>{

            Axios.get(EndPointPrefix+'/education/user_education',{
                  headers: {
                        "Authorization":'Token '+localStorage.getItem('gw_token')
                      }
            })
                  .then(res=>{
                        console.log(res);
                        if(res.data.success){
                              let temp=[];
                              res.data.data.forEach((data,ind)=>{
                                    if(ind==0){
                                          setCurrentEducationDetails(data);
                                          setShowField({...showField,
                                                Percentage : data.percentage ? false : true,
                                                Start_Date : data.start_date ? false : true,
                                                End_Date   : data.end_date ? false : true,
                                          })
                                    }
                                    else{
                                          temp.push(data);
                                    }

                              })
                        setAllEducationDetails(temp);
                          
                        }
                        else
                        alert.error()
                  })
                  .catch(err=>{
                        console.log(err);
                  })


      },[])

      useEffect(()=>{
      let isValid=false;
      if(currentEducationDetails.percentage && currentEducationDetails.start_date && currentEducationDetails.end_date)
      isValid=true;
      if(isValid)
      disableCurrentEducationButton.current.classList.remove('disabled')
      else
      disableCurrentEducationButton.current.classList.add('disabled')

      
      },[currentEducationDetails])

      useEffect(()=>{
            if(!showField.Percentage && !showField.Start_Date && !showField.End_Date)
            disableCurrentEducationButton.current.classList.add('disabled')

      },[showField])

    return(
        <>
        <div className="row my-3 justify-content-center">
        <button onClick={openEducationForm} className="add ripple"><span>+</span></button>
      </div>


      <div className="my-4">

                  { showEducationForm && (

                  <div className="card job-card-main">
                        <div className="card-body">
                              <EducationForm state={newEducationDetails} setState={setNewEducationDetails} appendToParentState={AddNewEducationDetails}/>

                        </div>
                  </div>

                  )}

      <div className="card job-card-main">

       <div className="card-body">
            <div className="row my-2">
                  <div className="col-md-6 col-sm-12 text-left">
                        <p className="fs-12 text-muted text-uppercase">Current Education Details</p> 
                  </div>
                  <div className="col-md-6 col-sm-12 text-right">
                        <i className="fas fa-info-circle mt-1 pt-1 text-muted " style={{marginLeft:'-25px',fontSize:'12px'}}/>
                  </div>

            </div>
            
            <div className="row my-3">
                  <div className="col-md-4 col-sm-12 text-left">
                  <label  htmlFor="college" className="fs-12 fw-500 mb-1 text-uppercase my-auto">College</label>
                  </div>
                  <div className="col-md-8 col-sm-12 text-left">
                        <span className="fs-13">{currentEducationDetails.college_name || <Skeleton width={100}/>}</span>            
                  </div>
            </div>
          

          <div className="row my-3">
                  <div className="col-md-4 col-sm-12 text-left">
                  <label  htmlFor="qualification" className="fs-12 fw-500 mb-1 text-uppercase my-auto">Highest Qualification</label>
                  </div>
                  <div className="col-md-8 col-sm-12 text-left">       
                        <span className="fs-13">{currentEducationDetails.highest_qualification || <Skeleton width={100}/>}</span>
                  </div>
          </div>

          <div className="row my-3">
                  <div className="col-md-4 col-sm-12 text-left">
                  <label htmlFor="degree" className="fs-12 fw-500 mb-1 text-uppercase my-auto">Recent/On-going Degree</label>
                  </div>
                  <div className="col-md-8 col-sm-12 text-left">
                        <span className="fs-13">{currentEducationDetails.degree || <Skeleton width={100}/>}</span>                        
                  </div>
            </div>
          
            { currentEducationDetails.specialization &&
            <div className="row my-3">
                        <div className="col-md-4 col-sm-12 text-left">
                        <label  htmlFor="branch" className="fs-12 fw-500 mb-1 text-uppercase my-auto">Branch</label>
                        </div>
                        <div className="col-md-8 col-sm-12 text-left">           
                              <span className="fs-13">{currentEducationDetails.specialization || <Skeleton width={100}/>}</span>    
                        </div>
            </div>
            }
          <div className="row my-3">
                  <div className="col-md-4 col-sm-12 text-left">
                  <label  htmlFor="year" className="fs-12 fw-500 mb-1 text-uppercase my-auto">Year</label>
                  </div>
                  <div className="col-md-8 col-sm-12 text-left">
                        <span className="fs-13">{currentEducationDetails.current_year || <Skeleton width={100}/>}</span>
                  </div>
          </div>

          <div className="row my-3">
                  <div className="col-md-4 col-sm-12 text-left">
                  <label  htmlFor="percentage" className="fs-12 fw-500 mb-1 text-uppercase my-auto">Current Percentage</label>
                  </div>

                 
                  {
                        showField.Percentage ?
                        <div className="col-md-8 col-sm-12 text-left my-auto">
                        <input onChange={handleCurrentEducationChange} type="number"  class="form-control shadow_1-lightest profile-field w-80" id="percentage" aria-describedby="emailHelp" placeholder="" />
                        </div>
                        :
                        <div className="col-md-8 col-sm-12 text-left my-auto">
                        <div className="row">
                              <div className="col">
                              <span className="fs-13">{currentEducationDetails.percentage || <Skeleton width={100}/>}</span>
                              </div>
                              <div className="col-auto">
                              <i onClick={()=>{setShowField({...showField, Percentage: true})}}className="fas fa-edit mt-1 pt-1 text-muted edit-icon"/>

                              </div>
                        </div>
                        </div>

                  }
          </div>

            <div className="row my-3">
                  <div className="col-md-4 col-sm-12 text-left">
                  <label  htmlFor="start_date" className="fs-12 fw-500 mb-1 text-uppercase my-auto">Course Start Date</label>
                  </div>
                  <div className="col-md-8 col-sm-12 text-left">            
                  <div className="form-group row text-left w-100">
                            <div className="row no-gutters w-80 ml-3">
                                  { !showField.Start_Date ?
                                    <span className="fs-13">{
                                          currentEducationDetails.start_date ?
                                          moment(currentEducationDetails.start_date).format('LL') 
                                          : <Skeleton width={100} 
                                    />}
                                    </span>
                                    :
                                    <div className="col">
                                        <input type="date" onChange={handleCurrentEducationChange} class="form-control shadow_1-lightest profile-field " id="start_date" aria-describedby="emailHelp" placeholder="" />
                                    </div>
                                  }

                          </div>
                    </div>                     
                  </div>
           </div>

          <div className="row my-3">
            <div className="col-md-4 col-sm-12 text-left">
            <label  htmlFor="end_date" className="fs-12 fw-500 mb-1 text-uppercase my-auto">Course End Date</label>
            </div>
            <div className="col-md-8 col-sm-12 text-left">
           
            <div className="form-group row text-left w-100">
                            <div className="row no-gutters w-80 ml-3">
                                  { !showField.End_Date ? 
                                    <span className="fs-13">{
                                          currentEducationDetails.end_date ?
                                          moment(currentEducationDetails.end_date).format('LL') 
                                          : <Skeleton width={100}/>}</span>
                                    :
                                    <div className="col">
                                        <input type="date" onChange={handleCurrentEducationChange} class="form-control shadow_1-lightest profile-field " id="end_date" aria-describedby="emailHelp" placeholder="" />
                                    </div>
                                  }

                          </div>
                    </div>             
            </div>

          </div>

                      <div className="row mt-4 pt-2 justify-content-center">
                              <button ref={disableCurrentEducationButton} onClick={updateCurrentEducation} className="btn btn-main btn-sm shadow-main">Update Details</button>
                        </div>
            </div>
      </div>

      {
            allEducationDetails && allEducationDetails.map((data,ind)=>{
                  return(
                        <div className="card job-card-main">
                        <div className="card-body">

                              <div className="row my-2">
                                    <div className="col-md-6 col-sm-12 text-left">
                                          <p className="fs-12 text-muted text-uppercase">Past Education Details</p> 
                                    </div>
                                    <div className="col-md-6 col-sm-12 text-right">
                                          <i className="fas fa-info-circle mt-1 pt-1 text-muted " style={{marginLeft:'-25px',fontSize:'12px'}}/>
                                    </div>

                              </div>

                           <div className="row my-3">
                                    <div className="col-md-4 col-sm-12 text-left">
                                    <label  htmlFor="college" className="fs-12 fw-500 mb-1 text-uppercase my-auto">Qualification</label>
                                    </div>
                                    <div className="col-md-8 col-sm-12 text-left">
                                          <span className="fs-13">{data.highest_qualification || <Skeleton width={100}/>}</span>            
                                    </div>
                              </div>
                              {
                                    data.education<=2 ?
                                    <>
                                          <div className="row my-3">
                                          <div className="col-md-4 col-sm-12 text-left">
                                          <label  htmlFor="college" className="fs-12 fw-500 mb-1 text-uppercase my-auto">Board</label>
                                          </div>
                                          <div className="col-md-8 col-sm-12 text-left">
                                                <span className="fs-13">{data.board_name || <Skeleton width={100}/>}</span>            
                                          </div>
                                          </div>
                                    </>

                                    :
                                          <div className="row my-3">
                                          <div className="col-md-4 col-sm-12 text-left">
                                          <label  htmlFor="college" className="fs-12 fw-500 mb-1 text-uppercase my-auto">College</label>
                                          </div>
                                          <div className="col-md-8 col-sm-12 text-left">
                                                <span className="fs-13">{data.college_name || <Skeleton width={100}/>}</span>            
                                          </div>
                                          </div>
                              }


                              <div className="row my-3">
                                    <div className="col-md-4 col-sm-12 text-left">
                                    <label  htmlFor="college" className="fs-12 fw-500 mb-1 text-uppercase my-auto">Percentage</label>
                                    </div>
                                    <div className="col-md-8 col-sm-12 text-left">
                                          <span className="fs-13">{data.percentage || <Skeleton width={100}/>}</span>            
                                    </div>
                              </div>

                              <div className="row my-3">
                                    <div className="col-md-4 col-sm-12 text-left">
                                    <label  htmlFor="college" className="fs-12 fw-500 mb-1 text-uppercase my-auto">Year of Passing</label>
                                    </div>
                                    <div className="col-md-8 col-sm-12 text-left">
                                          <span className="fs-13">{data.end_date || <Skeleton width={100}/>}</span>            
                                    </div>
                              </div>


                        </div>
                  </div>
                  )

            })
      }




            </div>
        </>
    )
}

export default StudentProfileEducation;