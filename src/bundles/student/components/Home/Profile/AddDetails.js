import React, { useState, useEffect, useRef } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import WorkExperienceForm from '../../Forms/WorkExperienceForm';
import SkillsForm from '../../Forms/SkillsForm';
import ProjectForm from '../../Forms/ProjectsForm';
import EducationForm from '../../Forms/EducationForm';


const FormModal = ({appendToParentState,formState,detailType,formDetails,showForm,setShowForm}) => {

  const disableSubmitButton=useRef();
  const toggle = () =>setShowForm(!showForm);
  const handleAdd= () => { 
      appendToParentState();
        toggle();    
 }
 useEffect(()=>{
    //console.log(formState)
    //console.log(detailType)
 },[formState])
  return (
    <div>
     
      <Modal isOpen={showForm} toggle={toggle} className=''>
        <ModalHeader toggle={toggle}>{formDetails.Title}</ModalHeader>
        <ModalBody>
                {formDetails.FormComponent}
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={handleAdd}>{formDetails.ButtonText}</Button>
          <Button color="secondary" onClick={toggle}>Cancel</Button>
        </ModalFooter>
      </Modal>
    </div>
  );
}

const AddDetails=({ appendToParentState,formState,setFormState,detailType,showForm,setShowForm})=>{
    const [formDetails,setFormDetails]=useState({
        Title:'',
        Type:detailType,
        FormComponent:null,
        ButtonText: ''
    })
    useEffect(()=>{
        //console.log(detailType,showForm);
        switch (detailType) {
                    case "ADD_WORKEX":      setFormDetails({...formDetails,
                                                Title:'Add New Work Experience',
                                                FormComponent: <WorkExperienceForm type={detailType} newWorkEx={formState} setNewWorkEx={setFormState}/>,
                                                ButtonText: 'Add Experience'
                                            })
                
                                            break;
                    case "EDIT_WORKEX":      setFormDetails({...formDetails,
                                                Title:'Edit Work Experience',
                                                FormComponent: <WorkExperienceForm type={detailType} newWorkEx={formState} setNewWorkEx={setFormState}/>,
                                                ButtonText: 'Save Changes'
                                            })
                
                                            break;

            case "ADD_TECHNICAL_SKILL":     setFormDetails({...formDetails,
                                                Title:'Add New Technical Skill',
                                                FormComponent:  <SkillsForm type={"Technical Skills"} newSkill={formState} setNewSkill={setFormState}/>,
                                                ButtonText: 'Add Skill'
                                            })
                            
                                            break;
            case "ADD_INTERPERSONAL_SKILL":     setFormDetails({...formDetails,
                                                Title:'Add New Interpersonal Skill',
                                                FormComponent:  <SkillsForm type={"Interpersonal Skills"} newSkill={formState} setNewSkill={setFormState}/>,
                                                ButtonText: 'Add Skill'
                                            })
                            
                                            break;
                    case "ADD_PROJECT":     setFormDetails({...formDetails,
                                                Title:'Add New Gig/Project',
                                                FormComponent:  <ProjectForm type={detailType} newProject={formState} setNewProject={setFormState}/>,
                                                ButtonText: 'Add Project'
                                            })
                            
                                            break;
                    case "EDIT_PROJECT":     setFormDetails({...formDetails,
                                                Title:'Edit Project',
                                                FormComponent:  <ProjectForm type={detailType} newProject={formState} setNewProject={setFormState}/>,
                                                ButtonText: 'Save Changes'
                                            })
                            
                                            break;
                  case "ADD_EDUCATION":     setFormDetails({...formDetails,
                                                Title:'Add Education Details',
                                                FormComponent:  <EducationForm/>,
                                                ButtonText: 'Add Education'
                                            })
                            
                                            break;
            
                                 default:   break;
        }
    },[])
    return(
        <>
            <FormModal  appendToParentState={appendToParentState} formState={formState} type={detailType} formDetails={formDetails} showForm={showForm} setShowForm={setShowForm}/>
        </>
    )
}

export default AddDetails;