import React, { useState, useEffect } from 'react'
import { EndPointPrefix} from '../../../../../constants/constants';
import AddDetails from './AddDetails';
import axios from 'axios';
import Axios from 'axios';
import {useAlert} from 'react-alert';
import Skeleton from 'react-loading-skeleton';
import moment from 'moment';



const StudentProfileWorkExperience=()=>{
  const alert=useAlert();
  const [loading,setLoading]=useState(true)
  const [showWorkExperienceForm,setShowWorkExperienceForm]=useState(false);
  const [workEx,setWorkEx]=useState([])
  const [type,setType]=useState('')
  const [selectedWorkEx,setSelectedWorkEx]=useState({})

  const [newWorkEx,setNewWorkEx]=useState({
    id:null,
    company_id:null,
    company_name:'',
    company_website:'',
    job_type_id:null,
    job_type_name:'',
    start_date:null,
    end_date:null,
    job_designation:'',
    job_description:'',
    skills:[]
  })

  const AddWorkEx=()=>{
    console.log(typeof(workEx))
    let data=newWorkEx;
    let isValid=true;
    console.log(newWorkEx)
    Object.values(newWorkEx).forEach((val,ind)=>{
      if(ind>2 && !val){
        console.log(ind,val)
        isValid=false;
        return;
      }
    })
    if(!isValid){
      alert.error('Please fill all the details first')
      return;
    }
    console.log('posting data: ',data);
    Axios.post(EndPointPrefix+'/company/work_experience/',data,{
        headers:{
          "Authorization":'Token '+localStorage.getItem('gw_token')
      }
    })
      .then(res=>{
        console.log(res);
        if(res.data.success){
          alert.success(res.data.data.message)
          setNewWorkEx({...newWorkEx,id:res.data.data.work_id})
        }
        else
        alert.error(res.data.error);
      })
      .catch(err=>{
        console.log(err);
      })

  }

  const DeleteWorkEx=(id)=>{
    Axios.delete(EndPointPrefix+`/company/work_experience/${id}/`,{
      headers:{
        "Authorization":'Token '+localStorage.getItem('gw_token')
      }
    })
      .then(res=>{
        console.log(res);
        if(res.data.success){
          alert.success(res.data.data.message)
          let temp=workEx.filter((workex)=>{
            return workex.id!=id;
          })
          setWorkEx(temp);
        }
        else{
          alert.error(res.data.error)
        }
      })
      .catch(err=>{
        console.log(err);
      });
  }

  useEffect(()=>{
    console.log(newWorkEx)
    if(newWorkEx && newWorkEx.id){
      setWorkEx([...workEx,newWorkEx])
    }
  },[newWorkEx])

  const ChangeWorkEx=()=>{
    //send patch request to update details
    //console.log('Changed: ',selectedWorkEx)
    let temp=[...workEx]
    temp.forEach((workex,ind)=>{
      if(workex.id==selectedWorkEx.id){
        console.log('found! ');
        let res=updateWorkEx(selectedWorkEx)
        res.then((response)=>{
          temp[ind]=selectedWorkEx;
          setWorkEx(temp)
        })
      }
    })
  }

  const updateWorkEx=async(workex)=>{
    let result=Axios.patch(EndPointPrefix+`/company/work_experience/${workex.id}/`,workex,{
      headers: {
        "Authorization":'Token '+localStorage.getItem('gw_token')
      }
  })
      .then(res=>{
        console.log(res);
        if(res.data.success)
        alert.success(res.data.data.message)
        else
        alert.error(res.data.error)
      })
      .catch(err=>{
        console.log(err);
      })
    return result;
    }


  const EditWorkEx=(workex)=>{
    console.log(workex)
    let temp=workex;
    console.log(temp)
    setSelectedWorkEx(temp);
    openWorkExperienceForm("EDIT_WORKEX")
  }

  useEffect(()=>{
    console.log(selectedWorkEx);
  },[selectedWorkEx])

  useEffect(()=>{
    setLoading(true)
    axios.get(EndPointPrefix+'/company/work_experience/',{
        headers:{
          "Authorization":'Token '+localStorage.getItem('gw_token')
      }
    })
      .then(res=>{
        setLoading(false)
        console.log(res);
        if(res.data.success){
          //user already has some work experience, populate state
          let temp=[];
          res.data.data.forEach(workex=>{
            temp.push(workex)
          })
          setWorkEx(temp)
        }
      })
      .catch(err=>{
        console.log(err);
      })
  },[])

  const openWorkExperienceForm=(workex_type)=>{
    setType(workex_type)
    setShowWorkExperienceForm(true);
  }
    return(
        <>
        {
          showWorkExperienceForm && (<AddDetails 
          appendToParentState={ type=="ADD_WORKEX" ? AddWorkEx : ChangeWorkEx} 
          formState={ type=="ADD_WORKEX" ? newWorkEx : selectedWorkEx} 
          setFormState={ type=="ADD_WORKEX" ? setNewWorkEx : setSelectedWorkEx} 
          detailType={type} showForm={showWorkExperienceForm} setShowForm={setShowWorkExperienceForm}/>)
        }

      <div className="row my-3 justify-content-center">
        <button onClick={()=>{openWorkExperienceForm("ADD_WORKEX")}} className="add ripple"><span>+</span></button>
      </div>
      { 
        loading ? <div>Loading...</div>
        :
        
      workEx.length>0
      ?
       workEx.map((workex,ind)=>{
          return(
            
            <div className="card job-card-main" key={workex.job_type_id}>
              <div className="card-body">
                <div className="row">
                  <div className="col-md-8 col-7 text-left">
                    <p className="fs-12 text-muted">Work Experience {ind+1}</p> 
                  </div>
                  <div className="col-md-4 col-5 text-right">
                    <i onClick={()=>{EditWorkEx(workex)}}className="fas fa-edit edit-icon mx-2"></i>
                    <i onClick={()=>{DeleteWorkEx(workex.id)}} className="fas fa-trash edit-icon mx-2"></i>
                  </div>
                </div>
                <div className="row my-2">
                  <div className="col-md-1 col-sm-4 text-center">
                    <img src="https://getwork.org/wp-content/uploads/2019/12/getwork_new.png" alt="" style={{height:'50px',width:'50px'}}/>
                  </div>
                  <div className="col-md-7 col-sm-8 text-left">
                    <p className="fs-14">
                    <b className="fs-16"> {workex.job_designation || <Skeleton width={100} duration={2}/>}</b> @  <span className="fw-500 link-text">{workex.company_name || <Skeleton width={100} duration={2}/>}</span>
                    </p>             
                  </div>
                  <div className="col-md-4 col-sm-12 text-md-right text-left">
                    <span className="fs-12 text-muted"><i class="far fa-calendar"></i> {moment(workex.end_date).diff(moment(workex.start_date),'months')} months</span>
                    <br/>
                    <span className="fs-13 text-muted"> {moment(workex.start_date).format('MMM DD')} - {moment(workex.end_date).format('MMM DD')}  </span>
              
                  </div>
                </div>
                <div className="row my-2">
                  <div className="col-md-8 col-sm-12 text-left">
                    <p className="fs-12">
                     {workex.job_description || <Skeleton width={100} duration={2}/>}
                    </p>
                  </div>
                  <div className="col-md-4 col-sm-12 text-left pl-md-5 pl-2">
                   {
                      workex.skills && workex.skills.map((skill)=>{
                        return(
                          <span key={skill.skill_id} className="badge badge-light bg-blue-light p-1 mx-2 my-1">{skill.skill_name}</span>

                        )

                      })
                   }

                  </div>
                </div>
                
              </div>

            </div>
           
          )

        })

        :
        <div>Add some past work experiences to enhance your GetWork score!</div>
       
      }

      
      
     

        </>
    )
}

export default StudentProfileWorkExperience;

{/* 
        <div className="card job-card-main">
              <div className="card-body">
                <div className="row">
                  <div className="col-md-8 col-sm-6 text-left">
                    <p className="fs-12 text-muted"> <Skeleton width={100} duration={2}/></p> 
                  </div>
                  <div className="col-md-4 col-sm-6 text-right">
                   
                    <Skeleton width={100} duration={2}/>
                    <Skeleton width={100} duration={2}/>
                  </div>
                </div>
                <div className="row my-2">
                  <div className="col-md-1 col-sm-4">
                    <Skeleton width={100} duration={2}/>
                  </div>
                  <div className="col-md-7 col-sm-8 text-left">
                    <p className="fs-14">
                    <b className="fs-16"> <Skeleton width={100} duration={2}/></b>   <span className="fw-500 link-text"><Skeleton width={100} duration={2}/></span>
                    </p>             
                  </div>
                  <div className="col-md-4 col-sm-12 text-md-right text-left">
                    <span className="fs-12 text-muted"><Skeleton width={100} duration={2}/></span>
                    <br/>
                    <span className="fs-13 text-muted"> <Skeleton width={100} duration={2}/> - <Skeleton width={100} duration={2}/>  </span>
              
                  </div>
                </div>
                <div className="row my-2">
                  <div className="col-md-8 col-sm-12 text-left">
                    <p className="fs-12">
                    <Skeleton width={100} duration={2}/>
                    <Skeleton width={100} duration={2}/>
                    <Skeleton width={100} duration={2}/>
                    <Skeleton width={100} duration={2}/>
                    </p>
                  </div>
                  <div className="col-md-4 col-sm-12 text-left pl-md-5 pl-2">
               
                  <Skeleton width={100} duration={2}/>
                  <Skeleton width={100} duration={2}/>
                  <Skeleton width={100} duration={2}/>
                    
                   

                  </div>
                </div>
                
              </div>

            </div> */}