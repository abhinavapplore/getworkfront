import React from 'react';
import WhiteLogo from '../../../../assets/img/getwork_white_new.png';
import './JobTypes.css';

const JobTypes=()=>{
    return(
        <>
            <nav className="navbar navbar-main navbar-expand-lg navbar-dark bg-dark shadow_1-lightest">
                <a className="navbar-brand" href="#">
                   <img src={WhiteLogo} className="logo"/>
                </a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon" />
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto text-dark">
                    <li className="nav-item active">
                        <a className="nav-link" href="#">Home <span className="sr-only">(current)</span></a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#">Link</a>
                    </li>
                    <li className="nav-item dropdown">
                        <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Dropdown
                        </a>
                        <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a className="dropdown-item" href="#">Action</a>
                        <a className="dropdown-item" href="#">Another action</a>
                        <div className="dropdown-divider" />
                        <a className="dropdown-item" href="#">Something else here</a>
                        </div>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link disabled" href="#">Disabled</a>
                    </li>
                    </ul>

                </div>
        </nav>
        <div className="job-type-page bg-grey-light">
            <div className="container">
                <div className="card job-card pt-3 shadow_1-lightest">
                    <div className="card-body">
                       <div className="row my-3 "><h2 className="fw-900 mx-auto fs-34">Which job types interest you?</h2></div>
                        <div className="row-my-1"><p className="text-muted mx-auto fs-20">Please select at least one job type. The more, the better!</p></div>

                    </div>
                </div>
            </div>


        </div>
        </>
    )
}

export default JobTypes;