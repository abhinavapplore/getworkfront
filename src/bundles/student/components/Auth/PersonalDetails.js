import React,{useState, useEffect} from 'react';
import {passwordRegex as PasswordRegex} from '../../../../constants/constants';
import axios from 'axios';
import {EndPointPrefix} from '../../../../constants/constants';
import { Tooltip } from 'reactstrap';
import { useAlert } from 'react-alert'


const StudentPersonalDetails=({step,setStep,studentDetails,setStudentDetails})=>{

    const alert=useAlert();
    const [disableNext,setDisableNext]=useState(true);
    const [tooltipOpen, setTooltipOpen] = useState(false);
    const toggle = () => setTooltipOpen(!tooltipOpen);
    const [showPassword,setShowPassword]=useState(false);
    let passwordFieldType= showPassword?'text':'password';
    let tooltipText= showPassword?'Hide Password':'Show Password';
    let disableClass=disableNext?'disabled':'';
    //console.log('inside personal dets: ',step,studentDetails);

    useEffect(()=>{
        //console.log('updated student details: ',studentDetails);
        if(studentDetails.FirstName && studentDetails.LastName && studentDetails.PhoneNo && 
            studentDetails.Email && studentDetails.Password && PasswordRegex.test(studentDetails.Password))
            setDisableNext(false);
        else
            setDisableNext(true);
            
    },[studentDetails])

    useEffect(()=>{
        console.log('set student id as ',studentDetails.StudentID);
    },[studentDetails.StudentID])

    const handleChange=(e)=>{
        //console.log(e.target.id,e.target.value)
         let stateName=e.target.id;
         let stateValue=e.target.value;
        e.preventDefault();
        setStudentDetails({...studentDetails,[stateName]:stateValue})

    }

     const createNewStudent=()=>{
        let isSuccess=false;
        axios.post(EndPointPrefix+'/profile/create/',{
                
                "email":studentDetails.Email,
                "first_name":studentDetails.FirstName,
                "last_name":studentDetails.LastName,
                "password":studentDetails.Password,
                
            
                
        })
            .then(res=>{
                if(res.data.error){
                alert.error(res.data.error)
                return;
                }
                isSuccess=true;
                console.log('create profile api response: ',res);
                setStudentDetails({...studentDetails,StudentID:res.data.id})
                alert.success('Successfully created the user!')
                //activateStudent();
                //setStep(step+1);
            })
            .catch(err=>{
                console.log(err);
                alert.error('Some error occured in creating the user');
            })
            
    }

    const activateStudent=()=>{
        if(studentDetails.StudentID){
        axios.post(EndPointPrefix+'/profile/user/',
                {
                    "user":studentDetails.StudentID,
                    "type": 1,
                    "is_active":true
                }
                
            )
            .then(res=>{
                console.log('res from activate user api: ',res);
                alert.success('Successfully activated user!')
                setStudentDetails({...studentDetails,isActive:res.data.is_active})
                setStep(step+1)
            })
            .catch(err=>{
                alert.error('Some error occured in activating the user')
                console.log(err);
            })
        }
        // else{
        //     alert.error('Student id not set!')
        // }
    }

    useEffect(()=>{
        if(!studentDetails.isActive)
        activateStudent()
    },[studentDetails.StudentID])

    const handleSubmit=(e)=>{
        e.preventDefault();
        console.log('details captured: ',studentDetails);
        if(studentDetails.PhoneNo.length!=10){
        alert.error('Please enter a valid Indian Mobile number');
        return;
        }
        createNewStudent();
        
    }

    return(
        <>
                <div className="row pl-3">
                    <h1 className="mb-4 login-heading text-left">Enter your Personal Details</h1>
                </div>

                    <form onChange={handleChange} onSubmit={handleSubmit} className="my-md-0 my-2"> 
                        <div className="row">
                            <div className="col-6">
                            <div className="form-group text-left">
                            <label htmlFor="FirstName" className="fw-500">First Name</label>
                            <input type="text" value={studentDetails.FirstName} className="form-control mt-2 mb-1 shadow_1-lightest" id="FirstName" aria-describedby="emailHelp" placeholder="" required autoFocus/>

                            </div>
                            </div>
                            <div className="col-6">
                            <div className="form-group text-left">
                            <label htmlFor="LastName" className="fw-500">Last Name</label>
                            <input type="text" value={studentDetails.LastName} className="form-control mt-2 mb-1 shadow_1-lightest" id="LastName" aria-describedby="emailHelp" placeholder="" required autoFocus/>


                    </div>
                            </div>
                        </div>

                    <div className="form-group text-left">
                            <label htmlFor="PhoneNo" className="fw-500">Phone No</label>
                            <input type="number" value={studentDetails.PhoneNo} className="form-control mt-2 mb-1 shadow_1-lightest" id="PhoneNo" aria-describedby="emailHelp" placeholder="" required autoFocus/>


                    </div>
                    <div className="form-group text-left">
                            <label htmlFor="Email" className="fw-500">Enter your Email address</label>
                            <input type="email" value={studentDetails.Email} className="form-control mt-2 mb-1 shadow_1-lightest" id="Email" aria-describedby="emailHelp" placeholder="" required autoFocus/>


                    </div>
                    <div className="form-group text-left">
            
                            <label htmlFor="Password" className="fw-500">Create your Password</label>
                            <small id="emailHelp" className="form-text text-muted">Passworld must be atleast 8 characters long and contain at least one uppercase, one lowercase and one digit</small>
                            <div className="row no-gutters">
                                <div className="col">
                                <input type={passwordFieldType} value={studentDetails.Password} className="form-control mt-2 mb-1 shadow_1-lightest" id="Password" placeholder="" required autoFocus pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"/>
                                </div>
                                <div className="col-auto">
                                    <i onClick={()=>{setShowPassword(!showPassword)}} id="toggleShowPassword" className="fas fa-eye show-password-icon"></i>
                                </div>
                                <Tooltip className="fs-13" placement="right" isOpen={tooltipOpen} target="toggleShowPassword" toggle={toggle}>
                               {tooltipText}
                                </Tooltip>
                            </div>

                           


                    </div>

                      <div className="row my-1 pt-3">
                          <div className="col-6 text-left">
          
                        <button className={disableClass +" shadow_1 btn btn-lg btn-main btn-login btn-next text-uppercase font-weight-bold mb-2"}  type="submit"><span>Next <i className="fas fa-angle-double-right"/></span></button>

                          </div>
               

                        </div>
                    </form>
    
        </>
    )
}

export default StudentPersonalDetails;