import React,{useEffect,useState, useRef} from 'react';
import Logo from '../../../../assets/img/getwork-logo.png';
import {useAlert} from 'react-alert';
import axios from 'axios';
import SelectSearch from 'react-select-search'
import {useHistory} from 'react-router-dom';
import {veteranStatuses,Ethincities,Genders, EndPointPrefix} from '../../../../constants/constants'
import Loader from '../../../common/components/UI/Loader';

const StudentIdentification=({step,setStep,studentDetails,setStudentDetails})=>{

    const disableButton=useRef();
    const [loading,setLoading]=useState(false);
    const alert=useAlert();
    let history=useHistory();
    console.log(studentDetails)
    const gotoLogin=()=>{
        history.push('/login');
    }

    const getEthnicityList=()=>{
        axios.get(EndPointPrefix+'/education/ethnicity/')
            .then(res=>{
                console.log(res.data);
                let temp=[];
                res.data.forEach(ethnicity=>{
                   
                    let tempObj={
                        name:ethnicity.name,
                        value:ethnicity.name,
                        id:ethnicity.id
                    }
                    console.log(tempObj)
                    temp.push(tempObj);
                    hashEthnicity[ethnicity.name]=ethnicity.id;
                })
                setEthnicityList(temp);
                //console.log(ethnicityList);
                console.log(hashEthnicity);
            })
            .catch(err=>{
                console.log(err);
            })
    }

    useEffect(()=>{
        getEthnicityList();
        disableButton.current.classList.add('disabled');
    },[])



    const [ethnicityList,setEthnicityList]=useState();
    const [hashEthnicity,setHashEthnicity]=useState({});
    const [gender,setGender]=useState('')
    const [veteranStatus,setVeteranStatus]=useState('')
    const [ethnicity, setEthnicity]=useState('')


    useEffect(()=>{
        if(gender && veteranStatus && ethnicity)
            disableButton.current.classList.remove('disabled');
        else
            disableButton.current.classList.add('disabled');
    },[gender,veteranStatus,ethnicity])


    const LoginStudent=()=>{

            localStorage.setItem('gw_token',studentDetails.Token);
            history.push('/student/dashboard')

          
    }

    const sendEducationalDetails=()=>{
        let data={
            "type_id":studentDetails.Type,
            "user_id":studentDetails.StudentID,
            "education_level":studentDetails.QualificationID,
            "college_country":1,
            "gender":gender,
            "ethnicity":hashEthnicity[ethnicity],
            "veteran":veteranStatus,
            "current_year":Number(studentDetails.Year),
            "referral_code": studentDetails.RefferalCode?studentDetails.RefferalCode:null

            }
  
        switch (studentDetails.Case) {
            case 1: data={
                        ...data,
                        "college_id":studentDetails.CollegeID,
                        "degree_id":studentDetails.CourseID,
                        "specialization_id":studentDetails.BranchID ? studentDetails.BranchID : null,
                        "college_name":null,
                        "college_city":null,
                        "college_state":null,
                        "degree_name":null,
                        "specialization_name":null
                    }
                    break;
            case 2: data={
                        ...data,
                        "college_id":studentDetails.CollegeID,
                        "degree_id":studentDetails.CourseID,
                        "specialization_id":studentDetails.BranchID ? studentDetails.BranchID : null,
                        "college_name":null,
                        "college_city":null,
                        "college_state":null,
                        "degree_name":null,
                        "specialization_name":null
                    }
                    break;
            case 3: data={
                        ...data,
                        "college_id":studentDetails.CollegeID,
                        "degree_id":studentDetails.CourseID,
                        "specialization_name":studentDetails.Branch ? studentDetails.Branch : null,
                        "college_name":null,
                        "college_city":null,
                        "college_state":null,
                        "degree_name":null,
                        "specialization_id":null
                    }
                    break;
            case 4: data={
                        ...data,
                        "college_id":studentDetails.CollegeID,
                        "degree_name":studentDetails.Course,
                        "specialization_name":studentDetails.Branch ? studentDetails.Branch : null,
                        "college_name":null,
                        "college_city":null,
                        "college_state":null,
                        "degree_id":null,
                        "specialization_id":null
                    }
                    break;
            case 5: data={
                        ...data,
                        "college_name":studentDetails.CollegeName,
                        "degree_id":studentDetails.CourseID,
                        "specialization_id":studentDetails.BranchID ? studentDetails.BranchID : null,
                        "college_id":null,
                        "college_city":studentDetails.CollegeCityID,
                        "college_state":studentDetails.CollegeStateID,
                        "degree_name":null,
                        "specialization_name":null
                    }
                    break;
            case 6: data={
                        ...data,
                        "college_name":studentDetails.CollegeName,
                        "degree_id":studentDetails.CourseID,
                        "specialization_name":studentDetails.Branch ? studentDetails.Branch : null,
                        "college_id":null,
                        "college_city":studentDetails.CollegeCityID,
                        "college_state":studentDetails.CollegeStateID,
                        "degree_name":null,
                        "specialization_id":null
                    }
                    break;
            case 7: data={
                        ...data,
                        "college_name":studentDetails.CollegeName,
                        "degree_name":studentDetails.Course,
                        "specialization_name":studentDetails.Branch ? studentDetails.Branch : null,
                        "college_city":studentDetails.CollegeCityID,
                        "college_state":studentDetails.CollegeStateID,
                        "degree_id":null,
                        "college_id":null,
                        "specialization_id":null
                    }
                    break;
        
            default:
                    break;
        }
        console.log('Data: ',data);
        axios.post(EndPointPrefix+'/education/student_user',{ ...data })
            .then(res=>{
                setLoading(false);
                console.log(res);
                if(res.data.success)
                alert.success(res.data.data.message)
                else{
                    alert.error(res.data.error)
                    return;
                }
                alert.success('User signup successfully completed!')
                LoginStudent();
            })
            .catch(err=>{
                console.log(err);
                alert.error('There was some error');
            })

    }

    const completeSignup=()=>{
        setLoading(true)
        sendEducationalDetails();
    }

    const GenderDropdown= () =>( <SelectSearch options={Genders} onChange={setGender} value={gender} placeholder="Select" />  )
    const EthincityDropdown=()=>( <SelectSearch options={ethnicityList} onChange={setEthnicity} value={ethnicity} placeholder="Select" /> )
    const VeteranStatusDropdown=()=>( <SelectSearch options={veteranStatuses} onChange={setVeteranStatus} value={veteranStatus} placeholder="Select" /> )

    return loading ? 
    <Loader/>
    :
    (
        <>
        <div className="container fluid-container">
        <div className="row my-3">
                <div className="col-6 text-left">
                    <img src={Logo} alt="" className="logo-lg"/>

                </div>
                <div className="col-6 text-right pt-3">

                <button onClick={()=>{gotoLogin()}}className=" shadow_1 btn btn-lg btn-main btn-login btn-next text-uppercase font-weight-bold mb-2" ><span>Logout</span></button>

                </div>
            </div>
            <div className="row justify-content-center">
                <h3 className="fw-900">Heads up!</h3>
                <div className="mx-md-5 px-md-5 mx-0 px-0">
                <p className="px-md-5 px-0 my-3">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa fugit ab 
                    veritatis, voluptas minus tempora temporibus consequuntur. Quidem, impedit. 
                    Laudantium sed perferendis voluptatibus praesentium cupiditate? Accusamus porro 
                    repudiandae nulla eius.
                </p>
                </div>

            </div>
            <div className="row my-3">
                <div className="col-md-6 text-left">

                    <div className="form-group required text-left">
                            <label htmlFor="Gender" className="fw-500 control-label">Gender</label>
                            <GenderDropdown className="form-control mt-2 mb-1 shadow_1-lightest" id="Gender"/>


                    </div>
                </div>
                <div className="col-md-6 text-right">

                    <div className="form-group required text-left">
                            <label htmlFor="Ethnicity" className="fw-500 control-label">Ethnicity</label>
                            <EthincityDropdown className="form-control mt-2 mb-1 shadow_1-lightest" id="Ethnicity" />


                    </div>
                </div>
            </div>
            <div className="row text-left my-1 mx-1">
                <h6>Veteran Status:</h6>
                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor 
                    invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos 
                    et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata 
                    sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing
                     elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, 
                     sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita 
                     kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
                    Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, 
                    vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio 
                    dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla 
                    facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh 
                    uismod tincidunt ut laoreet dolore magna aliquam erat volutpat.Ut wisi enim ad minim 
                    veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea 
                    commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse 
                    molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan
                     et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore 
                     te feugait nulla facilisi.

                </p>

                <div className="form-group text-left required w-100 my-1">
                            <label htmlFor="Veteran" className="fw-500 control-label">Veteran Status</label>
                            <VeteranStatusDropdown className="form-control mt-2 mb-1 shadow_1-lightest" id="Veteran" />


                    </div>
            </div>
            <div className="row my-3 pt-md-2 pt-3">
                          <div className="col-6 text-left">


                          </div>
                        <div className="col-6 text-right">
                        <button ref={disableButton} onClick={completeSignup} className=" shadow_1 btn btn-lg btn-main btn-login btn-next text-uppercase font-weight-bold mb-2"><span>Find Jobs <i class="fas fa-angle-double-right"/></span></button>

                        </div>

             </div>
        </div>

        </>
    )
}

export default StudentIdentification;