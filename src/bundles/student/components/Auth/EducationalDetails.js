import React,{useEffect, useState, useRef} from 'react';
import axios from 'axios';
import {useAlert} from 'react-alert'
import SelectSearch from 'react-select-search'
import '../../../../SelectSearch.css';
import {years,highestQualifications,EndPointPrefix} from '../../../../constants/constants'


const StudentEducationalDetails=({step,setStep,showField,setShowField,studentDetails,setStudentDetails})=>{
    
    //UI state variables
    const firstTime=useRef(true);
    const alert=useAlert();
    const [tooltipOpen, setTooltipOpen] = useState(false);
    const toggle = () => setTooltipOpen(!tooltipOpen);
    const [showTooltip,setShowTooltip]=useState(false);

    //UI state variables for conditionally showing fields


    // const [collegeList,setCollegeList]=useState([])
    let hashQualification={
        "Graduation":3,
        "Post Graduation":4,
        "PhD":5
    }
    const [qualificationList,setQualificationList]=useState([])
    
    //hash maps for storing ids and returning them in O(1)
    let  [hashState,setHashState]=useState({})  
    let  [hashCity,setHashCity]=useState({})
    let  [hashCollege,setHashCollege]=useState({})
    let  [hashCourse,setHashCourse]=useState({})
    let  [hashBranch,setHashBranch]=useState({})

    
    //state variables for storing user details
    const [stateList,setStateList]=useState([])
    let   [cityList,setCityList]=useState([])
    const [collegeList,setCollegeList]=useState('')
    const [courseList,setCourseList]=useState('')
    const [branchList,setBranchList]=useState('');
    const [college,setCollege]=useState('');
    const [collegeState,setCollegeState]=useState('');
    const [collegeCity,setCollegeCity]=useState('');
    const [collegeID,setCollegeID]=useState(null);
    const [courseID,setCourseID]=useState(null);
    const [branchID,setBranchID]=useState(null);
    const [qualification,setQualification]=useState('');
    const [course,setCourse]=useState('');
    const [branch,setBranch]=useState('');
    const [year,setYear]=useState('');

    //Fields and Dropdown components
    const QualificationDropdown = () => (
        
        <SelectSearch
            value={qualification}
            onChange={setQualification}
            options={highestQualifications}
            placeholder="Select your ongoing/recent acquired degree"
            
        />
     );
    const YearDropdown= () =>(
        <SelectSearch
            value={year}
            options={years}
            placeholder="Select"
            onChange={setYear}
        />
    )
     const TypeAndSearchCollege = () => (
        <SelectSearch
            options={collegeList}
            value={college}
            onChange={setCollege}
            search
            placeholder="Type and Search your college from the list"
            
        />
     );

     const TypeAndSearchState = () => (
        <SelectSearch
            options={stateList}
            value={collegeState}
            onChange={setCollegeState}
            search
            placeholder="Search State"
            
        />
     );

     const TypeAndSearchCity = () => (
        <SelectSearch
            options={cityList}
            value={collegeCity}
            onChange={setCollegeCity}
            search
            placeholder="Search City"
            
        />
     );

     const TypeAndSearchCourse = () => (
        <SelectSearch
            options={courseList}
            search
            value={course}
            onChange={setCourse}
            placeholder="Type and Search your course from the list"
        />
     );

     const TypeAndSearchBranch = () => (
        <SelectSearch
            options={branchList}
            search
            value={branch}
            onChange={setBranch}
            placeholder="Type and Search your branch from the list"
        />
     );


    //LIFECYCLE HOOKS

    //this lifecycle hook fires when component first mounts
    useEffect(()=>{ 
            //console.log('student details: ',studentDetails);
            getCollegeList(); 
            if(firstTime.current){
                firstTime.current=false;
                return;
            }
            else{
                setCollege(studentDetails.CollegeName);
                setCollegeID(studentDetails.CollegeID);
                setCourse(studentDetails.Course);
                setCourseID(studentDetails.CourseID)
                setBranch(studentDetails.branch);
                setBranchID(studentDetails.BranchID);
                setQualification(studentDetails.Qualification);
                setYear(studentDetails.year)

            }
            
        
    },[])

    //this lifecycle hook fires when some details are set
    useEffect(()=>{
        //console.log('students educational details: ',college,collegeState,collegeCity,qualification,course,branch,year);   
        //console.log('main hook fired!')
        
        if(!showField.College && hashCollege[college]){
        let courseID='',branchID='';

        if(hashCourse[course] && hashCollege[college].onBoard)  
        courseID=hashCourse[course].d_id;
        else 
        courseID=hashCourse[course];

        if(!showField.Branch && hashBranch[branch])
        branchID=hashBranch[branch].b_id;
        
        setStudentDetails({
            ...studentDetails,
            // CollegeState:hashState[collegeState],
            // collegeCity:hashCity[collegeCity],
            CollegeName:college,
            CollegeID:hashCollege[college].c_id,
            Course:course,
            CourseID:courseID,
            Branch:branch,
            BranchID:branchID,
            Qualification:qualification,
            QualificationID:hashQualification[qualification],
            Year:year
        })
    }
        else{
        let branchID='';
        if(!showField.Branch && hashBranch[branch])
        branchID=hashBranch[branch].b_id;
            //console.log('setting details for new college');
            setStudentDetails({
                ...studentDetails,
                CollegeStateID:hashState[collegeState],
                CollegeCityID:hashCity[collegeCity],
                CollegeName:college,
                CourseID:hashCourse[course],
                BranchID:branchID,
                Course:course,
                Branch:branch,
                Qualification:qualification,
                QualificationID:hashQualification[qualification],
                Year:year
            })
        }
    
            
    },[college,collegeState,collegeCity,qualification,course,branch,year])

    // this lifecycle hook fires when qualification,college,branch,or course is selected by the user
    useEffect(()=>{
        //check if college is on boarded or not
       if(!showField.College && hashQualification[qualification]){

            //CASE 1: college is on board, GET degrees for the selected college ID
            if(hashCollege[college] && hashCollege[college].onBoard && hashCollege[college].c_id){
                let collegeID=hashCollege[college].c_id;
                let qualificationID=hashQualification[qualification];
                getDegreeListByQualification(collegeID,qualificationID);
            }

            //CASE 2: college is not on board but in GetWork db, get all degrees
            if(hashCollege[college] && !hashCollege[college].onBoard){
            //console.log(`${college} is in the list, get all degrees`)
            getAllDegrees(hashQualification[qualification]);
            }


        }
        else if(showField.College){
             //CASE 3: college is not in GetWork db
             getAllDegrees(hashQualification[qualification]);
        }

        
    },[qualification,college,branch,course])

    
    //this lifecycle hook fires when course is selected by the user
    // useEffect(()=>{
    //         //console.log(hashCourse)
    //         //console.log('user selected :',course)
           
            
    // },[course])
    
    //this lifecycle hook fires when college's state is set
    useEffect(()=>{
        // console.log(collegeState);
        // console.log(hashState[collegeState]);
        let stateID=hashState[collegeState];
        //console.log(`collegeState of state id ${stateID}`);
        getCityList(stateID);
    },[collegeState])


    useEffect(()=>{
        if(showField.College)
        getStateList();
    },[showField.College])

    useEffect(()=>{
        if(showField.College && showField.Course){
            setShowField({...showField,Branch:true})
        }
        console.log("dets: ",college,hashCollege,hashCollege[college.toString()])
            
        if(!showField.College && hashCollege[college.toString()] && hashCollege[college.toString()]["onBoard"]){
            console.log('here')
            if(hashCourse[course] && hashCourse[course].isBranch && hashCourse[course].d_id){                   
                let collegeID=hashCollege[college].c_id;
                let qualificationID=hashQualification[qualification];
                let courseID=hashCourse[course].d_id;
                console.log('here abc: ',hashCourse[course])
                getBranchListByID(collegeID,qualificationID,courseID)
            }
        }
        else if((!showField.College && hashCollege[college] && !hashCollege[college].onBoard) || showField.College){               
            let courseID=hashCourse[course];
            getAllBranchesByDegree(courseID);
        }
        else{
            console.log('reached here')
        }
    },[course])

    //API Calls
    //GET all degrees
    const getAllDegrees=(qualificationID)=>{
        axios.get(EndPointPrefix+'/education/degree/?search='+qualificationID)
            .then(res=>{
                console.log('res from get all degree list api: ',res);
                let temp=[]
                res.data.forEach(degreeData=>{
                    hashCourse[degreeData.name]=degreeData.id;
                    let tempObj={
                        name:degreeData.name,
                        value:degreeData.name,
                        id:degreeData.id
                    }
                    temp.push(tempObj);
                })
                //console.log(hashCourse);
                setCourseList(temp);
            })
            .catch(err=>{
                console.log(err);
            })
    }

    //GET branches by Course/Degree id
    const getAllBranchesByDegree=(degreeID)=>{
        axios.get(EndPointPrefix+'/education/getspecialization/?search='+degreeID)
            .then(res=>{
                console.log('res from all branches by degree: ',res.data);
                let temp=[];
                
                res.data.forEach(branchData=>{

    
                    hashBranch[branchData.specialization.name]={
                        b_id:branchData.specialization.id
                        
                    }
                    let tempObj={
                        name:branchData.specialization.name,
                        value:branchData.specialization.name,
                        id:branchData.specialization.id
                    }
                    temp.push(tempObj);
                })
    
                setBranchList(temp)

            })
            .catch(err=>{
                console.log('err');
            })
    }


    //GET all citites by state id
    const getCityList=(stateID)=>{
            axios.get(EndPointPrefix+'/location/city/?search='+stateID)
                .then(res=>{
                    console.log('response from city list by state id: ',res)
                    let temp=[];
                    let citySet=new Set;
                    res.data.forEach(cityObj=>{
                        if(citySet.has(cityObj.city))
                        return;
                        hashCity[cityObj.city]=cityObj.city_id;
                        citySet.add(cityObj.city)
                        let tempObj={
                            name:cityObj.city,
                            value:cityObj.city,
                            state_id:cityObj.state_id,
                            id:cityObj.city_id
                        }
                        temp.push(tempObj)
                    })
         
                    setCityList(temp);
    
                })
                .catch(err=>{
                    console.log(err);
                })
    }
    
    
    //GET all colleges
    const getCollegeList=()=>{
            axios.get(EndPointPrefix+'/education/college/')
            .then(res=>{
                console.log('res from college list api: ',res.data)
                
                let temp=[];
                res.data.forEach(collegeData=>{
                    hashCollege[collegeData.name]={
                        c_id:collegeData.id,
                        onBoard:collegeData.is_onboarded
                    }
                    let tempObj={
                        name:collegeData.name,
                        value:collegeData.name,
                        id:collegeData.id
                    }
                    temp.push(tempObj);
                })
    
                setCollegeList(temp)
                
            })
            .catch(err=>{
                console.log(err);
            })
    }
    
    //GET all states
    const getStateList=()=>{
            axios.get(EndPointPrefix+'/location/state/')
            .then(res=>{
                console.log('res from state list api: ',res.data)
                
                let temp=[];
                res.data.forEach(stateObj=>{
                    hashState[stateObj.state]=stateObj.state_id;
                    let tempObj={
                        name:stateObj.state,
                        value:stateObj.state,
                        id:stateObj.state_id
                    }
                    temp.push(tempObj)
                })
                //console.log(hashState);
                setStateList(temp)
                
            })
            .catch(err=>{
                console.log(err);
            })
    }
    
    //GET all course/degrees by qualification/education level
    const getDegreeListByQualification=(collegeID,qualificationID)=>{
            axios.get(EndPointPrefix+'/education/collegeeducation/?college_id='+collegeID+'&qualification_level='+qualificationID)
            .then(res=>{
                console.log('res from degree list by college id api: ',res)
                // res.data.forEach(courseData=>{
                //     console.log(courseData.degree)
                // })
                let temp=[];
                let uniqueDegrees=new Set();
                res.data.forEach(courseData=>{
                    if(uniqueDegrees.has(courseData.degree.id))
                    return;
                    else
                    uniqueDegrees.add(courseData.degree.id);
    
                    hashCourse[courseData.degree.name]={
                        d_id:courseData.degree.id,
                        isBranch:courseData.degree.is_specialized
                    }
                    let tempObj={
                        name:courseData.degree.name,
                        value:courseData.degree.name,
                        id:courseData.degree.id
                    }
                    temp.push(tempObj);
                })
    
                setCourseList(temp)
                
            })
            .catch(err=>{
                console.log(err);
            })
    }
    
     //GET all branches by college id, education lv and degree id
    const getBranchListByID=(collegeID,qualificationID,degreeID)=>{
            axios.get(EndPointPrefix+'/education/collegeeducation/?college_id='+collegeID+'&qualification_level='+qualificationID+'&degree_id='+degreeID)
                .then(res=>{
                    console.log('res from get branch list by id api: ',res.data)
                    let temp=[];
                    
                    res.data.forEach(branchData=>{
    
        
                        hashBranch[branchData.specialization.name]={
                            b_id:branchData.specialization.id
                            
                        }
                        let tempObj={
                            name:branchData.specialization.name,
                            value:branchData.specialization.name,
                            id:branchData.specialization.id
                        }
                        temp.push(tempObj);
                    })
        
                    setBranchList(temp)
                })
                .catch(err=>{
                    console.log(err);
                })
    }



    //EVENT HANDLERS

    const handleChange=(e)=>{
        //console.log(e.target.id)

        let val=e.target.value;
        switch(e.target.id){
            case 'CollegeName'         :    setCollege(val);            break;
            case 'Course'              :    setCourse(val);             break;
            case 'HighestQualification':    setQualification(val);      break;
            case 'Branch'              :    setBranch(val);             break;
            case 'Year'                :    setYear(val);               break; 
        }
        //console.log(studentDetails)
        //console.log('inside handle change');

    }

     const validateRefferalCode=async ()=>{
        let isValid=false;
        const response= await axios.post(EndPointPrefix+'/referrals/user_referrals/',{
            "type": 1,
            "referral_code": studentDetails.RefferalCode
        })

            console.log(response)
            response.data.success?alert.success(response.data.message):alert.error(response.data.message);
            return response
            
    }

    const setDetails=()=>{
        if(!showField.College){
            //College is selected from the list
            if(hashCollege[college] && hashCollege[college].onBoard){
                //College is on board
                if(!showField.Course && !showField.Branch){
                    setStudentDetails({...studentDetails,Case:1})
                    alert.show('CASE 1 education details')
                    setStep(step+1);
                }
                else{
                    alert.error('Please select all your fields from dropdown only!');
                }
            }
            else if(hashCollege[college] && !hashCollege[college].onBoard){
                //college is not on board
                if(!showField.Course && !showField.Branch){
                    //all fields selected from dropdown only
                    setStudentDetails({...studentDetails,Case:2})
                    alert.show('CASE 2 education details');
                    setStep(step+1);
                }
                else if(!showField.Course && showField.Branch){
                    //course from list branch not in list
                    setStudentDetails({...studentDetails,Case:3});
                    alert.show('CASE 3 education details');
                    setStep(step+1)
                }
                else{
                    //both course and branch not in list
                    setStudentDetails({...studentDetails,Case:4});
                    alert.show('CASE 4 education details');
                    setStep(step+1)
                }
            }
        }
        else{
            //College is not selected from the list
            if(!showField.Course && !showField.Branch){
                //all fields selected from dropdown only
                setStudentDetails({...studentDetails,Case:5})
                alert.show('CASE 5 education details');
                setStep(step+1);
            }
            else if(!showField.Course && showField.Branch){
                //course from list branch not in list
                setStudentDetails({...studentDetails,Case:6});
                alert.show('CASE 6 education details');
                setStep(step+1)
            }
            else{
                //both course and branch not in list
                setStudentDetails({...studentDetails,Case:7});
                alert.show('CASE 7 education details');
                setStep(step+1)
            }
        }
    }

    const handleSubmit=(e)=>{
        e.preventDefault();
        if(!studentDetails.RefferalCode){
            setDetails();
            return;
        }
        const res=validateRefferalCode();
        console.log(res);
        res.then((data)=>{
            console.log(data);
            if(!data.data.success) return;
            console.log('reached here');
            setDetails();
        })
        // console.log(valid)
        // if(!valid) return;
       
        //setStep(step+1);
      
    }




    // COMPONENT UI
    return(
        <>
                    <div className="row pl-3">
                    <h1 className="mb-4 login-heading text-left">Enter your Educational Details</h1>

                    </div>

                    <form onSubmit={handleSubmit} className="my-md-0 my-2"> 
                    <div className="form-group required text-left mb-3">
                           
                            {showField.College && (
                            <>
                            <label htmlFor="CollegeName" className="fw-500 control-label">Enter your College Name </label>
                            <input type="text" className="form-control mt-2 mb-1 shadow_1-lightest" id="CollegeName" aria-describedby="emailHelp" placeholder="" onChange={handleChange} required autoFocus/>
                            
                            {/* {showField.CollegeState && ( */}
                                <div className="form-group text-left mb-2 mt-3">
                                <label htmlFor="CollegeLocation" className="fw-500 control-label">Choose your college Location </label>
                                <div className="row">
                                    <div className="col-6">
                                    <TypeAndSearchState className="form-control shadow_1-lightest" />
                                    </div>
                                    
                                    <div className="col-6">
                                    <TypeAndSearchCity className="form-control shadow_1-lightest" />
                                    </div>
                                    


                                </div>
                               
                            </div>
                            </>
                            ) 
                        }
                        { !showField.College && (
                            <>
                            <label htmlFor="CollegeName" className="fw-500 control-label">Choose your College </label>
                            <TypeAndSearchCollege className="form-control shadow_1-lightest"/>
                            </>
                            )}

                        { 
                        <div className="form-check mb-3">
                        <input type="checkbox" checked={showField.college} onChange={()=>{setShowField({...showField,College:!showField.College})}} className="form-check-input my-2" id="college-checkbox" />
                        <label className="form-check-label" htmlFor="college-checkbox">
                        <small id="emailHelp" className="form-text my-1 fw-500">My college is not in the list</small>
                        </label>
                        </div> 
                        }


                    </div>
                    <div className="form-group required text-left mb-3">
                        {
                            college && showField.Qualification && (
                                <>
                                <label htmlFor="Qualification" className="fw-500 control-label">Mention your Highest Qualification</label>
                                <input type="text" className="form-control mt-2 mb-1 shadow_1-lightest" id="HighestQualification" aria-describedby="emailHelp" placeholder="" onChange={handleChange} required autoFocus/>

                                 </>
                            )
                        }
                        {
                            college && !showField.Qualification && (
                                <>
                                <label htmlFor="Qualification" className="fw-500 control-label">Select your Highest Qualification</label>
                                <QualificationDropdown className="form-control shadow_1-lightest"/>
   
                                </>

                            )
 
                        }

                    </div>
                    <div className="form-group required text-left mb-3">
                        {
                            qualification && showField.Course && (
                                <>
                                <label htmlFor="Course" className="fw-500 control-label">Mention your Course</label>
                                <input type="text" className="form-control mt-2 mb-1 shadow_1-lightest" id="Course" aria-describedby="emailHelp" placeholder="" onChange={handleChange} required autoFocus/>

                                </>
                            )
                        }
                        {
                            qualification && !showField.Course && (
                                <>
                                <label htmlFor="Course" className="fw-500 control-label">Choose your Course</label>
                                <TypeAndSearchCourse className="form-control shadow_1-lightest" />
                                </>
                            )
                        }
                        {
                             qualification && (
                                <> 
                                <div className="form-check mb-3">
                                <input type="checkbox" checked={showField.Course} onChange={()=>{setShowField({...showField,Course:!showField.Course})}} className="form-check-input my-2" id="course-checkbox" />
                                <label className="form-check-label" htmlFor="course-checkbox">
                                <small id="emailHelp" className="form-text my-1 fw-500">My Course is not in the list</small>
                                </label> 
                                </div>
                                </>
                            )
                        }


                    </div>
                    <div className="form-group required text-left mb-3">
                        {
                            course  && showField.Branch && (
                                <>
                                <label htmlFor="Branch" className="fw-500 control-label">Mention your Branch</label>
                                <input type="text" className="form-control mt-2 mb-1 shadow_1-lightest" id="Branch" aria-describedby="emailHelp" placeholder="" onChange={handleChange} required />
                                </>
                            )
                        }
                        {
                            course && !showField.Branch && (
                                <>
                                <label htmlFor="Branch" className="fw-500 control-label">Choose your Branch</label>
                                <TypeAndSearchBranch className="form-control shadow_1-lightest" />
                                </>
                            )
                        }
                        {
                             course && (
                                <>
                                <div className="form-check mb-3">
                                <input type="checkbox" checked={showField.Branch} onChange={()=>{setShowField({...showField,Branch:!showField.Branch})}} className="form-check-input my-2" id="branch-checkbox" />
                                <label className="form-check-label" htmlFor="branch-checkbox">
                                <small id="emailHelp" className="form-text my-1 fw-500">My Branch is not in the list</small>
                                </label> 
                                </div>
                                </>
                            )
                        }


                    </div>
                    <div className="form-group required text-left mb-3">
                        {
                             course && (
                                <>
                                <label htmlFor="Year" className="fw-500 control-label">Select your current year</label>
                                <YearDropdown className="form-control shadow_1-lightest" /> 
                                </>
                            )
                        }


                    </div>
                    <div className="form-group text-left mb-3">
                        {
                             year && (
                                <>
                                <label htmlFor="RefferalCode" className="fw-500">Refferal Code</label>
                                <input onChange={(e)=>{setStudentDetails({...studentDetails,RefferalCode:e.target.value})}} type="text" className="form-control mt-2 mb-1 shadow_1-lightest" id="RefferalCode" aria-describedby="emailHelp" placeholder=""/>
                                </>
                            )
                        }


                    </div>

                      <div className="row my-1 pt-md-2 pt-3">

                        <div className="col-6 text-left">
                            {
                                year && (
                                    <button className=" shadow_1 btn btn-lg btn-main btn-login btn-next text-uppercase font-weight-bold mb-2"  type="submit"><span>Next <i class="fas fa-angle-double-right"/></span></button>

                                )
                            }

                        </div>

                        </div>
                    </form>
     
        </>
    )
}

export default StudentEducationalDetails;