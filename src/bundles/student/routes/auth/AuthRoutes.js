import React from 'react';
import {BrowserRouter, Route, Router, Switch} from 'react-router-dom';
import StudentLogin from '../../components/login/Login';
import StudentSignup from '../../components/signup/Signup';
import StudentSignupDetails from '../../components/signup/SignupDetails';
import StudentIdentification from '../../components/signup/StudentIdentification';
import StudentDashboard from '../../../../StudentSection/Page/StudentDashboard/Index'
import Profile from '../../../../StudentSection/Page/Profile/Index'
import AppliedJob from '../../../../StudentSection/Page/AppliedJob/Index'
import HiddenJob from '../../../../StudentSection/Page/HiddenJob/Index'
import SavedJob from '../../../../StudentSection/Page/SavedJob/Index'
import OpenJob from '../../../../StudentSection/Page/OpenJob/Index'
import InterviewJob from '../../../../StudentSection/Page/InterviewJob/Index'
import StudentBlog from '../../../../StudentSection/Page/Blog/Index'
const AuthRoutes=()=>{
    return(
    <Route>
        <Route exact path="/student/login" component={StudentLogin}/>
        <Route exact path="/student/signup" component={StudentSignup}/>
        <Route exact path="/student/signup-details" component={StudentSignupDetails}/>
        <Route exact path="/student/identification" component={StudentIdentification}/>
        <Route exact path="/student/profile" component={Profile}/>
        <Route exact path="/student/applied" component={AppliedJob}/>
        <Route exact path="/student/hidden" component={HiddenJob}/>
        <Route exact path="/student/saved" component={SavedJob}/>
        <Route exact path="/student/open" component={OpenJob}/>
        <Route exact path="/student/interview" component={InterviewJob}/>
        <Route exact path='/' component={StudentDashboard}/>
        <Route exact path='/student/blog' component={StudentBlog}/>
    </Route>   
    )

}

export default AuthRoutes;