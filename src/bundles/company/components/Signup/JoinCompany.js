import React,{useEffect, useState} from 'react';
import {useAlert} from 'react-alert';
import SelectSearch from 'react-select-search'
import axios from 'axios';
import {useHistory} from 'react-router-dom';
import { EndPointPrefix } from '../../../../constants/constants';
import AdminApprove from '../UI/AdminApprove';
import Loader from '../../../common/components/UI/Loader';

const JoinCompany=({companyUserDetails,setCompanyUserDetails,step,setStep})=>{
  
  const alert=useAlert();
  const [loading,setLoading]=useState(false);
  const [isJoined,setIsJoined]=useState(false)
  
  const [tempCompList,setTempCompList]=useState([])
  const [companyID,setCompanyID]=useState(null);
  const [domainCompany,setDomainCompany]=useState('');
  const [domainCompanyID,setDomainCompanyID]=useState('');
  const [jobRole,setJobRole]=useState('')
  const [jobRolesList,setJobRolesList]=useState({})

  const TypeAndSearchCompany = () => (
    
    <SelectSearch
        options={tempCompList}
        search
        value={company}
        onChange={setCompany}
        placeholder="Type and Search your company from the list"
    />
 );

  const [companyList,setCompanyList]=useState([]);
  const [company,setCompany]=useState('')

  let jobRoles=Object.keys(jobRolesList).map(jobrole=>{return {'name':jobrole,'value':jobrole}})

  const JobRolesDropdown=()=>(
    <SelectSearch
  options={jobRoles}
  value={jobRole}
  onChange={setJobRole}
  placeholder="Select your Job Role"
/>
)
    useEffect(()=>{
      console.log(jobRolesList[jobRole])
    },[jobRole])

  useEffect(()=>{
    setLoading(true);
    // ?domain='+companyUserDetails.
    getJobRoles()
    axios.get(EndPointPrefix+'/company/company_list/?domain='+companyUserDetails.Email)
      .then(res=>{
        setLoading(false);
        let temp=[];
        console.log(res);
        //console.log(res.data.data)
        //console.log(res.data.data.all_companies);
        if(res.data.data.domain_company.length>0){
          //set domain company and return
          let domainCompanies=res.data.data.domain_company;
                domainCompanies.forEach(company=>{
                  setDomainCompany(company.company)
                  setCompany(company.company)
                  setDomainCompanyID(company.id)                  
          })
          // return;
        }
        let allCompanies=res.data.data.all_companies;
        allCompanies.forEach(company=>{
          let tempObj={
            [company.company]:company.id
          }
          temp.push(tempObj)
          
        })
        let domainCompanies=res.data.data.domain_company;
        domainCompanies.forEach(company=>{
          let tempObj={
            [company.company]:company.id
          }
          temp.push(tempObj)
          
        })
        setCompanyList(temp)
        
      })
      .catch(err=>{
        console.log(err);
      })

  },[])

  useEffect(()=>{
    if(companyList){
    let arr=companyList.map((comp)=>{for(let key in comp) return {name:key, value:key}})
    setTempCompList(arr);
    }
    
  },[companyList])

  const joinCompany=()=>{
   // if(domainCompany) setCompany(domainCompany)
    console.log('isnide join comp',companyList);
         let cid=null;
      let flag=false;
    companyList.forEach(comp=>{
      //console.log(comp)
 
      let c_name=Object.keys(comp)[0];
      let c_id=Object.values(comp)[0];
        console.log('isnide here: ',c_name,company)
        if(c_name==company){
          console.log('found id: ',c_id);
           setCompanyID(c_id)
           cid=c_id
           flag=true;
           return;
        }
       
      
  
      //if(comp==company) console.log(comp[company])
    })
    if(flag){
      console.log('calling method post');
       sendData(cid);
       //setIsJoined(true);
     }
    // console.log(companyList[company])
  }

  const getJobRoles=()=>{
 
    axios.get(EndPointPrefix+'/company/user_type/?main_user=company')
    .then(res=>{
        console.log(res.data.data);
        let temp={};
        res.data.data.forEach(data=>{
            temp[data.sub_user]=data.id;
            
        })
        setJobRolesList({...temp});
    })
    .catch(err=>{
        console.log(err);
    })

}

  const sendData=(compID)=>{
    setLoading(true);
    let arr=[...companyUserDetails.CandidatePreferences,...companyUserDetails.JobPreferences]
    axios.post(EndPointPrefix+'/company/company_user/',{
      "user": companyUserDetails.CompanyUserID,
      "company_id":compID,
      "job_title":companyUserDetails.JobTitle,
      "is_third_party": false,
      "preferences":{
      "job_segment":companyUserDetails.CandidatePreferences,
      "job_type":companyUserDetails.JobPreferences,
      },
      "is_coworking":false,
      "job_role":jobRolesList[jobRole],
      "college_id": companyUserDetails.CollegeID?companyUserDetails.CollegeID:null,
      "college_name": companyUserDetails.CollegeName?companyUserDetails.CollegeName:null,
      "graduation_year": companyUserDetails.GraduatingYear?companyUserDetails.GraduatingYear:null,
      "referral_code": companyUserDetails.RefferalCode?companyUserDetails.RefferalCode:null
      
      
    })
      .then(res=>{
        setLoading(false)
        console.log(res);
        if(res.data.success){
          alert.success(res.data.data.message)
          //setIsJoined(true);
          history.push({
            pathname:'/company/admin-approve',
            company
          })
        }
        else{
            alert.error(res.data.error)
            return;
        }
      })
      .catch(err=>{
        console.log(err);
        alert.error('Oops! There was some error, try again later');
      })

  }

  const history=useHistory();
    return loading ? <Loader/>
    :
    (
        <>

  
                <h1 className="mb-4 login-heading text-left">Find and Join your company </h1>

                  <div className="row pl-4 my-2 justify-content-center">
                    <span className="fs-16">Can't find yours? <strong onClick={()=>{setStep(step+1)}} className="link-text">Create it here</strong> </span>
                  </div>

                  
                  <div className="form-group text-left my-4">
                    <label  className="fw-500">Your Job Role</label>
                      <JobRolesDropdown className="form-control shadow_1-lightest"/>
                  </div>

                      {domainCompany?(
                    <>
                    <div class="card shadow_1">
                      <div class="card-body">
                        We found your company to be <b>{domainCompany}</b>
                        <button className="shadow_1 btn btn-lg btn-main btn-login btn-next" onClick={joinCompany}>Request to Join!</button>

                      </div>
                    </div>
                    </>
                  ):(
                    <>
                      <TypeAndSearchCompany></TypeAndSearchCompany>
                      <div className="row my-1 pt-3">
                          {/* <div className="col-6 text-left">
                            <button onClick={()=>{setStep(step-1)}} className=" shadow_1 btn btn-lg btn-main btn-login btn-next 
                            text-uppercase font-weight-bold mb-2" type="submit"><span>
                                 <i class="fas fa-angle-double-left"/> Back</span></button>
                          </div> */}
                            <div className="col-6 text-right">
                              <button className="shadow_1 btn btn-lg btn-main btn-login btn-next" onClick={joinCompany}>Join</button>
                            </div>
                        </div>
                    </>

                  ) 
                  }


              
        




              

        </>
    )
}

export default JoinCompany;