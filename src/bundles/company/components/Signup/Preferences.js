import React,{useState, useEffect, useRef} from 'react';
import axios from 'axios';
import {useAlert} from 'react-alert';
import ScrollableCheckboxList from '../../../common/components/UI/ScollableCheckboxList';
import {years,EndPointPrefix} from '../../../../constants/constants';
import EmployerGuidelines from '../UI/EmployerGuidelines';
import SelectSearch from 'react-select-search'
import CompanyUserDetails from './PersonalDetails';

const Preferences=({companyUserDetails,setCompanyUserDetails,step,setStep})=>{

    const disableButton=useRef();
    const alert=useAlert();
    const [showGuidelinesModal,setShowGuidelinesModal]=useState(false);

    //UI state variables
    const [showField,setShowField]=useState({
        College:false
    })
    const [jobTitles,setJobTitles]=useState([])
    const [jobTitle,setJobTitle]=useState(companyUserDetails.JobTitle);
    const [collegeList,setCollegeList]=useState({})
    const [candidatePreferences,setcandidatePreferences]=useState([])
    const [jobPreferences,setJobPreferences]=useState([])

    //state variables
    const [selectedCandidatePreferences,setSelectedCandidatePreferences]=useState([])
    const [selectedJobPreferences,setSelectedJobPreferences]=useState([])
    const [college,setCollege]=useState(CompanyUserDetails.CollegeName)
    const [year,setYear]=useState(companyUserDetails.GraduatingYear);
    
    let colleges=Object.keys(collegeList).map(clg=>{return {'name':clg, 'value':clg} })
    const TypeAndSearchCollege=()=>(
        <SelectSearch
        options={colleges}
        search
        value={college}
        onChange={setCollege}
        placeholder="Select your college"
    />
    )

    const YearDropdown= () =>(
        <SelectSearch
            value={year}
            options={years}
            placeholder="Select your grad year"
            onChange={setYear}
        />
    )

    const getCollegeList=()=>{
        axios.get(EndPointPrefix+'/education/college/')
        .then(res=>{
            console.log('res from college list api: ',res.data)
            
            let temp={};
            res.data.forEach(collegeData=>{
                temp[collegeData.name]=collegeData.id;
            })

            setCollegeList({...temp})
            
        })
        .catch(err=>{
            console.log(err);
        })
    }

    const getJobTitles=()=>{
        axios.get(EndPointPrefix+'/company/user_type/?main_user=company')
        .then(res=>{
            console.log(res.data.data);
            let temp=[];
            res.data.data.forEach(data=>{
                let tempObj={
                    id:data.id,
                    name:data.sub_user
                }
                temp.push(tempObj)
                setJobTitles(temp)
            })
        })
        .catch(err=>{
            console.log(err);
        })
    }

    const getCandidatePreferences=()=>{
        
        axios.get(EndPointPrefix+'/job_info/job_segment/')
            .then(res=>{
                console.log(res.data.data);
                let temp=[];
                res.data.data.forEach(data=>{
                    let tempObj={
                        id:data.id,
                        name:data.job_segment_name
                    }
                    temp.push(tempObj)
                    setcandidatePreferences(temp)
                })
            })
            .catch(err=>{
                console.log(err);
            })
    }   

    const getJobPreferences=()=>{
        axios.get(EndPointPrefix+'/job_info/job_type/')
        .then(res=>{
            console.log(res.data.data);
            let temp=[];
            res.data.data.forEach(data=>{
                let tempObj={
                    id:data.id,
                    name:data.job_type_name
                }
                temp.push(tempObj)
                setJobPreferences(temp)
            })
        })
        .catch(err=>{
            console.log(err);
        })
    }

    useEffect(()=>{
        getCollegeList();
        getJobTitles();
        getCandidatePreferences();
        getJobPreferences();

        disableButton.current.classList.add('disabled');

    },[])

    useEffect(()=>{
            if(jobTitle && selectedCandidatePreferences.length>0 && selectedJobPreferences.length>0)
                disableButton.current.classList.remove('disabled');
            else
                disableButton.current.classList.add('disabled');

    },[jobTitle,selectedCandidatePreferences,selectedJobPreferences])

    const ValidateRefferalCode=async ()=>{

        const response= await axios.post(EndPointPrefix+'/referrals/user_referrals/',{
            "type": 2,
            "referral_code": companyUserDetails.RefferalCode
        })

            console.log(response)
            response.data.success?alert.success(response.data.data.message):alert.error(response.data.error);
    return response
            
    }

    const setDetails=()=>{
        let cid=null,cname=null;
        if(showField.College){
            cid=null;
            cname=college;
        }
        else{
            cname=null;
            cid=collegeList[college]
        }

        setCompanyUserDetails({...companyUserDetails,
            JobTitle:jobTitle,
            CandidatePreferences:selectedCandidatePreferences,
            JobPreferences:selectedJobPreferences,
            CollegeID: showField.College?null:collegeList[college],
            CollegeName: showField.College?college:null,
            GraduatingYear:year
        })

    }

    const handleSubmit=(e)=>{
        e.preventDefault();
        if(!companyUserDetails.RefferalCode){
            setDetails();
            showEmployerGuidelines();
            return;
        }
        const validate=ValidateRefferalCode();
        validate.then((res)=>{
            if(!res.data.success) return;
           setDetails();
        showEmployerGuidelines();
        })

       

    }
    const showEmployerGuidelines=()=>{
        //setStep(step+1);
        setShowGuidelinesModal(true);       
    }
    return(
        <> 

        <div className="row pl-3">
                    <h1 className="mb-4 login-heading text-left">Tell us about yourself</h1>
                </div>

                    <form  onSubmit={handleSubmit} className="my-md-0 my-2"> 
                    <div className="form-group text-left">
                        <div className="row">
                            <div className="col-6">
                            <label htmlFor="almaMatter" className="fw-500">Your alma matter</label>
                            {showField.College ? (
                                <>
                                <input onChange={(e)=>{setCollege(e.target.value)}} type="text" className="form-control mt-2 mb-1 shadow_1-lightest" id="almaMatter" aria-describedby="emailHelp" placeholder=""  />
                                </> 
                            ) : (
                                <>
                                <TypeAndSearchCollege className="form-control shadow_1-lightest"/>
                                </>
                            )}
                            <div className="form-group text-left ml-4 mb-1">
                                <input type="checkbox" checked={showField.college} onChange={()=>{setShowField({...showField,College:!showField.College})}} className="form-check-input my-2" id="college-checkbox" />
                                <label className="form-check-label" htmlFor="college-checkbox">
                                <small id="emailHelp" className="form-text my-1 fw-500">My college is not in the list</small>
                                </label>
                            </div>

                            </div>
                            <div className="col-6">
                            <label htmlFor="gradYear" className="fw-500">Year of Graduation</label>
                                <input onChange={(e)=>{setYear(e.target.value)}} type="date" id="gradYear" className="form-control mt-2 mb-1 shadow_1-lightest" id="almaMatter" aria-describedby="emailHelp" value={year} placeholder="Year of graduation"  />

                            </div>
                        </div>
                    </div>
                    <div className="form-group required text-left">
                            <label htmlFor="JobTitle" className="fw-500 control-label">Your Job Title</label>
                            <input onChange={(e)=>{setJobTitle(e.target.value)}} type="text" className="form-control mt-2 mb-1 shadow_1-lightest" id="JobTitle" aria-describedby="emailHelp" placeholder="i.e. University Recruiter" value={jobTitle} required />
                    </div>

                    <div className="form-group required text-left">
                            <label htmlFor="Preferences" className="fw-500 control-label">Candidate Preferences</label>
                            <small id="emailHelp" className="form-text text-muted">Tell us the type of candidates you'd like us to find</small>
                            <ScrollableCheckboxList state={selectedCandidatePreferences} setState={setSelectedCandidatePreferences} listData={candidatePreferences}/>

                    </div>
                    <div className="form-group required text-left">
                            <label htmlFor="Email" className="fw-500 control-label">Job Preferences</label>
                             <small id="emailHelp" className="form-text text-muted">You'll be mostly hiring for </small>
                            <ScrollableCheckboxList state={selectedJobPreferences} setState={setSelectedJobPreferences} listData={jobPreferences}/>

                    </div>
                    
                    <div className="form-group text-left">
                    <label htmlFor="RefferalCode" className="fw-500">Refferal Code</label>
                                <input onChange={(e)=>{setCompanyUserDetails({...companyUserDetails,RefferalCode:e.target.value})}} type="text" className="form-control mt-2 mb-1 shadow_1-lightest" id="RefferalCode" aria-describedby="emailHelp" placeholder=""/>
                    </div>

                      <div className="row my-1 pt-3">
                          <div className="col-6 text-left">
                            <button ref={disableButton} className=" shadow_1 btn btn-lg btn-main btn-login btn-next 
                            text-uppercase font-weight-bold mb-2" type="submit"><span>
                                Next <i class="fas fa-angle-double-right"/></span></button>
                          </div>
                        </div>

                    </form>
                    {
                       showGuidelinesModal && (        
                     <EmployerGuidelines 
                     companyUserDetails={companyUserDetails}
                     setCompanyUserDetails={setCompanyUserDetails}
                     renderModal={showGuidelinesModal} 
                     setRenderModal={setShowGuidelinesModal} step={step} setStep={setStep}/>)
                    }
        </>
    )
}

export default Preferences;