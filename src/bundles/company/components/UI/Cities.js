import React,{useEffect, useState} from 'react';
import SelectSearch from 'react-select-search'
import axios from 'axios';
import {useAlert} from 'react-alert'
import { EndPointPrefix } from '../../../../constants/constants';



const Cities=({cityID,Id,setId})=>{
    const [cityList,setCityList]=useState({})
    const [city,setCity]=useState()
    const [defaultCity,setDefaultCity]=useState(cityID)
    const cities=Object.keys(cityList).map((city)=>{return {'name':city,'value':city}})
    const TypeAndSearchCity = () => (
        <SelectSearch
            options={cities}
            value={city}
            onChange={setCity}
            search
            placeholder={defaultCity ? defaultCity : 'Search City'}
            
        />
     );

    const getAllCities=()=>{
        axios.get(EndPointPrefix+'/location/city/')
            .then(res=>{
                // console.log(res.data)
                let temp={};
                res.data.forEach(city=>{
                    temp[city.city]=city.city_id;

                });
                setCityList({...temp})
            })
            .catch(err=>{
                console.log(err)
            })
        setCity(cityList[Id])
    }

    useEffect(()=>{

        getAllCities();
    },[])

    // useEffect(()=>{
       
    //         Object.entries(cityList).map(([key,value])=>{
    //             console.log(key,value)
    //         })

    // },[cityID])

    // useEffect(()=>{

    // },[cityList])

    useEffect(()=>{

        setId(cityList[city])
    },[city])

    return(
        <>
        <TypeAndSearchCity/>

        </>

    )
}

export default Cities;