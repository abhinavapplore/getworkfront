import React from 'react';

const EmaillHelper=()=>{
    return(
        <>
        <div className="row my-2 fs-12 text-left">
            <p><b>Didn’t receive the email?</b></p><br></br>
            <p>Is yinavem492@remail7.com your correct email without typos? If not, you can 
                <a href="">restart the sign up process</a></p>
            <p>Check your spam folder</p>
            <p>Add handshake@m.joinhandshake.com to your contacts</p>
            <p>Click here to resend the email</p>
            
            Still having trouble?
            Contact us
            </div>
        </>
    )
}

export default EmaillHelper;