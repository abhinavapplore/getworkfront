import React,{useState} from 'react';
import {companyCardsData} from '../../../../constants/constants';
import './CompanyCard.css';
import RequestModal from './RequestModal';
const companyImgStyles={
    height:'6rem',
    width: '6rem'

}


const CompanyCard=()=>{
  const [renderModal,setRenderModal]=useState(false);
  const [companyName,setCompanyName]=useState('');
  let cardData=[];
  companyCardsData.forEach((compCardData)=>{
    cardData.push(
          <div className="card card-flip shadow_1-lightest m-2" style={{height:'130px'}}>
        <div className="card-front text-dark ">
          <div className="card-body card-body-padding-small">
              <div className="row text-left">
                  <div className="col-4">
                  <img  src={compCardData.Logo} alt="company logo" style={companyImgStyles} />
                  </div>
                  <div className="col-8" style={{width:'225px'}}>
                  <i className="fa fa-arrow-circle-right fa-1x float-right" />
            <h3 className="card-title fs-16 fw-500">{compCardData.Name}</h3>
            <p className="fs-12">{compCardData.Indsutry}</p>

            <p className="fs-13">{compCardData.Address}</p>
                  </div>
              </div>

          </div>
        </div>
        <div className="card-back bg-white">
          <div className="card-body text-dark">
            <p className="card-text fs-12">
            Company description 
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Possimus non harum, 
            eaque assumenda ipsum blanditiis
            </p>
            <button onClick={()=>{
              setCompanyName(compCardData.Name)
              setRenderModal(true)
              }} className="btn btn-sm btn-outline-primary">Request to Join</button>
          </div>
        </div>
      </div>
    )
  })
    return(
        <> 
        {cardData}
        {renderModal &&  <RequestModal renderModal={renderModal} setRenderModal={setRenderModal} companyName={companyName}/>}
        </>
    )
}

export default CompanyCard;