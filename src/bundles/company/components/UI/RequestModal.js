
import React,{useState} from 'react';
import { Modal,ModalBody, ModalHeader, ModalFooter } from 'reactstrap';
import {useLocation, useHistory} from 'react-router-dom';


const RequestModal=({companyName,renderModal,setRenderModal})=>{

        const [showModal, setShowModal] = useState(renderModal);
      
        const toggle = () => {
            setShowModal(!showModal);
            setRenderModal(!renderModal);
        };
      
        return (
          <div>

            <Modal isOpen={showModal} toggle={toggle} size="lg" >

              <ModalBody>
                  <div className="row justify-content-center my-2">
                      <h5>Requested to Join {companyName} sent!</h5> 
                  </div>
                  <div className="row justify-content-center my-2">
                      <p className="fs-13">
                      Your request to join {companyName} as so and so designation has been sent to the Company Admin.
                      You will receive verification details shortly on example@example.com 
                      </p>

                      
                  </div>
              </ModalBody>


            </Modal>
          </div>
        );
      
   
}

export default RequestModal;