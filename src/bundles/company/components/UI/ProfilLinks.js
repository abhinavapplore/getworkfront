import React,{useState,useEffect} from 'react';
import {useAlert} from 'react-alert';
import SelectSearch from 'react-select-search';
import Axios from 'axios';
import { EndPointPrefix } from '../../../../constants/constants';

const ProfileLinks=({state,setState})=>{
    const [list,setList]=useState({})
    const [selectedHandle,setSelectedHandle]=useState('')
    useEffect(()=>{
        Axios.get(EndPointPrefix+'/shared/social_links/',{
            headers: {
                "Authorization":'Token '+localStorage.getItem('gw_token')
              }
        })
            .then(res=>{
                console.log(res.data.data);
                let temp={}
                res.data.data.forEach(link=>{
                    temp[link.name]=link.id;
                })
                setList(temp)
            })
            .catch(err=>{
                console.log(err);
            })
    },[])

    useEffect(()=>{
        if(selectedHandle){
            setState({...state,Name:selectedHandle})
        }
    },[selectedHandle])

    const TypeAndSearchSocialHandles=()=>(
        <SelectSearch 
            onChange={setSelectedHandle}
            value={selectedHandle}
            options={Object.keys(list).map((l)=>{return {'name': l , 'value': l}})}
            search
            placeholder='Search a handle'
        />
    )
    return(
        <>
          <TypeAndSearchSocialHandles/>  
        </>
    )
}

export default ProfileLinks;