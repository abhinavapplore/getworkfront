import React,{useEffect, useState} from 'react';
import SelectSearch from 'react-select-search'
import axios from 'axios';
import {useAlert} from 'react-alert'
import { EndPointPrefix } from '../../../../constants/constants';



const JobTypes=({defaultValue,state,setState})=>{
    const [jobtypeList,setJobtypeList]=useState({})
    const [jobtype,setJobtype]=useState(defaultValue)

    const JobTypes=Object.keys(jobtypeList).map((jobtype)=>{return {'name':jobtype,'value':jobtype}})
    const TypeAndSearchJobtype = () => (
        defaultValue==null ?
        <SelectSearch
            options={JobTypes}
            value={jobtype}
            onChange={setJobtype}
            placeholder="Select"
        />
        :
     <SelectSearch
            options={JobTypes}
            value={jobtype}
            onChange={setJobtype}
           
        />
     );

    const getAllJobTypes=()=>{
        axios.get(EndPointPrefix+'/job_info/job_type')
            .then(res=>{
                console.log(res)
                let temp={};
                res.data.data.forEach(jobtype=>{
                    temp[jobtype.job_type_name]=jobtype.id;
                });
                //console.log(temp)
                setJobtypeList({...temp})
            })
            .catch(err=>{
                console.log(err)
            })
        //setJobtype(jobtypeList[Id])
    }

    useEffect(()=>{
        console.log(defaultValue)
        getAllJobTypes();
    },[])

    useEffect(()=>{
        setState({job_type_id:jobtypeList[jobtype],job_type_name:jobtype})
        //console.log('changed')
    },[jobtype])

    return(
        <>
        <TypeAndSearchJobtype/>

        </>

    )
}

export default JobTypes;