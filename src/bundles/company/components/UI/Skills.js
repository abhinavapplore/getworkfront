import React,{useState,useEffect} from 'react';
import SelectSearch from 'react-select-search'
import axios from 'axios';
import {useAlert} from 'react-alert'
import { EndPointPrefix } from '../../../../constants/constants';

const Skills=({type, newSkill, setNewSkill})=>{
    const [skillList,setSkillList]=useState({})
    const [skill,setSkill]=useState()

    const Skills=Object.keys(skillList).map((skill)=>{return {'name':skill,'value':skill}})
    
    const TypeAndSearchskill = () => (

         <SelectSearch
            options={Skills}
            value={skill}
            onChange={setSkill}
            search
            placeholder="Search skill"
            
        />
     );

    const getAllSkills=()=>{
        axios.get(EndPointPrefix+'/education/skills')
            .then(res=>{
                console.log('response from all skills',res)
                //console.log(res.data.data.skills)
                let temp={};
                res.data.data.skills.forEach(skill=>{
                    if(type=="all")
                    temp[skill.skill_name]=skill.skill_id;
                    else if(skill.skill_type==type)
                    temp[skill.skill_name]=skill.skill_id;
                });
                //console.log(temp)
                setSkillList({...temp})
            })
            .catch(err=>{
                console.log(err)
            })
       
    }

    useEffect(()=>{
        getAllSkills();
        
    },[])

    useEffect(()=>{
        setNewSkill({
            skill_name: skill,
            skill_id: skillList[skill],
        })
    },[skill])


    return(
        <>
        <TypeAndSearchskill/>

        </>

    )
}

export default Skills;