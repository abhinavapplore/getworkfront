import React from 'react';
import {Link, useHistory, useLocation} from 'react-router-dom';
import {useAlert} from 'react-alert';
import SideImage from '../../../common/components/UI/SideImage';
import AuthHeader from '../../../common/components/UI/AuthHeader';

const EmailConfirmation=()=>{

    const alert=useAlert();
    const history=useHistory();
    const location=useLocation();
    console.log('here',location);
    return(
        <>
                <div className="container-fluid">
        <div className="row no-gutter full-page-view">

         <SideImage/>
          <div className="col-md-8 col-lg-8 y-scroll full-page-view">
            <div className="row">
   
               <AuthHeader/>
            
            </div>
            <div className="login d-flex align-items-center py-5 mx-md-0 mx-3 px-md-0 px-2">
              <div className="container">
                <div className="row">
                  <div className="col-md-9 col-lg-6 mx-auto">
                  <div className="row justify-content-center mx-auto my-2">
                <i class="fas fa-envelope-square fa-5x"></i>
            </div>
            <div className="row justify-content-center mx-auto my-2">
                <p className="fs-18 fw-500">
                Great! You've successfully signed up for GetWork.
                </p>
                <p className="fs-14">
                We've sent you a link to confirm your email address. Please use that link to confirm your email and then <Link to="/login">login </Link>again.

                </p>
            </div>
            <div className="row mt-4 fs-12 text-left helper-box">

                    <p><b>Didn’t receive the email?</b></p><br></br>
                    <ul>
                        <li><p>Is {location.email} your correct email without typos? If not, you can 
                        <Link to="signup"> restart the sign up process</Link></p></li>
                        <li><p>Check your spam folder</p></li>
                        <li><p>Add getwork@m.joingetwork.com to your contacts</p></li>
                        <li><p>Click here to resend the email</p></li>
                    </ul>

                <p><b>Still having trouble? </b></p><br/> &nbsp;
                <p><Link to=""> Contact us</Link></p>
                
              
            

            </div>
                 </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
            {/* <div className="row justify-content-center mx-auto my-2">
                <i class="fas fa-envelope-square fa-5x"></i>
            </div>
            <div className="row justify-content-center mx-auto my-2">
                <p className="fs-18 fw-500">
                Great! You've successfully signed up for GetWork.
                </p>
                <p className="fs-14">
                We've sent you a link to confirm your email address. Please check your inbox. It could take up to 10 minutes to show up in your inbox.

                </p>
            </div>
            <div className="row mt-4 fs-12 text-left helper-box">

                    <p><b>Didn’t receive the email?</b></p><br></br>
                    <ul>
                        <li><p>Is {location.email} your correct email without typos? If not, you can 
                        <a href=""> restart the sign up process</a></p></li>
                        <li><p>Check your spam folder</p></li>
                        <li><p>Add getwork@m.joingetwork.com to your contacts</p></li>
                        <li><p>Click here to resend the email</p></li>
                    </ul>

                <p><b>Still having trouble? </b></p><br/> &nbsp;
                <p><Link to="/company/register"> Contact us</Link></p>
                
              
            

            </div> */}
        </>
    )

}

export default EmailConfirmation;