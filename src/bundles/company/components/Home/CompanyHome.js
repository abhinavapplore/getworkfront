import React,{useState, useEffect, useRef} from 'react';
import CompanyRoutes from '../../../../routes/CompanyRoutes';
import Sidebar from '../../../common/components/UI/Sidebar';
import StudentNavbar from '../../../student/components/UI/StudentNavbar';
import Axios from 'axios';
import { EndPointPrefix } from '../../../../constants/constants';
import {useAlert} from 'react-alert';


const  CompanyHomeComponent =()=>{
    const alert=useAlert();
    const firstRender=useRef()
    const [loading,setLoading]=useState(true)

    const [userDetails,setUserDetails]=useState(
      localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user'))
      :
      {}
      )
    const [companyDetails,setCompanyDetails]=useState(
      localStorage.getItem('company') ? JSON.parse(localStorage.getItem('company'))
      :
      {}
    )

    const getUserDetails=()=>{
      let token=localStorage.getItem('gw_token');
      const userType=localStorage.getItem('user_type')
      Axios.get(EndPointPrefix+'/shared/user/?user_type='+userType,{
        headers:{
          "Authorization":'Token '+token
        }
      })
        .then(res=>{
          
          
          if(res.data.success){
            console.log(res);
            
            console.log("hey");
            const {email,first_name,last_name,id}=res.data.data;
          
            
            console.log(userDetails);
            localStorage.setItem('user',JSON.stringify(userDetails))
            localStorage.setItem('company',JSON.stringify(companyDetails))
           // alert(JSON.stringify(userDetails))
            setUserDetails({
              email,first_name,last_name,id
            })
            setCompanyDetails(res.data.data.company_user_details)
            setLoading(false)
            //localStorage.setItem('company',JSON.stringify())

            
            localStorage.setItem('user',JSON.stringify({email,first_name,last_name,id}))
            localStorage.setItem('company',JSON.stringify(res.data.data.company_user_details))
          }
          else{
            alert.error(res.data.error);
          }
        })
        .catch(err=>{
          console.log(err);
        })
    }

    useEffect(()=>{
      // if(firstRender.current){
        //setLoading(true)
        
        if(Object.values(userDetails).length==0 || Object.values(companyDetails).length==0)
        console.log("abhinav is a coder")
        getUserDetails()
      // }
    },[])

    // useEffect(()=>{
    //   setLoading(false)
    //   if(Object.values(userDetails).length==4 && Object.values(companyDetails).length==6){
    //     console.log(userDetails)
    //     console.log(companyDetails)
    //     localStorage.setItem('user',JSON.stringify(userDetails))
    //     localStorage.setItem('company',JSON.stringify(companyDetails))
    //     firstRender.current=false;
    //     setLoading(false)
    //      console.log(userDetails);
    //      console.log(companyDetails);
    //   }
    // },[userDetails,companyDetails])

   
    return(
     loading ?
     <>Loading...</>
     :
      <>
      <div className="row company-page">
        <div className="col-2 d-none d-md-block">
          <Sidebar userType={"COMPANY"}/>
        </div>
        <div className="col-10">
          <StudentNavbar name={userDetails.first_name+' '+userDetails.last_name} dp={userDetails.profile_picture}/>
              <CompanyRoutes data={companyDetails}/>      
        </div>
      </div>
      
      </> 
     
  
     
     
      )
  
  

}

export default CompanyHomeComponent;