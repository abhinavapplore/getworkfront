import React,{useEffect,useState, useRef} from 'react'
import SelectSearch from 'react-select-search'
import {environment as env} from '../../../../../../../environments/environment'
import Axios from 'axios';
import {useAlert} from 'react-alert'
import { Button } from '@material-ui/core';


export default function Basics({state,setState,setStep}) {

    const firstRender=useRef()

    const alert=useAlert();
    const [jobTypes,setJobTypes]=useState({})
    const [empTypes,setEmpTypes]=useState({})
    const [jobType,setJobType]=useState(
        jobTypes[state.Basics.job_type] ? jobTypes[state.Basics.job_type] : ''
    )
    const [empType,setEmpType]=useState()

    const getJobTypes=()=>{
        Axios.get(env.niyuktiBaseUrl+'type/')
            .then(res=>{
                // console.log(res)
                // console.log('job types: ',res.data.data)
                if(res.data.success){
                    let temp={}
                    res.data.data.forEach(job_type=>{
                        temp[job_type.job_type]=job_type.id
                    })
                    setJobTypes({...temp})
                }
                else
                alert.error(res.data.error)
            })
            .catch(err=>{
                console.log(err)
            })
    }

    const getEmployementTypes=()=>{
        Axios.get(env.niyuktiBaseUrl+'emp_type/')
        .then(res=>{
            // console.log(res)
            // console.log('emp types: ',res.data.data)
            if(res.data.success){
                let temp={}
                res.data.data.forEach(emp_type=>{
                    temp[emp_type.job_type]=emp_type.id
                })
                setEmpTypes({...temp})
            }
            else
            alert.error(res.data.error)
        })
        .catch(err=>{
            console.log(err)
        })

    }

    useEffect(()=>{
        //component did mount
        // if(state.Basics.job_type && jobTypes[state.Basics.job_type])
        // setJobType(jobTypes[state.Basics.job_type])
        getJobTypes();
        getEmployementTypes();
       
    },[])

    useEffect(()=>{
        if(jobType){
        //    console.log('changing')
           setState({...state,Basics:{...state.Basics,job_type:jobTypes[jobType]}})
        }
       
    },[jobType])

    useEffect(()=>{
        if(empType){
            console.log(empTypes[empType])
            setState({...state,Basics:{...state.Basics,employment_type:empTypes[empType]}})
            // if(empType==='Full Time'){
            //     setState({...state,Basics:{...state.Basics,job_duration_end:null}})
            // }
         }
         if(empType===1){
            setState({...state,Basics:{...state.Basics,job_duration_end:null}})
         }
    },[empType])
    // useEffect(()=>{
       
    //    if(empType==='Full Time'){
    //             setState({...state,Basics:{...state.Basics,job_duration_end:null}})
    //         }
    // },[empType])

    const options=[
        {name:'option 1',value:'option 1'}
    ]
    const jobtypes=Object.keys(jobTypes).map(job_type=>{return{'name':job_type,'value':job_type}})
    const emptypes=Object.keys(empTypes).map(emp_type=>{return{'name':emp_type,'value':emp_type}})

    const JobTypes= () =>( <SelectSearch options={jobtypes} value={jobType} onChange={setJobType} placeholder="Select" />  )
    const Employementypes= () =>( <SelectSearch options={emptypes} value={empType} onChange={setEmpType} placeholder="Select"/>  )
   const checkValidation=()=>{



    if(state.Basics.job_title!=='' && state.Basics.job_type!==null && state.Basics.employment_type!==null && state.Basics.job_duration_start!==null ){
        setStep(2)
    }else{
        alert.error('Please FIll all required fields.')
    }


    }
    return (
        <div>
        <div className="card-body">
                            <div className="row text-left">
                                <h3 className="pl-3 fs-24 card-title"><strong>1. Basics</strong></h3>
                            </div>
                            <hr className="mt-0 mb-0"/>
                            <div className="basics-form px-2 py-4">
                                <div className="row mx-auto">
                                    <div className="col-3 text-left"><p className="fs-18 gray-2 my-auto">Title *</p></div>
                                    <div className="col-9 text-left">
                                    <input required onChange={(e)=>{setState({...state,Basics:{...state.Basics,job_title:e.target.value}})}} 
                                    type="text" className="form-control input-secondary" value={state.Basics.job_title}/></div>
                                </div>
                                <div className="row mx-auto my-4">
                                    <div className="col-3 text-left my-auto"><p className="fs-18 gray-2 my-auto">Job Type *</p></div>
                                    <div className="col-4 text-left">
                                        <JobTypes/>

                                    </div>
                                </div>
                                <div className="row mx-auto my-4">
                                    <div className="col-3 text-left my-auto"></div>
                                    <div className="col-9 text-left">
                                    {
                                        jobType==='Internship' && (

                                        <div className="row">
                                            <div className="col-3 text-left"><p className="fs-18 gray-2">PPO</p></div>
                                            <div className="col-3 text-left">
                                                <div className="form-check form-check-inline">
                                                    <input className="form-check-input" checked={state.Basics.ppo==='true'} 
                                                    onChange={(e)=>{setState({...state,Basics:{...state.Basics,ppo:e.target.value}})}}
                                                    type="radio" name="inlineRadioOptions" id="inlineRadio1" defaultValue="true" />
                                                    <label className="form-check-label fs-18" htmlFor="inlineRadio1">Yes</label>
                                                </div>
                                            </div>
                                            <div>
                                            
                                            <div className="form-check form-check-inline">
                                                <input className="form-check-input" 
                                                onChange={(e)=>{setState({...state,Basics:{...state.Basics,ppo:e.target.value}})}}
                                                checked={state.Basics.ppo==='false'} type="radio" name="inlineRadioOptions" id="inlineRadio2" defaultValue="false" />
                                                <label className="form-check-label fs-18" htmlFor="inlineRadio2">No</label>
                                            </div>
                                            </div>

                                        </div>
                                        )
                                    }
                                    </div>
                                </div>
                                <div className="row mx-auto my-4">
                                    <div className="col-3 text-left my-auto"><p className="fs-18 gray-2 my-auto">Employement Type *</p></div>
                                    <div className="col-4 fs-18 text-left">
                                        <Employementypes/>

                                    </div>
                                </div>
                                <div className="row mx-auto mt-4">
                                    <div className="col-3 text-left my-auto"></div>
                                    <div className="col-9 text-left">
                                            <p className="fs-18 gray-2">Duration *</p>
                                    </div>
                                </div>
                                <div className="row mx-4">
                                    <div className="col-3 text-left my-auto"></div>
                                    <div className="col-4 text-left">
                                            <div className="row"><p className="fs-18 gray-3">Start Date</p></div>
                                            <div className="row">
                                               
                                                <input  onChange={(e)=>{setState({...state,Basics:{...state.Basics,job_duration_start:e.target.value}})}}
                                                type="date" value={state.Basics.job_duration_start} className="form-control input-small"/>
                                            </div>
                                    </div>
                                    {
                                        empType==='Part Time' && (
                                            <div className="col-4 text-left">
                                            <div className="row"><p className="fs-18 gray-3">End Date</p></div>
                                            <div className="row">
                                                <input onChange={(e)=>{setState({...state,Basics:{...state.Basics,job_duration_end:e.target.value}})}} 
                                                type="date" value={state.Basics.job_duration_end} className="form-control input-small"/>
                                               
                                            </div>
                                    </div>
                                        ) 
                                    }
                                    {/* <Button onclick={()=>{coneole.log('')}}>Back</Button> */}
                                  
                                </div>
                                <div className='row mx-auto mt-4'>
                                <div className='mt-2'>
                                        
                                        <button className='btn btn-main-lg' onClick={checkValidation}>Next</button>
                                        </div>
                                </div>
                            </div>
                        </div>
        </div>
    )
}
