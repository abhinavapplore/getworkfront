import React,{useState,useEffect} from 'react'
import { Editor } from 'react-draft-wysiwyg';
import SelectSearch from 'react-select-search';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { EditorState, convertToRaw,ContentState} from 'draft-js';
import Axios from 'axios';
import { environment as env } from '../../../../../../../environments/environment';
import {useAlert} from 'react-alert';
import draftToHtml from 'draftjs-to-html';
import { Button, RadioGroup, FormControlLabel, Radio } from '@material-ui/core';


export const Details = ({state,setState,setStep}) => {

    const alert=useAlert();
    const [editorState,setEditorState]=useState(EditorState.createWithContent(ContentState.createFromText(state.Details.job_description)))
    const [jobRoles,setJobRoles]=useState({})
    const [jobRole,setJobRole]=useState()
    const [salaryType,setSalaryType]=useState()
    const [cityList,setCityList]=useState({})
    const [city,setCity]=useState()

    const handleEditorStateChange=(editor_state)=>{
        setEditorState(editor_state)
    }

    const SalaryTypes=[
        {name:'PER ANNUM',value:'PER ANNUM'},
        {name:'PER MONTH',value:'PER MONTH'},
        {name:'PER HOUR',value:'PER HOUR'},
    ]

    const getJobRoles=()=>{
        Axios.get(env.niyuktiBaseUrl+'role/')
            .then(res=>{
                // console.log(res);
                if(res.data.success){
                    let temp={}
                    // console.log(res.data.data)
                   for(let i=0; i<res.data.data.length; i++){
                        //console.log(res.data.data[i])
                        temp[res.data.data[i].job_role_name]=res.data.data[i].id
                        //console.log(temp)
                    }
                    // console.log(temp)
                    setJobRoles({...temp})
                }
                else
                alert.error(res.data.error)
            })
            .catch(err=>{
                console.log(err)
            })
    }

    const getAllLocations=()=>{
        Axios.get(env.praveshBaseUrl+'/location/city/')
            .then(res=>{
                let temp={}
                // console.log('res from city api: ',res)
                // console.log(res.data)
                for(let i=0; i<res.data.length; i++){
                    temp[res.data[i].city]=res.data[i].city_id
                }
                setCityList({...temp})
            })
            .catch(err=>{
                console.log(err)
            })
    }

    useEffect(()=>{
        getJobRoles();
        getAllLocations();
    },[])

    const jobroles=Object.keys(jobRoles).map(jobrole=>{return{'name':jobrole,'value':jobrole}})
    const cities=Object.keys(cityList).map(City=>{return{'name':City,'value':City}})
    console.log(cities)
    const JobRoles= () =>( <SelectSearch options={jobroles} value={jobRole} onChange={setJobRole} placeholder="Select" />  )
    const SalaryType= () =>( <SelectSearch options={SalaryTypes} value={salaryType} onChange={setSalaryType} placeholder="Select" />  )
    const CityList= () =>( <SelectSearch disabled={state.Details.allow_remote} options={cities} value={city} onChange={setCity} placeholder="search city" search />)

    useEffect(()=>{
        if(jobRole){
            setState({...state,Details:{...state.Details,job_role:jobRoles[jobRole]}})
        }
    },[jobRole])

    useEffect(()=>{
        const description=draftToHtml(convertToRaw(editorState.getCurrentContent()))
        // console.log(description)
        if(description){
            setState({...state,Details:{...state.Details,job_description:description.toString()}})
        }
    },[editorState])

    useEffect(()=>{
        if(salaryType){
            setState({...state,Details:{...state.Details,salary_payment_type:salaryType.toUpperCase()}})
        }
    },[salaryType])

    useEffect(()=>{
        if(!state.Details.is_equity){
            setState({
                ...state,
                Details:{
                    ...state.Details,
                    equity_min:0,
                    equity_max:0
                }
            })
        }
    },[state.Details.is_equity])
    

    useEffect(()=>{
        if(state.Details.allow_remote){
            setState({...state,
                Details:{
                    ...state.Details,
                    job_location:[]
                }
            })
        }
    },[state.Details.allow_remote])

    const UpdateDocuments=(e)=>{
        // console.log(e.target.value)
        const document=e.target.value;
        if(state.Details.documents.includes(document)){
            let temp=state.Details.documents.filter((doc)=>doc!==document)
            setState({
                ...state,
                Details:{
                    ...state.Details,
                    documents: temp
                }
            })
        }
        else{
            setState({
                ...state,
                Details:{
                    ...state.Details,
                    documents: [...state.Details.documents,document]
                }
            })
        }
    }

    useEffect(()=>{
        if(city){
            if(!state.Details.job_location.includes(city)){
                const currentLocation={
                    city:city,
                    city_id:cityList[city]
                }
                setState({
                    ...state,
                    Details:{
                        ...state.Details,
                        job_location:[...state.Details.job_location,currentLocation]
                    }
                })
            }
        }
    },[city])

    const removeLocation=(location)=>{
        if(state.Details.job_location.length>0 && state.Details.job_location.includes(location)){
            let temp=state.Details.job_location.filter(loc=>loc.city_id!==location.city_id)
            setState({
                ...state,
                Details:{
                    ...state.Details,
                    job_location: temp
                }
            })
        }
    }

    const checkValidation=()=>{

        if(state.Details.job_description!==null && state.Details.job_role !==null && state.Details.vacancy!==null && state.Details.salary_type && state.Details.is_equity!==null && state.Details.documents.length>0 && state.Details.job_location.length>0 && state.Details.is_service_bond!==null){
           
          if(salaryRange()){
            alert.error('PleaseEnter right CTC package.')
          }else if(equityRange()){
            alert.error('Please Enter right equity range')
          }else{
              setStep(3)
          }
           
        }else{
            alert.error('Please FIll all required fields.')
        }
    }
const salaryRange=()=>{

    if(state.Details.salary_type==='PAID'){
        if(state.Details.ctc_max<state.Details.ctc_min){
          return true  // alert.error('PleaseEnter right CTC package.')
        }else{
            return false
        }
    }else{
        return false
    }

}

const equityRange=()=>{

    if(state.Details.is_equity===false){
        if(state.Details.equity_min>state.Details.equity_max){
           return true // alert.error('Please Enter right equity range')
        }else{
            return false
        }
}else{
    return false
}

}
    return (
                <div>
        <div className="card-body">
                            <div className="row text-left">
                                <h3 className="pl-3 fs-24 card-title"><strong>2. Details</strong></h3>
                            </div>

                            <hr className="mt-0 mb-0"/>

                            <div className="basics-form px-2 py-4">

                                {/* Description */}
                                <div className="row mx-auto">
                                    <div className="col-3 text-left"><p className="fs-18 gray-2 my-auto">Description *</p></div>
                                    <div className="col-8 text-left">
                                        <Editor
                                        value={state.Details.job_description}
                                            editorState={editorState}
                                            toolbarClassName=""
                                            wrapperClassName="editor-wrapper"
                                            editorClassName="editor"
                                            onEditorStateChange={handleEditorStateChange}
                                            onChange={(editorEvent)=>{setState({...state,Details:{...state.Details,job_description:editorEvent.blocks[0].text}})}}
                                            />
                                            {/* <Editor /> */}
                                            <p className="fs-18 gray-3 mt-1">
                                            You can copy - paste a description from your website, we'll retain all the formatting
                                            </p>
                                    </div>
                                </div>

                                {/* Job Role */}
                                <div className="row mx-auto my-4">
                                    <div className="col-3 text-left"><p className="fs-18 gray-2">Job Role(s) *</p></div>
                                    <div className="col-8 text-left">
                                        <JobRoles/>
                                   
                                    <p className="fs-18 gray-3 mt-1">
                                    Job roles are search facets for students who are looking for a certain type of work. Your selection(s) will help the students interested in these roles find your jobs. Learn more.
                                    </p>
                                    </div>
                                </div>

                                {/* No of applicants */}
                                <div className="row mx-auto my-4">
                                    <div className="col-3 text-left my-auto"><p className="fs-18 gray-2 my-auto">How many students do you expect to hire for this position? *</p></div>
                                    <div className="col-8 text-left">
                                        <input type="number" value={state.Details.vacancy} onChange={(e)=>{setState({...state,Details:{...state.Details,vacancy:e.target.value}})}} className="form-control input-secondary input-small"/>
                                        <p className="fs-18 gray-3 mt-1">This number can be approximate and will not be displayed to students</p>
                                    </div>
                                </div>

                                {/* Compensation */}
                                <div className="row mx-auto my-4">
                                    <div className="col-3 text-left"><p className="fs-18 gray-2">Salary Range *</p></div>
                                    <div className="col-8 text-left">
                                        <div className="row fs-18 mb-3">
                                            <div className="col-4 text-left">
                                                <div className="form-check form-check-inline">
                                                {/* <input className="form-check-input" type="radio" checked={state.Details.salary_type==="PAID"}
                                                onChange={(e)=>{setState({...state,Details:{...state.Details,salary_type:e.target.value}})}}
                                                 name="inlineRadioOptions" id="inlineRadio1" value="PAID" />
                                                <label className="form-check-label" htmlFor="inlineRadio1">Paid</label> */}
                                                <FormControlLabel 
                                        checked={state.Details.salary_type==="PAID"} 
                                                onChange={(e)=>{setState({...state,Details:{...state.Details,salary_type:e.target.value}})}}
                                        value="PAID" control={<Radio color="primary" />} label="PAID" />
                                            </div>
                                            </div>
                                            <div className="col-4 text-left">
                                                <div className="form-check form-check-inline">
                                                {/* <input className="form-check-input" type="radio" checked={state.Details.salary_type==="UNPAID"} 
                                                onChange={(e)=>{setState({...state,Details:{...state.Details,salary_type:e.target.value}})}}
                                                name="inlineRadioOptions" id="inlineRadio2" value="UNPAID" />
                                                <label className="form-check-label" htmlFor="inlineRadio2">Unpaid</label> */}
                                              
                                           <FormControlLabel 
                                       checked={state.Details.salary_type==="UNPAID"} 
                                                onChange={(e)=>{setState({...state,Details:{...state.Details,salary_type:e.target.value}})}}
                                        value="UNPAID" control={<Radio color="primary" />} label="UNPAID" />
                                            </div>
                                            </div>
                                        </div>
                                        {
                                            state.Details.salary_type==="PAID" && (<>
                                        <div className="row pl-3 mb-3" >
                                            <SalaryType/>
                                         
                                        </div>
                                                 <div className="row mb-3 pl-3">

                                                <input type="number" value={state.Details.ctc_min} onChange={(e)=>{setState({...state,Details:{...state.Details,ctc_min:e.target.value}})}} className="mr-2 form-control input-secondary input-small"/> -

                                                <input type="number" value={state.Details.ctc_max} onChange={(e)=>{setState({...state,Details:{...state.Details,ctc_max:e.target.value}})}} className="ml-2 form-control input-secondary input-small"/>

                                                <span className="my-2 ml-2">INR</span>

                                                </div>
                                           </> )
                                        }
                                       
                                    </div>
                                </div>

                                {/* Equity */}
                                <div className="row mx-auto my-4">
                                    <div className="col-3 text-left"><p className="fs-18 gray-2">Equity Range *</p></div>
                                    <div className="col-9 text-left">
                                        <div className="row pl-3 ">
                                            <input type="number" disabled={state.Details.is_equity} value={state.Details.equity_min} onChange={(e)=>{setState({...state,Details:{...state.Details,equity_min:e.target.value}})}} className="mr-2 form-control input-secondary input-small"/> -
                                            <input type="number" disabled={state.Details.is_equity} value={state.Details.equity_max} onChange={(e)=>{setState({...state,Details:{...state.Details,equity_max:e.target.value}})}} className="ml-2 form-control input-secondary input-small"/>
                                            
                                            <div className="form-check form-check-inline ml-2">
                                                <input className="form-check-input" type="checkbox" checked={state.Details.is_equity}  onChange={()=>{setState({...state,Details:{...state.Details,is_equity:!state.Details.is_equity}})}} id="inlineCheckbox1" defaultValue="option1" />
                                                <label className="form-check-label" htmlFor="inlineCheckbox1">No equity</label>
                                             </div>
                                            
                                        </div>
                                    </div>
                                </div>

                                {/* Documents */}
                                <div className="row mx-auto mt-4">
                                    <div className="col-3 text-left"><p className="fs-18 gray-2">Required Documents *</p></div>
                                    <div className="col-8 text-left fs-18">
                                    <div>
                                        <div className="form-check mb-2">
                                            <input className="form-check-input" checked={state.Details.documents.includes('Resume')} type="checkbox" 
                                            onChange={(e)=>{UpdateDocuments(e)}}
                                            defaultValue="Resume" id="defaultCheck1" />
                                            <label className="form-check-label" htmlFor="defaultCheck1">
                                            Resume
                                            </label>
                                        </div>
                                        <div className="form-check mb-2">
                                            <input className="form-check-input" checked={state.Details.documents.includes('Cover Letter')} 
                                            onChange={(e)=>{UpdateDocuments(e)}}
                                            type="checkbox" defaultValue="Cover Letter" id="defaultCheck2" />
                                            <label className="form-check-label" htmlFor="defaultCheck2">
                                           Cover Letter
                                            </label>
                                        </div>
                                        <div className="form-check mb-2">
                                            <input className="form-check-input" checked={state.Details.documents.includes('Transcript')} 
                                            onChange={(e)=>{UpdateDocuments(e)}}
                                            type="checkbox" defaultValue="Transcript" id="defaultCheck3" />
                                            <label className="form-check-label" htmlFor="defaultCheck3">
                                           Transcript
                                            </label>
                                        </div>
                                        <div className="form-check mb-2">
                                            <input className="form-check-input" checked={state.Details.documents.includes('Other')} 
                                            onChange={(e)=>{UpdateDocuments(e)}}
                                            type="checkbox" defaultValue="Other" id="defaultCheck4" />
                                            <label className="form-check-label" htmlFor="defaultCheck4">
                                            Other Document (e.g. work sample, course schedule, or other misc documents)
                                            </label>
                                        </div>
                                        </div>

                                    </div>
                                </div>

                                {/* Job Location */}
                                <div className="row mx-auto my-4">
                                    <div className="col-3 text-left my-auto"><p className="fs-18 gray-2 my-auto">Where are you hiring for this position? *</p></div>
                                    <div className="col-8 text-left">
                                        {/* <input type="text" className="form-control input-secondary input-small"/> */}
                                        <CityList/>
                                        <div className="form-check mt-1 fs-18">
                                        <div className="selected-locations text-left my-2">
                                            {
                                                state.Details.job_location.length>0 && state.Details.job_location.map(location=>{
                                                    return(
                                                        <span className="mx-2 px-3 py-2 badge badge-dark">{location.city}<i onClick={()=>{removeLocation(location)}} className="ml-3 cp fas fa-times"></i></span>
                                                    )
                                                })
                                            }
                                        </div>
                                        <input className="form-check-input" checked={state.Details.allow_remote} onChange={(e)=>{setState({...state,Details:{...state.Details,allow_remote:!state.Details.allow_remote}})}} type="checkbox" defaultValue id="defaultCheck1" />
                                        <label className="form-check-label" htmlFor="defaultCheck1">
                                            Allow remote applicants
                                        </label>
                                        </div>

                                    </div>
                                </div>

                                {/* Service Bond */}
                                <div className="row mx-auto my-4">
                                    <div className="col-3 text-left"><p className="fs-18 gray-2">Service Bond *</p></div>
                                    <div className="col-8 text-left">
                                        <div className="mb-2">
                                        <div className="form-check form-check-inline mr-5">
                                            {/* <input className="form-check-input" 
                                           checked={state.Details.is_service_bond=="true"}
                                            onChange={(e)=>{setState({...state,Details:{...state.Details,is_service_bond:e.target.value}})}}
                                             type="radio" name="inlineRadioOptions" id="inlineRadio3" value="true" />
                                            <label className="form-check-label" htmlFor="inlineRadio3">Yes</label> */}
                                        </div>
                                        <div className="form-check form-check-inline ml-5">
                                     

                                        <FormControlLabel 
                                        checked={state.Details.is_service_bond=="true"}
                                             onChange={(e)=>{setState({...state,Details:{...state.Details,is_service_bond:e.target.value}})}} type="radio"  name="inlineRadioOptions" 
                                           
                                        value="true" control={<Radio color="primary" />} label="Yes" />
                                           <FormControlLabel 
                                        checked={state.Details.is_service_bond=="false"}
                                             onChange={(e)=>{setState({...state,Details:{...state.Details,is_service_bond:e.target.value}})}} type="radio"  name="inlineRadioOptions" 
                                           
                                        value="false" control={<Radio color="primary" />} label="No" />
    
                                            {/* <input className="form-check-input" 
                                            checked={state.Details.is_service_bond=="false"}
                                             onChange={(e)=>{setState({...state,Details:{...state.Details,is_service_bond:e.target.value}})}} type="radio"  name="inlineRadioOptions" 
                                            id="inlineRadio4" value="false" />
                                            <label className="form-check-label" htmlFor="inlineRadio4">No</label> */}
                                        </div>
                                        </div>
                                        {
                                            state.Details.is_service_bond==='true' &&(
                                                <div className="row pl-3">
                                            <input type="number" value={state.Details.service_bond} onChange={(e)=>{setState({...state,Details:{...state.Details,service_bond:e.target.value}})}} className="form-control input-secondary input-small"/> <span className="ml-2 my-2">months</span>
                                        </div>
                                            )
                                        }
                                       

                                    </div>
                                </div>
                                {/* <Button onclick={()=>{coneole.log('')}}>Back</Button>
                                    <Button onclick={()=>{coneole.log('')}}>Next</Button> */}
                                    <div className='mt-2'>
                                    <button className='btn mr-2 btn-danger' onClick={()=>{setStep(1)}}>Back</button>
                                        
                                        <button className='btn btn-main-lg' onClick={checkValidation}>Next</button>
                                        </div>
                            </div>
                        </div>
        </div>
    )
}
