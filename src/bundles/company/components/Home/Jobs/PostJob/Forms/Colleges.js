import React,{useState,useEffect} from 'react'
import college_placeholder from '../../../../../../../assets/images/college_placeholder.png'
import dropdown_icon_blue from '../../../../../../../assets/images/icons/dropdown_icon_blue.png'
// import done_icon_white from '../../../../../../../assets/images/done_icon_blue.png'
import plus_icon_blue from '../../../../../../../assets/images/icons/plus_icon_blue.png'
import dropdown_icon from '../../../../../../../assets/images/icons/dropdown_icon.png'
import SelectSearch from 'react-select-search';
import Axios from 'axios';
import { environment as env } from '../../../../../../../environments/environment';
import { Button } from '@material-ui/core'
import { useAlert } from 'react-alert'

function Colleges({state,setState,setStep}){
    const alert=useAlert();

    const [collegeList,setCollegeList]=useState({})
    const [college,setCollege]=useState()
    const [collegeName,setCollegeName]=useState()
    const[mainDegreeSelected,setMainDegreeSelected]=useState(false)
    const[selectedCollegeData,setSelectedCollegeData]=useState({})
    const[selectedDegreeId,setSelectedDegreeId]=useState({})
    const getAllColleges=()=>{
        console.log({
            company_id:state.Basics.company_id,
            compnay_skills:state.Preferences.eligibility_criteria.skills,
            company_courses:state.Preferences.eligibility_criteria.courses,
        })
        const tempDegreeData=state.Preferences.eligibility_criteria.courses.map((item)=>({id:item.degree_id,name:item.degree_name}))
        const tempSpecData=state.Preferences.eligibility_criteria.courses.map((item)=>({id:item.specialization_id,name:item.specialization_name}))
     
        Axios.post(env.praveshBaseUrl+'/company/new_college_rank/',

            {
                company_id:state.Basics.company_id,
                company_skills:state.Preferences.eligibility_criteria.skills,
                company_courses:tempDegreeData,
                company_specialization:tempSpecData
                
            }

        )
            .then(res=>{
                if(res.data.success){
                    let temp={};
                    // console.log(res.data.data.results)
                    res.data.data.results.forEach(data=>{
                        let details=[];
                        
                        data.education.forEach(dets=>{
                            const {degree_id,degree_name,specialization}=dets;
                            const tempObj={
                                degree_id,degree_name,specialization
                            }
                            details.push(tempObj)

                        })
                        // console.log(details)
                        temp[data.college_name]={
                            college_name:data.college_name,
                            college_id:data.college_id,
                            college_logo:data.college_logo,
                            college_location:data.college_location,
                            education_details:data.education
                        }
                    })
                    console.log(temp)
                    setCollegeList({...temp})
                }
            })
            .catch(err=>{
                console.log(err)
            })
    }

    useEffect(()=>{
        if(!college)
        return;
        const currentCollege=collegeList[college];
        console.log(currentCollege)
        let allColleges=state.Colleges.college;
        let newCollege={
            college_id:currentCollege.college_id,
            college_name:currentCollege.college_name,
            preferences: currentCollege.education_details              
            
        }
        console.log('new clg: ',newCollege)
        let collegeExist=false;
        allColleges.forEach(clg=>{
            if(clg.college_name==college){
                //college already in the list
                collegeExist=true;
                return;
            }
        })
        if(collegeExist)   return;
        //college is not in the list
        allColleges.push(newCollege)
        setState({
            ...state,
            Colleges:{
                ...state.Colleges,
                college:allColleges
            }
        })
    },[college])

    const removeCollege=(remove_college)=>{
        console.log(remove_college)
        let allColleges=state.Colleges.college;
        let collegeExist=false;
        allColleges.forEach(clg=>{
            if(clg.college_name==college){
                //college exists in the list
                collegeExist=true;
                return;
            }
        })
        if(!collegeExist)   return;
        //college in the list,remove it
        allColleges=allColleges.filter(clg=>clg.college_id!==remove_college.college_id)
        setState({
            ...state,
            Colleges:{
                ...state.Colleges,
                college: allColleges
            }
        })
        setCollege()
    }

    useEffect(()=>{
        getAllColleges()
    },[])

    const mainSelectedSpec=(e,collegeData,selectedDegreeId,collegeName)=>{
        setCollegeName(collegeName)
        setSelectedCollegeData(collegeData)
        setSelectedDegreeId(selectedDegreeId)
        setMainDegreeSelected(!mainDegreeSelected)

       
        // console.log(event.target)
    }

    useEffect(()=>{

        if(mainDegreeSelected){

            // console.log(selectedCollegeData,selectedDegreeId)
            let tempData=selectedCollegeData
            let filterSpecData=selectedCollegeData.education_details.filter((item)=>item.degree_id ===selectedDegreeId)
            tempData={...tempData,education_details:[...filterSpecData]}
            
            let allColleges=state.Colleges.college;
            let newCollege={
                college_id:tempData.college_id,
                college_name:tempData.college_name,
                preferences: tempData.education_details              
                
            }
            console.log('new clg: ',newCollege)
            let collegeExist=false;
            allColleges.forEach(clg=>{
                if(clg.college_name==college){
                    //college already in the list
                    collegeExist=true;
                    return;
                }
            })
            if(collegeExist)   return;
            //college is not in the list
            allColleges.push(newCollege)
            setState({
                ...state,
                Colleges:{
                    ...state.Colleges,
                    college:allColleges
                }
            })
            // setState({
            //     ...state,
            //     Colleges:{
            //         ...state.Colleges,
            //         college: tempData
            //     }
            // })
            
        }else{
            removeCollege(selectedCollegeData)
        }

    },[mainDegreeSelected])
    const checkValidation=()=>{
        
        if(state.Colleges.hiring_type==='OFF CAMPUS'){
            if(state.Colleges.apply_start_date!==null && state.Colleges.apply_end_date!==null ){
                setStep(5)
            }else{
                alert.error('Please FIll all required fields.')
            }
        }else{

            if(state.Colleges.hiring_type!=='' && state.Colleges.college.length>0 && state.Colleges.apply_start_date!==null && state.Colleges.apply_end_date!==null ){
                setStep(5)
            }else{
                alert.error('Please FIll all required fields.')
            }
        }

    }
    //const [state,setState]=useState(1);
    const JobPostings= () =>( <SelectSearch value={college} onChange={setCollege} search options={Object.keys(collegeList).map((clg)=>{return{'name':clg,'value':clg}})} placeholder="Search your colleges to add job postings" />  )
  
    return (
                               <div>
            <div className="card-body">
                            <div className="row text-left">
                                <h3 className="pl-3 fs-24 card-title"><strong>4. Colleges</strong></h3>

                            </div>

                            <hr className="mt-0 mb-0"/>

                            <div className="basics-form px-0 py-4">

                                {/* Job Type */}
                                <div className="row mx-auto my-4">
                                    <div className="col-3 text-left"><p className="fs-18 gray-2">How do you want to hire? *</p></div>
                                    <div className="col-8 text-left fs-18 gray-2">
                                    <div>
                                        <div className="form-check form-check-inline">
                                            <input className="form-check-input" checked={state.Colleges.hiring_type==="ON CAMPUS"} onChange={(e)=>{setState({...state,Colleges:{...state.Colleges,hiring_type:"ON CAMPUS"}})}} type="radio" name="inlineRadioOptions" id="inlineRadio1" defaultValue="ON CAMPUS" />
                                            <label className="form-check-label" htmlFor="inlineRadio1">On campus</label>
                                        </div>
                                        <div className="form-check form-check-inline">
                                            <input className="form-check-input" checked={state.Colleges.hiring_type==="OFF CAMPUS"}  onChange={(e)=>{setState({...state,Colleges:{...state.Colleges,hiring_type:"OFF CAMPUS"}})}} type="radio" name="inlineRadioOptions" id="inlineRadio2" defaultValue="OFF CAMPUS" />
                                            <label className="form-check-label" htmlFor="inlineRadio2">Off campus</label>
                                        </div>
                                    </div>
                                 </div>
                                </div>

                                {/* Job Posting  */}
                                {
                                    state.Colleges.hiring_type=="ON CAMPUS" &&
                                    
                                <div className="row mx-auto my-4">
                                    <div className="col-3 text-left"><p className="fs-18 gray-2">Job Postings</p></div>
                                    <div className="col-8 text-left">
                                        {/* <JobPostings/> */}
                                        <div className="selected text-left my-2">
                                        {
                                            state.Colleges.college.length>0 && state.Colleges.college.map(clg=>{
                                                return(
                                                    <span key={clg.college_id} className="m-2 px-3 py-2 badge badge-dark">{clg.college_name}<i onClick={()=>{removeCollege(clg)}} className="ml-3 cp fas fa-times"></i></span>

                                                )
                                            })
                                        }
                                    </div>
                                        <button className="btn btn-sec-lg mr-3 mt-2 d-none">Add my colleges</button>
                                        <button className="btn btn-sec-lg mt-2 d-none">Add favourite colleges</button>
                                    </div>
                                </div>
                            }
                                {/* Apply Dates */}
                                <div className="row mx-auto mt-4 mb-2">
                                    <div className="col-3 text-left"><p className="fs-18 gray-2">Global Apply Start Date *</p></div>
                                    <div className="col-2 text-left">
                                        <input type="date" value={state.Colleges.apply_start_date} onChange={(e)=>{setState({...state,Colleges:{...state.Colleges,apply_start_date:e.target.value}})}} className="form-control input-secondary input-small"/>
                                    </div>
                                </div>
                                <div className="row mx-auto mb-2">
                                    <div className="col-3 text-left"><p className="fs-18 gray-2">Global Apply End Date *</p></div>
                                    <div className="col-2 text-left">
                                        <input type="date" value={state.Colleges.apply_end_date}  onChange={(e)=>{setState({...state,Colleges:{...state.Colleges,apply_end_date:e.target.value}})}} className="form-control input-secondary input-small"/>
                                    </div>
                                </div>
                                {
                                    state.Colleges.hiring_type=="ON CAMPUS" &&
                                    <>
                                    
                                    <div className="row mx-auto my-4">
                                    <div className="college-list-box">
                                        <div className="row justify-content-center fs-18  mt-4 mb-3 px-5">
                                            <p className="text-white"><strong>Choose the schools where this job should be posted </strong></p>
                                            <p  className="gray-4 mx-5">Each school can have its own apply start date or expiration date, or you can set global dates that apply to all postings of this job.</p>
                                        </div>
                                        <div className="row">
                                        {
                                        Object.keys(collegeList).map(college=>{
                                            let clg=collegeList[college]
                                           
                                          return(
                                            <div className="card college-item-box w-100  mx-5" key={clg.college_id}>
                                                            <div className="card-body p-2">
                                                                <div className="row">
                                                                    <div className="col-2">
                                                                        <img src={college_placeholder} className='img-fluid' alt=""/>
                                                                    </div>
                                                                    <div className="col-8 text-left">
                                                                        <div className="card-title fs-24 mb-0"><strong>{clg.college_name}</strong></div>
                                                                        <p className="fs-18 gray-2 mb-0">{clg.college_location}</p>
                                                                    </div>
                                                                    <div className="col-2 my-auto">
                                                                        <div className="icon-container ">
                                                                            <img className="cp ml-2" data-toggle="collapse"  data-target={"#collapse"+clg.college_id} src={dropdown_icon_blue} alt=""/>
                                                                        </div>
                                                                        <div className="icon-container">
                                                                            <img className="cp" src={plus_icon_blue} alt=""/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="collapse" id={"collapse"+clg.college_id}>
                                                                    <div className="card card-body" style={{boxShadow:'none'}}>
                                                                    <div className="row">
                                                                        <div className="col-2 text-left">
                                                                            <p className="fs-18"><strong>Courses</strong></p>
                                                                        </div>
                                                                        <div className="col-10 text-left">
                                                                        <div className="form-check fs-18">
                                                                            <input className="form-check-input" type="checkbox" name="exampleRadios" onClick={()=>setCollege(college)} id="exampleRadios1" defaultValue="option1" defaultChecked />
                                                                            <label className="form-check-label" htmlFor="exampleRadios1">
                                                                            Select all Courses
                                                                            </label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                        <div className="row">
                                                                            <div className="col"><hr/></div>
                                                                            <div className="col-auto">Or</div>
                                                                            <div className="col"><hr/></div>
                                                                        </div>

                                                                        {
                                                                            clg.education_details && clg.education_details.map((clg_edu,id)=>{
                                                                              
                                                                                return(
                                                                                        <div className="row">
                                                                                            <div className="card dropdown-card-wrapper mx-3">
                                                                                                <div className="card-body p-1">
                                                                                                    <div className="row">
                                                                                                        <div className="col-1 text-right">
                                                                                                            <div className="form-check">
                                                                                                                <input type="checkbox" className="form-check-input" onChange={(e)=>mainSelectedSpec(e,clg,clg_edu.degree_id,college)} id={clg_edu.degree_id} />
                                                                                                                
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div className="col-10 text-left">
                                                                                                            <label className="form-check-label" htmlFor={clg_edu.degree_id}>{clg_edu.degree_name}</label>
                                                                                                        </div>
                                                                                                        <div className="col-1">
                                                                                                            <img className="cp" data-toggle="collapse"  data-target={"#courses"+id} src={dropdown_icon} alt=""/>

                                                                                                        </div>
                                                                                                                    <div className="row pl-5">
                                                                                                            <div className="collapse ml-5 pl-5 my-2" id={"courses"+id}>
                                                                                                        {
                                                                                                            clg_edu.specialization.map((spec)=>(
                                                                                                                <>
                                                                                                                <div className='row pl-5'>

                                                                                                                <input type="checkbox" className="form-check-input" id={clg_edu.degree_name+spec.specialization_id}/>
                                                                                                                <label htmlFor={clg_edu.degree_name+spec.specialization_id} className="form-check-label ml-3">{spec.specialization_name}</label>
                                                                                                                
                                                                                                                </div>
                                                                                                           </> ))
                                                                                                        }
                                                                                                            </div>
                                                                                                        </div>
                                                                                                       

                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                )
                                                                            })
                                                                        }


                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                     )
                                              

                                                    
                                                })
                                      
                                    }
                                        </div>



                                        </div>
                                     </div>

                                    </>
                                }

                                     {/* {
                                         state.Colleges.hiring_type=="OFF CAMPUS" &&
                                         <>
                                         <div className="row">
                                            <div className="col-3 text-left"><p className="fs-18 gray-2">Courses</p></div>
                                            <div className="col-8 text-left">
                                                <div className="form-check fs-18">
                                                         <input className="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" defaultValue="option1" defaultChecked />
                                                              <label className="form-check-label" htmlFor="exampleRadios1">
                                                                   Select all Courses
                                                               </label>
                                                    </div>
                                                    <div className="row">
                                              <div className="col"><hr/></div>
                                                    <div className="col-auto">Or</div>
                                                           <div className="col"><hr/></div>
                                     </div>
                                     
                                         <div className="row">
                                                                <div className="card dropdown-card-wrapper mx-3">
                                                                    <div className="card-body p-1">
                                                                        <div className="row">
                                                                            <div className="col-1 text-right">
                                                                                <div className="form-check">
                                                                                    <input type="checkbox" className="form-check-input" id="exampleCheck1" />
                                                                                    
                                                                                </div>
                                                                            </div>
                                                                            <div className="col-10 text-left">
                                                                                <label className="form-check-label" htmlFor="exampleCheck1">B.tech</label>
                                                                            </div>
                                                                            <div className="col-1">
                                                                                <img className="cp" data-toggle="courses"  data-target="#courses" src={dropdown_icon} alt=""/>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                            </div>
                                        </div>


                                         </>
                                     } */}
                                     {/* <Button onclick={()=>{coneole.log('')}}>Back</Button>
                                    <Button onclick={()=>{coneole.log('')}}>Next</Button> */}


                            </div>
                            <div className='mt-2'>
                                    <button className='btn mr-2 btn-danger' onClick={()=>{setStep(3)}}>Back</button>
                                        
                                        <button className='btn btn-main-lg' onClick={checkValidation}>Next</button>
                                        </div>

                        </div>
        </div>
    )
}

export default Colleges;