import React,{useState,useEffect} from 'react'
import SelectSearch from 'react-select-search';
import Axios from 'axios';
import {environment as env} from '../../../../../../../environments/environment'
import { useAlert } from 'react-alert';

export const Preferences = ({state,setState,setStep}) => {
const alert=useAlert()
    const [educationLevel,setEducationLevel]=useState()
    const [graduationYear,setGraduationYear]=useState()
    const [degrees,setDegrees]=useState({})
    const [degree,setDegree]=useState()
    const [courses,setCourses]=useState({})
    const [course,setCourse]=useState()
    const [skills,setSkills]=useState({})
    const [skill,setSkill]=useState()
    const [workex,setWorkex]=useState()


    const graduation_years=[
        {name:'2020',value:'2020'},
        {name:'2021',value:'2021'},
        {name:'2022',value:'2022'}
    ]

    const education_levels={
        'Graduation': 3,
        'Post Graduation': 4,
        'Ph.D': 5
    }

    const work_experiences=[
        {name:'3',value:'3'},
        {name:'6',value:'6'},
        {name:'9',value:'9'},
    ]

    const options=[
        {name:'option 1',value:'option 1'}
    ]

    const GraduationYears= () =>( <SelectSearch value={graduationYear} onChange={setGraduationYear} options={graduation_years} placeholder="Select" />  )
    const EducationLevels= () =>( <SelectSearch options={education_levels} placeholder="Select" />  )
    const Degrees= () =>( <SelectSearch value={degree} onChange={setDegree} options={Object.keys(degrees).map((d)=>{return{'name':d,'value':d}})} placeholder="Select" />  )
    const Courses= () =>( <SelectSearch value={course} onChange={setCourse} options={Object.keys(courses).map(c=>{return{'name':c,'value':c}})} placeholder="Select" />  )
    const Skills= () =>( <SelectSearch value={skill} onChange={setSkill} options={Object.keys(skills).map(s=>{return{'name':s,'value':s}})} placeholder="Select" />  )
    const WorkEx= () =>( <SelectSearch value={workex} onChange={setWorkex} options={work_experiences} placeholder="Select" />  )

    useEffect(()=>{
        getAllSkills();
    },[])

    const getAllSkills=()=>{
        Axios.get(env.praveshBaseUrl+'/education/skills')
            .then(res=>{
                console.log('res from skill api: ',res);
                if(res.data.success){
                    let temp={}
                    res.data.data.skills.forEach(skill=>{
                        temp[skill.skill_name]=skill
                    
                    })
                    
                    setSkills(temp)
                }

            })
            .catch(err=>{
                console.log(err);
            })
    }

    const getDegrees=(url)=>{
        Axios.get(env.praveshBaseUrl+url)
            .then(res=>{
                console.log(res)
                if(res.data.success){
                    let temp={}
                    res.data.data.forEach((data)=>{
                        if(!data.user_created)
                        temp[data.name]=data
                    })
                    console.log(temp)
                    setDegrees(temp)
                }
                else{
                    console.log(res.data.error)
                    //throw an alert here
                }
            })
            .catch(err=>{
                console.log(err);
                //throw an alert here
            })
    }

    const getCourses=(courseUrl)=>{
        Axios.get(env.praveshBaseUrl+courseUrl)
            .then(res=>{
                console.log(res)
                if(res.data.success){
                    let temp={};
                    res.data.data.forEach(data=>{
                        if(!data.user_created)
                        temp[data.specialization_name]=data
                    })
                    setCourses(temp)
                }
                else{
                    console.log(res.data.error)
                    //throw an alert here
                }
            })
            .catch(err=>{
                console.log(err)
            })
    }

    const setDegreesURL=(edu_lvs)=>{
        console.log('inside get degree')
        let degreeURL='/education/get_degree?education_level=';
        for(let i=0; i<edu_lvs.length; i++){
            degreeURL+=JSON.stringify(education_levels[edu_lvs[i]])
            if(i<edu_lvs.length-1)
            degreeURL+=','
        }
        console.log(degreeURL)
        getDegrees(degreeURL)
    }

    const setCourseURL=(selectedDegrees)=>{
        let courseURL='/education/get_specialization?degree_id=';
        console.log(degrees,selectedDegrees)
        //return;
        for(let i=0; i<selectedDegrees.length; i++){
            courseURL+=JSON.stringify(degrees[selectedDegrees[i].name].id)
            if(i<selectedDegrees.length-1)
            courseURL+=','
        }
        console.log(courseURL)
        getCourses(courseURL)
    }

    useEffect(()=>{
        let edu_lvs=state.Preferences.eligibility_criteria.education_levels;
        console.log(edu_lvs)
        if(edu_lvs.length>0)
        setDegreesURL(edu_lvs)
    },[state.Preferences.eligibility_criteria.education_levels.length])

    useEffect(()=>{
        let selectedDegrees=state.Preferences.eligibility_criteria.degrees;
        console.log(selectedDegrees)
        if(selectedDegrees.length>0)
        setCourseURL(selectedDegrees)
    },[state.Preferences.eligibility_criteria.degrees.length])
    
    useEffect(()=>{
        if(workex){
            let selectedWorkex=state.Preferences.eligibility_criteria.experience;
            if(selectedWorkex.includes(workex))
            return;
            selectedWorkex.push(workex)
            setState({
                ...state,
                Preferences:{
                    ...state.Preferences,
                    eligibility_criteria: {
                        ...state.Preferences.eligibility_criteria,
                        experience: selectedWorkex
                    }
                }
            })
        }
    },[workex])

    useEffect(()=>{
        if(skill){
            console.log(skill)
            let tempRating={
                rating_name: "Expert", 
                rating_value: 5
            }
            let tempSkill={...skills[skill],...tempRating}
            let selectedSkills=state.Preferences.eligibility_criteria.skills;
            if(selectedSkills.map(selected_skill=>selected_skill.skill_name).includes(skill.skill_name))
            return;
            selectedSkills.push(tempSkill)
            // console.log(selectedSkills)
            setState({
                ...state,
                Preferences:{
                    ...state.Preferences,
                    eligibility_criteria:{
                        ...state.Preferences.eligibility_criteria,
                        skills:selectedSkills
                    }
                }
            })
        }
    },[skill])

    useEffect(()=>{
        if(course){
            let selectedCourses=state.Preferences.eligibility_criteria.courses;
            if(selectedCourses.map(selected_course=>selected_course.specialization_name).includes(course.specialization_name))
            return;
            console.log(course,courses)
            selectedCourses.push(courses[course])
            setState({
                ...state,
                Preferences:{
                    ...state.Preferences,
                    eligibility_criteria:{
                        ...state.Preferences.eligibility_criteria,
                        courses:selectedCourses
                    }
                }
            })
        }
    },[course])

    useEffect(()=>{

        if(degree){
            let selectedDegrees=state.Preferences.eligibility_criteria.degrees;
            const selectedDegreeNames=selectedDegrees.map(deg=>deg.degree_name)
            if(selectedDegreeNames.includes(degree))
            return;
            //add this degree to the list
            selectedDegrees.push(degrees[degree])
            //update state
            setState({
                ...state,
                Preferences:{
                    ...state.Preferences,
                    eligibility_criteria:{
                        ...state.Preferences.eligibility_criteria,
                        degrees:selectedDegrees
                    }
                }
            })
        }
    },[degree])

    useEffect(()=>{
        if(!graduationYear)
        return;
        if(state.Preferences.eligibility_criteria.graduation_years.includes(graduationYear))
        return;
        let temp=state.Preferences.eligibility_criteria.graduation_years;
        temp.push(graduationYear)
        setState({
            ...state,
            Preferences:{
                ...state.Preferences,
                eligibility_criteria:{
                    ...state.Preferences.eligibility_criteria,
                    graduation_years: temp
                }
            }
        })
    },[graduationYear])

    const removeExp=(exp)=>{
        console.log(exp)
        let selectedWorkex=state.Preferences.eligibility_criteria.experience;
        if(!selectedWorkex.includes(exp))
        return;
        selectedWorkex=selectedWorkex.filter(work_ex=>work_ex!==exp)
        setState({
            ...state,
            Preferences:{
                ...state.Preferences,
                eligibility_criteria:{
                    ...state.Preferences.eligibility_criteria,
                    experience:selectedWorkex
                }
            }
        })
        if(selectedWorkex.length==0)
        setWorkex()
    }

    const removeSkill=(Skill)=>{
        console.log('to remove',Skill)
        let selectedSkills=state.Preferences.eligibility_criteria.skills;
        if(!selectedSkills.map(selected_skill=>selected_skill.skill_name).includes(Skill.skill_name))
        return;
        //console.log(selectedSkills)
        selectedSkills=selectedSkills.filter(s=>s.skill_id!==Skill.skill_id)
        console.log(selectedSkills)
        setState({
            ...state,
            Preferences:{
                ...state.Preferences,
                eligibility_criteria:{
                    ...state.Preferences.eligibility_criteria,
                        skills:selectedSkills
                    
                }
            }
        })
        if(selectedSkills.length==0)
        setSkill()
    }

    const removeCourse=(Course)=>{
        let selectedCourses=state.Preferences.eligibility_criteria.courses;
        if(!selectedCourses.map(selected_course=>selected_course.specialization_name).includes(Course.specialization_name))
        return;
        selectedCourses=selectedCourses.filter(c=>c.specialization_id!==Course.specialization_id)
        setState({
            ...state,
            Preferences:{
                ...state.Preferences,
                eligibility_criteria:{
                    ...state.Preferences.eligibility_criteria,
                    courses:selectedCourses
                }
            }
        })
        if(selectedCourses.length==0)
        setCourse()
    }

    const removeGraduationYear=(year)=>{
        let temp=state.Preferences.eligibility_criteria.graduation_years;
        if(!temp.includes(year))
        return;
        temp=temp.filter((gradyear)=>gradyear!==year)
        setState({
            ...state,
            Preferences:{
                ...state.Preferences,
                eligibility_criteria:{
                    ...state.Preferences.eligibility_criteria,
                    graduation_years: temp
                }
            }
        })
        //if(temp.length==0) set
    }

    const handleEducationLevelChange=(e)=>{
        const educationlevel=e.target.value;
        let educationLevels=state.Preferences.eligibility_criteria.education_levels;
        if(educationLevels.includes(educationlevel)){
            ///remove it
            console.log('to remove')
            educationLevels=educationLevels.filter((edu_lv)=>edu_lv!==educationlevel)
            console.log(educationLevels)
        }else{
            //add it
            educationLevels.push(educationlevel)
        }
        setState({
            ...state,
            Preferences:{
                ...state.Preferences,
                eligibility_criteria:{
                    ...state.Preferences.eligibility_criteria,
                    education_levels: educationLevels
                }
            }
        })
    }

    const removeDegree=(deg)=>{
        let temp=state.Preferences.eligibility_criteria.degrees;
        //const degreeNames=temp.map(t=>t.name)
        if(temp.map(d=>d.name).includes(deg.name)){
            temp=temp.filter(d=>d.id!==deg.id)
            setState({
                ...state,
                Preferences:{
                    ...state.Preferences,
                    eligibility_criteria:{
                        ...state.Preferences.eligibility_criteria,
                        degrees: temp
                    }
                }
            })
            if(temp.length==0) setDegree()
        }
    }
    const checkValidation=()=>{

        if(state.Preferences.eligibility_criteria.graduation_years.length>0 && state.Preferences.eligibility_criteria.education_levels.length>0  && state.Preferences.eligibility_criteria.degrees.length>0 && state.Preferences.eligibility_criteria.skills.length>0 && state.Preferences.eligibility_criteria.experience.length>0 ){
            setStep(4)
        }else{
            alert.error('Please FIll all required fields.')
        }
    }


    return (
                        <div>
        <div className="card-body">
                            <div className="row text-left">
                                <h3 className="pl-3 fs-24 card-title"><strong>3. Preferences</strong></h3>
                            </div>

                            <hr className="mt-0 mb-0"/>

                            <div className="basics-form px-2 py-4">

                                {/* Eligibel Grad Years */}
                                <div className="row mx-auto my-4">
                                    <div className="col-3 text-left"><p className="fs-18 gray-2">Eligible Graduation Years *</p></div>
                                    <div className="col-5 text-left">
                                    <GraduationYears/>
                                    <div className="selected text-left my-2">
                                        {
                                            state.Preferences.eligibility_criteria.graduation_years.length>0 && state.Preferences.eligibility_criteria.graduation_years.map(year=>{
                                                return(
                                                    <span className="mx-2 px-3 py-2 badge badge-dark">{year}<i onClick={()=>{removeGraduationYear(year)}} className="ml-3 cp fas fa-times"></i></span>

                                                )
                                            })
                                        }
                                    </div>
                                    <p className="fs-18 gray-3 mt-1">
                                    Enter passout years eligible for this job. 
                                    </p>
                                    </div>
                                </div>

                                {/* Qualifications */}
                                <div className="row mx-auto my-4">
                                    <div className="col-3 text-left"><p className="fs-18 gray-2">Qualifications *</p></div>
                                    <div className="col-5 text-left">
                                        {
                                            Object.keys(education_levels).map(edu_lv=>{
                                                return(
                                                    <div className="form-check">
                                                        <input type="checkbox" defaultValue={edu_lv} value={edu_lv} className="form-check-input" id={edu_lv} 
                                                        checked={state.Preferences.eligibility_criteria.education_levels.includes(edu_lv)}
                                                        onChange={handleEducationLevelChange}
                                                        />
                                                        <label className="form-check-label" htmlFor={edu_lv}>{edu_lv}</label>
                                                    </div>
                                                )
                                            })
                                        }

                                    </div>
                                </div>

                                {/* Degrees */}
                                <div className="row mx-auto my-4">
                                    <div className="col-3 text-left"><p className="fs-18 gray-2">Degree *</p></div>
                                    <div className="col-5 text-left">
                                        <Degrees/>
                                    <div className="selected text-left my-2">
                                        {
                                            state.Preferences.eligibility_criteria.degrees.length>0 && state.Preferences.eligibility_criteria.degrees.map(deg=>{
                                                return(
                                                    <span key={deg.degree_id} className="m-2 px-3 py-2 badge badge-dark">{deg.name}<i onClick={()=>{removeDegree(deg)}} className="ml-3 cp fas fa-times"></i></span>

                                                )
                                            })
                                        }
                                    </div>
                                    </div>
                                </div>

                                {/* Courses */}
                                {
                                   Object.keys(courses).length>0 && 
                                    <div className="row mx-auto my-4">
                                        <div className="col-3 text-left"><p className="fs-18 gray-2">Courses *</p></div>
                                        <div className="col-8 text-left">
                                            <Courses/>
                                            <div className="selected text-left my-2">
                                            {
                                                state.Preferences.eligibility_criteria.courses.length>0 && state.Preferences.eligibility_criteria.courses.map(spec=>{
                                                    return(
                                                        <span key={spec.specialization_id} className="m-2 px-3 py-2 badge badge-dark">{spec.specialization_name}<i onClick={()=>{removeCourse(spec)}} className="ml-3 cp fas fa-times"></i></span>

                                                    )
                                                })
                                            }
                                        </div>
                                        </div>
                                    </div>

                                }


                                {/* Minimum percentage*/}
                                <div className="row mx-auto my-4">
                                    <div className="col-3 text-left"><p className="fs-18 gray-2">Minimum percentage (optional)</p></div>
                                    <div className="col-5 text-left">
                                        <input 
                                        onChange={(e)=>{setState({...state,Preferences:{...state.Preferences,eligibility_criteria:{...state.Preferences.eligibility_criteria,percentage:e.target.value}}})}} type="number" className="form-control input-secondary input-small"/>
                                    </div>
                                </div>
                        
                                {/* Skills */}
                                <div className="row mx-auto my-4">
                                    <div className="col-3 text-left"><p className="fs-18 gray-2">Choose Skills *</p></div>
                                    <div className="col-8 text-left">
                                    <Skills/>
                                    <div className="selected text-left my-2">
                                        {
                                            state.Preferences.eligibility_criteria.skills.length>0 && state.Preferences.eligibility_criteria.skills.map(curr_skill=>{
                                                return(
                                                    <span key={curr_skill.skill_id} className="m-2 px-3 py-2 badge badge-dark">{curr_skill.skill_name}<i onClick={()=>{removeSkill(curr_skill)}} className="ml-3 cp fas fa-times"></i></span>

                                                )
                                            })
                                        }
                                    </div>
                                    <p className="fs-18 gray-3 mt-1">
                                    These consolidate individual courses across every school on Getwork.
                                    </p>
                                    </div>
                                </div>


                                {/* Work Experience */}
                                <div className="row mx-auto my-4">
                                    <div className="col-3 text-left"><p className="fs-18 gray-2">Work Experience *</p></div>
                                    <div className="col-8 text-left">
                                        <WorkEx/>
                                        <div className="selected text-left my-2">
                                        {
                                            state.Preferences.eligibility_criteria.experience.length>0 && state.Preferences.eligibility_criteria.experience.map(exp=>{
                                                return(
                                                    <span className="m-2 px-3 py-2 badge badge-dark">{exp} months<i onClick={()=>{removeExp(exp)}} className="ml-3 cp fas fa-times"></i></span>

                                                )
                                            })
                                        }
                                    </div>
                                    </div>
                                </div>

                                <div className='mt-2'>
                                    <button className='btn mr-2 btn-danger' onClick={()=>{setStep(2)}}>Back</button>
                                        
                                        <button className='btn btn-main-lg' onClick={checkValidation}>Next</button>
                                        </div>


                            </div>
                        </div>
        </div>
    )
}
