import React from 'react'
import done_icon from '../../../../../../assets/images/icons/done-icon.png';
import Axios from 'axios';
import { environment as env } from '../../../../../../environments/environment';
import { useHistory } from 'react-router-dom';

const doneStyles={
    background: '#DEFFEC',
    fontWeight: 'bold',
    fontSize: '18px',
    lineHeight: '22px',
    color: '#219653'
}

const currentStyles={
    background:'#C1D9ED',
    fontWeight: 'bold',
    fontSize: '18px',
    lineHeight: '22px',
    color: '#3282C4'
}

const doneIconStyles={
    float: 'right',
    paddingTop: '5px',
    paddingRight: '10px'
}

export const NavPanel = ({step,setStep,state}) => {
    const history=useHistory()
    const CreateJob=(status)=>{
        console.log(state)
        const data={
            ...state.Basics,
            ...state.Details,
            ...state.Preferences,
            ...state.Colleges,
            job_status:status
        }
        console.log(data)
        Axios.post(env.niyuktiBaseUrl+'company/job_post/',{
            ...data
        })
            .then(res=>{
                alert(res.data.data.message)
                history.push('/company/dashboard')
                console.log(res)
            })
            .catch(err=>{
                console.log(err)
            })

    }

    return (
        <div>
            <div className="card gw-card w-100" style={{height:'100vh',position: 'absolute',left: '-1%'}}>
                        <div className="card-body mx-auto my-5">
                            <div className="post-job-nav mt-5 pt-3">
                                <div className="row my-3"><button style={step==1 ? currentStyles: doneStyles} className="btn btn-nav text-left" onClick={()=>{setStep(1)}}><span className="ml-3 fs-18 ">1. Basics {step>1 && <img style={doneIconStyles} src={done_icon} alt=""/>}</span></button></div>
                                <div className="row my-3"><button style={step==2 ? currentStyles : step>2 ? doneStyles : {}} className="btn btn-nav text-left" onClick={()=>{setStep(2)}}><span className="ml-3 fs-18 ">2. Details {step>2 && <img style={doneIconStyles} src={done_icon} alt=""/>}</span></button></div>
                                <div className="row my-3"><button style={step==3 ? currentStyles : step>3 ? doneStyles : {}}  className="btn btn-nav text-left" onClick={()=>{setStep(3)}}><span className="ml-3 fs-18 ">3. Preferences {step>3 && <img style={doneIconStyles} src={done_icon} alt=""/>}</span></button></div>
                                <div className="row my-3"><button style={step==4 ? currentStyles : step>4 ? doneStyles : {}}  className="btn btn-nav text-left" onClick={()=>{setStep(4)}}><span className="ml-3 fs-18 ">4. Colleges {step>4 && <img style={doneIconStyles} src={done_icon} alt=""/>}</span></button></div>
                                <div className="row my-3"><button style={step==5 ? currentStyles : step>5 ? doneStyles : {}}  className="btn btn-nav text-left" onClick={()=>{setStep(5)}}><span className="ml-3 fs-18 ">5. Preview {step>5 && <img style={doneIconStyles} src={done_icon} alt=""/>}</span></button></div>
                                
                            </div>
                            <div className="post-job-buttons my-5">
                                <div className="row my-3"><button className="btn btn-main-lg" onClick={()=>{CreateJob("OPEN")}}>Post Job</button></div>
                                <div className="row my-3"><button className="btn btn-sec-lg" onClick={()=>{CreateJob("DRAFT")}}>Save as draft</button></div>
                            </div>
                        </div>
                    </div>
        </div>
    )
}
