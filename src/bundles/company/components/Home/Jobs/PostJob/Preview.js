import React,{useEffect,useState} from 'react'
import Axios from 'axios';
import { environment as env } from '../../../../../../environments/environment';

export const Preview = ({state,setStep}) => {
    const companyID=JSON.parse(localStorage.getItem('company')).company
    const [companyDetails,setCompanyDetails]=useState()

    const getCompanyDetails=()=>{
        Axios.get(env.praveshBaseUrl+`/company/details/?company_id=${companyID}`)
            .then(res=>{
                console.log(res)
                if(res.data.success){
                    setCompanyDetails(res.data.data)
                }
                else{
                    console.log(res.data.error)
                }
            })
            .catch(err=>{
                console.log(err)
            })
    }

    useEffect(()=>{
        getCompanyDetails()
    },[])

    return (
        <div className="px-5 py-5">
         <div>
           <button className='btn mr-2 btn-danger' onClick={()=>{setStep(4)}}>Back</button>
            
                         </div>
            <div className="row mt-4 mb-2 text-left">
                <h1 className="fs-24"><strong>
                    {state.Basics.job_title}
                </strong></h1>
            </div>
            <div className="row mt-4 mb-2 text-left">
                {
                    state.Preferences.skills && state.Preferences.skills.length>0 && 
                    state.Preferences.skills.map(skill=>{
                        return(
                            <div className="applicant-skill-badge">
                                <span>{skill}</span>
                            </div>
                        )
                    })
                  
                }
            </div>
            <div className="row mt-4 mb-2 text-left">
                <div className="col-4 text-left">
                    {
                        state.Details.ctc_min && state.Details.ctc_max && 
                        <>
                        <p className="fs-20 mb-1"><strong>Compensation</strong></p>
                        <p className="fs-16 gray-2">
                            <span>{state.Details.ctc_min && state.Details.ctc_min } -</span>
                            <span>{state.Details.ctc_max && state.Details.ctc_max }</span>                       
                        </p>
                        </>
                    }

                </div>
                <div className="col-4 text-left">
                    {
                        state.Details.equity_min && state.Details.equity_max ? 
                       ( <>
                        <p className="fs-20 mb-1"><strong>Equity Range</strong></p>
                        <p className="fs-16 gray-2">
                            <span>{state.Details.equity_min && state.Details.ctc_min } -</span>
                            <span>{state.Details.equity_max && state.Details.ctc_max}</span>                       
                        </p>
                        </>):null
                    }

                </div>
            </div>
            <hr/>
            <div className="row mt-4 mb-2 text-left">
                {
                    state.Details.job_location.length>0 && 
                    <>
                    <div className="col-4 text-left">
                        <p className="fs-20 mb-1"><strong>Office Location</strong></p>
                        <p className="fs-16 gray-2">
                            <span>{state.Details.job_location.length>0 && state.Details.job_location.map(location=>location.city) } </span>      
                        </p>
                    </div>
                    </>
                }
                {
                    state.Details.job_role && 
                    <>
                    <div className="col-4 text-left">
                    <p className="fs-20 mb-1"><strong>Job Type</strong></p>
                        <p className="fs-16 gray-2">
                            <span>{state.Details.job_role=== 1 ? (<div>Full Time</div>) :(<div>Part  Time</div>) }</span>                       
                        </p>
                    </div>
                    </>
                }
                {
                    state.Details && 
                    <>
                    <div className="col-4 text-left">
                    <p className="fs-20 mb-1"><strong>Service Bond</strong></p>
                        <p className="fs-16 gray-2">
                            <span>{state.Details.is_service_bond==='false' ? 'N0':`${state.Details.service_bond} Month` } </span>                       
                        </p>
                    </div>
                    </>
                }

            </div>
            <div className="row mt-4 mb-2 text-left flex-col">
                {
                    state.Details.job_description && 
                    <>
                    <p className="fs-20 mb-1"><strong>Job Description</strong></p>
                    <br/>
                    <p className="mb-0">{state.Details.job_description}</p>
                    </>
                }

            </div>
            {
                state.Preferences.eligibility_criteria.experience.length>0 ||
                state.Preferences.eligibility_criteria.graduation_years.length>0 ||
                state.Preferences.eligibility_criteria.degrees.length>0 ||
                state.Preferences.eligibility_criteria.courses.length>0 ||
                state.Preferences.eligibility_criteria.percentage
                &&
                <>
                <div className="mt-4 mb-2 text-left">
                    <p className="fs-20 mb-1"><strong>Preferences</strong></p>
                    <ul className="fs-16 gray-2 m-0 py-2 px-4">
                        <li>Work Experience: {state.Preferences.eligibility_criteria.experience.map(exp=><span className="mr-2">{exp}</span>)}</li>
                        <li>Eligible Graduation Years: {state.Preferences.eligibility_criteria.graduation_years.map(year=><span className="mr-2">{year}</span>)}</li>
                        <li>Degree: {state.Preferences.eligibility_criteria.degrees.map(degree=><span className="mr-2">{degree}</span>)}</li>
                        <li>Course: {state.Preferences.eligibility_criteria.courses.map(course=><span className="mr-2">{course}</span>)}</li>
                        <li>Percentage: <span>{state.Preferences.eligibility_criteria.percentage}</span></li>
                    </ul>
                </div>
                </>
            }
            {
                companyDetails &&
                <>
                <div className="mt-4 mb-2 text-left">
                    <p className="fs-20 mb-1"><strong>About {companyDetails.company_name}</strong></p>
                    <div className="applicant-skill-badge my-3">
                        <span>{companyDetails.industry}</span>
                    </div>
                    <p className="fs-16 gray-2">
                        {companyDetails.company_description}
                    </p>
                    <p>
                        <span><strong>Company Size: </strong></span>
                        <span className="gray-2">{companyDetails.size}</span>
                    </p>
                    <p>
                        <span><strong>Check us out @ </strong></span>
                        <span className="cp" style={{color:' #0645AD'}}>{companyDetails.website}</span>
                    </p>
                </div>
                </>
            }
            {
                state.Details.documents && state.Details.documents.length>0 &&
                <>
                <div className="mt-4 mb-2 text-left">
                    <p className="fs-20 mb-1"><strong>Documents required</strong></p>
                    <ul className="fs-16 gray-2 m-0 py-2 px-4">
                        {
                            state.Details.documents && state.Details.documents.map(doc=>{
                                return <li><span>{doc}</span></li>
                            })
                        }
                    </ul>
                </div>
                </>
            }

        </div>
    )
}
