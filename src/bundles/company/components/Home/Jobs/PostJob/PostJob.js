import React,{useState,useEffect,useRef} from 'react';
import { NavPanel } from './NavPanel';
import Basics from './Forms/Basics';
import { Details } from './Forms/Details';
import { Preferences } from './Forms/Preferences';
import Colleges from './Forms/Colleges';
import { Preview } from './Preview';

const NavPanelWrapperStyles={
    // position: 'fixed',
    // // left: '200px',
    width: '295px'
}

const PostJob=()=>{
    const [step,setStep]=useState(1);

    const [formData,setFormData]=useState({
        Basics:{
            user_id:JSON.parse(localStorage.getItem('user')).id,
            company_id:JSON.parse(localStorage.getItem('company')).company,
            job_title:'',
            job_type:null,
            ppo:true,
            employment_type:null,
            job_duration_start:null,
            job_duration_end:null,
            rounds: [
                {
                    "round_id": 1,
                    "round_no": 2
                },
                {
                    "round_id": 2,
                    "round_no": 1
                }
            ]
        },
        Details:{
            job_description:'',
            job_role:null,
            vacancy:null,
            salary_type:'',
            salary_payment_type:'',
            ctc_min:null,
            ctc_max:null,
            is_equity:false,
            equity_min:null,
            equity_max:null,
            documents:[],
            job_location:[],
            allow_remote:false,
            is_service_bond:false,
            service_bond:0
        },
        Preferences:{
            eligibility_criteria : {
                graduation_years:[],
                education_levels:[],
                college_preferences:[],
                degrees:[],
                courses:[],
                skills:[],
                experience:[],
                percentage:null,

            },
            backlog:false
        },
        Colleges:{
            hiring_type:'',
            college:[],
            apply_start_date:null,
            apply_end_date:null
        }
    })

    return(
        <>
       
            <div className="row">
                    <div className="col-md-3 col-12" style={NavPanelWrapperStyles}>
                            <NavPanel step={step} setStep={setStep} state={formData}/>
                </div>
                {/* div className="col-1"></<div> */}
                <div className="col-md-8 col-12 my-5">
                    <div className="card section-card mt-5 ml-5">
                        
                           { step==1 && <Basics state={formData} setState={setFormData} setStep={setStep}/> }
                           { step==2 && <Details state={formData} setState={setFormData} setStep={setStep}/> }
                           { step==3 && <Preferences state={formData} setState={setFormData} setStep={setStep}/> }
                           { step==4 && <Colleges state={formData} setState={setFormData} setStep={setStep}/> }
                           { step==5 && <Preview state={formData} setState={setFormData} setStep={setStep}/> }
                          
                                                    
                    </div>
                </div>
            </div>

        </>
    )
}

export default PostJob;