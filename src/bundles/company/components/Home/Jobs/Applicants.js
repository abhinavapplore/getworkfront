/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable */
import React,{useEffect,useState, useRef} from 'react';
import ApplicantsPane from '../../../../common/components/Panes/ApplicantsPane';
import {useAlert} from 'react-alert';
import behanceLogo from '../../../../../assets/images/behance-logo.png'
import dribbleLogo from '../../../../../assets/images/dribble-logo.png'
import close_icon from '../../../../../assets/images/icons/close-icon.png'
import Axios from 'axios';
import Skeleton from 'react-loading-skeleton';

const bg={
    background: '#F2F2F2'
}

const mainHRStyles={
    width: '100vw',
    marginLeft: '-30px'
}

const Applicants=()=>{

    const forceUpdate = React.useReducer(bool => !bool)[1];

    let companyID=JSON.parse(localStorage.getItem('company')).company

    const alert=useAlert();

    const [changeStatus,setChangeStatus]=useState(false)
    const [loadCurrentApplicant,setLoadCurrentApplicant]=useState(false)

    //initial data
    const [allJobsData,setAllJobsData]=useState([])
    const [allApplicantsData,setAllApplicantsData]=useState([])


    //jobs data at one component instance
    const [filteredJobsData,setFilteredJobsData]=useState([])

    //pane variables
    const [jobRoles,setJobRoles]=useState([])
    const [selectedJobRole,setSelectedJobRole]=useState('All')
    const [status,setStatus]=useState('All')
    const [sortBy,setSortBy]=useState('All')

    //current selected job
    const [currentJob,setCurrentJob]=useState({})

    //all applicants data
    const [jobApplicants,setJobApplicants]=useState([])

    //applicants data at one component instance
    const [allFilteredApplicantsData,setAllFilteredApplicantsData]=useState([])
    const [filteredJobApplicants,setFilteredJobApplicants]=useState([])

    //viewed applicant
    const [currentApplicant,setCurrentApplicant]=useState({})

    //right panel variables for storing all filter values
    const [jobSkills,setJobSkills]=useState([])
    const [jobDegrees,setJobDegrees]=useState([])
    const [jobColleges,setJobColleges]=useState([])
    const [jobLocations,setJobLocations]=useState([])
    const [jobWorkEx,setJobWorkEx]=useState([])
    const [jobPassoutYear,setJobPassoutYear]=useState([])

    //
    const [currentJobSkill,setCurrentJobSkill]=useState('All')
    const [currentJobDegree,setCurrentJobDegree]=useState('All')
    const [currentJobCollege,setCurrentJobCollege]=useState('All')
    const [currentJobLocation,setCurrentJobLocation]=useState('All')
    const [currentWorkEx,setCurrentWorkEx]=useState('All')
    const [currentPassoutYear,setCurrentPassoutYear]=useState('All')

    //selected filter values
    const [selectedJobSkills,setSelectedJobSkills]=useState([])
    const [selectedJobDegrees,setSelectedJobDegrees]=useState([])
    const [selectedJobColleges,setSelectedJobColleges]=useState([])
    const [selectedJobLocations,setSelectedJobLocations]=useState([])
    const [selectedJobWorkEx,setSelectedJobWorkEx]=useState([])
    const [selectedPassoutYear,setSelectedPassoutYear]=useState([])

    const [newURL, setNewURL] = useState("");
  const [newEndPoint, setNewEndPoint] = useState("");
  const [end, setEnd] = useState(false);

  const GetData = (baseURL, endpoint) => {
    Axios.get(baseURL+endpoint)
    .then(res=>{
        console.log(res.data.data.results.length)
        if (res.data.data.next === null) {
            setEnd(true);
          } else {
            setNewURL(res.data.data.next.slice(0, 20));
            setNewEndPoint(res.data.data.next.slice(20));
          }
        setAllJobsData(allJobsData.concat([...res.data.data.results]))
        setFilteredJobsData(filteredJobsData.concat([...res.data.data.results]))
        let temp=['All'];
        res.data.data.results.forEach(job=>{
            temp.push(job.job_title)
        })
        setJobRoles(temp);
        setSelectedJobRole(temp[0])
        const allJobs=allJobsData.concat([...res.data.data.results]);
        const allApplicants=[]
        allJobs.forEach(job=>allApplicants.push(...job.applicants))
        setAllApplicantsData(allApplicants)
        setAllFilteredApplicantsData(allApplicants)
        console.log(allJobsData.length)
    })
  }

    const getApplicantsData=()=>{
        Axios.get(`http://54.162.60.38/job/company/applicants/?company_id=${btoa(companyID.toString())}&state=dHJ1ZQ==`)
            .then(res=>{
                console.log(res);
                console.log(res.data.data.results.length)
                if(res.data.success){
                    if (res.data.data.next === null) {
                        setEnd(true);
                      } else {
                        setNewURL(res.data.data.next.slice(0, 20));
                        setNewEndPoint(res.data.data.next.slice(20));
                      }
                    setAllJobsData(res.data.data.results)
                    setFilteredJobsData(res.data.data.results)
                    setCurrentJob(res.data.data.results[0])
                    setJobApplicants(res.data.data.results[0].applicants)
                    let temp=['All'];
                    res.data.data.results.forEach(job=>{
                        temp.push(job.job_title)
                    })
                    setJobRoles(temp);
                    setSelectedJobRole(temp[0])
                    const allJobs=res.data.data.results;
                    const allApplicants=[]
                    allJobs.forEach(job=>allApplicants.push(...job.applicants))
                    setAllApplicantsData(allApplicants)
                    setAllFilteredApplicantsData(allApplicants)
                }
                else{

                }
            })
            .catch(err=>{
                console.log(err);
            })
    }

    useEffect(()=>{
        getApplicantsData()
    },[])

    const getJobApplicant=()=>{
        console.log(currentApplicant)
    }

    useEffect(()=>{
        if(currentApplicant){
            getJobApplicant()
        }
    },[currentApplicant, getJobApplicant])

    useEffect(()=>{
        if(currentJob && currentJob.applicants && currentJob.applicants.length>0){
            setJobApplicants(currentJob.applicants)
            setFilteredJobApplicants(currentJob.applicants)
            getApplicantDetails(currentJob.applicants[0].job_id,currentJob.applicants[0].user_id)
            setJobSkills(['All',...currentJob.filter.skills])
            setJobColleges(['All',...currentJob.filter.colleges])
            setJobDegrees(['All',...currentJob.filter.degree])
            setJobLocations(['All',...currentJob.filter.current_city])
            setJobWorkEx(['All',...currentJob.filter.work_experience])
            setJobPassoutYear(['All',...currentJob.filter.end_date])
        }   
    },[currentJob])

    
    useEffect(()=>{
        setCurrentJob(filteredJobsData[0])
        let temp=filteredJobsData.map(job=>{
            return job.applicants.map(applicant=>{ return applicant})
        })
        setJobApplicants(...temp)
        setFilteredJobApplicants(temp)
    },[filteredJobsData])

    //filter helper function
    useEffect(()=>{

        FilterData();

    },[selectedJobRole, status, selectedJobColleges, selectedJobDegrees, selectedJobLocations, selectedJobSkills, selectedPassoutYear])

    const FilterData=()=>{
        if(selectedJobRole!=='All')
        setCurrentJob(...allJobsData.filter(jobs=>jobs.job_title==selectedJobRole))
        // else
        // setCurrentJob()
        //decide filter cases
        let temp=allApplicantsData.filter((applicant)=>{
            let flag=true;
            let Case=0;
            if(selectedJobRole!=='All' && selectedJobRole!==applicant.job_title){
                flag=false;
            }
            if(status!=='All' && applicant.status_name!==status){
                flag=false
            }
            if(selectedJobWorkEx.length>0 && !selectedJobWorkEx.includes(applicant.work_ex)){
                console.log('#3')
                flag=false;
                console.log(flag)
            }
            if(selectedJobColleges.length>0 && !selectedJobColleges.includes(applicant.education[0].college_name)){
                flag=false;
            }
            if(selectedJobDegrees.length>0 && !selectedJobDegrees.includes(applicant.education[0].degree)){
                flag=false;
            }
            if(selectedJobLocations.length>0 && !selectedJobLocations.includes(applicant.current_city)){
                flag=false;
            }
            if(selectedJobSkills.length>0){  
                if(applicant.skill.length==0)
                flag=false;
                else if(applicant.skill.length>0 && !applicant.skill.some(val=>selectedJobSkills.includes(val.skill_name)))
                flag=false;
            }   
            if(selectedPassoutYear.length>0 && !selectedPassoutYear.includes(applicant.education[0].end_date.split('-')[0])){
                flag=false;
            }
            return flag;
        })
        setAllFilteredApplicantsData(temp)
    }


    useEffect(()=>{
        console.log(filteredJobsData)
        if(filteredJobsData.length>0)
        setFilteredJobApplicants(filteredJobsData[0].applicants)
    },[])
  
    const sortByScore=(arr)=>{
        arr.sort((a,b)=>(b.score-a.score));
        setAllFilteredApplicantsData(arr);
    }

    
    const sortByApplyDate=(arr)=>{
        console.log('fired sort by date,',arr)
        arr.sort((a,b)=>{

            let bDate=Date.parse(b.create_time)
            let aDate=Date.parse(a.create_time)
            console.log('comparing: ',aDate,bDate)
            console.log(bDate-aDate)
            return bDate-aDate

        })
        console.log('end sort by date, ',arr)
        setAllFilteredApplicantsData(arr);
    }

    const sortByColleges=(arr)=>{
        console.log('brefor sort',arr)
        arr.sort((a,b)=>{
            if(a.education[0].college_name< b.education[0].college_name) { return -1; }
            if(a.education[0].college_name > b.education[0].college_name) { return 1; }
            return 0;
        })
        console.log('after sort',arr)

        setAllFilteredApplicantsData(arr)
    }

    useEffect(()=>{
        if(sortBy==='All')
        return;

        let arr=allFilteredApplicantsData;
        
        //let arr=filteredJobApplicants;
        console.log(sortBy)
        if(sortBy==='Apply Date')
        sortByApplyDate(arr);
        else if(sortBy==='Best Match')
        sortByScore(arr);
        else if(sortBy==='Colleges')
        sortByColleges(arr);

        forceUpdate()
        
    },[allFilteredApplicantsData, forceUpdate, sortBy])

    useEffect(()=>{
        if(allFilteredApplicantsData && allFilteredApplicantsData.length==0){
            setCurrentApplicant();
            return;
        }
        console.log(allFilteredApplicantsData)
        if(filteredJobApplicants && filteredJobApplicants.length>0){
        setCurrentApplicant(allFilteredApplicantsData[0])
        getApplicantDetails(allFilteredApplicantsData[0].job_id,allFilteredApplicantsData[0].user_id)
        }
    },[])

    const getApplicantDetails=(jobID,userID)=>{
        setLoadCurrentApplicant(true)
        Axios.get(`http://54.162.60.38/job/company/applicant_detail/?user_id=${btoa(userID.toString())}&job_id=${btoa(jobID.toString())}`)
                .then(res=>{
                    setLoadCurrentApplicant(false)
                    console.log(res);
                    if(res.data.data.success){
                        setCurrentApplicant(res.data.data.results[0])
                    }
                })
                .catch(err=>{
                    console.log(err)
                })
        
    }

    const ShortlistApplicant=(applicant_id,round)=>{
        
        setChangeStatus(true)
        Axios.post('http://54.162.60.38/job/company/status_update/',{
            "student_data": [
                {
                    "id":applicant_id,
                    "round": round,
                    "status": 2
                }
            ]
        })
            .then(res=>{
                console.log(res);
                if(res.data.success){
                    setChangeStatus(false)
                    setCurrentApplicant({...currentApplicant,status:2,status_name:'Shortlisted'})
                    alert.success(res.data.data.message)
                }else{
                    alert.error(res.data.error)
                }
            })
            .catch(err=>{
                console.log(err);
            })
    }

    const RejectApplicant=(applicant_id,round)=>{
        setChangeStatus(true)
        Axios.post('http://54.162.60.38/job/company/status_update/',{
            "student_data": [
                {
                    "id":applicant_id,
                    "round": round,
                    "status": 3
                }
            ]
        })
            .then(res=>{
                console.log(res);
                if(res.data.success){
                    setChangeStatus(false)
                    setCurrentApplicant({...currentApplicant,status:3,status_name:'Rejected'})
                    alert.success(res.data.data.message)
                }else{
                    alert.error(res.data.error)
                }
            })
            .catch(err=>{
                console.log(err);
            })        
    }

    const handleScroll = (event) => {
        let e = event.nativeEvent;
        if (
          e.target.scrollTop + 10 >=
          e.target.scrollHeight - e.target.clientHeight
        ) {
          if (end !== true) {
            GetData(newURL, newEndPoint);
          }
        }
      };

    return(
        <>
        <div className="mt-5 pt-2">
           <ApplicantsPane jobRoles={jobRoles} selectedJobRole={selectedJobRole} setSelectedJobRole={setSelectedJobRole}
            status={status} setStatus={setStatus} sortBy={sortBy} setSortBy={setSortBy}/>
        </div>
        <div className="row no-gutters pl-0 pr-0 mt-5 pt-4" >
            <div className="col-3">
                <div className="card gw-card " style={{height:'84vh',overflowY:'scroll',width:'100%'}} onScroll={handleScroll}>

                    { 
                    allFilteredApplicantsData &&  allFilteredApplicantsData.map((job_applicant,ind)=>{
                                return(
                                    
                                    <div className={"card applicant-card pl-1 my-0"} style={{cursor:"pointer"}}
                                    onClick={()=>{getApplicantDetails(job_applicant.job_id,job_applicant.user_id)}} >
                                        <div className="card-body">
                                        <div className="row">
                                            <div className="col-4 my-auto text-center p-0">
                                                <img className="small-avatar " src={job_applicant.profile_picture} alt=""/>
                                            </div>
                                            <div className="col-8 pl-4 my-auto">
                                                <div className="row"><p className="fs-18 mb-0"><b>{job_applicant.first_name+' '+job_applicant.last_name}</b></p></div>
                                                <div className="row"><p className="fs-18 fw-500 mb-0">
                                                   <span>{job_applicant.education[job_applicant.education.length-1].college_name},</span> 
                                                   <span> {job_applicant.education[job_applicant.education.length-1].college_location }</span> </p></div>
                                            </div>
                                        </div>
                                        
                                        <div className="row text-left mx-2 my-3">
                                            <p className="card-text fs-14 gray-2">
                                               {job_applicant && <span className="fw-500">Score: {job_applicant.score}</span>} &nbsp; ,  &nbsp; 
                                               {job_applicant && <span className="fw-500">Apply Date:{job_applicant.formatted_time}</span>}
                                            </p>
                                            
                                        </div>
                                        {/* <div className="row text-left mx-2 my-3">
                                            <p className="card-text fs-16 gray-2">
                                                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Eius quis doloremque eum perspiciatis 
                                            </p>
                                            
                                        </div> */}

                                        </div>

                                    </div>
                                )
                            })
                         
                        
                       
                    }
                  
                </div>
            </div>
            <div className="col-6">
            <div className="card gw-card" style={{height:'84vh',overflowY:'scroll',width:'100%',overflowX:'hidden'}}>
                {
                    loadCurrentApplicant ?
                    <>Loading...</>
                    :
                    currentApplicant && (
                        <div className="card-body p-0">
                        <div className="row pt-3 px-2">
                            <div className="col-4 pl-4">
                                <img className="big-avatar" src={currentApplicant.user ? currentApplicant.user.profile_picture : <Skeleton/>} alt=""/>
                            </div>
                            <div className="col-8 text-left my-4">
                                <h2 className="pl-3 fs-34">{currentApplicant.user ? currentApplicant.user.first_name+' '+currentApplicant.user.last_name : <Skeleton/> }</h2>
                                <p className="fs-18 gray-2 mb-1 pl-3">{currentApplicant.user ? currentApplicant.user.current_city : <Skeleton/>} , &nbsp;
                                { currentJob && currentJob.job_role_name ? currentJob.job_title : <Skeleton/>}</p>
                                
                                    <p className="gray-3 fs-18 mb-1 pl-3"> {currentApplicant.education ? currentApplicant.education[currentApplicant.education.length-1].college_name:<Skeleton/>}
                                   <span className="gray-3 fs-18 mb-1">, { currentApplicant.education ? currentApplicant.education[currentApplicant.education.length-1].college_location : <Skeleton/>}  </span>
                                   </p>
                               
                                <div className="row my-2 mx-1">
                                    {
                                        currentApplicant.student_details ?
                                        <button onClick={()=>{window.open(currentApplicant.student_details.resume)}} className="btn btn-sm bg-blue-light-2 mx-2"><i className="fas fa-download"></i> Resume</button>
                                        :
                                        <Skeleton/>
                                    }
                                    {
                                        currentApplicant.user ?
                                        <>
                                        <i className="fab fa-linkedin fa-2x mx-2 color-blue"></i>                                
                                        <img className="mx-2" src={behanceLogo} style={{height:'30px'}}/>
                                        <img className="mx-2" src={dribbleLogo} style={{height:'30px'}}/>
                                        <i className="mx-2" className="color-blue-light-2 fas fa-share-alt-square fa-2x"></i>     

                                        </>
                                        :
                                        <Skeleton/>
                                    }
                            
                                </div>
                     
                             </div>
                        </div>
                        {console.log(currentApplicant)}
                        <div className="row mb-1 justify-content-center pl-5">
                            {
                                changeStatus ?
                                <><span className="fs-12 text-info fw-500">Changing Status...</span></>
                                :
                                currentApplicant.user ?
                                currentApplicant.status==1 ?
                                <>
                                <button onClick={()=>ShortlistApplicant(currentApplicant.id,currentApplicant.round)} className="btn btn-success btn-change ml-5 mr-3"><b>Shortlist</b></button>
                                <button onClick={()=>RejectApplicant(currentApplicant.id,currentApplicant.round)} className="btn btn-danger btn-change mx-2"><b>Reject</b></button>
                                </>
                                :
                                <><p className="fs-14 fw-500">Status: <span className="fs-13 gray-2">{currentApplicant.status_name}</span></p></>
                                :
                                <Skeleton/>
                            }

                        </div>
                        
                        <div className="bg-gray-light fs-20 px-4" style={{background:'#F2F2F2'}}>
                        <hr style={mainHRStyles}/>

                                <div className="row d-flex flex-column mb-3">
                                    <div className="clr-black font-weight-bold fs-22">Summary</div>
                                    <div className="">
                                        {
                                            currentApplicant.student_details ?
                                            <span className="gray-2">{currentApplicant.student_details.about}asdfasdf</span>
                                            :
                                            <Skeleton/>
                                        }
                                    </div>
                                </div>
                                <div className="row d-flex flex-column mb-3">
                                    <div className="clr-black font-weight-bold fs-22">Experience</div>
                                    <div className="">
                                        {
                                            currentApplicant.work_experience && currentApplicant.work_experience[0] &&
                                            currentApplicant.work_experience[0].company_name ? currentApplicant.work_experience.map(w=>{
                                                return(
                                                    <span className="gray-2">
                                                        <span className="font-weight-bold">{w.company_name}, {w.job_designation}</span>
                                                        <br></br>
                                                        {w.start_date} - {w.end_date}
                                                        <br></br>
                                                        <span className="fs-18">{w.job_description}</span>
                                                    </span>
                                                )
                                            })
                                            :
                                            <Skeleton/>
                                        }
                                    </div>
                                </div>
                                <div className="row mb-3">
                                    <div className="col-6 p-0 d-flex flex-column">
                                        <div className="clr-black font-weight-bold fs-22">Education</div>
                                            {
                                                currentApplicant.education && currentApplicant.education[0] &&
                                                currentApplicant.education[0].degree ? currentApplicant.education.map(e=>{
                                                    return(
                                                        <span className="gray-2">
                                                            <span className="font-weight-bold">{e.degree}, {e.college_name}</span>
                                                            <br></br>
                                                            {e.start_date} - {e.end_date}
                                                        </span>
                                                    )
                                                })
                                                :
                                                <Skeleton/>
                                            }
                                    </div>
                                    <div className="col-6 p-0 d-flex flex-column">
                                        <div className="clr-black font-weight-bold fs-22">Skills</div>
                                        <div>
                                            {
                                                currentApplicant.skill && currentApplicant.education[0] ?
                                                 currentApplicant.skill.map(s=>{
                                                    return(
                                                        <span className="gray-2">
                                                            <span className="font-weight-bold">{s},</span>
                                                        </span>
                                                    )
                                                })
                                                :
                                                <Skeleton/>
                                            }
                                        </div>
                                        <div className="clr-black font-weight-bold fs-22 mt-2">Awards</div>
                                        <div>
                                            
                                        </div>
                                    </div>
                                </div>
                               
                                        {console.log(currentApplicant)}
                       
                        </div>

                    </div>
                    )
                }

                </div>
            </div>
            <div className="col-3">
            <div className="card gw-card" style={{height:'84vh',overflowY:'scroll',width: '100%'}}>
                {
                   currentJob && selectedJobRole=='All' ? (
                        <div className="card-body">
                            <div className="row my-2 text-left px-3">
                                <p className="fs-18 mb-0"><b>{currentJob.job_role_name ? currentJob.job_role_name : <Skeleton/>}</b></p>
                            </div>
                            <hr />
                            {/* <div class="dropdown-divider"></div> */}
                            <div className="mt-2 mb-4 text-left ">
                                <p className="fs-16 mb-1 fw-500">Application Preferences</p>
                                <p className="fs-16 gray-2">Rank Applications based on your preferences</p>
                                <p className="fs-16 mb-0 fw-500 mt-4">Work Experience</p>
                                { 
                                    selectedJobWorkEx && selectedJobWorkEx.map((job_workex)=> {
                                        return( <p className="fs-16 gray-2 my-2"> {job_workex} <span onClick={()=>{setSelectedJobWorkEx(selectedJobWorkEx.filter(workex=>workex!==job_workex))}} className="close-icon cp"><img src={close_icon} alt=""/></span></p>)
                                    })
                                }
                               
                                <div className="dropdown my-2">
                            <div style={bg} className="dropdown-sort dropdown-toggle-custom" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style={{whiteSpace:'nowrap',overflow:'hidden',textOverflow:'ellipsis'}}>
                                {currentWorkEx}
                            </div>
                            <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                {
                                    jobWorkEx && jobWorkEx.map(job_workex=>{
                                        return(
                                            <button onClick={()=>{
                                                setCurrentWorkEx(job_workex)
                                                if(job_workex!=='All' && !selectedJobWorkEx.includes(job_workex))
                                                setSelectedJobWorkEx([...selectedJobWorkEx,job_workex])
                                                }
                                            }  
                                            className="dropdown-item fs-18" value={job_workex} >{job_workex}</button>
                                        )   
                                    })
                                }
                              
                            </div>
                        </div> 
                            </div>
                            <div class="dropdown-divider"></div>
                            <div className="text-left  my-4">
                                <p className="fs-16 mb-1 fw-500">Skills</p>
                                {
                                    selectedJobSkills &&  
                                    selectedJobSkills.map((skill)=>{
                                        return(
                                            <p className="fs-16 gray-2 my-2">{skill}<span onClick={()=>{setSelectedJobSkills(selectedJobSkills.filter(jobskill=>jobskill!==skill))}} className="close-icon cp"><img  src={close_icon} alt=""/></span></p>
                                        )

                                    })
                                }  
                            <div className="dropdown my-2">
                            <div style={bg} className="dropdown-sort dropdown-toggle-custom" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style={{whiteSpace:'nowrap',overflow:'hidden',textOverflow:'ellipsis'}}>
                                {currentJobSkill}
                            </div>
                            <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                {
                                    jobSkills && jobSkills.map(skill=>{
                                        return(
                                            <button onClick={()=>{
                                                setCurrentJobSkill(skill)
                                                if(skill!=='All' && !selectedJobSkills.includes(skill))
                                                setSelectedJobSkills([...selectedJobSkills,skill])
                                                }}  className="dropdown-item fs-18" value={skill} >{skill}</button>
                                        )   
                                    })
                                }
                              
                            </div>
                        </div> 
                           
                            </div>
                            <div class="dropdown-divider"></div>
                            <div className="my-4 text-left">
                                <p className="fs-16 mb-1 fw-500">Location</p>
                                {selectedJobLocations ? 
                                    selectedJobLocations.map(jobLocation=>(<p className="fs-16 gray-2 my-1">{jobLocation}<span onClick={()=>{setSelectedJobLocations(selectedJobLocations.filter(joblocation=>joblocation!==jobLocation))}} className="close-icon cp"><img src={close_icon} alt=""/></span></p>)) : <Skeleton/>}
                                    <div className="dropdown my-2">
                            <div style={bg} className="dropdown-sort dropdown-toggle-custom" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style={{whiteSpace:'nowrap',overflow:'hidden',textOverflow:'ellipsis'}}>
                                {currentJobLocation}
                            </div>
                            <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                {
                                    jobLocations && jobLocations.map(job_location=>{
                                        return(
                                            <button onClick={()=>{
                                                setCurrentJobLocation(job_location)
                                                if(job_location!=='All' && !selectedJobLocations.includes(job_location))
                                                setSelectedJobLocations([...selectedJobLocations,job_location])
                                                }}  className="dropdown-item fs-18" value={job_location} >{job_location}</button>
                                        )   
                                    })
                                }
                              
                            </div>
                        </div> 

                            </div>
                            <div class="dropdown-divider"></div>
                            <div className="my-4 text-left">
                                <p className="fs-16 mb-1 fw-500">Colleges</p>
                               
                                    {selectedJobColleges ? selectedJobColleges.map(college=>{
                                        return(
                                            <p className="fs-16 gray-2 my-1">{college}<span className="close-icon cp" onClick={()=>{setSelectedJobColleges(selectedJobColleges.filter(jobcollege=>jobcollege!==college))}}><img src={close_icon} alt=""/></span></p>
                                        )
                                    }) : 
                                    <Skeleton/>}
                                    <div className="dropdown my-2">
                            <div style={bg} className="dropdown-sort dropdown-toggle-custom" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style={{whiteSpace:'nowrap',overflow:'hidden',textOverflow:'ellipsis'}}>
                                {currentJobCollege}
                            </div>
                            <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                {
                                    jobColleges && jobColleges.map(job_college=>{
                                        return(
                                            <button onClick={()=>{
                                                setCurrentJobCollege(job_college)
                                                if(job_college!=='All' && !selectedJobColleges.includes(job_college))
                                                setSelectedJobColleges([...selectedJobColleges,job_college])
                                                }}  className="dropdown-item fs-18" value={job_college} >{job_college}</button>
                                        )   
                                    })
                                }
                              
                            </div>
                        </div> 
                                
                            </div>

                            <div class="dropdown-divider"></div>
                            <div className="mt-2 mb-4 text-left fs-16">
                                <a href="">Edit Preferences</a>
                            </div>
                            <div className="my-4 text-left">
                               
                                {selectedPassoutYear ? selectedPassoutYear.map(year=>(<p className="fs-16 gray-2 my-1">{year}<span onClick={()=>{setSelectedPassoutYear(selectedPassoutYear.filter(Year=>Year!==year))}} className="close-icon cp"><img src={close_icon} alt=""/></span></p>)) : <Skeleton/>}
                                <div className="dropdown my-2">
                            <div style={bg} className="dropdown-sort dropdown-toggle-custom" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style={{whiteSpace:'nowrap',overflow:'hidden',textOverflow:'ellipsis'}}>
                                {currentPassoutYear}
                            </div>
                            <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                {
                                    jobPassoutYear && jobPassoutYear.map(year=>{
                                        return(
                                            <button onClick={()=>{
                                                setCurrentPassoutYear(year)
                                                if(year!=='All' && !selectedPassoutYear.includes(year))
                                                setSelectedPassoutYear([...selectedPassoutYear,year])
                                                }}  className="dropdown-item fs-18" value={year} >{year}</button>
                                        )   
                                    })
                                }
                              
                            </div>
                        </div> 
                            </div>

                                                    <div class="dropdown-divider"></div>
                            <div className="my-4 text-left">
                                <p className="fs-16 mb-1 fw-500">Branch</p>
                                {selectedJobDegrees ? selectedJobDegrees.map(degree=>(<p className="fs-16 gray-2 my-1">{degree}<span onClick={()=>{setSelectedJobDegrees(selectedJobDegrees.filter(jobdegree=>jobdegree!==degree))}} className="close-icon cp"><img src={close_icon} alt=""/></span></p>)) : <Skeleton/>}
                                <div className="dropdown my-2">
                            <div style={bg} className="dropdown-sort dropdown-toggle-custom" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style={{whiteSpace:'nowrap',overflow:'hidden',textOverflow:'ellipsis'}}>
                                {currentJobDegree}
                            </div>
                            <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                {
                                    jobDegrees && jobDegrees.map(job_degree=>{
                                        return(
                                            <button onClick={()=>{
                                                setCurrentJobDegree(job_degree)
                                                if(job_degree!=='All' && !selectedJobDegrees.includes(job_degree))
                                                setSelectedJobDegrees([...selectedJobDegrees,job_degree])
                                                }}  className="dropdown-item fs-18" value={job_degree} >{job_degree}</button>
                                        )   
                                    })
                                }
                              
                            </div>
                        </div> 
                            </div>

                        </div>
                    )
                    :
                    <></>
                }

                </div>
            </div>
        </div>
        </>
    )
}

export default Applicants;