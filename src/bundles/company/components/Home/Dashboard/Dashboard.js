import React,{useEffect,useState,useRef} from 'react';
import avatar from '../../../../../assets/images/avatar.png'
import JobPane from '../../../../common/components/Panes/JobPane';
import Axios from 'axios';
import { environment as env } from '../../../../../environments/environment';
import {useAlert} from 'react-alert';

const CompanyDashboard=()=>{
    const alert=useAlert();
    const firstRender=useRef(true)
    const [companyID,setCompanyID]=useState(
        localStorage.getItem('company')
        ?
        JSON.parse(localStorage.getItem('company')).company
        :
        null
    );
    const [searched,setSearched]=useState('')
    const [showMoreOpenJobs,setShowMoreOpenJobs]=useState(false)
    const [showMoreDraftedJobs,setShowMoreDraftedJobs]=useState(false)
    const [showMoreClosedJobs,setShowMoreClosedJobs]=useState(false)
    const [showOnly,setShowOnly]=useState('All Jobs')
    // let companyID=JSON.parse(localStorage.getItem('company')).company
    let [jobs,setJobs]=useState({
        open:'',
        closed:'',
        draft:'',
    })
    const [allJobs,setAllJobs]=useState([])
    const [openJobs,setOpenJobs]=useState([])
    const [savedJobs,setSavedJobs]=useState([])
    const [closedJobs,setClosedJobs]=useState([])
    const [filteredOpenJobs,setFilteredOpenJobs]=useState([])
    const [filteredSavedJobs,setFilteredSavedJobs]=useState([])
    const [filteredClosedJobs,setFilteredClosedJobs]=useState([])

    const getAllJobs=()=>{
        // const companyID=JSON.parse(localStorage.getItem('company')).company;
        Axios.get(`http://54.162.60.38/job/company/job_details/?company_id=${btoa(companyID.toString())}`,{
            headers:{
                "Authorization":'Token '+localStorage.getItem('gw_token')
            }
        })
            .then(res=>{
                console.log(res);
                if(res.data.success){
                    setJobs({...jobs,
                        open:res.data.data.open_jobs,
                        closed:res.data.data.close_jobs,
                        draft:res.data.data.draft_jobs
                    })
                    let openjobs=[],closedjobs=[],draftjobs=[]
                    res.data.data.jobs.forEach((job)=>{
                        if(job.job_status=="OPEN"){
                            //add it to open jobs
                            openjobs.push(job)
                            //setOpenJobs([...openJobs,...job])
                        }
                        else if(job.job_status=="DRAFT"){
                            //add it to draft jobs
                            draftjobs.push(job)
                            //setSavedJobs([...savedJobs,...job])
                        }
                        else{
                            //add it to close jobs
                            closedjobs.push(job)
                            //setClosedJobs([...closedJobs,...job])

                        }
                    })
                    setOpenJobs(openjobs);
                    setFilteredOpenJobs(openjobs);
                    setClosedJobs(closedjobs);
                    setFilteredClosedJobs(closedjobs)
                    setSavedJobs(draftjobs);
                    setFilteredSavedJobs(draftjobs);
                    setAllJobs(...openjobs,...closedjobs,...draftjobs);
                }
            })
            .catch(err=>{
                console.log(err);
            })
          
    }

    useEffect(()=>{
        let tempOpenJobs=openJobs.filter((job)=>{
            return job.job_role_name.includes(searched);
        })
        let tempClosedJobs=closedJobs.filter((job)=>{
            return job.job_role_name.includes(searched);
        })
        let tempDraftJobs=savedJobs.filter((job)=>{
            return job.job_role_name.includes(searched);
        })
        setFilteredOpenJobs(tempOpenJobs)
        setFilteredClosedJobs(tempClosedJobs)
        setFilteredSavedJobs(tempDraftJobs)
    },[searched])

    useEffect(()=>{
        console.log('company id,',companyID);
        //if(companyID)
        getAllJobs();
    },[companyID])

    useEffect(()=>{
        if(openJobs.length>0) console.log('open jobs: ',openJobs)
        if(closedJobs.length>0) console.log('closed jobs: ',closedJobs)
        if(savedJobs.length>0) console.log('saved jobs: ',savedJobs)
    },[openJobs,closedJobs,savedJobs])

    const showMore=(type)=>{
        if(type=='open')       setShowMoreOpenJobs(!showMoreOpenJobs);
        if(type=='drafted')    setShowMoreDraftedJobs(!showMoreDraftedJobs);
        if(type=='closed')     setShowMoreClosedJobs(!showMoreClosedJobs);
    }

    const OpenJobPage=(jobid)=>{
        console.log('open job for id: ',jobid)
        const url='/job/'+jobid;
        window.open(url,"_blank")
    }

    const UnpublishJob=(job)=>{
        Axios.post(env.niyuktiBaseUrl+'company/job_status/',{
            'job_id':job.id,
            'status': 'CLOSE'
        })
            .then(res=>{
                console.log(res)
                if(res.data.success){
                    let tempClosedJobs=[...closedJobs,job]
                    let tempFilteredClosedJobs=[...filteredClosedJobs,job]
                    setClosedJobs(tempClosedJobs)
                    setFilteredClosedJobs(tempFilteredClosedJobs)
                    alert.success('Job Closed Successfully!')
                    let openjobs=openJobs;
                    let filteredopenjobs=filteredOpenJobs;
                    openjobs=openjobs.filter(openjob=>openjob.id!==job.id)
                    filteredopenjobs=filteredopenjobs.filter(filteredopenjob=>filteredopenjob.id!==job.id)
                    setOpenJobs(openjobs)
                    setFilteredOpenJobs(filteredopenjobs)

                }
            })
            .catch(err=>{
                console.log(err)
            })
    }

    const PublishJob=(job)=>{
        Axios.post(env.niyuktiBaseUrl+'company/job_status/',{
            'job_id':job.id,
            'status':'OPEN'
        })
            .then(res=>{
                console.log(res)
                if(res.data.success){
                    let tempDraftedJobs=[...savedJobs,job]
                    let tempFilteredDraftedJobs=[...filteredSavedJobs,job]
                    setOpenJobs(tempDraftedJobs)
                    setFilteredOpenJobs(tempFilteredDraftedJobs)
                    alert.success('Job Opened Successfully!')
                    let saved_jobs=savedJobs;
                    let filtered_saved_jobs=filteredSavedJobs;
                    saved_jobs=saved_jobs.filter(saved_job=>saved_job.id!==job.id)
                    filtered_saved_jobs=filtered_saved_jobs.filter(filtered_saved_job=>filtered_saved_job.id!==job.id)
                    setSavedJobs(saved_jobs)
                    setFilteredSavedJobs(filtered_saved_jobs)
                }
            })
    }

    const DeleteJob=(job)=>{
        console.log(job)
        Axios.delete(env.niyuktiBaseUrl+`company/job_post/?job_id=${btoa(job.id)}`)
            .then(res=>{
                console.log(res)
                if(res.data.success){
                    alert.success(res.data.data.message)
                    switch(job.job_status){
                        case "OPEN"     : DeleteOpenJob(job)
                                            break;
                        case "DRAFT"    : DeleteDraftedJob(job)
                                            break;
                        case "CLOSE"   : DeleteCloseJob(job)
                                            break;
                        default         :   break;        
                    }
                }
            })
            .catch(err=>{
                console.log(err)
            })
    }

    const DeleteOpenJob=(job)=>{
        let current_jobs=openJobs;
        let filtered_jobs=filteredOpenJobs;
        current_jobs=current_jobs.filter(current_job=>current_job.id!==job.id)
        filtered_jobs=filtered_jobs.filter(filtered_open_job=>filtered_open_job.id!==job.id)
        setOpenJobs(current_jobs)
        setFilteredOpenJobs(filtered_jobs)
    }

    const DeleteDraftedJob=(job)=>{
        let current_jobs=savedJobs;
        let filtered_jobs=filteredSavedJobs;
        current_jobs=current_jobs.filter(current_job=>current_job.id!==job.id)
        filtered_jobs=filtered_jobs.filter(filtered_open_job=>filtered_open_job.id!==job.id)
        setSavedJobs(current_jobs)
        setFilteredSavedJobs(filtered_jobs)
    }

    const DeleteCloseJob=(job)=>{
        let current_jobs=closedJobs;
        let filtered_jobs=filteredClosedJobs;
        current_jobs=current_jobs.filter(current_job=>current_job.id!==job.id)
        filtered_jobs=filtered_jobs.filter(filtered_open_job=>filtered_open_job.id!==job.id)
        setClosedJobs(current_jobs)
        setFilteredClosedJobs(filtered_jobs)
    }
    

    return(
        <>
        <div className="mt-5 pt-5">
            <JobPane setShow={setShowOnly} setSearched={setSearched}/>
        </div>
                <div className="row container mx-5 mt-5">
                    {/* <div className={ showOnly==='All Jobs' || showOnly==='Open Jobs' ? "col-3":"col-1"}> */}
                    <div className="col-md-3 col-sm-12">
                    
                        <button className="mx-auto d-block d-md-none my-2 btn btn-sm btn-main btn-main font-weight-bold">Create new Job +</button>
                    
                    </div>
                    {/* <div className={ showOnly==='All Jobs' || showOnly==='Open Jobs' ? "col-6":"col-7"}> */}
                    <div className="col-md-10 col-sm-12">
                        {
                            openJobs && openJobs.length>0 && 
                            ( showOnly==='All Jobs' || showOnly==='Open Jobs') && 
                            (
                            <>
                                <div className="row my-2 textcenter position-relative">
                                    <p className="fs-18 fw-500">{jobs.open} open jobs</p>
                                    <div className="d-md-block d-none create-new-top">
                                        <button className="my-2 btn btn-sm btn-main btn-main font-weight-bold">Create new Job +</button>
                                    </div>
                                </div>

                                <div className="row justify-center">
                                    {
                                        
                                        filteredOpenJobs && filteredOpenJobs.map((openJob,ind)=>{
                                            if(!showMoreOpenJobs && ind>=3) return;
                                            else
                                            return(
                                                <div className="card gw-card " >
                                            <div className="card-body">
                                            <div className="text-left cp" onClick={()=>{OpenJobPage(openJob.id)}}>
                                                <p className="fs-18 fw-500 mb-0">{openJob.job_title}</p>
                                                <p className="fs-16 gray-2 mb-0">{openJob.job_location.city}</p>
                                            </div>

                                        </div>
                                        <div className="row w-100 py-3 text-left bg-blue-light mx-auto" style={{borderRadius:'4px'}}>
                                                        <div className="col-md-1 col-2 my-auto">
                                                            <img src={avatar} alt="avatar"/>
                                                        </div>
                                                        <div className="col-md-9 col-7 pl-4 text-left my-auto">
                                                            
                                                                <p className="fs-16 gray-2 mb-0">{openJob.posted_by[0].first_name+' '+openJob.posted_by[0].last_name}</p>
                                                                <p className="fs-12 gray-2 mb-0">{openJob.formatted_time}</p>
                                                    
                                                        </div>
                                                        <div className="col-md-2 col-2 text-left my-auto">
                                                            <div className="dropdown">
                                                                <div className="ml-auto small-box text-center cp"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id={"open-job-"+openJob.id}>
                                                                    <i className="fas fa-ellipsis-h gray-2 cp"></i>
                                                                </div>
                                                                <div class="dropdown-menu" aria-labelledby={"open-job-"+openJob.id}>
                                                                    <span onClick={()=>{OpenJobPage(openJob.id)}} class="dropdown-item cp">View Job</span>
                                                                    <span class="dropdown-item cp">Edit Job</span>
                                                                    <span onClick={()=>{UnpublishJob(openJob)}} class="dropdown-item cp">Unpublish Job</span>
                                                                    <span onClick={()=>{DeleteJob(openJob)}} class="dropdown-item cp text-danger">Delete Job</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                
                                            </div>
                                            )
                                        })
                                    }

                                </div>
                            

                        <div className="row textcenterright float-right mb-2">
                               {
                                   showMoreOpenJobs ? (
                                       <p className="fs-14 link-text fw-500" onClick={()=>{showMore('open')}}>Hide</p>
                                           ) : (
                                            <p className="fs-14 link-text fw-500" onClick={()=>{showMore('open')}}>Show More</p>

                                           )
                               }
                        </div>
                        </>
                        )
                    }

                    {
                        savedJobs && savedJobs.length>0 && 

                        (showOnly=='All Jobs' || showOnly=='Drafted Jobs') && (
                            <>
                            <div className="row text-left my-2 ">
                            <div className="col-6 text-left"> <p className="fs-18 fw-500">{jobs.draft} saved drafts</p> </div>
                        </div>

                        <div className="row">
                           
                                        <div className="card gw-card">
                                            <div className="table-responsive">
                                            <table className="table">
                                            <thead style={{textAlign:'left'}}>
                                                <tr>
                                                <th scope="col">Job</th>
                                                <th scope="col">Recruiter</th>
                                                <th scope="col"></th>
                                                <th scope="col">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                   filteredSavedJobs && filteredSavedJobs.map((savedJob,ind)=>{
                                                        if(!showMoreDraftedJobs && ind>=3) return;
                                                        return(
                                                            <tr style={{textAlign:'left'}} >
                                                                <td scope="row" className="cp" onClick={()=>{OpenJobPage(savedJob.id)}}>
                                                                    <p className="fs-16 mb-0">{savedJob.job_role_name}(Draft)</p>
                                                                    <p className="fs-14 mb-0 gray-2">{savedJob.job_location.city}</p>
                                                                </td>
                                                                <td>
                                                                    <img src={avatar} alt="avatar"/>
                                                                </td>
                                                                <td>
                                                                <div className="dropdown">
                                                                    <div className="small-box text-center cp"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id={"saved-job-"+savedJob.id}>
                                                                        <i className="fas fa-ellipsis-h gray-2 cp"></i>
                                                                    </div>
                                                                    <div class="dropdown-menu" aria-labelledby={"saved-job-"+savedJob.id}>
                                                                        <span onClick={()=>{OpenJobPage(savedJob.id)}} class="dropdown-item cp">View Job</span>
                                                                        <span onClick={()=>{DeleteJob(savedJob)}} class="dropdown-item cp text-danger">Delete Job</span>
                                                                    </div>
                                                                </div>
                                                                </td>
                                                                <td>
                                                                    <button onClick={()=>{PublishJob(savedJob)}} className="btn btn-main-outline">Publish Job</button>
                                                                </td>
                                                                </tr>
                                                        )
                                                    })
                                                }

                                            
                                            </tbody>
                                            </table>
                                            </div>
                                        </div>
                                  
                        </div>
                        <div className="row textcenterright float-right mb-2">
                        {
                                   showMoreDraftedJobs ? (
                                       <p className="fs-14 link-text fw-500" onClick={()=>{showMore('drafted')}}>Hide</p>
                                           ) : (
                                            <p className="fs-14 link-text fw-500" onClick={()=>{showMore('drafted')}}>Show More</p>

                                           )
                               }
                        </div>
                            </>
                        )
                    }

                    {
                        closedJobs && closedJobs.length>0 && 
                        ( showOnly==='All Jobs' || showOnly=='Closed Jobs') &&  (
                            <>
                                                            
                        <div className="row textcenter text-left my-2 ">
                            <p className="fs-18 fw-500">{jobs.closed} closed jobs</p>
                        </div>
                        <div className="row justify-center">
                             { 
                                 filteredClosedJobs && filteredClosedJobs.map((closedJob,ind)=>{
                                if(!showMoreClosedJobs && ind>=3) return;
                                return(
                                    <div className="card gw-card ">
                                        <div className="card-body">
                                            <div className="text-left cp"  onClick={()=>{OpenJobPage(closedJob.id)}}>
                                                <p className="fs-18 fw-500 mb-0">{closedJob.job_role_name}</p>
                                                <p className="fs-16 gray-2 mb-0">Hired:
                                                    <span className="ml-2 mr-3"><b>Ajay Verma, Rohan Misra, Vijay Verma, Punit Chawla ...</b></span>
                                                    <span className="ml-auto color-blue link-text"><b>and 25 more</b></span>
                                                </p>
                                            </div>

                                        </div>
                                        <div className="row w-100 py-3 text-left bg-blue-light mx-auto" style={{borderRadius:'4px'}}>
                                                <div className="col-md-1 col-2 my-auto">
                                                    <img src={avatar} alt="avatar"/>
                                                </div>
                                                <div className="col-md-9 col-7 pl-4 text-left my-auto">
                                                    
                                                        <p className="fs-16 gray-2 mb-0">{closedJob.posted_by[0].first_name+' '+closedJob.posted_by[0].last_name}</p>
                                                        <p className="fs-12 gray-2 mb-0">{closedJob.formatted_time}</p>
                                            
                                                </div>
                                                <div className="col-md-2 col-2 text-left my-auto">
                                                <div className="dropdown">
                                                                    <div className="ml-auto small-box text-center cp"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id={"closed-job-"+closedJob.id}>
                                                                        <i className="fas fa-ellipsis-h gray-2 cp"></i>
                                                                    </div>
                                                                    <div class="dropdown-menu" aria-labelledby={"closed-job-"+closedJob.id}>
                                                                        <span onClick={()=>{OpenJobPage(closedJob.id)}} class="dropdown-item cp">View Job</span>
                                                                        <span onClick={()=>{DeleteJob(closedJob)}} class="dropdown-item cp text-danger">Delete Job</span>
                                                                    </div>
                                                                </div>
                                                </div>
                                            </div>
                                        
                                    </div>
                                )
                            })} 

                        </div>
                        <div className="row textcenterright float-right mb-2">
                        {
                                   showMoreClosedJobs ? (
                                       <p className="fs-14 link-text fw-500" onClick={()=>{showMore('closed')}}>Hide</p>
                                           ) : (
                                            <p className="fs-14 link-text fw-500" onClick={()=>{showMore('closed')}}>Show More</p>

                                           )
                               }
                        </div>
                            </>
                        )
                    }

                    </div>
                    
                </div>
   

        </>
    )
}

export default CompanyDashboard;