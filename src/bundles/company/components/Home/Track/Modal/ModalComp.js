import React from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import styles from "./modal.module.css";
import defualtImg from "../../../../../../assets/icon/defaultUser.png";
import dribbleLogo from "../../../../../../assets/images/dribble-logo.png";
import behanceLogo from "../../../../../../assets/images/behance-logo.png";
import closeIcon from "../../../../../../assets/images/icons/close-icon.png";
import prevIcon from "../../../../../../assets/icon/prev.png";
import nextIcon from "../../../../../../assets/icon/next.png";
//import {StatusMap} from '../../../constants/constants'
import { StatusMap } from "../../../../../../constants/constants";
import Form from "react-bootstrap/Form";
import Axios from "axios";
import { useAlert } from "react-alert";
import { environment as env } from "../../../../../../environments/environment";
const ModalComp = (props) => {
	const alert = useAlert();
	const [applicantList, setApplicantList] = React.useState([]);
	const [activeData, setActiveData] = React.useState(false);
	const [activeIndex, setActiveIndex] = React.useState(0);
	const [feedback, setFeedback] = React.useState("");

	const handleSubmit = () => {
		let data = {
			applicant_id: applicantList[activeIndex].applicant_id,
			feedback: {
				user_id: applicantList[activeIndex].user_id,
				first_name: applicantList[activeIndex].first_name,
				last_name: applicantList[activeIndex].last_name,
				status_id: applicantList[activeIndex].status,
				status_name: applicantList[activeIndex].status_name,
				feedback: feedback,
			},
		};
		Axios.post(env.niyuktiBaseUrl + "company/applicant_feedback/", data)
			.then((res) => {
				console.log(res);
				if (res.data.success) {
					setFeedback("");
					handleNextChange();
				} else alert.error(res.data.error);
			})
			.catch((err) => {
				console.log(err);
			});
	};

	React.useEffect(() => {
		if (props.data) {
			let arr = [];
			if (props.activeJob == "All") {
				props.data.forEach((value) => {
					value.applicants.forEach((val) => {
						if (val.status_name == props.activeTab) {
							arr.push(val);
						}
					});
				});
			} else {
				props.data.forEach((value) => {
					if (value.job_title == props.activeJob) {
						value.applicants.forEach((val) => {
							if (val.status_name == props.activeTab) {
								arr.push(val);
							}
						});
					}
				});
			}

			setApplicantList([...arr]);
		}
	}, [props.activeJob, props.data, props.activeTab]);

	React.useEffect(() => {
		let arr = applicantList.filter((value, index) => {
			if (value.applicant_id == props.show) {
				setActiveIndex(index);
				return value;
			}
		});

		if (arr.length > 0) {
			setActiveData(arr[0]);
		} else {
			setActiveData(applicantList.length > 0 && applicantList[0]);
		}
	}, [props.show, applicantList]);

	const handlePrevChange = () => {
		let newIndex =
			activeIndex == 0 ? applicantList.length - 1 : activeIndex - 1;
		setActiveIndex(newIndex);
		setActiveData(applicantList[newIndex]);
	};
	const handleNextChange = () => {
		let newIndex =
			activeIndex == applicantList.length - 1 ? 0 : activeIndex + 1;
		setActiveIndex(newIndex);
		setActiveData(applicantList[newIndex]);
	};
	const UpdateStatus = (from, to) => {
		let data = [];

		data = [
			{
				id: applicantList[activeIndex].applicant_id,
				round: 2,
				status: StatusMap[to],
			},
		];
		Axios.post(env.niyuktiBaseUrl + "company/status_update/", {
			student_data: data,
		})
			.then((res) => {
				console.log(res);
				if (res.data.success) {
					alert.success(res.data.data.message);
					handleNextChange();
				} else alert.error(res.data.error);
			})
			.catch((err) => {
				console.log(err);
			});
	};

	let profileImg = activeData.profile_picture
		? activeData.profile_picture
		: defualtImg;

	console.log(activeData);
	return (
		<div className={styles.container}>
			{activeData && (
				<Modal
					show={props.show}
					onHide={props.onHide}
					centered
					size="lg"
					className={styles.modal}
				>
					<div className={styles.content}>
						<img
							src={prevIcon}
							className={styles.actionImg}
							onClick={handlePrevChange}
						/>

						<div
							className="row bg-white py-3 hey"
							style={{
								marginTop: "100px",
								border: "2px solid #000",
								minHeight: "700px",
								minWidth: "900px",
							}}
						>
							<div className="col-md-6 hey"  style = {{"border-right" : "1px solid #BDBDBD"}}>
								<div className="row">
									<div className="col-md-4 d-flex align-items-center justify-content-center">
										<img
											src={profileImg}
											alt=""
											className={styles.profileImg}
										/>
									</div>
									<div className="col-md-8">
										<h4>
											{activeData.first_name} {activeData.last_name}
										</h4>
										<p>{activeData.current_city}</p>

										<div className="row">
											<img className="mx-3" src={dribbleLogo} alt="" />
											<img className="mx-3" src={behanceLogo} alt="" />
										</div>
									</div>
								</div>
								<hr></hr>
				<div className="div_exp_skill">
								<div className="bg-light">
									{activeData.about ? (
										<>
											<p className="text-bold">Summary</p>
											<p style={{" fontFamily ": "Proxima Nova" }}>
												{activeData.about}
											</p>
										</>
									) : null}
									{activeData.work_experience.length == 0 ? null : (
										<>
											<p className="text-bold">Experience</p>
											{activeData.work_experience.map((val) => {
												return (
													<p className="paramar"
														style={{ "fontFamily": "Proxima Nova"}}
													>{`${val.company_name}  - ${val.job_designation}`}</p>
												);
											})}
										</>
									)}
								</div>

								<div className="bg-light d-flex">
									<div className="col-md-6 p-0" style={{"height":"100vh"}}>
										<p className="text-bold">Education</p>
										{activeData.education.map((val) => {
											return <p>{`${val.degree}  - ${val.college_name}`}</p>;
										})}
									</div>
									<div className="col-md-6 p-0">
										{activeData.skill.length == 0 ? null : (
											<>
												<p className="text-bold">Skills</p>
												{activeData.skill.map((val) => {
													return (
														<p>{`${val.rating_name} - ${val.skill_name}`}</p>
													);
												})}
											</>
										)}

										{activeData.project.length == 0 ? null : (
											<>
												<p className="text-bold">Projects</p>
												{activeData.project.map((val) => {
													return <p>{`${val.title}`}</p>;
												})}
											</>
										)}
									</div>
								</div>
							</div>
							</div>
							<div className="col-md-6 py-3">
								<div className="d-flex justify-content-end">
									<img
										src={closeIcon}
										alt=""
										className={styles.closeIcon}
										onClick={props.onHide}
									/>
								</div>
								<div style={{"text-align":"right" , "margin-right":"40px"}}>
									<Button
										variant="danger"
										size="sm"
										className="mr-2"
										onClick={() => {
											UpdateStatus("Applied", "Rejected");
										}}
									>
										Reject
									</Button>
									<Button
										variant="success"
										size="sm"
										onClick={() => {
											UpdateStatus("Applied", "Under Review");
										}}
									>
										Move to next step
									</Button>
								</div>

								<hr />

								<div>
									<h3>Feedback</h3>
									<Form.Group className="formms">
										<Form.Control
										
											as="textarea"
											rows={3}
											value={feedback}
											onChange={(e) => setFeedback(e.target.value)}
										/>
									</Form.Group>

									<Button
										variant="primary"
										size="sm"
										className="mt-2"
										onClick={handleSubmit}
									>
										Submit
									</Button>
								</div>
							</div>
						</div>

						<img
							src={nextIcon}
							className={styles.actionImg}
							onClick={handleNextChange}
						/>
					</div>
				</Modal>
			)}
		</div>
	);
};



export default ModalComp;
