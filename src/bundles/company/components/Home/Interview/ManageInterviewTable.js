import React from 'react'

const tableStyles={
  background: '#FFFFFF',
  borderRadius: '4px',
  overFlowY:'scroll',

}
export const ManageInterviewTable = ({data}) => {

  return (
    <div>
    <table class="table" style={tableStyles}>
      <thead>
        <tr>
        <th scope="col">ID<i className="fas fa-sort gray-2 ml-2"></i></th>
        <th scope="col">INTERVIEW TYPE<i className="fas fa-sort gray-2 ml-2"></i></th>
          <th scope="col">Name<i className="fas fa-sort gray-2 ml-2"></i></th>
          <th scope="col">Job Title<i className="fas fa-sort gray-2 ml-2"></i></th>
          <th scope="col">Slot Taken<i className="fas fa-sort gray-2 ml-2"></i></th>
          <th scope="col">Type<i className="fas fa-sort gray-2 ml-2"></i></th>
          <th scope="col">Date<i className="fas fa-sort gray-2 ml-2"></i></th>
          <th scope="col">Status<i className="fas fa-sort gray-2 ml-2"></i></th>
        </tr>
      </thead>
      {/* <hr/> */}
      <tbody>
      {
        data.map((item)=>(
          <tr>
          <th scope="row">{item.user_id}</th>
          <th scope="row">{item.interview_type ? item.interview_type : 'No'}</th>
          <td>{item.user}</td>
          <td>{item.job_title}</td>
          <td>{item.slot ? item.slot : 'No'}</td>
          <td>{item.job_type}</td>
          <td>{item.start_date ? item.start_date : 'No'}</td>
          <td>{item.status}</td>
        </tr>
        ))
      }
       
        </tbody>
    </table>
    </div>
  )
}
