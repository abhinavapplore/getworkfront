import React, { useState, useEffect } from 'react'
import SelectSearch from 'react-select-search';
import {ManageInterviewTable} from './ManageInterviewTable';
import Axios from 'axios';
import Filter from '../../../../../utils/filterJob'

export const Manage = () => {
    const status=[{name:'All',value:'All'},{name:'Invited',value:'Invited'},{name:'Accepted',value:'Accepted'},{name:'Interview Not Scheduled',value:'Interview Not Scheduled'},{name:'Rejected',value:'Rejected'}]
    const interviewTypes=[{name:'All',value:'All'},{name:'ONLINE',value:'ONLINE'},{name:'OFFLINE',value:'OFFLINE'}]
    const InterviewTypes=() =>( <SelectSearch value={selectedInterviewType} onChange={setSelectedInterviewType} options={interviewTypes} placeholder="Select" />  )
    // const Jobs=() =>( <SelectSearch options={job} placeholder="Select" />  )
    const ApprovalStatus=() =>( <SelectSearch value={selectedStatusType} onChange={setSelectedStatusType} options={status} placeholder="Select" />  )
    const[data,setData]=useState([]);
    const companyID=JSON.parse(localStorage.getItem('company')).company;
    const [allData,setAllData]=useState([])
    const[selectedInterviewType,setSelectedInterviewType]=useState(null)
    const[selectedJobType,setSelectedJobType]=useState('All')
    const[selectedStatusType,setSelectedStatusType]=useState(null)
    const[jobData,setJobData]=useState([])
    const[jobDataId,setJobDataId]=useState([])
    const [jobProfile,setJobProfile]=useState('All')
    const[selectedJobIdType,setSelectedJobIdType]=useState(null)
    const [filterData,setFilterData]=useState({job_title:null,interview_type:null,status:null})
    useEffect(()=>{

        Axios.get(`http://54.162.60.38/job/company/manage_interview/?company_id=${btoa(companyID.toString())}`).then((res)=>{
            setAllData(res.data.data);
            setData(res.data.data)
            console.log(res.data.data)
            let temp=['All']

            res.data.data.map((item)=>{
                    temp.push(item.job_title)
            })
            
            setJobData(temp)
        })
            // getJobData()
    },[])

    useEffect(()=>{
        // All data
        if(filterData.interview_type!==null && filterData.job_title!==null && filterData.status!==null){
            console.log(selectedJobType,selectedInterviewType,selectedStatusType)
         let tempData=allData.filter((item)=>item.interview_type===filterData.interview_type).filter((item)=>item.job_title===filterData.job_title).filter((item)=>item.status===filterData.status)
         setData(tempData)
        //  single Data
        }else  if(filterData.interview_type!==null && filterData.job_title==null && filterData.status==null){
            console.log(selectedJobType,selectedInterviewType,selectedStatusType)
         let tempData=allData.filter((item)=>item.interview_type===filterData.interview_type)
         setData(tempData)
        }else  if(filterData.interview_type==null && filterData.job_title!==null && filterData.status==null){
            console.log(selectedJobType,selectedInterviewType,selectedStatusType)
         let tempData=allData.filter((item)=>item.job_title===filterData.job_title)
         setData(tempData)
        }
        else  if(filterData.interview_type==null && filterData.job_title==null && filterData.status!==null){
            console.log(selectedJobType,selectedInterviewType,selectedStatusType)
         let tempData=allData.filter((item)=>item.status===filterData.status)
         setData(tempData)
        }
        // Double Data
        else  if(filterData.interview_type!==null && filterData.job_title!==null && filterData.status==null){
            console.log(selectedJobType,selectedInterviewType,selectedStatusType)
         let tempData=allData.filter((item)=>item.interview_type===filterData.interview_type).filter((item)=>item.job_title===filterData.job_title)
         setData(tempData)
        }
        else  if(filterData.interview_type!==null && filterData.job_title===null && filterData.status!==null){
            console.log(selectedJobType,selectedInterviewType,selectedStatusType)
         let tempData=allData.filter((item)=>item.interview_type===filterData.interview_type).filter((item)=>item.status===filterData.status)
         setData(tempData)
        }
        else  if(filterData.interview_type==null && filterData.job_title!==null && filterData.status!==null){
            console.log(selectedJobType,selectedInterviewType,selectedStatusType)
         let tempData=allData.filter((item)=>item.job_title===filterData.job_title).filter((item)=>item.status===filterData.status)
         setData(tempData)
        }
        // else  if(filterData.interview_type!==null && filterData.job_title!==null && filterData.status==null){
        //     console.log(selectedJobType,selectedInterviewType,selectedStatusType)
        //  let tempData=allData.filter((item)=>item.interview_type===filterData.interview_type).filter((item)=>item.job_title===filterData.job_title)
        //  setData(tempData)
        // }
      },[filterData])

 
    useEffect(()=>{
      
        if(selectedJobType=='All'){

             setFilterData({...filterData,job_title:null})
        }else{
            setFilterData({...filterData,job_title:selectedJobType})
        }
      
    
    },[selectedJobType])
    useEffect(()=>{
      
        if(selectedInterviewType=='All'){

             setFilterData({...filterData,interview_type:null})
        }else{
            setFilterData({...filterData,interview_type:selectedInterviewType})
        }
      
    
    },[selectedInterviewType])
    useEffect(()=>{
      
        if(selectedStatusType=='All'){

             setFilterData({...filterData,status:null})
        }else{
            setFilterData({...filterData,status:selectedStatusType})
        }
      
    
    },[selectedStatusType])



      const getJobData=()=>{
        Axios.get(`http://54.162.60.38/job/company/interview/?company_id=${btoa(companyID.toString())}`)
        .then(res=>{
            
            if(res.data.success){
                console.log(res.data.data)
                let temp=[]
                res.data.data.map((item)=>{
                        temp.push(item.job_title)
                })

                setJobData(temp)
             
            }
        }
        ).catch(err=>{
            console.log(err)
        })
    
    
        }
    return (
        <div>
            <div className="card manage-interview-card">
                <div className="row ml-5">
                    <div className="dropdown mt-3 mr-5">
                        <p className="dropdown-toggle cp fs-18 gray-2" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Interview Type
                        </p>
                       
                        <InterviewTypes/>
                    </div>
                   
                    <div className="row my-4 flex-col">
                        <div className="col-3 text-left my-auto"><p className="fs-18 gray-2 my-auto">Job</p></div>
                        <div className="col-4 text-left">
                        <div className="dropdown mt-2">
                            <div  className="dropdown-sort dropdown-toggle-custom" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span className="fs-15 d-inline-block text-truncate" style={{maxWidth:'150px'}}>{selectedJobType}</span>
                            </div>
                            <div className="dropdown-menu" style={{height:'50vh',overflowY:'scroll'}} aria-labelledby="dropdownMenuButton">
                                {
                                     jobData.length>0 && jobData.map(data=>{
                                        return(
                                            <button key={data} onClick={(e)=>{setSelectedJobType(e.target.value)}}   className="dropdown-item cp" value={data}  ><span className="d-inline-block text-truncate" style={{maxWidth:'150px'}}></span>{data}</button>
                                        )
                                    })
                                }
                               
                            </div>
                        </div>  
                        
                            </div>
                    </div>
                    <div className="dropdown mt-3 mr-4 ml-3">
                        <p className="dropdown-toggle cp fs-18 gray-2" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Approval Status
                        </p>
                        {/* <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a className="dropdown-item" href="#">Action</a>
                            <a className="dropdown-item" href="#">Another action</a>
                            <a className="dropdown-item" href="#">Something else here</a>
                        </div> */}
                        <ApprovalStatus/>
                    </div>


                </div>

            </div>
            <div className="position-card">
            {
                data.length ?  <ManageInterviewTable data={data}/> :'null'
            }
               
            </div>

        </div>
    )
}
