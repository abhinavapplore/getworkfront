import React, { useEffect, useState } from 'react'
import SelectSearch from 'react-select-search';
import Axios from 'axios';
import { useAlert } from 'react-alert';

export const Schedule = () => {
    const companyID=JSON.parse(localStorage.getItem('company')).company;
    const userID=JSON.parse(localStorage.getItem('user')).id;
    const [state,setState]=useState({
        "job_id": null,
        "company_id": companyID,
        "user_id": userID,
        "type": null,
        "start_date": null,
        "end_date": null,
        "link": null,
        "time_slot": null,
        "location": []
    
    })
    const alert=useAlert();
    const options=[{name:'option 1',value:'option 1'}]
    const[jobData,setJobData]=useState([])
    const[jobDataId,setJobDataId]=useState([])
    const [city,setCity]=useState()
    const [cityList,setCityList]=useState({})
    const [jobProfile,setJobProfile]=useState('All')
  
    useEffect(()=>{

       getJobData()
       getAllLocations()


    },[])

    useEffect(()=>{
        if(city){
            if(!state.location.includes(city)){
                const currentLocation={
                    city:city,
                    city_id:cityList[city]
                }
                setState({
                    ...state,
                    location:[...state.location,currentLocation]
                })
            }
        }
    },[city])


    const getJobData=()=>{
        Axios.get(`http://54.162.60.38/job/company/interview/?company_id=${btoa(companyID.toString())}`)
        .then(res=>{
            
            if(res.data.success){
                console.log(res.data.data)
                let temp=[]
                res.data.data.map((item)=>{
                        temp.push(item.job_title)
                })

                setJobData(temp)
                setJobDataId(res.data.data)
            }
        }
        ).catch(err=>{
            console.log(err)
        })
    
    
        }

        useEffect(()=>{
                    if(jobProfile!=='All'){
                        console.log(jobProfile)
                        let index=jobDataId.map((item)=>{console.log(item);return item.job_title}).indexOf(jobProfile)
                       console.log(index)
                        setState({...state,job_id:jobDataId[index].job_id})
                    }
        },[jobProfile])

        const getAllLocations=()=>{
            Axios.get('http://praveshtest.getwork.org/api/location/city/')
                .then(res=>{
                    let temp={}
                    // console.log('res from city api: ',res)
                    // console.log(res.data)
                    for(let i=0; i<res.data.length; i++){
                        temp[res.data[i].city]=res.data[i].city_id
                    }
                    setCityList({...temp})
                })
                .catch(err=>{
                    console.log(err)
                })
        }
            const removeLocation=(location)=>{
                if(state.Details.job_location.length>0 && state.Details.job_location.includes(location)){
                    let temp=state.Details.job_location.filter(loc=>loc.city_id!==location.city_id)
                    setState({
                        ...state,
                      location:temp
                        
                    })
                }
            }
            const cities=Object.keys(cityList).map(City=>{return{'name':City,'value':City}})
    
           const handleSubmit=()=>{
            if(state.type==='ONLINE'){
                if(state.job_id!==null && state.start_date!==null & state.end_date!==null && state.time_slot!==null && state.link!==null){
                    Axios.post(`http://54.162.60.38/job/company/interview/?company_id=${btoa(companyID.toString())}`,{
                        ...state
                    }).then((res)=>{
                        alert.success(res.data.message)
                    }).catch(err=>{
                       console.log(err)
                   })
                   
                }else{
                    alert.error('Fill all fields.')
                }
            }
            if(state.type==='OFFLINE'){
                if(state.job_id!==null && state.start_date!==null & state.end_date!==null && state.time_slot!==null && state.location.length>0){
                    Axios.post(`http://54.162.60.38/job/company/interview/?company_id=${btoa(companyID.toString())}`,{
                        ...state
                    }).then((res)=>{
                        alert.success(res.data.message)
                    }).catch(err=>{
                       console.log(err)
                   })
                }else{
                    alert.error('Fill all fields.')
                }
            }
           }
            
            const CityList= () =>( <SelectSearch  options={cities} value={city} onChange={setCity} placeholder="search city" search />)

    const JobProfiles=() =>( <SelectSearch value='job_id' name='job_title' style={{width:'340px'}} options={jobData} placeholder="Select" />  )
    return (
        <div>
            <div className="card schedule-interview-card">
                <card className="card-body">
                    <div className="row my-4">
                        <div className="col-3 text-left my-auto"><p className="fs-18 gray-2 my-auto">Job</p></div>
                        <div className="col-4 text-left">
                        <div className="dropdown ml-4">
                            <div  className="dropdown-sort dropdown-toggle-custom" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span className="fs-15 d-inline-block text-truncate" style={{maxWidth:'150px'}}>{jobProfile}</span>
                            </div>
                            <div className="dropdown-menu" style={{height:'50vh',overflowY:'scroll'}} aria-labelledby="dropdownMenuButton">
                                {
                                     jobData.length>0 && jobData.map(data=>{
                                        return(
                                            <button key={data} onClick={(e)=>{setJobProfile(e.target.value)}}   className="dropdown-item cp" value={data}  ><span className="d-inline-block text-truncate" style={{maxWidth:'150px'}}></span>{data}</button>
                                        )
                                    })
                                }
                               
                            </div>
                        </div>  
                        
                            </div>
                    </div>
                    <div className="row my-4">
                        <div className="col-3 text-left my-auto"><p className="fs-18 gray-2 my-auto">Interview Type</p></div>
                        <div className="col-9 text-left">
                        <div>
                            <div className="form-check form-check-inline">
                                <input className="form-check-input" type="radio" name="inlineRadioOptions" onChange={(e)=>{setState({...state,type:e.target.value})}} id="inlineRadio1" defaultValue="ONLINE" />
                                <label className="form-check-label" htmlFor="inlineRadio1">Online</label>
                            </div>
                            <div className="form-check form-check-inline">
                                <input className="form-check-input" type="radio" onChange={(e)=>{setState({...state,type:e.target.value})}} name="inlineRadioOptions" id="inlineRadio2" defaultValue="OFFLINE" />
                                <label className="form-check-label" htmlFor="inlineRadio2">Offline</label>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div className="row my-4">
                        <div className="col-3 text-left my-auto"><p className="fs-18 gray-2 my-auto">Start Date</p></div>
                        <div className="col-9 text-left"><input type="date" value={state.start_date} onChange={(e)=>setState({...state,start_date:e.target.value})} className="form-control input-secondary input-small"/></div>
                    </div>
                    <div className="row my-4">
                        <div className="col-3 text-left my-auto"><p className="fs-18 gray-2 my-auto">End Date</p></div>
                        <div className="col-9 text-left"><input type="date" value={state.end_date} onChange={(e)=>setState({...state,end_date:e.target.value})} className="form-control input-secondary input-small"/></div>
                    </div>
                    <div className="row my-4">
                        <div className="col-3 text-left my-auto"><p className="fs-18 gray-2 my-auto">Time Slots</p></div>
                        <div className="col-9 text-left"><input type="text" value={state.time_slot}  onChange={(e)=>setState({...state,time_slot:e.target.value})} className="form-control input-secondary input-small"/></div>
                    </div>
                    {
                        state.type==='ONLINE' && (
                            <div className="row my-4">
                        <div className="col-3 text-left my-auto"><p className="fs-18 gray-2 my-auto">Enter link for Interview</p></div>
                        <div className="col-9 text-left"><input type="url" value={state.link}  onChange={(e)=>setState({...state,link:e.target.value})} className="form-control input-secondary input-small"/></div>
                    </div>
                        )
                    }
                   
                    {
                        state.type==='OFFLINE' &&(
                            <div className="row my-4">
                        <div className="col-3 text-left my-auto"><p className="fs-18 gray-2 my-auto">Search College <br/> or Enter location</p></div>
                        <div className="col-3 text-left">
                        <CityList/>
                                        <div className="form-check mt-1 fs-18">
                                        <div className="selected-locations text-left my-2">
                                            {
                                                state.location.length>0 && state.location.map(location=>{
                                                    return(
                                                        <span className="mx-2 px-3 py-2 badge badge-dark">{location.city}<i onClick={()=>{removeLocation(location)}} className="ml-3 cp fas fa-times"></i></span>
                                                    )
                                                })
                                            }
                                        </div>
                                       
                                        </div>
                        </div>
                    </div>
                        )
                    }
                  
                    <div className="row my-4">
                        <div className="col-3"></div>
                        <div className="col-9 text-left"><button style={{width:'221px'}} onClick={handleSubmit} className="btn btn-main-lg">Request Interview</button></div>
                    </div>
                </card>
            </div>
        </div>
    )
}
