import React,{useState, useEffect} from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { Schedule } from './Schedule';
import { Manage } from './Manage';
import './Interview.css'

export const Interview = () => {
    return (
        <div className="interview-container">
        {/* <Tabs> */}
                {/* <TabList>
                <Tab style={{backgroundColor:'#E5E5E5'}}><span className="fs-14 fw-500 ">Schedule Interview</span></Tab>
                <Tab style={{backgroundColor:'#E5E5E5'}}><span className="fs-14  fw-500 ">Manage Interview</span></Tab>
                </TabList> */}

                            <ul style={{width:'100vw'}} className="interview-tabs nav nav-tabs pt-2" id="myTab" role="tablist" style={{background:'#fff', marginTop: '-10px'}}>
                                <li className="nav-item mt-3 ml-5 mr-3">
                                <a className="nav-link active" id="schedule-tab" data-toggle="tab" href="#schedule" role="tab" aria-controls="home" aria-selected="true">Schedule</a>
                                </li>
                                <li className="nav-item mt-3 mr-3">
                                <a className="nav-link" id="manage-tab" data-toggle="tab" href="#manage-interview" role="tab" aria-controls="profile" aria-selected="false">Manage</a>
                                </li>
                            </ul>

                            <div className="tab-content" id="myTabContent">
                                <div className="tab-pane fade show active" id="schedule" role="tabpanel" aria-labelledby="home-tab">
                                    <Schedule/>
                                </div>
                                <div className="tab-pane fade show active" id="manage-interview" role="tabpanel" aria-labelledby="home-tab">
                                    <Manage/>
                                </div>
                            </div>

    {/* <TabPanel><Schedule/></TabPanel>
    <TabPanel><Manage/></TabPanel> */}

  {/* </Tabs> */}
        </div>
    )
}
