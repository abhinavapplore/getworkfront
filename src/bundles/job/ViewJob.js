import React,{useState,useEffect} from 'react';
import {useLocation} from 'react-router-dom';
import GWLogo from '../../assets/images/getwork_new.png'
import Axios from 'axios';
import { Grid } from '@material-ui/core';

const JobViewStyles={
  Page:{
    height:'auto',
    background: '#E5E5E5'
  },
  Logo:{
    height:'100px',
    width:'100px'
  }

}
const ViewJob=()=>{
  const userType=localStorage.getItem('user_type');
  const location=useLocation();
  const [jobData,setJobData]=useState()
  useEffect(()=>{
    const urlParams=location.pathname.split('/');
    const jobID=urlParams[urlParams.length-1];
    
    console.log(userType)
    let jobUrl=`http://54.162.60.38/job/?job_id=${btoa(jobID.toString())}`;
    userType ? jobUrl+= `&user_type=${btoa(userType)}` : jobUrl+='';
    Axios.get(jobUrl)
      .then(res=>{
        console.log(res);
        if(res.data.success){
          setJobData(res.data.data)
        }
      })
      .catch(err=>{
        console.log(err);
      })

  },[])
  
  return(
    <>
    {
      jobData ? 
      <div className="job-view" style={JobViewStyles.Page}>
          <div className="row p-5">
            <div className="col-md-3 col-12">
              <div className="card gw-card w-100">
                <div className="card-body">
                  <div className="row">
                    <div className="col my-auto py-3">
                      <img style={JobViewStyles.Logo} src={GWLogo} alt=""/>
                    </div>
                  </div>
                  <div className="dropdown-divider"/>
                  <div className="text-left my-3">
                    <p className="fs-18 fw-500 mb-0">{jobData.job_role_name}</p>
                    <p className="fs-16 gray-2">{jobData.company[0].company_name}</p>
                  </div>
                  <div className="text-left my-3 gray-2 fs-14">
                    <p><i className="fas fa-globe mr-2 "/>{jobData.company[0].company_website}</p>
                    <p><i className="fas fa-briefcase mr-2"/>3rd Year Students</p>
                    <p><i className="fas fa-map-marker-alt mr-2"/>{
                      jobData.job_location && jobData.job_location && jobData.job_location.map(location=>{
                                return(<span className="mr-2">{location.city}</span>)
                              })
                    }</p>
                  </div>
                  <div className="row my-5 justify-content-center">
                    {
                      userType &&  <button className="mb-5 btn btn-login btn-main">
                        {userType=='student' ? <span>Apply Now </span>: <span>Edit Application</span>}
                        
                        
                        </button>

                    }
                  </div>
                </div>

              </div>
            </div>
            <div className="col-md-9 col-12">
              <div className="card gw-card w-100">
                <div className="card-body mx-5">
                    <div className="row my-2">
                      <div className="col-md-9 col-12 text-left my-auto">
                        <h4><b>{jobData.job_title}</b></h4>
                      </div>
                      <div className="col-md-3 col-12 text-left">
                        <h6>Compensation</h6>
                        <p className="fs-14 gray-2 m-0">₹ {jobData.ctc_max} - {jobData.ctc_min} LPA</p>
                      </div>
                    </div>
                    <div className="row my-2">
                    {
                      jobData.eligibility_criteria.skills.map((item)=>(
                        <span className="badge skill-chip  mx-2">{item.skill_name}</span>
                      ))
                    }
                      {/* <span className="badge skill-chip  mx-2">UX Design</span>
                      <span className="badge skill-chip  mx-2">Product Design</span>
                      <span className="badge skill-chip  mx-2">SAAS</span>
                      <span className="badge skill-chip  mx-2">Virtual Design</span> */}
                    </div>
                    <div className="text-left my-5">
                      <h6><b>Job Description</b></h6>
                     
                      <ul className="fs-14 ml-3 my-0 pl-0 gray-2">
                         {jobData.job_description}
                        </ul>
                    

                    </div>
                    <div className="row my-5 text-left pl-2">
                        <div className="col-md-4 col-12">
                            <div className="row pl-2"><h6><b>Office Location</b></h6></div>
                            <div className="row pl-2"><p className="gray-2 fs-14">{
                              jobData.job_location && jobData.job_location.map(location=>{
                                return(<span>{location.city}</span>)
                              })
                            }</p></div>
                        </div>
                        <div className="col-md-4 col-12">
                            <div className="row"><h6><b>Experience Required</b></h6></div>
                            <div className="row"><p className="gray-2 fs-14">3rd Year Students</p></div>
                        </div>
                        {/* <div className="col-md-4 col-12">
                            <div className="row"><h6><b>Working Days</b></h6></div>
                            <div className="row"><p className="gray-2 fs-14">{jobData.working_day}</p></div>
                        </div> */}
                    </div>
                    <div className="my-3 text-left">
                      <h6><b>About {jobData.company[0].company_name}</b></h6>
                      <p><span className="badge skill-chip my-3">{jobData.company[0].industry_name}</span></p>
                      <p className="fs-14 gray-2">
                            {jobData.company[0].company_description}
                       {/* <span className="link-text fw-500 color-blue"> see more</span> */}
                      </p>
                      <p className="fs-14 mb-1"><b>Company Size:</b> <span className="gray-2 fs-14 ml-2">{jobData.company[0].company_size}</span></p>
                      <p className="fs-14 mb-1"><b>Check out us @</b> <span className="gray-2 fs-14 ml-2">{jobData.company[0].company_website}</span></p>

                    </div>
                    {/* <div className="text-left my-5">
                      <h6><b>Ideal Candidate</b></h6>
                     
                      <ul className="fs-14 ml-3 my-0 pl-0 gray-2">
                          <li className="mb-1">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eveniet quaerat aut, magnam odio expedita repudiandae?</li>
                          <li className="mb-1">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eveniet quaerat aut, magnam odio expedita repudiandae?</li>
                          <li className="mb-1">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eveniet quaerat aut, magnam odio expedita repudiandae?</li>
                          <li className="mb-1">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eveniet quaerat aut, magnam odio expedita repudiandae?</li>
                         
                        </ul>
                    

                    </div> */}
                    {/* <div className="my-3 text-left">
                      <h6><b>Perks and Benefits</b></h6>
                    
                      <p className="fs-14 gray-2">
                      Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
                   
                      {jobData.perk}
                      <span className="link-text fw-500 color-blue"> see more</span>
                      </p>
                     

                    </div> */}
                    <Grid container>
                  <Grid xs={12} className='mg-top-10'>

               
                  {
                      jobData.documents!==undefined && (
                        <div>
                  <b>

                    <h6 className='viewjob_heading'>Documents required</h6>
                  </b>

                        <div className='viewjob_data'>
                        {
                          jobData.documents.map((item)=>(
                            <p>
                            • {' '} {item}
                            </p>
                          ))
                        }

                        {
                          jobData.documents.cover_letter==="required" &&( <>  • {' '} Cover letter</>)
                        }
                        </div>

                      
                      <div className='viewjob_data'>
                    
                        {
                          jobData.documents.resume==="required" &&( <>  • {' '} Resume</>)
                        }
                      </div>
                        
                     
                     
                      <div className='viewjob_data'>
                 

                        {
                          jobData.documents.transcipt==="required" && ( <>  • {' '} Transcipt</>)
                        }
                      </div>
                        </div>
                      )
                    }
                       
                    
                    
                  </Grid>
                </Grid>
                </div>
              </div>
            </div>
          </div>

      </div>
      :
      <div>Loading...</div>
    }
 
    </>
  )
}

export default ViewJob;