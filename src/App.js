import React, { useState } from "react";
import "./App.css";
import { transitions, positions, Provider as AlertProvider } from "react-alert";
import AlertTemplate from "react-alert-template-basic";
import IndexRoutes from "./routes/IndexRoutes";
import { environment as env } from "./environments/environment";
import Maintainence from "./bundles/common/components/UI/Maintainence";

// optional configuration
const options = {
  // you can also just use 'bottom center'
  position: positions.BOTTOM_RIGHT,
  timeout: 5000,
  offset: "30px",
  // you can also just use 'scale'
  transition: transitions.SCALE,
  containerStyle: {
    zIndex: 10001,
    fontSize: "14px",
    textTransform: "lowercase",
  },
};

const AuthGuard = () => {
  //decides which components to render
};
function App() {
  return env.maintainence ? (
    <Maintainence />
  ) : (
    <AlertProvider template={AlertTemplate} {...options}>
      <div className="App">
        <IndexRoutes />
      </div>
    </AlertProvider>
  );
}

export default App;
