// import {} from '../assets/images/google-logo.png';
export const FileUrl="http://praveshtest.getwork.org/media/"
export const EndPointPrefix="http://praveshtest.getwork.org/api";
export const emailRegex=/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
export const passwordRegex=/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/;
export const userTypeId='';
export const userType='';

export const StatusMap={
    "Applied": 1,
    "Shortlisted": 2,
    "Rejected":14,
    "Hired":13,
    "Submitted":5,
    "Under Review":8,
    "Interviewed": 7
    
}

export const collegeList = [
    {name: 'ABC Engineering College', value: 'ABC Engineering College'},
    {name: 'Galgotia Engineering College', value: 'Galgotia Engineering College'},
    {name:  'IP University', value:'IP University'},
    {name: 'MNNIT Allahbad', value:'MNNIT Allahbad'}

];
export const collegeLocationList = [
    {name: 'Delhi', value: 'Delhi'},
    {name: 'Mumbai', value: 'Mumbai'},
    {name:  'Jaipur', value:'Jaipur'},
    {name: 'Hyderabad', value:'Hyderabad'}

];
export const highestQualifications=[
    // {name:'10', value:'10', id:'1'},
    // {name:'12', value:'12', id:'2'},
    {name:'Graduation', value:'Graduation', id:'3'},
    {name:'Post Graduation', value:'Post Graduation', id:'4'},
    {name:'PhD', value:'PhD', id:'5'}
    ];

export const courseList = [
    {name: 'B.Tech', value: 'B.Tech'},
    {name: 'M.Tech', value: 'M.Tech'},
    {name:  'B.Sc', value:'B.sc'},
    {name: 'Ph.D', value:'Ph.D'}

];

export const branchList=[
    {name:'Electronics and Communication', value:'Electronics and Communication'},
    {name:'Computer Science', value:'Computer Science'},
    {name:'Civil Engineering', value:'Civil Engineering'},
    {name:'Business and Sales Management', value:'Business and Sales Management'}
]

export const years=[
    {name:'1',value:'1'},
    {name:'2',value:'2'},
    {name:'3',value:'3'},
    {name:'4',value:'4'},
    {name:'5',value:'5'}
];

export const Genders=[
    {name:'Male',value:'Male'},
    {name:'Female',value:'Female'},
    {name:'Decline to self identity',value:'Decline to self identity'}
]

export const Ethincities=[
    {name:'Indian Americans',value:'India Americans'},
    {name:'Japanese Americans',value:'Japanese Americans'},
    {name:'Korean Americans',value:'Korean Americans'},
    {name:'Caucasian race',value:'Caucasian race'},
    {name:'British Asian',value:'British Asian'},
    {name:'Dravidian Peoples',value:'Dravidian Peoples'},
    {name:'Indo-Caribbeans',value:'Indo-Caribbeans'},
    {name:'Han Chinese',value:'Han Chinese'},
    {name:'Han Chinese',value:'Han Chinese'},
    {name:'Turkik Peoples',value:'Chinese Americans'},
]

export const veteranStatuses=[
    {name:"I'm a protected veteran",value:"I'm a protected veteran"},
    {name:"I identify as one or more of the classifications of a protected veteran",value:"I identify as one or more of the classifications of a protected veteran"},
    {name:"I do not wish to answer",value:"I do not wish to answer"},
]

export const candidateTypeList=[
    'Agriculture, Food & Horticulture',
    'Arts & Design',
    'Business, Entrepreneurship & Human Resources',
    'Communications',
    'Computer Science, Information Systems & Technology',
    'Education',
    'Engineering',
    'General Studies',
    'Health Professions',
    'Humanities & Languages',
    'Life Science',
    'Math & Physical Sciences',
    'Natural Resources, Sustainability & Environmental Science',
    'Social Sciences'
]
export const jobTypes=['Internship','Part-Time','Full-Time']

export const companyCardsData=[
    {
        id:1,
        Name:'Google',
        Logo:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAT4AAACfCAMAAABX0UX9AAABlVBMVEX///8ZTcXeGgD/zwAzqTvdAAAARcP/zQAAQsP/0AAlpi+YwJvT3PMSSsQpqDMARMTu+e/4+v0ANLL09PPXAADRAAA/ac58qH+r2q////sOSMTIAADv8/u1w+ro6egAObsAP7ehtOXk6vgtXMqCm97K1fE3Y8z75uP51tP0vbfwoZn/99m3VU4AN70AKrAAO7i5dnHaNSb87OrtvwDZ297itgC9yuzg4ubf5fZ0kNpMcdCcr+QMQ6tNZKt1hbWSnb+nrsdggNQAPKbBxtJ+jbooTamdpbw5W7Skrs5XbKdziMSImctAW6Sbzp/X5tiT0JcWkSG3xLe92r9pp21Pp1ahuaJpea0tlzV8xoJRoFdhpWVmvW15rXtStlrN0+DqZ1vkMh7xrab3x8LtjYP/7a3/6JX/5YTnUUL/3l3Al5PRlI/UvLnDbmn/1zPNrDLOvojXz7K0V0//3FPV0cH/8sTHrlnbdW3/+N69MCXZSj7v0WjKKxr51dDKsrDnaWDm0pnVx5r/77m0Oy/jxFPXwXnXsyLQslDR34t3AAALFElEQVR4nO2c+2PSyBbHEzAh9DaNNNBdAhQojz4soi22tKUFim2tuute3du9r8VWap/qrW9dX3fXun/3zTwCedEoKqTX8/mJJAOZ+XLOzJkzkzAMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHxDRIuZmVhSJZbpHwn0ujanimJsbiglChKFZ1Pz6dhIr2t1OhhZyEoCryi8JOTzgsQrIsuKIi8IqXQGrNCB/jlRUsXKlxaXyhWV8uXFUp5XFVQ15FUF+3tdQTdTrCo8K0rLS7XhcPOkXL+8LCgsUVCpFntYP1cTTUuqSvzKqmy+Eq6sCdgCWVbJpcGF7chkJVWdRNkiHuZKghggy0qpWJdrdhpIojGCXym0uz67JlD94kvdrNfpII28k1+3Nz3CVaJfvty1Sp0a5pA0J6vHMNfioJ4taaSecv1k9RhmgWfzq12p0NnvCT/82JXbfR4LSD1xZdip3ByfqHShOszZG32En/7ajdt9Jhk0arCJmlO5mdxgpQvVYW7eunXr5zMqfX/rxu0+k0AKxST8hmO59XLbcfmL832fKt9Pjv7gAuYkNKAm3FXVH7B8Tp2xC+jH8TB/udf1MHIDyfdLr2vxEVSxfInZXtfDyI0BVb6fe10LZ/pxMKys97oeJv6uyjfwj17XwhlifEKl1/Uw8S8k3z97XQtHonjccJ3vEvlu9boWjiSx74prYeeiXeVXtesb+Heva+HIPPZdxTHo6zanQ74oi9OgkuvyAFi+73pdCycyJAnlupHjlMi3QEYOwXG+223QlHfgL72uhRNzPEkg13tdETOnQz4ycrB5kK8TAlnRpdaH5mx9Jvnk0bGx0ane1MeWQEp0ad9nkU8ev73JqWze3upZpcw05at8xo9M3dlqNBoT46NtroefbN/d2bm7fe7jQnOyjGyWb8ITjEzem4xwHo7zjH9Gbb8kmnydx31TW7tcMBQKIsPYnbB6Vvj3Pb9vetrn8/p8/p1zluvR/mS6Oj/UJJtdiqLzRvlGd7nIPkpIygchj8fDNTqt7pdFk4+/2uEPTGxykcn9w8P9e6pdeLhNs2Nt3/dOP/jP0eOj40s+v9fn3TNa4Eg6lTt/PicpooqSj8cHr9dwibBBvjEPFzmknw+DSD932F9giMgnrnX09bGLXPAhTeAPP+NQu3b1CeLwI6/vwWPijYF3036v1+d/oru+IOauVwqztfU8qoZSrtVqVF6DfKMeLvSm+aUtdB+PO0aQqrb1ItrBl++ozTpoHTawfputhp1T7e1p64ePkH5e37Z2HJ0X8mUi10wC/Ydsa/uRQb5djpts3UbedI/70rCZFTqIXO6o3fgF/Qlqf9rhc1WtB/r9RE98XqQftb/oPM83t3vM4IXm1p+ol2+C8wQPdT+zi+7yyhXLIEm6caWDzm9UbYWpEReRW3FvyUH4vtd/ybj+9BvR7zk+qPJiqZVlvIZmj3xVOzzbkm9KtbaI7j6qzXORh3o9e0c/lU/MfvK2s5eq8R0YT93B3RL3Gh/sqVo9NV4PezF76HNMYJVF3TWc+slp2y918o2rPxrRSsnju1zoxUH3FkxPhias1HlH5sRy/SYyuFUhsw0Q83uJPp5DdnZkun6XmB+KX9Qx37C6h3c6KJoT6ORDXcIrcnYKD/RvXOG3BG3sUKonFCpWeYnXIQxWsFIRc0PGsfkFx9SPe16vf/qx6fpzYn6/MUxGMkWbeNVATFEn0MmHRgos39hbLhS54BbDI8S0XXu5tlvni3OSpChINlpUXK4xr5FQEbPHy8R7G2Tc8E9blt73kHr+6QCTVn9Muqa/hLMX2pqLTj58IyY8vht0l+FhAnGqCT9nX6B4dXl5ZW1xfWPpclnbX7pKo5RXltJvsX6q9277kEyW1v6ueS+yesUwXiFBbeTD/0joYPLhPdf0eHq00IWV7Dd9y8OyHKbBrLa9dJZEDzbyTWD5QjI2Mxv5nmP5vO8YFK8r8/pLOHZpIx/uDlxJUfNeY2PsCNCSg8OkR2oNiE3GiHxTzH17+Rg/7fzmRXOwjoIAsUTd3eS85iHeRTTNT0g6lGzKJzOyh1qZiVFyfpTIRCM8PY9w5/eeDFnCjO5KkUc7NKmdm4YOz8uOm/e1GdHkY+MOz2xY5LP61BRubGiWymcOXBhmx0fkI3GKfrhH1idoe1d18uHuNNguG9Z7FjT3FR1mvjr5GCyfTeID94mRYSrfO8v1bSzfMTOCb5rT/WExiRVXtISMTj7cnXLPOm/f1yar5Q2U+RPnHnr5SIBsbRSW74WM+z6v973lOpbP+1/aZ4i6/nZOEVsbXHXyjZJgyLWDB+51aPRyon56+RpkjLCUwfP5e6qTYvmmLQlmEtCo4XRURIOHkNYuZFT1Ks1iOvmY28ZMhPuYEZr6DZ3gv3r57nD2nR+yyuAbXYBnAst3Cf1JdfyckqbfDKuUdAsuevlec/pMhBtJ5jT9lFT7ya9ePtr5mb13ykPzMGEtQjGBhg7fMf5YX0GP4khDyf7+WJVXDA806eWjsbibu79rcVEbP6SFdoUM8pEA2Zz1RXEft48+7RDzM3vvI68ujXVlJS9IQk7gJUUsGeYUBvnkEPmvbutv9rqTZn41Wo/8sUK2jQEa5LMfPFCX+AL3n2Eyv9gxXkdGSY0PHxYqq+VVtOBiWqzC8jV3WI1R/bgGjV+mtibdtZOdqZekpgMLVXsBDfK9DuqSexSUSNcWdbZ9NpGzetL/wdTyIu4DjSvNP6Kd4WfO3DxLrHcswlEBd581Go3b3B8uU09t+tV80wAVfihpHkMCmTni3cp5Mtk4sLrvM84T2tcO3mP97uvdF2egzVksnPQxZl/Cv/YNIPoGzvxyE1duMkju5kEropF7bsu8IAqLeb5lgcJQOtYfjQYQ0WJsjhUUUeHzy2sbqzS6OcBrhputCYHaH0b2W7/3Aenne6S7w56NenTc5/UTuJvfNdG6xMPJSCiICL1yWc6vRf1qQmiaoMhLkpLKDg1lUywv8YqUX75+uVbQ/fHjIby4S+ceY2857pU+/yy/96Gu7pFmf+E9r/eDRT06ARF5pyn38OGb/f39w4IbLU9juLKYF3i0bI3bJIr4nRpCPl9aL+vfcEBLT2IBLza2xid2Vae6YOqS3k17/X6v/y7qAM/d9Xunn9r1WWm6ybBNxvGUIdfK62ulUgKzXFpZ3ChXrMpRChdeEKcKRf7Yt2ojH1/y4Vmu3++bvvTUanqIqLbTZr6T1WY3EpZnC/V6vVCYHZYddvYECsipDg/bjIXy0fGfDz58ePDn8ZG1RDSDdrhkWYXuFcn+v+j3RQnIcsA6mY4mqykhlxssrS2u0xclKJ++XvqNEk2LgpBYXK0T6y6TqEk6acEPaJJJSWJ+vdbqGOolrF985oQvAZSYorDa/iCKvCaxH7PiAjAZQWR5y+tg5lHQnnNzWOcSsqLdmyfwZk337VJ3HehZJsXmXURo9gvyOYLWOey2U6P4Oe7WCa17QGvk0hWbC0MizYYB7cEP49huyBwSDXv+AFuQ9YlZ63nVefOue0DHfeDNVDZ7QmKSsui2x9pdSBEv7EnmF6FGs8oyDBwfAX4VoCgYX0MZnZec36MFIKoCSfDNNDNU0VgqvgLqfSTXcgpeUUlV08lYLJmupuKDG65bPnMv9cWEgNYEFF4QhHg8XtqAbu+TmL2yvlZK5POJ5ZXrSxWwvE9HHp4tFAqzTusBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwLfF/wD4HlKPY0OuHgAAAABJRU5ErkJggg==',
        Indsutry:'Software and Tech',
        Address: 'Mountain View, California, United States'
    },
    {
        id:1,
        Name:'Apple',
        Logo:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAhFBMVEX///8AAADv7+/r6+v6+vooKCjg4ODMzMzQ0ND19fX8/Pxvb2+xsbG0tLTy8vLJycl1dXXW1tabm5s8PDympqaPj48SEhJPT08dHR03NzdERETAwMDm5uaWlpaJiYmDg4NiYmJaWlovLy8jIyNeXl4ODg4XFxdxcXF8fHxSUlJCQkKioqKDjmFtAAAJTElEQVR4nO1d51rrOhDEiZ1eSK+Qzgnw/u93CRwgiR1Ls7tj53zX8xtkTyyttsxKDw8FChQoUKBAgQIFChQoUKBAgQIF/kcoVXrVbmvYfxo9j576w1a32quU8n4pI/TaT+vBNEjCdLB+avfyfkEF6rV+8yWR2iVemv1aPe+XhVHqrl49yP3iddX9h2ZtZfg6huh9Yfw6rOT96j5Y9iMBu29E/WXeBNJRfh8o6H1h8F7Om8ZNVJtqel9oVvOmkojh1ojfCdth3nSuURoZ0vvC6J5sa/lozu+E470syNIzhd8Jz3fxHe3n5zlGedN7aB+oBIPg0M6VX29O5nfCJEfnnLcAL/GcE79qckzEwDQPFyDk7BC3sAqzJtibZUowCGYZr8Z+xvxO6GfIr77JgWAQ7DPLBPQ0EaAGUUYRcjsnfidksv0/5UgwCJ74BB9zJRgEj2R+4SRngkHw2mASrOvTMHoMiCa1lJcRvUREixrL2Tmi6ZiSgv/SvRD8oEj5ih3qFI2wLHnUsSdYpxHcrhYfs24N/pO9uWHFEo+1r/FBhsHMmuCeQi9Y/SwoOF++tiXICXf3ZzYR/YYfv44lQU44uDh/xAb/f8OAcWHN7YTJxaYWStZ514pg2ZzdB5oWzzDaFkU/rwvHq4fURKPMbLxwRrx0TVBaGzCJpRgh/Sb2FGny3CDoZyzCccwhkT9F74S/GTL7Rjz1KS9gzbUEGVmZ2CJ8eFCMpszcVMxo/eIQd5pbmvF0KUZG9SwuQmio4paJhuDQitUZoniN5V03okK3UbLhdIm4N6k21/Jw2EoBdIG4p4Vp/BLQTHh3L8g8KQf2sccYBC41IUNKkT7mhHQNBhUaG04F5nrN9ExGlTlvlOThjkIwNqwXGDtFzChYTNFPtAQMJTJfNy41TnbZkZd7+YQXu+HSspAFb/s6R8rnReq2grgIDfdZpez37+9nrqdCzSlLsHaapeVaX+3GxAHuiRR35oTxNmKVsDDHBs9Ax/CynQ1mHqs5mhz7rdb785+JUii+Rgh2dM8K9k/Vyqfz0ij3huvbEtRdc3Eevy5rI415RUIM1T61Xlw9qr74k/iHx2pCiaws70h5j492EzsxvRsS+7B1nVaet2+a965QDxH5E5T7Uqvbhcve8dfERM/pyZW27Dv6q1ClWe5J+nvXF48nY7J97DrFovWV5PkJSbwbwwtXgkdeL+yUO37Oh6TeFfmWvoWT1FipvBT8zr6vIJqkO/O2uhJe8/Kcpg1JC8WOoHDp+PTYXmDstwIkYfcLRaWEZ9z9xOCSUgVJvAubG78ihsA9XLhHlQHN2HqpbAQ+qfc+hALfFX18U3wjMpcn/UWJNJvw4FuacXZAlGj0aZGCo3tx1SAdMr/DoyRcgrOInA56YZZh7N6X4bnBMTPi4rN7R4TzpJRPKJfruvOmqAoxLo2xgDxP5J5SG3BEymavUC44f/EOmOuLi38MUJcTDKauF0Jr6pQmHZVe1xUDoKaUMUmXGoJOY4quAIYlFeVofuCqJIKh04BAsK5L+7sCKHAJMJahUnPt2i422HCS4rILSsWua7sAsz+EkwBEaaIzuGI5cHiCodFZ0o8d2jE+OJw9QX312ZThgcBQKVR0MQTViIz8hbrAn56qAZ02lXj1BtSCyHS3DVzmawJDdZdcuvUDQ2tGikYtIUjPToOO9x8CQ/UsTXe9QYaMb6iWSpkyXBMYqneLdIbgOmTYUvWOn74OQVvK2A+1XpvDloI7vssHlKChZZieEw7B0QgM1cbUUQcGRyMcbaBrggqMPW/PqjIGbQuNY3gwm07JB+u8Gpf2C9QGImI5b+g6FFw7GLjMObVDlcLd5UmCPhMjm6j8iK4zXUGfyaMgKYEmre8qr6G1Zc55jaGiF8J1lATqM1FMjWqeutJ/aNvomsNQse271k0DlGI4y3VSSKXmbjEG6hXSDk4VUnTvX6huj3dUo2yiut8HNaacHfETInPjPpUHDkCJB8OHgkDKo5KCyuWo9zQsUCX01mNQ9Hd7YzJ8aIDXZvg4yvACJx/ui5WEfboQYUUZ+eBbbE55/dwoQ6PjqG4AlCx7jQk79tRD77FF4yeUhLUQayZDrI/NL6uCq8qIt1CBVsHTS96gDE3PaLwEZmd8paD4YQO0qxnAzKKvvAdPWNLuuwGL+t69SRuYIiP3/YESJu/x1yvjcQvpI4KZP38NmqAxiNK7hqZUgHwDLp6jaIWTm9xN3kEQexKyGaiHDL0C3pBPiPXB5D7Qjv8gypGYZ07Rd8C0rnW4BdfcdyuDQtAX0O0QqFpebRmiVUQ0TJUUYk0vZILXCbxfSdTWhvkM+BfGW+gkspatXbQPF0kFLofkI5rt+3DHvKQLUnR4qVF3QhV+sCgvLZIjm7g2uF8suwMSDF2+cLCIo+Bma2mxXaSDHOitDV6tEFcWRFcGTLQU8dY1uURS1hK/SaFYqnwizbQLlr8iYSvTX02SPcT64nH+t/HuMGgubiwdwSa1lhOUHne9S3BuqrFfa59QzCxLzmtUGTepJOLKRe0k18ii4eXLNUSHpiovgNgIKc7aP1M1rKZUQs7O3SsPRZZNeyCAXOw5bba7terife/IF0wnz63ForWSnieqzoFR7gkyhIHCFUx4ZQwLX593pZwBvE/ZS4XRoeIUGMXceV4AnA6z64F1hxzwYFi3zP+O3CRYdl3VlYcYU7A1rcuWOafsazA2Lnfdn0E1LwXdm29D6Na5L4qUdqR72hbNNsJLaNvm7MA4NOYT9/IVSV/whPtYi7TzYE8wu/lGAbMbZJPRU54ApMaB1uDxDcEp25aYEVsDvtEwuN1DjDVVj/wD20t+EJAl5b/Iy6RSjeglysI7NlQYZLAEz2B+XZMTsiKoAtVst40xtSEgGZK2KzGaNJ11KrpZ5Ta2ZDfmNsJsVuMonw/4hQrhArUrvJI7x5zocpP+UW4T9AxDHseI2r7pj3DI2TkOwzwX4CUYHMd3xO8TizdTfm8Z+qDeqCkPAD7DI+lSEDVKMq3BFQbDbF1sEJWR4mbBD+xGeW9/HuiNpEtyPqInYaywbDfRg9WnzTaxFZWBcNk6+jZtvBxbyzvbGnzRqLVW893tvfKwm69atWyyS0yUKrV2f9Xcbybzt9ls9jafbPbNVb9dq9y10ZShEYbhv//JChQoUKBAgX8O/wF0hKES3RqHNgAAAABJRU5ErkJggg==',
        Indsutry:'Software and Tech',
        Address:'Cupertino, California, United States'
    },
    {
        id:1,
        Name:'JP Morgan Chase',
        Logo:'https://media-exp1.licdn.com/dms/image/C4E0BAQGvyWbgDy4E3A/company-logo_200_200/0?e=2159024400&v=beta&t=XBaDDrS3L-Ygv8SOzchzLhqnKbaYoj7Qxv-12dbdqDQ',
        Indsutry:'Investment Banking',
        Address:' New York, New York, United States'
    },
    {
        id:1,
        Name:'Adobe',
        Logo:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAA/1BMVEX+/v7////tIjUqJCYAAADtAAD4xsjrABf72+HrJTPmKDnuIDL8//8kHiAgGBp1dXVSTlA4ODjthYvuEiv28u/m5uZMTEwaFhcKAAD4+PjV1dUjHB6kpKTCwsL//P73//8PAAfGxsbe3t4WDBCHh4dra2tWVlaysrLsACDs7OxBPD4cFBeRkZGBgYEtLS0lJSVgYGDrAAzwz9HpO0erq6vfAArmBCnotrfpQk/yACXslJjytLjnanPqXGPwycbrp6ny3+HsdoHnJSXoIkDrRVTtjprqXmjqforzw8754uLrm6PyABTwqKbpT1zpg5H0AADuPlHooKvnfn3sZm/iiIajLUx6AAANc0lEQVR4nO2da3vaxhKAmSBCbK2cEHETYAgQY3FzjO2mceyT2GmSpklzTlv3//+Wo71IAl1GCxZI6NF86NOI1bKvZnZmdnaFC5B1KSQ9gK1LTrj/khPuv+SE+y854f5LTrj/khPuv+SE+y854f5LTrj/khPuv+SE+y+BhIW9lcwT5jrcf8JM6BAdXHyEJOiuEMHHu15H1n8NtMe4COHZL4eS8vZXdEAXZdmOzG/WWAsno5PJaXiPgTCbEZZUSTF7aE+9d7IdlZ8SKFT6/X5h3gkdYJyEB0+k5Po9wXu6+SHX0ZPyU4CrzmB0XIGz8Mm4c0LVrHZxRENfg3BUG5z0T2DeChvh7nWo30Z1Rr5IIlLC89pgNJulinBxEQVIxgt5wtPZ4Dko8GYtV7NVQvUdie7rP6osYQFGnRq0Rsfp8TTmDT4JWV8f5HwNI4TZaHQeaqO7JzxYRKvQstNDKSVSQopQ2FHElyL89FIm/4OPpjzhrrI2SULzToqwah6uQYj3tVtC9SdET0Pa271MwJAi3K2VquUPEtPQEnJXiotwtzr8dCkHaCFeSviaFBKa36Vs1ALs3ZRjItytlZYMSRV2CfkteiamT4f6vXypgLy/3kPC8oWkkdL+7qLNNHVWqn9ep9wDD5G+JnU6/HGzTj2LXESuMNJGqD4x1gC0ECOT07RZqflFfhayHiMDRsp0eFi6W5MwspqRMkL1q1xK6gr5PSJgpMxKy1HVC690yR8RviZdOlQve+vuDBDyc48ID8yPZE0jtfr8FVdiuqzUNNYGLBR6+FI/VTrU/9lk96qHJ6epIixJVS+80q2WsWpGmqxU/byBjVqdklssr0mTDsvohlq44MlpmgifyFYvvL2Sd4gSU2Sl5vtNd8kJlpymSIfl6kbTkHZL9oHwQH0g3Q0JC+RleExMjZWqEdULvCiPVDNSo0P1EltVEEALcF3yNdTXpIYQr1504SuWkxPyLbT8nRor1dF9e/Ks9A1dOZLLsI7TokP9JToLyf31V3TZAX+HJadpIfzlD3T8VfNJ+Q7t2ggz05RYqfoVd6Tfr6O1HFKwSYkOF+gwSOHQuq2ME16EBIyUEL7FR/+Ujh73tqHJaTqsNGJDDT7TwauXuK+5CV5hpEOHn6pYwgZirzci6zHUQCWmglC9JQVk8PD7J9ZMv0UjInl/HYSYCitdoNULUjXFyMtoO6iWNibctg7fYWdJgfxpLxzK+AqSBJ4DSwNh+QatA/d+cwZeRo/Vkg9BASMNVlrGFoZdHirsZ4F+R+BWWwp0qOPHgeHnJ6ep+hlX4p+mHzEFhLj/WD0WVHqG6JsUqmZA94lbqY6npL3fl/JN1bzH11AByWnyOlw8xVcVK2NWdbRcZS0jU0iIn/HyHrE0v+Mz8bNvIiZupeZHPMZ5tiTUt3hJ6qkvYCSuwxK6oQa+EPcDHTAQn40kTKjq93io+I/Xdag/0RUGee+tnCZtpT/usAFDze85opJYr9KT1uFPfFr91+/9ca0Tcuu5JWHCH+iGGjF0f46i6mhxmFz8L01Wqh5i68IC+RiQSqvm34Ctl73eN1EdquZ71G30Ak85q096aEXAs9WWrJUuqqjrD9nbXURUM8yD5QeTqJXqD7if8YUKoUQ8kyUvV25LVIe4MsJfNohYjfyxYqZJEqqXPTx4hx071NG3hmC1mpGglaplvMQbfq5SfYunek+X52+COjzAAxt2NtZEnw2B5WeTICFua13shRj1MGIjZyk5TdBK8TNeELbTwgR/i5ZuxqVAhxFnvMgDdrxZv5WuZiRGqJcjqhf4e2nlsWw1IzErPTjEysBd+IIf38YPUJHeO93+osR0aH5H/YyB8rGcHUW8MRMnxM94oSfV+P3fUF/TW9ieOCkr1e/xmtlfUa/C6H/10P2cf20rT0iHB6Vnm4cKLniwcZPThAj1SzxkP5R1n3iVGFHNsJPThKy0jP5oQte4f+kXXxENX1zaldNkdKiqeK0lUD57ynS4NwY760uGcN031Ni9f3vmZlSRR7RPxkoXaEYScq/vXFdEVmSsQRi3DtWHTc47g7cSihdbu4TvyyVCGPn7OoHiP9eFBowuebZIykrVA7R6ET6IS2/A+AdfnrCttiR0eP1xs0P5vp9rUfE3wXiVIAHCQxMNZMjdvl0XE31WhJ0DS8BKTXxDDRuyz9ccohvI7K22BHS4QI8DI0K6FwtP1McrrtYy+jABQvXnpiq07v/Ns+TQv6KvocCDvnsrjdimxsW/GVW6Q9tbAWbnOlT1Dd9QY/dXven3NZ7/9S7VnRN++rI5oJWSeytwEful8LFs7tpKN39DjYr/ZcpF1NbAznV4u/kbamwk3ld+o7Z3XpZ2TbjG7+sE9vDdtxDGeoTCnRRhjFZ6EN0I78FXKdbRhUqXXO5Wh9/+fBwgD3GegDFG+iTwq8SvTMZIeHG3BkzwkL+VvPIvvkvzYZdWWgD8948lhJCqR4wqfsOGgAn+Yvk2vjZdhFuRzBNmX4fZJ8ytNCfcA8k8YfZ1mH3C3Epzwj2QzBNmX4e7J4zuL+Zv3AIhegvUmCAFGNGiFhPiNnRYQ4YIoDQtUU4QwgltoiB/OWYtiZ8Q+oqQoCOGFmHRkqPnCOGLttVCq8RlpvETzhpFJs0gJUoRanESbkGH521BGPT3ezJACIWikOEsFYSxWykMFEHYHqWCMH4dnjbp+JiZZpRwfmQNb0QxlICglwUrnVjje3VFvY3STwNh3DqE8ZCytShGY9XViI78hJ4vWCIM+ub1HvgWCFuUQGGabJ+vYNROr/p+HVpdD46vTlvuVziEAOPjq+PxyndbF1tXHdr8MYiPIuxQHb4RQdHNaqA1UZSmopx6CAE6DZ4BvbYPjDmExgn74GTpwAJU51Y3w6bS6MgOKXYdUjRLebPpiquBU4XnAUp/uEwI40rTDp9H4g/gCcKJUZyyD6ZFBxH6w6lorpzjv+y6PcImj/UsZih2VsNtd0kEIYwbmntR4YicsDg5PxLXp+fi6+F4qZshkrxHIT6CEGp0DK9OOZKd1YDRXgUUhABn7ANNfDxkyhKEmnuPeFK8c6vZkF9F/kQuDvgowmOmuwGM6WBsVwOzV2xMmiJs1SacMROdVk6K7PpwvkRo3a40+f9pdf486rRVu9jp8MsNqVHFTTin82Q6Ftmp+Bu2wAGP6q3WaOoSgsF0YmV3YJyxy6/oxLUJG6NWa8It1Xpkjo1qBvDnFxhv5RAfQ1jXqJOw2jNnqrAFlLViZEOjl1kYsQk7jFyxbFM0GXZcQt68ornXad/cYrnHxtIGFPARhGBQs2tbPgDmDWcCweupM5u4CxKEo7ZtylA4o8Nv113CZt/V2xHtsWar0E7vtYqMO42ZkDsY64nDVdN2NSCGrDDamhMPhZEy/RTg+RFvAzZhowr2I6MB1urxlXh69LJma38jxEcQdppifnBWtoACgw1ee8HGVnAJeQjhMQJmQ3vCreSlXM+UnD+D6YwPhxk7m54bAD6GkM++saOgV5SQ+4Xpa+4QXUJusGKuHjdt57FKKHQ7FlNy2BmzUh0nRP5I7nYIrfjGTYrKsGjDDpatcYmw4zRxcgIrknoIZ1PxGPiNxSnP8TR5ZxorIZtk2ou+JS3u+RyDDSDkg28sE7IpHEQ4EOa9IlKE8eqQm5rWdJ6yNW0c9Vx5CV9LEQ6FDo1UEPLhLAkLHKE6HMrrcGwTthuuSM3DeK10onkIaVYjNw95yG/65qGIpYZ948nsNRX23zl2OBMB3JjQdgYrhjQGOV8q/tEK8aXAVy30Oa09qhgJuaVpR0zatrsD4w27/MJL2F/KLl2f6SGs88UiiP87mq9dvomTkJtdW9zBdEizGhiJnMYNjv6c5kREdidraxhuTjOd266XFjceDbg54fNGkeeQ7F+Mi2adoXkp00r7BctL3xTte+28lDd3p6dQedBewY4IoSCyDkH4mu3QnHXBXltUaMeVpbWFEwnsDJs/BP/aosmyJLGoqq3z1IMRNyUcL+WZzrKgSQckHP2k368vrw/5DcVKDQZHtud114fTev+4wq6LdFvEImU2GI/Hg6uRnDbj1KFAEi5cBAlqbPbYtGbTXr7zMbMlVlEbinIUr0s4MUez1/giObdrIQ2l0RgqTSVo52e7hIzDqVWD0XBcjcEJXeH1DcvLijIFVxovenDCs4rTeGrP7NV61ko5dj3ETQnr2sr3uq5mqUj2RmmxRfLIri25OcKQu0lrMcwM1sFpv7EXulY3yyWt4Y51aOmsrWlas+MQzofWhTZf+F7xsU2VFmt2VAeBWBGWeKQ85+tZMIq0wcS6hX3QrLiZCwzqylSofdpUpF46jpFwUKlbUnFyRTie0AtnIu8csfq19Y8RvWrXcwGuJryybRfqwaCf158DnGr0g1l36bstvzw/Y+0n81OZnC0YcVMr9bZcuWClNrVaYalzt5FBz2249zmfA6x+4HxqGIF7NtKA2zoxFNpLeP8h19cazg4Jk5LME2Zfh9knzK00J9wDyTxh9nWYfcLcSnPCPZDME2Zfh9knzK00J9wDyTxh9nWYfcLcSnPCPZDME2Zfh9knzK00J9wDyTxh9nWYfcIgmv8DqflpPT9gVW8AAAAASUVORK5CYII=',
        Indsutry:'Computer Software',
        Address:'Cupertino, California, United States'
    },
]

const JobRoles=[
{
    "Accountant":[
                "Accountant",
                "Accountant Intern",
                "Budget Analyst",
            ]
},
 { "Actors & Models":[
        "Actor",
        "Actor Intern",
        "Actor Intern",
        "Actor Understudy",
    ]
 },
 { "Actuaries":[
        "Actuaries Intern",
        "Actuaries Mathematician",
        "Pricing Actuary",
    ]
 },
    { "Administrative Services Managers":[
        "Administrative Services Managers Intern",
        "Business Office Unit",
        "University Registrar",
    ]
}
]

export const TechnicalSkills=[
    'NodeJS','AngularJS','Git','Account Management','Business Development','Sales',
    'React','Flutter','Android App Development','iOS App Development','Swift',
    'Scala','Marketing','Finance'
]

export const InterpersonalSkills=[
    'Tech Savy','Collaborative','Work Ethic','Punctual','Collected','Tenacious',
    'Teamwork','Responsibility','Adaptability','Dependability','Leadership',
    'Flexibility','Patience'
]

export const Boards=[
    {'name':'CBSE'},
    {'name':'ICSE'},
    {'name':'MP Board'}
]