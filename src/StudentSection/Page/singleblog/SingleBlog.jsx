import React, { useEffect, useState } from 'react'
import Layout from '../../Layout/Layout'
import { Grid } from '@material-ui/core'
import {httpRequest} from '../../../utils/httpRequest';
import baseUrl from '../../../common/CONSTANT'
import BlogCard from '../../Components/Blog/BlogCard';

export default function SingleBlog({location}) {
    const {data}=location
    const [blogData,setBlogData]=useState([]);
    console.log(data)
    useEffect(() => {


       
        GetData(baseUrl.pravesh.BASE_URL,'api/blog/all/',null,setBlogData)
       
       
  
  
       
      },[])
  
      const GetData=async(baseUrl,endPoint,body,updateState)=>{
  
        let res = await httpRequest(baseUrl,endPoint,body)
        updateState(res.data.results)
  
      }
  
    return (
        <Layout>

        <div className='single-blog'>
            <Grid container justify='flex-start'>
                <Grid xs={12} className='text-left'>
                    <span className='fs-12'>

                    content 

                    </span>
                    <span className='fs-12 fw-700 '> {data.title}</span>
                </Grid>
            </Grid>
            <Grid container>
                <Grid xs={12} className='text-left'>
                    <h2 className='fw-700 text-left'>
                            {
                                data.title
                            }
                    </h2>
                </Grid>
            </Grid>
            <Grid container>
                <Grid xs={12}>
                <div className='flex align-item-center fs-12 mg-top-10 '>
    by {data.author.first_name} {' '} {data.author.last_name}
</div>
                </Grid>
            </Grid>
            <Grid container className='mg-top-10'>
                <Grid xs={12}>
                    {
                        data.image !==null && <img src={data.image} alt='blog'/>
                    }
                </Grid>
            </Grid>
            <Grid container>
                <Grid xs={12} className='fs-20 fw-300 text-gray text-left'>
                    {
                        data.body
                    }
                </Grid>
            </Grid>
            <Grid container>
               <Grid xs={12}>
               <div className='flex flex-col align-item-baseline'>

               <h2 className='fs-20 fw-700 float-left text-green mg-top-10 heading-underline'>
               Blog
            </h2>  
      
            
               </div>
            <BlogCard data={blogData} /> 
               </Grid>
           </Grid>
        </div>
        </Layout>
    )
}
