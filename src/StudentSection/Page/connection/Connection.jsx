import React, { useState, useEffect } from 'react'
import { Grid } from '@material-ui/core'
import ConnectionCard from '../../Components/Connections/ConnectionCard'
import baseUrl from '../../../common/CONSTANT'
import {httpRequest} from '../../../utils/httpRequest';
import Layout from '../../Layout/Layout';
export default function Connection() {
    const [connectionData,setConnectionData]=useState([]);
    useEffect(() => {
        const token=localStorage.getItem('gw_token');
        const data=JSON.parse(localStorage.getItem('user_details'))
    console.log(data)
       
    
        GetData(baseUrl.pravesh.BASE_URL,'api/shared/user_connections/?user_type=Student',{headers:token},setConnectionData)
        
       
  
  
       
      },[])
      const GetData=async(baseUrl,endPoint,body,updateState)=>{

        let res = await httpRequest(baseUrl,endPoint,body)
        updateState(res.data.results)
       
  
      }
    return (
        <div>
        <Layout >

            <Grid container>
               <Grid xs={12}>
               <div className='flex flex-col align-item-baseline'>

               <h2 className='fs-20 fw-700 float-left text-green mg-top-10 heading-underline'>
               Connections
            </h2>  
      
            
               </div>
            <ConnectionCard data={connectionData} /> 
               </Grid>
           </Grid>
        </Layout>
        </div>
    )
}
