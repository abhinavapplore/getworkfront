import React, { useState, useEffect } from 'react'
import { Card, CardContent, Grid, Avatar } from '@material-ui/core'
import GetAppIcon from '@material-ui/icons/GetApp';
import { Link } from 'react-router-dom';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import {httpRequestGet} from '../../../utils/httpRequest';
import baseUrl from '../../../common/CONSTANT'
import CloseIcon from '@material-ui/icons/Close';
import Layout from '../../Layout/Layout'
import Download from '../../../assets/svg/bx_bx-download.svg'
import { BreakpointProvider, Breakpoint } from 'react-socks';
import PublicProfile from '../../Components/PublicProfile/Index'

export default function Index() {
    const[project,setProjectData]=useState([])
    const[profile,setProfileData]=useState(null)
    const[education,setEducationData]=useState([])
    const[exp,setExpData]=useState([])
    useEffect(()=>{
        const token=localStorage.getItem('gw_token');
        GetData(baseUrl.pravesh.BASE_URL,'api/education/user_education',{headers:token},setEducationData)
        GetData(baseUrl.pravesh.BASE_URL,'api/company/work_experience',{headers:token},setExpData)
        // GetData(baseUrl.pravesh.BASE_URL,'api/education/user_skill',{headers:token},setSkillsData)
        // GetData(baseUrl.pravesh.BASE_URL,'api/education/student_details',{headers:token},setAboutData)
       
        GetData(baseUrl.pravesh.BASE_URL,'api/education/user_project',{headers:token},setProjectData)
        GetData(baseUrl.pravesh.BASE_URL,'api/shared/user/?user_type=Student',{headers:token},setProfileData)
       
    },[])
    const GetData=async(baseUrl,endPoint,body,updateState)=>{

        let res = await httpRequestGet(baseUrl,endPoint,body)
        updateState(res.data)
       

 
      }
    return (
        <div>
        <Layout>

        <PublicProfile/>
        </Layout>

            
        </div>
    )
}
