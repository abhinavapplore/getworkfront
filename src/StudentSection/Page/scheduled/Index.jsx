/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react'
import Layout from '../../Layout/Layout'
import { Grid, Paper, Avatar, Select, MenuItem } from '@material-ui/core'
import JobsCard from '../../Components/JobsCard/JobsCard'
import {httpRequest} from '../../../utils/httpRequest'
import baseUrl from '../../../common/CONSTANT'
import filterJob from '../../../utils/filterJob'
import { BreakpointProvider, Breakpoint } from 'react-socks'
import NewJobCard from '../../Components/JobsCard/NewJobCard'
import Loader from '../../../bundles/common/components/UI/Loader'
export default function Index() {
    const [jobData,setJobData]=useState([])
    const [allData,setAllData]=useState([])
    const [filterValue,setFilterValue]=useState('All')
    const [loader,setLoader]=useState(false)

    const [newURL, setNewURL] = useState("");
  const [newEndPoint, setNewEndPoint] = useState("");
  const [end, setEnd] = useState(false);
  const data = JSON.parse(localStorage.getItem("user_details"));

    useEffect(()=>{
        const token=localStorage.getItem('gw_token');
        GetData(baseUrl.niyukti.BASE_URL,`job/student/interview/?user_id=${window.btoa(data.id)}`,{headers:token},setJobData)
        

    },[])
    const GetData=async(baseUrl,endPoint,body,updateState)=>{
        setLoader(true)
        // console.log(baseUrl,endPoint,body)
                let res = await httpRequest(baseUrl,endPoint,body)
                if (res.data.next === null) {
                    setEnd(true);
                  } else {
                    setNewURL(res.data.next.slice(0, 20));
                    setNewEndPoint(res.data.next.slice(20));
                  }
                // console.log(res.data.results)
                setAllData(allData.concat([...res.data.results]))
                updateState(jobData.concat([...res.data.results]))
                // let data=filterJob(allData,'is_accepted',true)
                // setJobData(data)
                setLoader(false)
        
          
              }

              
              const handleChange=(event)=>
              {
                  setFilterValue(event.target.value)
                //   console.log(filterValue)
                //   let data=filterJob(allData,'applicant_status_name',filterValue)
                //   console.log(data)


              }

              useEffect(()=>{
                
                let data=filterJob(allData,'is_accepted',true)
                setJobData(data)
              },[allData])

              const handleScroll = (event) => {
                let e = event.nativeEvent;
                if (
                  e.target.scrollTop + 10 >=
                  e.target.scrollHeight - e.target.clientHeight
                ) {
                  if (end !== true) {
                    const token = localStorage.getItem("gw_token");
                    GetData(newURL, newEndPoint, { headers: token }, setJobData);
                  }
                }
              };

    return (
        <>
            <Layout>
            {
                !loader ? (


<div className='container__jobs'>
<BreakpointProvider>
<Breakpoint large up>

    {/* {data.map((card, index) => <NewJobCard key={index} {...card} />)} */}
    <div id="myid" className="scrollY1"  style={{ marginTop: "10px"}} onScroll={handleScroll}>

    {       jobData.length ?  <NewJobCard data={jobData} invite={true} reject={false} fullView={false} /> : ''}
    </div>
</Breakpoint>
<Breakpoint medium down>
{       jobData.length ?  <NewJobCard data={jobData} fullView={true} /> : ''}
</Breakpoint>

</BreakpointProvider>
</div>
                ):(<div><Loader/></div>)
            }
</Layout>

        </>


    )
}
