/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import Layout from "../../Layout/Layout";
import { Grid, Paper, Avatar, Select, MenuItem } from "@material-ui/core";
import JobsCard from "../../Components/JobsCard/JobsCard";
import { httpRequest } from "../../../utils/httpRequest";
import baseUrl from "../../../common/CONSTANT";
import filterJob from "../../../utils/filterJob";
import sortJob from "../../../utils/sortJob";
import { BreakpointProvider, Breakpoint } from "react-socks";

import ArrowRightAltIcon from "@material-ui/icons/ArrowRightAlt";
import { Link } from "react-router-dom";
import NewJobCard from "../../Components/JobsCard/NewJobCard";
import Loader from "../../../bundles/common/components/UI/Loader";
export default function Index() {
  const [jobData, setJobData] = useState([]);
  const [allData, setAllData] = useState([]);
  const [filterValue, setFilterValue] = useState("All");
  const [sortValue, setSortValue] = useState("All");

  const [newURL, setNewURL] = useState("");
  const [newEndPoint, setNewEndPoint] = useState("");
  const [end, setEnd] = useState(false);
  const data = JSON.parse(localStorage.getItem("user_details"));

  useEffect(() => {
    const token = localStorage.getItem("gw_token");
    GetData(
      baseUrl.niyukti.BASE_URL,
      `job/student/job_details/?user_id=${window.btoa(data.id)}&status=MQ==`,
      { headers: token },
      setJobData
    );
  }, []);
  const GetData = async (baseUrl, endPoint, body, updateState) => {
    // console.log(baseUrl, endPoint, body);
    let res = await httpRequest(baseUrl, endPoint, body);
    console.log(res)
    if (res.data.next === null) {
      setEnd(true);
    } else {
      setNewURL(res.data.next.slice(0, 20));
      setNewEndPoint(res.data.next.slice(20));
    }
    updateState(jobData.concat([...res.data.results]));
    setAllData(allData.concat([...res.data.results]));
  };
  const handleChange = (event) => {
    setFilterValue(event.target.value);
    //   console.log(filterValue)
    //   let data=filterJob(allData,'applicant_status_name',filterValue)
    //   console.log(data)
  };
  const handleChangeSort = (event) => {
    setSortValue(event.target.value);
    //   console.log(filterValue)
    //   let data=filterJob(allData,'applicant_status_name',filterValue)
    //   console.log(data)
  };

  useEffect(() => {
    console.log(filterValue);
    if (filterValue.length) {
      let data = filterJob(allData, "applicant_status_name", filterValue);
      setJobData(data);
    }
  }, [allData, filterValue]);

  useEffect(() => {
    console.log(sortValue);
    if (sortValue.length) {
      let data = sortJob(allData, sortValue);

      setJobData(data);
    }
  }, [allData, sortValue]);

  const handleScroll = (event) => {
    let e = event.nativeEvent;
    if (
      e.target.scrollTop + 10 >=
      e.target.scrollHeight - e.target.clientHeight
    ) {
      if (end !== true) {
        const token = localStorage.getItem("gw_token");
        GetData(newURL, newEndPoint, { headers: token }, setJobData);
      }
    }
  };

  return (
    <>
      <BreakpointProvider>
        <Breakpoint large up>
          <Layout>
            {jobData.length ? (
              <div className="applie1 mt-5">
                <Grid container>
                  <Grid xs={4}>
                    <div className="fs-16 fw-700 text-left">
                      {jobData.length} applied jobs
                    </div>
                  </Grid>
                  <Grid xs={4}>
                    <span className="fs-16 fw-700 mg-right-15">Filter by</span>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      style={{
                        width: 150,
                        background: "white",
                        height: "40px",
                      }}
                      variant="outlined"
                      value={filterValue}
                      onChange={handleChange}
                    >
                      <MenuItem value="All">All</MenuItem>
                      <MenuItem value="Applied">Applied</MenuItem>
                      <MenuItem value="Interview">Interview</MenuItem>
                      <MenuItem value="Review">Review</MenuItem>
                      <MenuItem value="Shortlisted">Shortlisted</MenuItem>
                      <MenuItem value="Rejected">Rejected</MenuItem>
                      <MenuItem value="Hired">Hired</MenuItem>
                    </Select>
                  </Grid>
                  <Grid xs={4}>
                    <span className="fs-16 fw-700 mg-right-15">sort by</span>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={sortValue}
                      style={{
                        width: 150,
                        background: "white",
                        height: "40px",
                      }}
                      variant="outlined"
                      onChange={handleChangeSort}
                    >
                      <MenuItem value="All">All</MenuItem>
                      <MenuItem value="company[0].company_name">
                        Company Name
                      </MenuItem>
                      {/* <MenuItem value='Interview'>Interview</MenuItem>
          <MenuItem value='Reviewed'>Reviewed</MenuItem>
          <MenuItem value='Shortlisted'>Shortlisted</MenuItem>
          <MenuItem value='Rejected'>Rejected</MenuItem>
          <MenuItem value='Hired'>Hired</MenuItem> */}
                    </Select>
                  </Grid>
                </Grid>
                <div id="myid" className="scrollY1"  style={{ marginTop: "10px"}} onScroll={handleScroll}>
                <div>
                  {jobData.length ? (
                      <NewJobCard
                        data={jobData}
                        fullView={false}
                        applied={true}
                      />
                  ) : (
                    <div className="text-gray fs-20 fw-700 mg-top-20">
                      No Jobs
                    </div>
                  )}
                </div>
                </div>
              </div>
            ) : (
              <div>
                <Loader />
              </div>
            )}
          </Layout>
        </Breakpoint>
        <Breakpoint medium down>
          <Layout>
            <div className="open-job text-left">
              <Grid container>
                <Grid xs={12}>
                  <Grid xs={12}>
                    <span className="fs-16 fw-700 mg-right-15">Filter by</span>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={filterValue}
                      variant="outlined"
                      autoWidth={true}
                      style={{ width: 200 }}
                      onChange={handleChange}
                    >
                      <MenuItem value="All">All</MenuItem>
                      <MenuItem value="Applied">Applied</MenuItem>
                      <MenuItem value="Interview">Interview</MenuItem>
                      <MenuItem value="Reviewed">Reviewed</MenuItem>
                      <MenuItem value="Shortlisted">Shortlisted</MenuItem>
                      <MenuItem value="Rejected">Rejected</MenuItem>
                      <MenuItem value="Hired">Hired</MenuItem>
                    </Select>
                  </Grid>
                </Grid>
              </Grid>
              <Grid container className="mg-top-10 mg-bottom-10">
                <Grid xs={12}>
                  <div className="fs-16 fw-700 text-left">
                    {jobData.length} Applied jobs
                  </div>
                </Grid>
              </Grid>
              <div className="container__jobs">
                {jobData.length ? (
                  <NewJobCard data={jobData} fullView={true} applied={true} />
                ) : (
                  "No Jobs"
                )}
              </div>
            </div>
          </Layout>
        </Breakpoint>
      </BreakpointProvider>
    </>
  );
}
