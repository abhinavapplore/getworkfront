import React, { useEffect, useState } from 'react'
import { Grid, Card, CardContent, Avatar, Modal } from '@material-ui/core'
import {httpRequestGet} from '../../../utils/httpRequest';
import baseUrl from '../../../common/CONSTANT'
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import EditorEduction from '../../Components/Editor/EditEduction';
import EditExp from '../../Components/Editor/EditExp';
import EditProject from '../../Components/Editor/EditProject';
import EditProfile from '../../Components/Editor/EditProfile';
import Layout from '../../Layout/Layout';
import VisibilityIcon from '@material-ui/icons/Visibility';
import PublicPorfile from '../../Components/PublicProfile/Index'

export default function Index() {

    const[exp,setExpData]=useState([])
    const[about,setAboutData]=useState([])
    const[skills,setSkillsData]=useState({user_skill:[]})
    const[education,setEducationData]=useState([])
    const[project,setProjectData]=useState([])
    const[profile,setProfileData]=useState(null)
    const[open,handleClose]=useState(false);
    const[editData,setEditData]=useState(null);
    const[value,setVlaue]=useState('');

    useEffect(()=>{
        const token=localStorage.getItem('gw_token');
        GetData(baseUrl.pravesh.BASE_URL,'api/education/user_education',{headers:token},setEducationData)
        GetData(baseUrl.pravesh.BASE_URL,'api/company/work_experience',{headers:token},setExpData)
        GetData(baseUrl.pravesh.BASE_URL,'api/education/user_skill',{headers:token},setSkillsData)
        GetData(baseUrl.pravesh.BASE_URL,'api/education/student_details',{headers:token},setAboutData)
       
        GetData(baseUrl.pravesh.BASE_URL,'api/education/user_project',{headers:token},setProjectData)
        GetData(baseUrl.pravesh.BASE_URL,'api/shared/user/?user_type=Student',{headers:token},setProfileData)
       
    },[])
    const GetData=async(baseUrl,endPoint,body,updateState)=>{

        let res = await httpRequestGet(baseUrl,endPoint,body)
        updateState(res.data)
        console.log(profile)

 
      }
      const openEdit=(value)=>{
        console.log(value)
          setVlaue(value)
        handleClose(!open)
        
      }

      const Editor =()=>{
         console.log(value)
        switch(value){
            case 'edu': console.log('edu') 
             return  <EditorEduction handleClosey={callback} />
                        break;
             case 'exp': return <EditExp handleClosey={callback}/>
                        break;
             case 'project':  console.log('pro') 
                        return <EditProject handleClosey={callback}/>
                                    break;
               case 'profile':  console.log('pro') 
                               return <EditProfile handleClosey={callback}/>
                                          break;
               case 'pub':  
                           return <PublicPorfile handleClosey={callback} exp={exp} project={project} education={education} profile={profile}/>
                                  break;
            default:console.log('')
        }
      }
     const  callback = (value) => {
         handleClose(value)
        // do something with value in parent component, like save to state
    }

    return (
        <Layout>

        <div className='profile' id='profile'>
            <Grid container justify='center'>
            <Grid xs={2} className='mg-top-20' style={{position:'fixed',top:10,left:'15%',width:'25%'}}>
            <div className='text-green fs-16 fw-700 text-left'>

            A complete profile has more chances of being noticed by recruiters

            </div>
            <Card className='mg-top-20'>
                    <CardContent style={{    padding:' 0px 5px 10px 20px'}}>
                        <div className='text-left'>
                        <a href='#profile' className='fs-16'>Profile</a>  
                        </div>
                    </CardContent>
                </Card>
                <Card className='mg-top-20'>
                    <CardContent style={{    padding:' 0px 5px 10px 20px'}}>
                        <div className='text-left'>
                        <a href='#about' className='fs-16'>About</a>  
                        </div>
                    </CardContent>
                </Card>
                <Card className='mg-top-20'>
                    <CardContent style={{    padding:' 0px 5px 10px 20px'}}>
                        <div className='text-left'>
                        <a href='#experience' className='fs-16'>Experience</a>  
                        </div>
                    </CardContent>
                </Card>
                <Card className='mg-top-20'>
                    <CardContent style={{    padding:' 0px 5px 10px 20px'}}>
                         <div className='text-left'>
                         <a href='#education' className='fs-16'>Education</a>  
                        </div>
                    </CardContent>
                </Card>
                <Card className='mg-top-20'>
                    <CardContent  style={{    padding:' 0px 5px 10px 20px'}}>
                        <div className='text-left'>
                             <a href='#skill' className='fs-16'>Skills</a>  
                        </div>
                    </CardContent>
                </Card>
                <Card className='mg-top-20'>
                    <CardContent  style={{    padding:' 0px 5px 10px 20px'}}>
                        <div className='text-left'>
                        <a href='#project' className='fs-16'>Project</a>  
                        </div>
                    </CardContent>
                </Card>
            </Grid>
                <Grid xs={10} className='mg-left-10' style={{marginLeft:'10%'}}>
                <Card className='mg-top-20 '>
                        <CardContent>
                        <div className='float-right pointer'>
                            <EditIcon onClick={()=>openEdit('profile')}/>
                        </div>
                            <Grid container>
                              {profile!==null && ( <><Grid xs={2}>
                                    <Avatar src={profile.profile_picture} alt='' style={{height:200,width:200}}/>
                                </Grid>
                                <Grid xs={7} style={{marginLeft:'10%',marginTop:'5%'}}>

                                <div className='text-left mg-top-20 mg-left-10'>

                                    <h4 className='mg-top-20'>
                                        {profile.first_name} {profile.last_name} {' '}, {' '} {profile.current_city_name}
                                    </h4>
                                    <div className='fs-20 fw-300'>
                                       {profile.student_user_details.education[0].degree}
                                    </div>
                                    <div className='fs-20 fw-300 '>
                                        {profile.student_user_details.education[0].college_name}
                                    </div>
                                </div>
                                <div className='flex align-item-center fs-12 text-blue' onClick={()=>openEdit('pub')}>

<VisibilityIcon/> View your Public porfile
</div>

                                    
                                </Grid></>)}
                                
                            </Grid>

                            {/* <Grid container>
                                <Grid xs={12} justify='center' onClick={()=>openEdit('pub')}>
                                <div className='flex align-item-center fs-12'>

                                <VisibilityIcon/> View your Public porfile
                                </div>

                                </Grid>
                            </Grid> */}
                        </CardContent>
                    </Card>
                    <Card className='mg-top-20' id='about'>
                        <CardContent>
                            <Grid container>
                          
                                <Grid xs={12}>
                                    <h4 className='text-left'>
                                        About
                                    </h4>
                                    <div className='fs-16 text-gray fw-300 text-left'>
                                       {
                                           about.about
                                       }
                                    </div>
                                   
                                    
                                </Grid>
                            </Grid>
                        </CardContent>
                    </Card>
                    <Card className='mg-top-20' id='experience'>
                        <CardContent>
                        <div className='float-right pointer'>
                            <AddIcon onClick={()=>openEdit('exp')}/>
                        </div>
                            <Grid container>
                            
                                <Grid xs={12}>
                                

                                    <h4 className='text-left'>
                                        Experience
                                    </h4>
                                    <div>
                                      {
                                          exp.map((item)=>(
                                              <div className='text-left fs-16 fw-700 mg-top-10'>
                                                  {
                                                      item.company_name
                                                  }{' '},{' '}
                                                  <span className='text-gray fw-300'>
                                                      {
                                                          item.job_designation
                                                      }
                                                  </span>
                                                  <div className='fs-12 text-gray fw-300'>

                                                   {
                                                       item.start_date
                                                   }
                                                   to   
                                                   {
                                                       item.end_date
                                                   }
                                                   </div>
                                                   <div className='fs-12 text-gray fw-300'>

                                                   {
                                                       item.job_description
                                                   }
                                                   
                                                   </div>
                                                   {/* <EditIcon onClick={}/> */}
                                              </div>
                                          ))
                                      }
                                    </div>
                                    
                                </Grid>
                            </Grid>
                        </CardContent>
                    </Card>
                    <Card className='mg-top-20' id='education'>
                        <CardContent>
                        <div className='float-right pointer'>
                            <AddIcon onClick={()=>openEdit('edu')}/>
                        </div>
                            <Grid container>
                                
                                <Grid xs={12}>
                                    <h4 className='text-left'>
                                       Education
                                    </h4>
                                    <div>
                                       {
                                           education.map((item)=>(
                                               <div className='text-left fs-16 fw-700 mg-top-10'>
                                               <div>

                                                   {
                                                       item.degree
                                                   }{' '}, {' '}
                                                   <span className='text-gray fw-300'>

                                                   {
                                                       item.college_name
                                                   }
                                                   </span>
                                               </div>
                                                   <div className='fs-12 text-gray fw-300'>

                                                   {
                                                       item.start_date
                                                   }
                                                   to   
                                                   {
                                                       item.end_date
                                                   }
                                                   </div>
                                               </div>
                                           ))
                                       }
                                    </div>
                                   
                                    
                                </Grid>
                            </Grid>
                        </CardContent>
                    </Card>
                    <Card className='mg-top-20' id='skill'>
                        <CardContent>
                            <Grid container>
                              
                                <Grid xs={12}>
                                    <h4 className='text-left'>
                                      Skills
                                    </h4>
                                    <div>
                                       {
                                           skills.user_skill.map((item)=>(
                                               <div className='fs-12 text-gray fw-300 text-left'>
                                                   {item.skill_name}
                                               </div>
                                           ))
                                       }
                                    </div>
                                   
                                    
                                </Grid>
                            </Grid>
                        </CardContent>
                    </Card>
                    <Card className='mg-top-20' id='project'>
                        <CardContent>
                        <div className='float-right pointer'>
                            <AddIcon onClick={()=>openEdit('project')}/>
                        </div>
                            <Grid container>
                               
                                <Grid xs={12}>
                                    <h4 className='text-left'>
                                       Project
                                    </h4>
                                    <div>
                                       {
                                        project.map((item)=>(
                                              <div className='text-left fs-16 fw-700 mg-top-10  '>
                                                  {
                                                      item.title
                                                  }
                                                  <div className='fs-12 text-gray fw-300'>

                                                   {
                                                       item.start_date
                                                   }{' '}
                                                   to  
                                                   {' '} 
                                                   {
                                                       item.end_date
                                                   }
                                                   </div>
                                                   <div className='fs-12 text-gray fw-300'>

                                                   {
                                                       item.description
                                                   }
                                                   
                                                   </div>
                                                   <div className='fs-12 text-gray fw-700'>
                                                    <a href={item.links} rel='noopener noreffer' target='_blank'>Link</a>
                                                   </div>
                                              </div>
                                          ))  
                                       }
                                    </div>
                                    
                                    
                                </Grid>
                            </Grid>
                        </CardContent>
                    </Card>
                </Grid>
            </Grid>
            <Modal 
        open={open}
        onClose={()=>handleClose(!open)}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
      <Grid container justify='center'>
          <Grid xs={10}>
       <Editor value={value}/>

          </Grid>
      </Grid>
      </Modal>
        </div>
        </Layout>
    )
}
