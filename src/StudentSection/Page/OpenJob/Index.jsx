import React, { useState, useEffect } from 'react'
import { Grid, Paper, Avatar } from '@material-ui/core'
import Layout from '../../Layout/Layout'
import {httpRequest} from '../../../utils/httpRequest'
import baseUrl from '../../../common/CONSTANT'
import JobsCard from '../../Components/JobsCard/JobsCard'
import SmallCard from '../../Components/SmallCardJob/SmallCard'
import ViewJob from '../../Components/viewJob/ViewJob'
import { BreakpointProvider, Breakpoint } from 'react-socks'
import Filter from '../../Components/filter/filter'
import ArrowRightAltIcon from '@material-ui/icons/ArrowRightAlt';
import './openjob.css'
import TopDescriptionSection from '../../Components/TopDescriptionSection/TopDescriptionSection'
import { Link } from 'react-router-dom'

export default function Index() {
    const [jobData,setJobData]=useState([])
    const [singleJobData,setSingleJobData]=useState(null)
    const [filter,setFiltersData]=useState(null)
    const data=JSON.parse(localStorage.getItem('user_details'))

    useEffect(()=>{
        const token=localStorage.getItem('gw_token');
        GetData(baseUrl.niyukti.BASE_URL,`job/student/job/?user_id=${window.btoa(data.id)}&college_id=${window.btoa(data.student_user_details.education[0].college)}`,{headers:token},setJobData)
        

    },[])
    const GetData=async(baseUrl,endPoint,body,updateState)=>{
        // console.log(baseUrl,endPoint,body)
                let res = await httpRequest(baseUrl,endPoint,body)
              
                setSingleJobData(res.data.results.length && res.data.results[0])
                 updateState(res.data.results)
                 setFiltersData(res.data.filter)

        
          
              }

              const selectedJob=(item)=>{
                  setSingleJobData(item)
              }
    return (
        <>
 <BreakpointProvider>
 {jobData.length ?(<>
        <Breakpoint large up>
<Grid container justify='center'>

       <Layout>

        
  
         
       <div className='open-job text-left '>
           <Grid container className='relative' >
               <Grid xs={12}>

               <Filter filterData={filter} setJobData={setJobData} setSingleJobData={setSingleJobData} style={{marginTop:'92px'}}/>

               </Grid>

           </Grid>
           
          
           <Grid container className='mg-top-10'> 
             

               {
                jobData.length ? (
                    <>
                    {/* <Grid container className='mg-top-20'>
                <Grid xs={10}>
                <div className='fs-16 fw-700 text-left'>
                  
                </div>
                </Grid>
            </Grid> */}
                   
                    <Grid xs={4} className='scrollY'>
                        {
                            jobData.length ?  <SmallCard setSingleJobData={selectedJob} selectedJobData={singleJobData} data={jobData}/> :   <div className='text-gray fs-20 fw-700 mg-top-20'>No Jobs</div>
                        }
                          
                        </Grid>
                        <Grid xs={8} className=''>
                        {
                            singleJobData!==null ?  <ViewJob data={singleJobData} open={true} apply={true}/> :   <div className='text-gray fs-20 fw-700 mg-top-20'>No Jobs</div>
                        }
                          
                        </Grid>
                  
                    </>
                ):null
            }

           
           </Grid>
          
          
         
           
         
</div>  
        </Layout>
        </Grid>
        </Breakpoint>
        <Breakpoint medium down>
            <Layout>
            <div className='open-job text-left'>
            <Grid container >
                <Grid xs={12}>
                    <Filter filterData={filter} setJobData={setJobData} setSingleJobData={setSingleJobData}/>
                </Grid>
            </Grid>
            <Grid container className='mg-top-10 mg-bottom-10'>
                <Grid xs={12}>
                <div className='fs-16 fw-700 text-left'>
                    {
                        jobData.length
                    } {' '} open jobs
                </div>
                </Grid>
            </Grid>
                    <Grid container>
                    <Grid xs={12} className='scrollY'>
                        {
                            jobData.length ?  <SmallCard setSingleJobData={selectedJob} apply={true} viewJob={true} selectedJobData={singleJobData} data={jobData}/> :   <div className='text-gray fs-20 fw-700 mg-top-20'>No Jobs</div>
                        }
                          
                        </Grid>
                        
                    </Grid>

                
            </div>

        </Layout>
            </Breakpoint></>):<div><Loader/></div>}
        </BreakpointProvider>
        
        </>

    )
}
