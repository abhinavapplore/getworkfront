/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import { Grid, Paper, Avatar } from "@material-ui/core";
import Layout from "../../Layout/Layout";
import { httpRequest } from "../../../utils/httpRequest";
import baseUrl from "../../../common/CONSTANT";
import JobsCard from "../../Components/JobsCard/JobsCard";
import SmallCard from "../../Components/SmallCardJob/SmallCard";
import ViewJob from "../../Components/viewJob/ViewJob";
import { BreakpointProvider, Breakpoint } from "react-socks";
import Filter from "../../Components/filter/filter";
import ArrowRightAltIcon from "@material-ui/icons/ArrowRightAlt";
import "./openjob.css";
import TopDescriptionSection from "../../Components/TopDescriptionSection/TopDescriptionSection";
import { Link } from "react-router-dom";

import Loader from "../../../bundles/common/components/UI/Loader";
export default function Index() {
  const [loading, setLoading] = useState(true);
  const [jobData, setJobData] = useState([]);
  const [singleJobData, setSingleJobData] = useState(null);
  const [filter, setFiltersData] = useState(null);
  const data = JSON.parse(localStorage.getItem("user_details"));

  const [newURL, setNewURL] = useState("");
  const [newEndPoint, setNewEndPoint] = useState("");
  const [end, setEnd] = useState(false);

  useEffect(() => {
    const token = localStorage.getItem("gw_token");
    GetData(
      baseUrl.niyukti.BASE_URL,
      `job/student/job/?user_id=${window.btoa(
        data.id
      )}&college_id=${window.btoa(
        data.student_user_details.education[0].college
      )}`,
      { headers: token },
      setJobData
    );
  }, []);
  const GetData = async (baseUrl, endPoint, body, updateState) => {
    let res = await httpRequest(baseUrl, endPoint, body);
    if (res.data.next === null) {
      setEnd(true);
    } else {
      setNewURL(res.data.next.slice(0, 20));
      setNewEndPoint(res.data.next.slice(20));
    }

    setSingleJobData(res.data.results.length && res.data.results[0]);
    updateState(jobData.concat([...res.data.results]));
    setFiltersData(res.data.filter);
    setLoading(false);
  };

  const selectedJob = (item) => {
    setSingleJobData(item);
  };

  const handleScroll = (event) => {
    let e = event.nativeEvent;
    if (
      e.target.scrollTop + 10 >=
      e.target.scrollHeight - e.target.clientHeight
    ) {
      if (end !== true) {
        const token = localStorage.getItem("gw_token");
        GetData(newURL, newEndPoint, { headers: token }, setJobData);
      }
    }
  };

  return (
    <>
      <BreakpointProvider>
        <>
          <Breakpoint large up>
            <Layout>
              {!loading ? (
                <>
                  <div className="open-job text-left ">
                    {jobData.length ? (
                      <>
                        <div className="container__jobs">
                          <Filter
                            filterData={filter}
                            setJobData={setJobData}
                            fullView={true}
                            setSingleJobData={setSingleJobData}
                          />
                        </div>

                        <Grid container className="mg-top-10">
                          {jobData !== null ? (
                            <>
                              <Grid container className="mg-top-20">
                                <Grid xs={10}>
                                  <div className="fs-16 fw-700 text-left">
                                    {jobData.length} open jobs
                                  </div>
                                </Grid>
                              </Grid>

                              <Grid
                                xs={4}
                                className="scrollY"
                                style={{height: "70vh"}}
                                onScroll={handleScroll}
                              >
                                {jobData.length ? (
                                  <SmallCard
                                    setSingleJobData={selectedJob}
                                    selectedJobData={singleJobData}
                                    data={jobData}
                                  />
                                ) : (
                                  <div className="text-gray fs-20 fw-700 mg-top-20">
                                    No Jobs
                                  </div>
                                )}
                              </Grid>
                              <Grid xs={8} className="">
                                {singleJobData ? (
                                  <ViewJob
                                    data={singleJobData}
                                    open={true}
                                    closeIcon={true}
                                    apply={true}
                                  />
                                ) : (
                                  <div className="text-gray fs-20 fw-700 mg-top-20">
                                    No Jobs
                                  </div>
                                )}
                              </Grid>
                            </>
                          ) : null}
                        </Grid>
                      </>
                    ) : (
                      <div className="fs-20 fw-700 mg-top-20">No Open Jobs</div>
                    )}
                  </div>
                </>
              ) : (
                <div>
                  <Loader />
                </div>
              )}
            </Layout>
          </Breakpoint>
          <Breakpoint medium down>
            <Layout>
              <div className="open-job text-left">
                <Grid container>
                  <Grid xs={12}>
                    {filter !== null ? (
                      <Filter
                        filterData={filter}
                        setJobData={setJobData}
                        setSingleJobData={setSingleJobData}
                        fullView={true}
                      />
                    ) : null}
                  </Grid>
                </Grid>
                <Grid container className="mg-top-10 mg-bottom-10">
                  <Grid xs={12}>
                    <div className="fs-16 fw-700 text-left">
                      {jobData.length} open jobs
                    </div>
                  </Grid>
                </Grid>
                <Grid container>
                  <Grid
                    xs={12}
                    className="scrollY"
                    style={{ border: "12px solid #000" }}
                  >
                    {jobData.length ? (
                      <SmallCard
                        setSingleJobData={selectedJob}
                        apply={true}
                        viewJob={true}
                        selectedJobData={singleJobData}
                        data={jobData}
                      />
                    ) : (
                      <div className="text-gray fs-20 fw-700 mg-top-20">
                        No Jobs
                      </div>
                    )}
                  </Grid>
                </Grid>
              </div>
            </Layout>
          </Breakpoint>
        </>
      </BreakpointProvider>
    </>
  );
}
