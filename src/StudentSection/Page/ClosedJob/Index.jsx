/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react'
import Layout from '../../Layout/Layout'
import { Grid, Paper, Avatar } from '@material-ui/core'
import JobsCard from '../../Components/JobsCard/JobsCard'
import {httpRequest} from '../../../utils/httpRequest'
import baseUrl from '../../../common/CONSTANT'
import filterJob from '../../../utils/filterJob'
import WorkIcon from "@material-ui/icons/Work";
import NewJobCard from '../../Components/JobsCard/NewJobCard'
import styles from './newStyle.scss'
import { BreakpointProvider, Breakpoint } from 'react-socks'
import Loader from '../../../bundles/common/components/UI/Loader'
export default function Index() {
    const [jobData,setJobData]=useState([])
    const [allData,setAllData]=useState([])
    const [filterValue,setFilterValue]=useState('All')
    const[loader,setLoader]=useState(false)

    const [newURL, setNewURL] = useState("");
  const [newEndPoint, setNewEndPoint] = useState("");
  const [end, setEnd] = useState(false);
  const data = JSON.parse(localStorage.getItem("user_details"));

    useEffect(()=>{
        const token=localStorage.getItem('gw_token');
        GetData(baseUrl.niyukti.BASE_URL,`job/student/job_details/?user_id=${window.btoa(data.id)}&status=Y2xvc2U=`,{headers:token},setJobData)
        

    },[])
    const GetData=async(baseUrl,endPoint,body,updateState)=>{
      setLoader(true)
        // console.log(baseUrl,endPoint,body)
                let res = await httpRequest(baseUrl,endPoint,body)
                console.log(res.data)
                if (res.data.next === null) {
                  setEnd(true);
                } else {
                  setNewURL(res.data.next.slice(0, 20));
                  setNewEndPoint(res.data.next.slice(20));
                }
                 updateState(jobData.concat([...res.data.results]))
                 setAllData(allData.concat([...res.data.results]))
                 setLoader(false)
        
          
              }
              useEffect(()=>{
                
                // let data=filterJob(allData,'job_status','OPEN')

                setJobData(allData)
              },[allData])

              const handleScroll = (event) => {
                let e = event.nativeEvent;
                if (
                  e.target.scrollTop + 10 >=
                  e.target.scrollHeight - e.target.clientHeight
                ) {
                  if (end !== true) {
                    const token = localStorage.getItem("gw_token");
                    GetData(newURL, newEndPoint, { headers: token }, setJobData);
                  }
                }
              };
    return (
        <>

       

<Layout>
{
  !loader ?
  (
 <div className='container__jobs'>
      
      <BreakpointProvider>
          <Breakpoint large up>
          <Grid container className='mt-4'>
                <Grid xs={10}>
                <div className='fs-16 fw-700 text-left'>
                  {jobData.length } Closed jobs
                </div>
                </Grid>
            </Grid>
            <div id="myid" className="scrollY1"  style={{ marginTop: "10px"}} onScroll={handleScroll}>
        {       jobData.length ?  <NewJobCard data={jobData} fullView={false} applied={true}/> :   <div className='text-gray fs-20 fw-700 mg-top-20'>No Jobs</div>}
          </div></Breakpoint>
          <Breakpoint medium down>
          {       jobData.length ?  <NewJobCard data={jobData} fullView={true} applied={true}/> :   <div className='text-gray fs-20 fw-700 mg-top-20'>No Jobs</div>}
          </Breakpoint>
      </BreakpointProvider>
      
      </div>

  ):(
    <div><Loader/></div>
  )
}
</Layout>
        </>


    )
}



 

export const data = [
  {
    Icon: WorkIcon,
    role: "Graphic Designer",
    company: "Tata Consultancy",
    location: "Gurugram",
    position: "Full Time",
    compensation: "$20k - 25k per year",
    growth: "1.5% - 2%",
    action: "Invited You",
    duration: "15 august - 20 august",
  },
  {
    Icon: WorkIcon,
    role: "Software Engineer",
    company: "Tata Consultancy",
    location: "Bangalore",
    position: "Full Time",
    compensation: "$40k - 45k per year",
    growth: "1.5% - 2%",
    action: "Invited You",
    duration: "15 august - 20 august",
  },
  {
    Icon: WorkIcon,
    role: "Business Development Manager",
    company: "Tata Consultancy",
    location: "Pune",
    position: "Full Time",
    compensation: "$30k - 35k per year",
    growth: "1.5% - 2%",
    action: "Invited You",
    duration: "15 august - 20 august",
  },
  {
    Icon: WorkIcon,
    role: "Senior Software Engineer",
    company: "Infosys",
    location: "Bangalore",
    position: "Full Time",
    compensation: "$80k - 85k per year",
    growth: "4.5% - 6%",
    action: "Invited You",
    duration: "15 august - 20 august",
  },
  {
    Icon: WorkIcon,
    role: "QA",
    company: "Tata Consultancy",
    location: "Pune",
    position: "Full Time",
    compensation: "$20k - 25k per year",
    growth: "1.5% - 2%",
    action: "Invited You",
    duration: "15 august - 20 august",
  },
];



     
// <Grid container justify='center'>

// <Layout>

 

 
// <div className='applied mg-left-10'>
    
   
   
   
   
//     <Grid container> 
//         <Grid xs={12} className='big-card-res'>
        
//  {       jobData.length ?  <JobsCard data={jobData} fullView={true} applied={true}/> : ''}
//         </Grid>
//     </Grid>
    
// </div>  
//  </Layout>

//  </Grid>