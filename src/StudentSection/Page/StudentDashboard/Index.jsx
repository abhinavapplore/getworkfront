import React,{useState, useEffect} from 'react'
import Layout from '../../Layout/Layout'
import TopDescriptionSection from '../../Components/TopDescriptionSection/TopDescriptionSection'
// import NewJobCard from '../../Components/NewJobCard/NewJobCard'
import { Grid } from '@material-ui/core';
import BlogCard from '../../Components/Blog/BlogCard';
import ConnectionCard from '../../Components/Connections/ConnectionCard';
import  { Breakpoint, BreakpointProvider } from 'react-socks';
import {httpRequest} from '../../../utils/httpRequest';
import baseUrl from '../../../common/CONSTANT'
import { Link } from 'react-router-dom';
import ArrowRightAltIcon from '@material-ui/icons/ArrowRightAlt';
import Navbar from '../../Components/Navbar/Navbar';
import filterData from '../../../utils/filterJob'
import filterJob from '../../../utils/filterJob';
import NewJobCard from '../../Components/JobsCard/NewJobCard'
import './studentDash.css'

export default function Index() {

    const [jobsData,setJobsData]=useState([]);
    const [interviewData,setInterviewData]=useState([]);
    const [blogData,setBlogData]=useState([]);
    const [connectionData,setConnectionData]=useState([]);
    const [allData,setAllData]=useState([])
    const [schJob,setSchJob]=useState([])
    const [allInterviewJob,setAllInterviewJob]=useState([])
    const[loader,setLoader]=useState(false)
    const [isReject,setIsReject]=useState(false)
  

    useEffect(() => {
       setLoader(true)
      const token=localStorage.getItem('gw_token');
      const data=JSON.parse(localStorage.getItem('user_details'))
      
//   console.log(data)
     
      GetData(baseUrl.niyukti.BASE_URL,`job/student/job/?user_id=${window.btoa(data.id)}&college_id=${window.btoa(data.student_user_details.education[0].college)}`,null,setJobsData)
      GetData(baseUrl.niyukti.BASE_URL,`job/student/interview/?user_id=${window.btoa(data.id)}&college_id=${window.btoa(data.student_user_details.education[0].college)}`,null,setInterviewData)
      GetData(baseUrl.pravesh.BASE_URL,'api/blog/all/',null,setBlogData)
      GetData(baseUrl.pravesh.BASE_URL,'api/shared/user_connections/?user_type=Student',{headers:token},setConnectionData)
      setLoader(false);
     


     
    },[])

    const GetData=async(baseUrl,endPoint,body,updateState)=>{

      let res = await httpRequest(baseUrl,endPoint,body)
      updateState(res.data.results)
     

    }
    useEffect(()=>{

    console.log('interview',interviewData)
let data=filterJob(interviewData,'is_accepted',false)
setAllInterviewJob(data)
console.log('inv',allInterviewJob)
},[interviewData])
useEffect(()=>{
   console.log('interview',interviewData)
let data=filterJob(interviewData,'is_accepted',true)
console.log('sch',data)

setSchJob(data)
},[interviewData])
              

    useEffect(()=>{

      if(allData.length){
         let tempData=filterData(allData,'')
      }

    },[])

    const UpdateData=(jobId)=>{

      // console.log(jobId)

    }
 

    return (
        <>
          <BreakpointProvider>
        <Breakpoint large up>
<Grid container justify='center'>

       <Layout>
        {!loader?(<>

        
  
          <TopDescriptionSection/>
       <div className='student-dash mg-left-10'>
           <Grid container className='relative' >
               <Grid xs={12}>

            <h2 className='fs-20 fw-700 float-left text-green heading heading-underline mg-top-10'>
                Jobs
            </h2>  

            {/* <NewJobCard data={jobsData.slice(0,3)} interview={true} apply={true}  UpdateData={UpdateData} hideStatus={false} all={true}/> 
            */}
            <NewJobCard data={jobsData.slice(0,3)} interview={true} apply={true}  UpdateData={UpdateData} hideStatus={false} all={true}/> 
           
               </Grid>

           </Grid>
           {
              jobsData.length>3 &&(
                 <Grid container justify='flex-start'>
                    <Grid xs={12}>

               <Link to='/student/open' className='float-left fs-16 mg-top-10 flex align-item-center'>All open jobs <ArrowRightAltIcon/></Link>
                    </Grid>
                 </Grid>
              )
           }
          
           <Grid container> 
               <Grid xs={12}>
               <div className='flex flex-col align-item-baseline'>

               <h2 className='fs-20 fw-700 float-left text-green mg-top-10 heading heading-underline'>
                Interviews
            </h2>  
      
            <h2 className='fs-16 float-left'>Invited you</h2>
               </div>
               {

                  <NewJobCard data={allInterviewJob.slice(0,3)} setIsReject={setIsReject} isReject={isReject} invite={true}  reject={true} all={false}/>
               }
               </Grid>
           </Grid>
           {
            allInterviewJob.length>3 &&(
                 <Grid container justify='flex-start'>
                    <Grid xs={12}>

               <Link to='/student/invited' className='connection-link'>All Invited interviews <ArrowRightAltIcon/></Link>
                    </Grid>
                 </Grid>
              )
           }
           <Grid container>
               <Grid xs={12}>
               <div className='flex flex-col align-item-baseline'>

               <h2 className='fs-20 fw-700 float-left text-green mg-top-10 heading-underline'>
               
            </h2>  
      
            <h2 className='fs-16 float-left'>Scheduled interviews</h2>
               </div>
            <NewJobCard data={schJob.slice(0,3)} interview={false} reject={false} all={false}/> 
               </Grid>
           </Grid>
           {
              schJob.length>3 &&(
                 <Grid container justify='flex-start'>
                    <Grid xs={12}>

               <Link to='/student/scheduled' className='connection-link'>All scheduled interviews <ArrowRightAltIcon/></Link>
                    </Grid>
                 </Grid>
              )
           }
           <Grid container style={{width:771}}>
               <Grid xs={12}>
               <div className='flex flex-col align-item-baseline'>

               <h2 className='fs-20 fw-700 float-left text-green mg-top-10 heading heading-underline'>
               Blog
            </h2>  
      
            
               </div>
            <BlogCard data={blogData} /> 
               </Grid>
           </Grid>
           <Grid container style={{width:771}}>
               <Grid xs={12}>
               <div className='flex flex-col align-item-baseline'>

               <h2 className='fs-20 fw-700 float-left text-green mg-top-10 heading heading-underline'>
               Connections
            </h2>  
      
            
               </div>
            <ConnectionCard data={connectionData} /> 
               </Grid>
           </Grid>
</div>  
     </>   ):<div>Loading</div>}
        </Layout>
        </Grid>
        </Breakpoint>
        <Breakpoint medium down>
<Grid container justify='center'>
<TopDescriptionSection/>
       <Layout>
           <Grid container className='relative'>
               <Grid xs={12}>

            <h2 className='fs-24 fw-700 float-left text-green heading heading-underline'>
                Jobs
            </h2>  

            <NewJobCard data={jobsData.slice(0,3)} interview={true} apply={true}  UpdateData={UpdateData} hideStatus={false} all={true}/> 
            

               </Grid>

           </Grid>
           
           {
              jobsData.length>3 &&(
                 <Grid container justify='flex-start'>
                    <Grid xs={12}>

               <Link to='/student/open' className='connection-link'>All open jobs <ArrowRightAltIcon/></Link>
                    </Grid>
                 </Grid>
              )
           }
           <Grid container>
               <Grid xs={12}>
               <div className='flex flex-col align-item-baseline'>

               <h2 className='fs-24 fw-700 float-left text-green mg-top-10 heading heading-underline'>
                Interview
            </h2>  
      
            <h2 className='fs-16 float-left'>Invited you</h2>
               </div>
               <NewJobCard data={allInterviewJob.slice(0,3)} setIsReject={setIsReject} isReject={isReject} invite={true}  reject={true} all={false}/> 
               </Grid>
           </Grid>
           {
              allInterviewJob.length>3 &&(
                 <Grid container justify='flex-start'>
                    <Grid xs={12}>

               <Link to='/student/invited' className='connection-link'>All Invited interviews <ArrowRightAltIcon/></Link>
                    </Grid>
                 </Grid>
              )
           }
           <Grid container>
               <Grid xs={12}>
               <div className='flex flex-col align-item-baseline'>

               <h2 className='fs-20 fw-700 float-left text-green mg-top-10 heading-underline'>
               
            </h2>  
      
            <h2 className='fs-16 float-left'>Scheduled interviews</h2>
               </div>
               <NewJobCard data={schJob.slice(0,3)} interview={false} reject={false} all={false}/> 
               </Grid>
           </Grid>
           {
              schJob.length>2 &&(
                 <Grid container justify='flex-start'>
                    <Grid xs={12}>

               <Link to='/student/scheduled' className='connection-link'>All scheduled interviews <ArrowRightAltIcon/></Link>
                    </Grid>
                 </Grid>
              )
           }
           <Grid container >
               <Grid xs={12}>
               <div className='flex flex-col align-item-baseline'>

               <h2 className='fs-20 fw-700 float-left text-green mg-top-10 heading-underline'>
               Blog
            </h2>  
      
            
               </div>
            <BlogCard data={blogData} /> 
               </Grid>
           </Grid>
           <Grid container>
               <Grid xs={12}>
               <div className='flex flex-col align-item-baseline'>

               <h2 className='fs-20 fw-700 float-left text-green mg-top-10 heading heading-underline'>
               Connections
            </h2>  
      
            
               </div>
            <ConnectionCard data={connectionData} /> 
               </Grid>
           </Grid>

        </Layout>
        </Grid>
        </Breakpoint>
        </BreakpointProvider>
       </>
    )
}
