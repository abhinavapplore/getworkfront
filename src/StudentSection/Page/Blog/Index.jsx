import React, { useEffect, useState } from 'react'
import { Grid } from '@material-ui/core'
import {httpRequest} from '../../../utils/httpRequest';
import baseUrl from '../../../common/CONSTANT'
import BlogCard from '../../Components/Blog/BlogCard';
import Layout from '../../Layout/Layout';
import Loader from '../../../bundles/common/components/UI/Loader';

export default function Index() {
    const [blogData,setBlogData]=useState([]);
    const [loader,setLoader]=useState(false);
    useEffect(()=>{
        
        setLoader(true)
        GetData(baseUrl.pravesh.BASE_URL,'api/blog/all/',null,setBlogData)
        setLoader(false)

    },[])
    const GetData=async(baseUrl,endPoint,body,updateState)=>{

        let res = await httpRequest(baseUrl,endPoint,body)
        updateState(res.data.results)
  
      }
    return (
        <Layout>
            {!loader ? (
        <div>

        <Grid container>
            <Grid xs={12}>

           <BlogCard data={blogData}/>
                
            </Grid>
        </Grid>
            
        </div>
        ): <Loader />}
        </Layout>
    )
}
