/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react'
import { Grid, Paper, Avatar } from '@material-ui/core'
import Layout from '../../Layout/Layout'
import {httpRequest} from '../../../utils/httpRequest'
import baseUrl from '../../../common/CONSTANT'
import JobsCard from '../../Components/JobsCard/JobsCard'
import NewJobCard from '../../Components/JobsCard/NewJobCard'
import { BreakpointProvider, Breakpoint } from 'react-socks'
import Loader from '../../../bundles/common/components/UI/Loader';
export default function Index() {
    const [jobData,setJobData]=useState([])
    const [loader, setLoader] = useState(false);

    const [newURL, setNewURL] = useState("");
    const [newEndPoint, setNewEndPoint] = useState("");
    const [end, setEnd] = useState(false);
    const data=JSON.parse(localStorage.getItem('user_details'))

    useEffect(()=>{
        const token=localStorage.getItem('gw_token');

        GetData(baseUrl.niyukti.BASE_URL,`job/student/job_details/?user_id=${window.btoa(data.id)}&status=c2F2ZWQ=`,{headers:token},setJobData)
        

    },[])
    const GetData=async(baseUrl,endPoint,body,updateState)=>{
        // console.log(baseUrl,endPoint,body)
        setLoader(true);    
                let res = await httpRequest(baseUrl,endPoint,body)
                console.log(res.data)

                if (res.data.next === null) {
                    setEnd(true);
                  } else {
                    setNewURL(res.data.next.slice(0, 20));
                    setNewEndPoint(res.data.next.slice(20));
                  }
                 updateState(jobData.concat([...res.data.results]))
                 setLoader(false);
          
              }

              const handleScroll = (event) => {
                let e = event.nativeEvent;
                if (
                  e.target.scrollTop + 10 >=
                  e.target.scrollHeight - e.target.clientHeight
                ) {
                  if (end !== true) {
                    const token = localStorage.getItem("gw_token");
                    GetData(newURL, newEndPoint, { headers: token }, setJobData);
                  }
                }
              };

    return (
        <>

      
<Layout>

<div className='container__jobs'>
<BreakpointProvider>
<Breakpoint large up>
{!loader ? (<>
<Grid xs={10}>
                <div className='fs-16 fw-700 text-left'>
                  {jobData.length } Saved jobs
                </div>
                </Grid>
    {/* {data.map((card, index) => <NewJobCard key={index} {...card} />)} */}
    <div id="myid" className="scrollY1"  style={{ marginTop: "10px"}} onScroll={handleScroll}>
    {       jobData.length ?  <NewJobCard saveJob={true} data={jobData} fullView={false}/> : ''}
    </div>
    </>) : <Loader />  }
</Breakpoint>

<Breakpoint medium down>
{!loader ? (<>
{       jobData.length ?  <NewJobCard saveJob={true} data={jobData} fullView={true}/> : ''}
</>) : <Loader />  }
</Breakpoint>

</BreakpointProvider>
</div>
</Layout>
        </>

    )
}
