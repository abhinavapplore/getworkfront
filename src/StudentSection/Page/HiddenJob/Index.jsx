/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react'
import Layout from '../../Layout/Layout'
import { Grid, Paper, Avatar } from '@material-ui/core'
import JobsCard from '../../Components/JobsCard/JobsCard'
import {httpRequest} from '../../../utils/httpRequest'
import baseUrl from '../../../common/CONSTANT'
import NewJobCard from '../../Components/JobsCard/NewJobCard'
import { BreakpointProvider, Breakpoint } from 'react-socks'
import Loader from '../../../bundles/common/components/UI/Loader'
export default function Index() {
    const [jobData,setJobData]=useState([])
    const[loader,setLoader]=useState(false)

    const [newURL, setNewURL] = useState("");
  const [newEndPoint, setNewEndPoint] = useState("");
  const [end, setEnd] = useState(false);
  const data=JSON.parse(localStorage.getItem('user_details'))

    useEffect(()=>{
        const token=localStorage.getItem('gw_token');
        const data=JSON.parse(localStorage.getItem('user_details'))
        GetData(baseUrl.niyukti.BASE_URL,`job/student/job_details/?user_id=${window.btoa(data.id)}&status=aGlkZGVu`,{headers:token},setJobData)
        

    },[])
    const GetData=async(baseUrl,endPoint,body,updateState)=>{
        setLoader(true)
        // console.log(baseUrl,endPoint,body)
                let res = await httpRequest(baseUrl,endPoint,body)
                console.log(res.data)

                if (res.data.next === null) {
                    setEnd(true);
                  } else {
                    setNewURL(res.data.next.slice(0, 20));
                    setNewEndPoint(res.data.next.slice(20));
                  }
                 updateState(jobData.concat([...res.data.results]))
                 setLoader(false)
        
          
              }
              const handleScroll = (event) => {
                let e = event.nativeEvent;
                if (
                  e.target.scrollTop + 10 >=
                  e.target.scrollHeight - e.target.clientHeight
                ) {
                  if (end !== true) {
                    const token = localStorage.getItem("gw_token");
                    GetData(newURL, newEndPoint, { headers: token }, setJobData);
                  }
                }
              };
    return (
        <>

                    <Layout>
                    {
                        !loader?(

                    <div className='container__jobs mt-5'>
        <BreakpointProvider>
            <Breakpoint large up>
            <Grid container className='mt-4'>
                <Grid xs={10}>
                <div className='fs-16 fw-700 text-left'>
                  {jobData.length } Hidden jobs
                </div>
                </Grid>
            </Grid>
            <div id="myid" className="scrollY1"  style={{ marginTop: "10px"}} onScroll={handleScroll}>
                        {       jobData.length ?  <NewJobCard data={jobData} all={true} fullView={false} hideStatus={true} applied={true}/> :   <div className='text-gray fs-20 fw-700 mg-top-20'>No Jobs</div>}
                        </div></Breakpoint>
            <Breakpoint medium down>
            {       jobData.length ?  <NewJobCard data={jobData} all={true} fullView={true} hideStatus={true} applied={true}/> :   <div className='text-gray fs-20 fw-700 mg-top-20'>No Jobs</div>}
            </Breakpoint>

        </BreakpointProvider>
                    </div>
                        ):(
                            <div><Loader/></div>
                        )
                    }

                    </Layout>
         
        </>


    )
}
