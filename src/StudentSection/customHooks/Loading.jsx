

import { useState } from 'react';
function useLoading() {
   const [loading, setLoading] = useState(false);
   
   const changeLoading = (value) => {
      setLoading(value) 
    }
   return { loading, changeLoading };
}
export default useLoading;