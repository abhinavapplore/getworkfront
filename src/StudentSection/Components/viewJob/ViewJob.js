import React, { useState, useEffect } from "react";
import { useLocation } from "react-router-dom";
// import GWLogo from '../../assets/images/getwork_new.png'
import Axios from "axios";
import { Grid, Card, CardContent, Divider, Button } from "@material-ui/core";
import "./ViewJob.css";
import httpRequest from "../../../utils/httpRequest";
import CloseIcon from "@material-ui/icons/Close";
import AttachMoneyIcon from "@material-ui/icons/AttachMoney";
import WorkOutlineIcon from "@material-ui/icons/WorkOutline";
import HourglassEmptyIcon from "@material-ui/icons/HourglassEmpty";
import Loading from "../../customHooks/Loading";
import RoomIcon from "@material-ui/icons/Room";
import MoneyIcon from "../../../assets/svg/bx_bx-money.svg";
const JobViewStyles = {
  Page: {
    height: "auto",
    background: "#E5E5E5",
  },
  Logo: {
    height: "100px",
    width: "100px",
  },
};
const ViewJob = ({ data, handleClosey, open, apply, hide, closeIcon }) => {
  // console.log(data)
  // console.log(open)
  const userType = localStorage.getItem("user_type");
  const location = useLocation();
  const [jobData, setJobData] = useState();
  const [isApply, setIsApply] = useState(false);
  const [isHide, setIsHide] = useState(false);

  // console.log(data)

  useEffect(() => {
    // if(data.length){
    //   console.log('hi')

    //   const urlParams=location.pathname.split('/');
    //   const jobID=urlParams[urlParams.length-1];

    //   console.log(userType)
    //   let jobUrl=`http://54.162.60.38/job/?job_id=${btoa(jobID.toString())}`;
    //   userType ? jobUrl+= `&user_type=${btoa(userType)}` : jobUrl+='';
    //   Axios.get(jobUrl)
    //     .then(res=>{
    //       console.log(res);
    //       if(res.data.success){
    //         setdata(res.data.data)
    //       }
    //     })
    //     .catch(err=>{
    //       console.log(err);
    //     })

    // }else{
    //   console.log('bye')
    //   setJobData(data)
    // }
    setJobData(data);
  }, [data, jobData]);

  return (
    <>
      {data !== null ? (
        <Grid
          container
          justify="center"
          onClick={open ? () => {} : () => handleClosey(false)}
        >
          <Grid xs={open ? 12 : 9}>
            <Card style={{ height: "100vh", overflowY: "scroll" }}>
              <CardContent>
                {!closeIcon && (
                  <span
                    className="float-right pointer"
                    onClick={() => handleClosey(false)}
                  >
                    <CloseIcon />
                  </span>
                )}

                <Grid container >
                  <Grid xs={12}>
                    <Grid container>
                      <Grid xs={12} sm={10}>
                        <h4>
                          <b>{data.job_role_name}</b>
                        </h4>
                      </Grid>
                      <Grid container>
                        <Grid xs={12} sm={10}>
                          <div className="flex flex-wrap">
                            {data.eligibility_criteria.skills.map((item) => (
                              <span className="viewjob_tag">
                                {item.skill_name}
                              </span>
                            ))}
                          </div>
                        </Grid>
                      </Grid>

                      <Grid container className="mg-top-10">
                        <Grid xs={6} sm={7} className="mg-top-10">
                          <b className="flex align-item-baseline">
                            <img
                              src={MoneyIcon}
                              style={{ width: 15, marginRight: 5 }}
                            />
                            <h6 className="viewjob_heading flex align-item-center">
                              {" "}
                              Compensation
                            </h6>
                          </b>
                          <div className="flex align-item-center viewjob_data">
                            ₹ {data.ctc_max} - {data.ctc_min}{" "}
                            {data.salary_payment_type}
                          </div>
                        </Grid>
                        <Grid xs={6} sm={4} className="mg-top-10">
                          <b>
                            <h6 className="viewjob_heading">Equity range</h6>
                          </b>
                          <div className="viewjob_data">
                            {data.equity_min}% - {data.equity_max}%
                          </div>
                        </Grid>
                      </Grid>
                      <Grid container>
                        <Grid
                          xs={12}
                          className="flex"
                          justify="flex-end"
                          style={{
                            borderBottom: "1px solid #DEDEDE",
                            paddingBottom: 10,
                          }}
                        >
                          {hide && (
                            <>
                              {" "}
                              <HiddenBtn
                                isHide={isHide}
                                setIsHide={setIsHide}
                                jobId={data.job_id}
                                reject={false}
                              />{" "}
                            </>
                          )}
                          {apply && (
                            <>
                              {" "}
                              <ApplyButton
                                isHide={isHide}
                                setIsHide={setIsHide}
                                jobId={data.job_id}
                                reject={false}
                                setIsApply={setIsApply}
                              />{" "}
                            </>
                          )}
                        </Grid>
                      </Grid>
                      <hr />
                      <Divider />
                      <Grid container className="mg-top-10">
                        <Grid xs={12} sm={7}>
                          <b>
                            <h6 className="viewjob_heading flex align-item-center">
                              {" "}
                              <RoomIcon /> Office location
                            </h6>
                          </b>
                          <div className="viewjob_data">
                            {data.job_location[0]?.city}
                          </div>
                        </Grid>
                        <Grid xs={12} sm={3}>
                          <b>
                            <div className="flex align-item-center">
                              <WorkOutlineIcon />
                              <h6 className="mg-0 viewjob_heading">Job type</h6>
                            </div>
                          </b>
                          <div className="mg-left-10 viewjob_data">
                            {data.job_type_name}
                          </div>
                        </Grid>
                      </Grid>
                      <Grid container>
                        <Grid xs={12} sm={7} className="mg-top-10">
                          <b>
                            <div className="flex align-item-center">
                              <HourglassEmptyIcon />
                              <h6 className="mg-0 viewjob_heading">
                                Employment type
                              </h6>
                            </div>
                          </b>
                          <div className="mg-left-10 viewjob_data">
                            {data.employment_type_name}
                          </div>
                        </Grid>
                        <Grid xs={12} sm={5} className="mg-top-10">
                          {data.job_duration_end !== null && (
                            <>
                              <b>
                                <h6 className="viewjob_heading">Duration</h6>
                              </b>
                              <div className="viewjob_data">
                                {data.job_duration_start} -{" "}
                                {data.job_duration_end}
                              </div>
                            </>
                          )}
                        </Grid>
                      </Grid>
                      <Grid container>
                        <Grid x={12} className="mg-top-10">
                          <b>
                            <h6 className="viewjob_heading">Description</h6>
                          </b>
                          <div className="viewjob_data">
                            {data.job_description}
                          </div>
                        </Grid>
                      </Grid>
                      <Grid container>
                        <Grid xs={12} className="mg-top-10">
                          <b>
                            <h6 className="viewjob_heading">Preferences</h6>
                          </b>
                          <div>{}</div>
                        </Grid>
                      </Grid>
                      <Grid container>
                        <Grid xs={12} className="mg-top-10">
                          <b>
                            <h6 className="viewjob_heading">
                              About {data?.company?.company_name}
                            </h6>
                          </b>

                          <div className="flex flex-wrap">
                            <span className="viewjob_tag">
                              {data?.company?.industry_name}
                            </span>
                          </div>
                          <div className="mg-top-10 viewjob_data ">
                            {data.job_description}
                          </div>
                        </Grid>
                      </Grid>
                      <Grid container>
                        <Grid xs={12} className="mg-top-10">
                          <div>
                            <span className="fs-16 fw-700">Company size :</span>{" "}
                            <span>{data?.company?.company_size}</span>
                          </div>
                          <div>
                            <span className="fs-16 fw-700">check out us @</span>
                            <span className="viewjob_data">
                              {data?.company?.company_website}
                            </span>
                          </div>
                        </Grid>
                      </Grid>
                      <Grid container>
                        <Grid xs={12} className="mg-top-10">
                          {data.documents !== undefined && (
                            <div>
                              <b>
                                <h6 className="viewjob_heading">
                                  Documents required
                                </h6>
                              </b>

                              <div className="viewjob_data">
                                {data.documents.cover_letter === "required" && (
                                  <> • Cover letter</>
                                )}
                              </div>

                              <div className="viewjob_data">
                                {data.documents.resume === "required" && (
                                  <> • Resume</>
                                )}
                              </div>

                              <div className="viewjob_data">
                                {data.documents.transcipt === "required" && (
                                  <> • Transcipt</>
                                )}
                              </div>
                            </div>
                          )}
                        </Grid>
                      </Grid>
                      <Grid container>
                        <Grid xs={12} className="flex" justify="flex-end">
                          {hide && (
                            <>
                              {" "}
                              <HiddenBtn
                                isHide={isHide}
                                setIsHide={setIsHide}
                                jobId={data.job_id}
                                reject={false}
                              />{" "}
                            </>
                          )}
                          {apply && (
                            <>
                              {" "}
                              <ApplyButton
                                isApply={isApply}
                                setIsApply={setIsApply}
                                jobId={data.job_id}
                                reject={false}
                              />
                            </>
                          )}
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </CardContent>
            </Card>
          </Grid>
        </Grid>
      ) : (
        ""
      )}
    </>
  );
};

export default ViewJob;

const ApplyButton = ({ jobId, reject, isApply, setIsApply }) => {
  const loading = Loading();
  const [done, setDone] = useState(false);

  const data = JSON.parse(localStorage.getItem("user_details"));

  const handelActive = (jobId) => {
    loading.changeLoading(true);
    fetch("http://54.162.60.38/job/student/apply/", {
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      body: `job_id=${jobId}&user_id=${data.id}&round=1&status=1&feedback=1`,
    })
      .then((res) => res.json())
      .then((data) => {
        alert(data.data.message);
        loading.changeLoading(false);
        setDone(true);
        setIsApply(true);
      });
  };
  return (
    <Button
      variant="contained"
      style={{ backgroundColor: "#3282C4", float: "right" }}
      className="text-white fw-700"
      disabled={isApply}
      onClick={() => handelActive(jobId)}
    >
      {reject
        ? isApply
          ? "Accepted"
          : "Accept"
        : isApply
        ? "Applied"
        : "Apply"}
      {/* {
            done ? 'Applied' : 'Apply'
        } */}
    </Button>
  );
};

const HiddenBtn = ({ jobId, hideStatus, isHide, setIsHide }) => {
  const [done, setDone] = useState(hideStatus);
  const handelActive = (jobId) => {
    const data = JSON.parse(localStorage.getItem("user_details"));

    if (hideStatus) {
      fetch("http://54.162.60.38/job/student/status/", {
        method: "POST",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        body: `job_id=${jobId}&user_id=${data.id}&status=unhide`,
      })
        .then((res) => res.json())
        .then((data) => {
          alert(data.data.message);
          setIsHide(false);
        });
    } else {
      fetch("http://54.162.60.38/job/student/status/", {
        method: "POST",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        body: `job_id=${jobId}&user_id=${data.id}&status=hidden`,
      })
        .then((res) => res.json())
        .then((data) => {
          alert(data.data.message);
          setIsHide(true);
        });
    }
  };

  return (
    <>
      {isHide ? (
        <>
          {" "}
          <Button
            className="mg-right-15"
            style={{ marginRight: 15 }}
            disabled={true}
            variant="outlined"
          >
            Hidden
          </Button>
        </>
      ) : (
        <>
          <Button
            variant="outlined"
            style={{ marginRight: 15 }}
            className="pointer text-blue mg-right-15"
            onClick={(event) => {
              event.stopPropagation();
              handelActive(jobId);
            }}
          >
            <span className="fs-12 text-blue">hide</span>
          </Button>
        </>
      )}
    </>
  );
};
