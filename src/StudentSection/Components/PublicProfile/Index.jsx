import React, { useState, useEffect } from 'react'
import { Card, CardContent, Grid, Avatar } from '@material-ui/core'
import GetAppIcon from '@material-ui/icons/GetApp';
import { Link } from 'react-router-dom';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import {httpRequestGet} from '../../../utils/httpRequest';
import baseUrl from '../../../common/CONSTANT'
import CloseIcon from '@material-ui/icons/Close';
import './publicprofile.css'
import Download from '../../../assets/svg/bx_bx-download.svg'
import { BreakpointProvider, Breakpoint } from 'react-socks';
export default function Index({handleClosey,userId}) {
    console.log(userId)

    const[project,setProjectData]=useState([])
    const[profile,setProfileData]=useState(null)
    const[education,setEducationData]=useState([])
    const[allData,setAllData]=useState(null)
    const[exp,setExpData]=useState([])
    const[skill,setSkillData]=useState([])
    // const[exp,setExpData]=useState([])
    useEffect(()=>{
        console.log(userId)
        const token=localStorage.getItem('gw_token');
        const data=JSON.parse(localStorage.getItem('user_details'))
        let id;
        if(userId){
            
             id=userId
        }
        else{
             id=data.id
        }
        GetData(baseUrl.pravesh.BASE_URL,`api/shared/public_profile/?user_id=${id}`,{headers:token},setAllData)
        // GetData(baseUrl.pravesh.BASE_URL,'api/education/user_education',{headers:token},setEducationData)
        // GetData(baseUrl.pravesh.BASE_URL,'api/company/work_experience',{headers:token},setExpData)
        // // GetData(baseUrl.pravesh.BASE_URL,'api/education/user_skill',{headers:token},setSkillsData)
        // // GetData(baseUrl.pravesh.BASE_URL,'api/education/student_details',{headers:token},setAboutData)
       
        // GetData(baseUrl.pravesh.BASE_URL,'api/education/user_project',{headers:token},setProjectData)
        // GetData(baseUrl.pravesh.BASE_URL,'api/shared/user/?user_type=Student',{headers:token},setProfileData)
       
    },[])
    const GetData=async(baseUrl,endPoint,body,updateState)=>{

        let res = await httpRequestGet(baseUrl,endPoint,body)
        updateState(res.data)
        console.log(res.data)
       
       

 
      }

      useEffect(()=>{
          if(allData!==null){

              setEducationData(allData.student_user_details.education)
              setProjectData(allData.student_user_details.project)
              setExpData(allData.student_user_details.work_ex)
              setSkillData(allData.student_user_details.skill)

          }
      },[allData])
    return (
        <BreakpointProvider>
            <Breakpoint large up >

        <div className='public-profile-modal mt-5' onClick={()=>handleClosey(false)} >
        <Grid container justify='center'>

            <Grid xs={12} sm={userId ?6:8}>
            <Card className='pd-0'>
                        <CardContent className='pd-0 scrollY' style={{padding:0, zIndex: 1500}}>
                        {
                            userId !==undefined ? (
                                <div className='float-right pt-2 pointer' onClick={()=>handleClosey(false)}>
                           <CloseIcon/>
                        </div>
                            ):<div className='pt-3'/>
                        }
                        {/* <div className='float-right pt-2 pointer' onClick={()=>handleClosey(false)}>
                           <CloseIcon/>
                        </div> */}
                          {
                              allData!== null && (<Grid container className='px-4 pb-4'>
                                <Grid xs={3}>
                                    <Avatar src={allData.profile_picture} alt='' style={{height:150,width:150}} />
                                </Grid>
                                <Grid xs={7} style={{marginLeft:'55px'}}>

                                <div className='text-left mg-left-10 pl-2'>

                                    <h4 className='mg-top-20'>
                                        {allData.first_name} {allData.last_name} {' '} {' '} {allData.current_city_name}
                                    </h4>
                                    <div>
                                    {allData.student_user_details.education[0].degree}
                                    </div>
                                    <div>
                                    {allData.student_user_details.education[0].college_name}
                                    </div>
                                </div>

                                <Grid container>
                                <Grid xs={4}>
                                {
                                    allData.student_user_details.resume && ( <a href={`https://${allData.student_user_details.resume}`} target='_blank' className='flex align-item-center resume-btn ml-2'><img src={Download} className='ml-2'/>resume</a>)
                                }
                                       
                                    </Grid>
                                    <Grid xs={3}>
                                        {/* <Link to={''}><LinkedInIcon/></Link> */}
                                    </Grid>
                                </Grid>

                                    
                                </Grid>
                            </Grid>)
                            }
                            <Grid container className='click-bg px-2' style={{height:'100%'}}>
                                <Grid xs={12} className='pd-10 '>

                              

                            { profile!==null && (<> 
                            
                                <Grid container>
                                    <Grid xs={12}>
                                        <div className='fs-20 text-left fw-700 heading'>
                                           555555555555555 About
                                        </div>
                                    </Grid>
                                </Grid>

                             <Grid container>
                                    <Grid xs={12}>

                                        <div>
                                            {allData.student_user_details.about ===null ?  <div className='fs-16 fw-700'>No data</div>:allData.student_user_details.about}
                                        </div>
                                    </Grid>
                                </Grid></>) 
                                }
                                <Grid container className='0'>
                            
                                <Grid xs={12}>
                                

                                    {exp.length ?(<div>
                                        <h4 className='text-left heading'>
                                        Experience 
                                    </h4>
                                      {
                                          exp.map((item)=>(
                                              <div className='text-left text-content fw-700'>
                                                  {
                                                      item.company_name
                                                  }{' '},{' '}
                                                  <span className='text-content fw-300'>
                                                      {
                                                          item.job_designation
                                                      }
                                                  </span>
                                                  <div className='fs-12 text-content fw-300'>

                                                   {
                                                       item.start_date
                                                   }
                                                    
                                                   {
                                                       item?.end_date ? " to " + item.end_date : ""
                                                   }
                                                   </div>
                                                   <div className='fs-12 text-content fw-300'>

                                                   {
                                                       item.job_description
                                                   }
                                                   
                                                   </div>
                                              </div>
                                          ))
                                      }
                                    </div>):<div className='fs-16 fw-700'>{' ' }</div>}
                                    
                                </Grid>
                            </Grid>
                            <Grid container>
                            <Grid xs={12}>
                                   <h4 className='text-left heading'>
                                   {project.length?'Project' :null}
                                    
                                   </h4>
                                      {
                                       project.map((item)=>(
                                   <div>
                                             <div className='text-left text-content fw-700'>
                                                 {
                                                     item.title
                                                 }
                                                 <div className='fs-12 text-content fw-300 text-content'>

                                                  {
                                                      item.start_date
                                                  }
                                                   
                                                  {
                                                      item?.end_date ? " to " + item.end_date : ""
                                                  }
                                                  </div>
                                                  <div className='fs-12 text-content fw-300'>

                                                  {
                                                      item.description
                                                  }
                                                  
                                                  </div>
                                                  <div className='fs-12 text-content fw-700'>
                                                   <a href={`https://${item.links}`} rel='noopener noreffer' target='_blank'>Link</a>
                                                  </div>
                                             </div>
                                   </div>
                                   
                                   
                                         ))  
                                      }
                               </Grid>
                            </Grid>
                            <Grid container className='mg-top-20'>
                                <Grid xs={6}>
                                <Grid container>
                                
                                <Grid xs={12}>
                                {
                                    education.length? ( <h4 className='text-left heading'>
                                       Education
                                    </h4>):null
                                }
                                  
                                    <div>
                                       {
                                           education.map((item)=>(
                                               <div className='text-left mg-top-10'>
                                               <div className='text-content fw-700'>

                                                   {
                                                       item.degree
                                                   }{' '}, {' '}
                                                   <span className='text-content fw-300'>

                                                   {
                                                       item.college_name
                                                   }
                                                   </span>
                                               </div>
                                                   <div className='fs-12 text-content fw-300'>

                                                   {
                                                       item.start_date
                                                   }
                                                      
                                                   {
                                                       item?.end_date ? " to " + item.end_date : ""
                                                   }
                                                   </div>
                                               </div>
                                           ))
                                       }
                                    </div>
                                   
                                    
                                </Grid>
                            </Grid>
                            <Grid xs={6}>
                            <Grid container>
                               
                               {/* <Grid xs={12}>
                                   <h4 className='text-left'>
                                   {project.length?'Project' :null}
                                    
                                   </h4>
                                      {
                                       project.map((item)=>(
                                   <div>
                                             <div className='text-left text-content fw-700'>
                                                 {
                                                     item.title
                                                 }
                                                 <div className='fs-12 text-content fw-300 text-content'>

                                                  {
                                                      item.start_date
                                                  }
                                                  to   
                                                  {
                                                      item.end_date
                                                  }
                                                  </div>
                                                  <div className='fs-12 text-content fw-300'>

                                                  {
                                                      item.description
                                                  }
                                                  
                                                  </div>
                                                  <div className='fs-12 text-content fw-700'>
                                                   <a href={item.links} rel='noopener noreffer' target='_blank'>Link</a>
                                                  </div>
                                             </div>
                                   </div>
                                   
                                   
                                         ))  
                                      }
                               </Grid> */}
                           </Grid>
                            </Grid>
                                </Grid>
                                <Grid xs={6}>
                                <Grid container>
                                
                                <Grid xs={12}>
                                {
                                    skill.length? ( <h4 className='text-left heading'>
                                       Skill
                                    </h4>):null
                                }
                                  
                                    <div>
                                       {
                                           skill.map((item, index)=>(
                                               <span className='fs-16 fw-700 text-content'>
                                               {
                                                    item.skill_name
                                                }{ index === skill.length-1 ? "" : ', '}
                                                  
                                               </span>
                                           ))
                                       }
                                    </div>
                                   
                                    
                                </Grid>
                            </Grid>
                            {/* <Grid xs={6}>
                                      {
                                       project.map((item)=>(
                            <Grid container>
                               
                               <Grid xs={12}>
                                   <h4 className='text-left'>
                                      Project
                                   </h4>
                                   <div>
                                             <div className='text-left text-content fw-700'>
                                                 {
                                                     item.title
                                                 }
                                                 <div className='fs-12 text-content fw-300 text-content'>

                                                  {
                                                      item.start_date
                                                  }
                                                  to   
                                                  {
                                                      item.end_date
                                                  }
                                                  </div>
                                                  <div className='fs-12 text-content fw-300'>

                                                  {
                                                      item.description
                                                  }
                                                  
                                                  </div>
                                                  <div className='fs-12 text-content fw-700'>
                                                   <a href={item.links} rel='noopener noreffer' target='_blank'>Link</a>
                                                  </div>
                                             </div>
                                   </div>
                                   
                                   
                               </Grid>
                           </Grid>
                                         ))  
                                      }
                            </Grid> */}
                                </Grid>
                            </Grid>
                                </Grid>
                            </Grid>

                        </CardContent>
                    </Card>
                 
            </Grid>
        </Grid>
         
        </div>
            </Breakpoint>
            <Breakpoint medium down>

<div className='public-profile-modal mt-5' style={{marginTop: '45px'}}>
<Grid container justify='center'>
    <Grid xs={12} sm={6}>
    <Card className='mg-top-20  pd-0'>
                <CardContent className='pd-0 scrollY' style={{padding:0}}>
                {

                }
                <div className='float-right pd-10 pointer' onClick={()=>handleClosey(false)}>
                   <CloseIcon/>
                </div>
                  {
                      allData!== null && (<Grid container className='pd-10'>
                        <Grid xs={12} className='flex justify-center'>
                            <Avatar src={allData.profile_picture} alt='' style={{height:150,width:150}} />
                        </Grid>
                        <Grid xs={12} style={{marginLeft:'5%'}}>

                        <div className='text-left mg-top-20 mg-left-10'>

                            <h4 className='mg-top-20 text-center'>
                                {allData.first_name} {allData.last_name} {' '} {' '} {allData.current_city_name}
                            </h4>
                            <div className='text-center'>
                            {allData.student_user_details.education[0].degree}
                            </div>
                            <div className='text-center'>
                            {allData.student_user_details.education[0].college_name}
                            </div>
                        </div>

                        <Grid container justify='center'>
                        <Grid xs={12}>
                        {
                            allData.resume_path !==null && ( <Link to={''} className='flex align-item-center resume-btn justify-center' style={{width:100,marginLeft:'5rem'}}><GetAppIcon/>Resume</Link>)
                        }
                               
                            </Grid>
                            <Grid xs={3}>
                                {/* <Link to={''}><LinkedInIcon/></Link> */}
                            </Grid>
                        </Grid>

                            
                        </Grid>
                    </Grid>)
                    }
                    <Grid container className='click-bg' style={{height:'100%'}}>
                        <Grid xs={12} className='pd-10 '>

                      

                    { profile!==null && (<> 
                    
                        <Grid container>
                            <Grid xs={12}>
                                <div className='fs-20 text-left fw-700'>
                                    About
                                </div>
                            </Grid>
                        </Grid>

                     <Grid container>
                            <Grid xs={12}>

                                <div>
                                    {allData.student_user_details.about ===null ?  <div className='fs-16 fw-700'>No data</div>:allData.student_user_details.about}
                                </div>
                            </Grid>
                        </Grid></>) 
                        }
                        <Grid container className='mg-top-20'>
                    
                        <Grid xs={12}>
                        

                            {exp.length ?(<div>
                                <h4 className='text-left'>
                                Experience 
                            </h4>
                              {
                                  exp.map((item)=>(
                                      <div className='text-left text-content fw-700'>
                                          {
                                              item.company_name
                                          }{' '},{' '}
                                          <span className='text-content fw-300'>
                                              {
                                                  item.job_designation
                                              }
                                          </span>
                                          <div className='fs-12 text-content fw-300'>

                                           {
                                               item.start_date
                                           }
                                          
                                           {
                                               item?.end_date ? " to " + item.end_date : ""
                                           }
                                           </div>
                                           <div className='fs-12 text-content fw-300'>

                                           {
                                               item.job_description
                                           }
                                           
                                           </div>
                                      </div>
                                  ))
                              }
                            </div>):<div className='fs-16 fw-700'>{' ' }</div>}
                            
                        </Grid>
                    </Grid>
                    <Grid container className='mg-top-20'>
                        <Grid xs={6}>
                        <Grid container>
                        
                        <Grid xs={12} className='mt-3'>
                        {
                            education.length? ( <h4 className='text-left'>
                               Education
                            </h4>) :null
                        }
                          
                            <div>
                               {
                                   education.map((item)=>(
                                       <div className='text-left text-content fw-700 mg-top-10'>
                                       <div>

                                           {
                                               item.degree
                                           }{' '}, {' '}
                                           <span className='text-content fw-300'>

                                           {
                                               item.college_name
                                           }
                                           </span>
                                       </div>
                                           <div className='fs-12 text-content fw-300'>

                                           {
                                               item.start_date
                                           }
                                           
                                           {
                                               item?.end_date ? " to " + item.end_date : ""
                                           }
                                           </div>
                                       </div>
                                   ))
                               }
                            </div>
                           
                            
                        </Grid>
                    </Grid>
                    <Grid xs={12}>
                    <Grid container>
                       
                       <Grid xs={12} className='mt-3'>
                           <h4 className='text-left'>
                           {project.length?'Project' :null}
                           </h4>
                           <div>
                              {
                               project.map((item)=>(
                                     <div className='text-left text-content fw-700'>
                                         {
                                             item.title
                                         }
                                         <div className='fs-12 text-content fw-300'>

                                          {
                                              item.start_date
                                          }
                                           
                                          {
                                              item?.end_date ? " to " + item.end_date : ""
                                          }
                                          </div>
                                          <div className='fs-12 text-content fw-300'>

                                          {
                                              item.description
                                          }
                                          
                                          </div>
                                          <div className='fs-12 text-content fw-700'>
                                           <a href={item.links} rel='noopener noreffer' target='_blank'>Link</a>
                                          </div>
                                     </div>
                                 ))  
                              }
                           </div>
                           
                           
                       </Grid>
                   </Grid>
                   <Grid container>
                                
                                <Grid xs={12} className='mt-3'>
                                {
                                    skill.length? ( <h4 className='text-left'>
                                       Skill
                                    </h4>):null
                                }
                                  
                                    <div className='flex'>
                                       {
                                           skill.map((item, index)=>(
                                               <span className='fs-12 fw-700 text-content'>
                                               {
                                                    item.skill_name
                                                }{ index === skill.length-1 ? "" : ', '}
                                                  
                                               </span>
                                           ))
                                       }
                                    </div>
                                   
                                    
                                </Grid>
                            </Grid>
                    </Grid>
                        </Grid>
                    </Grid>
                        </Grid>
                    </Grid>

                </CardContent>
            </Card>
         
    </Grid>
</Grid>
 
</div>
    </Breakpoint>
        </BreakpointProvider>
    )
}
