import React from 'react'
import { Card, CardContent, Avatar, Grid } from '@material-ui/core'
import './blog.css'
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
const color=['yellow','white','#68F7F7','#F2F2F2']

export default function BlogCard({data}) {
    const useStyles = makeStyles({
        overlay: {
            display: 'inlineBlock',
            backgroundSize: '100 100%',
            boxShadow: 'inset 2000px 0 0 0 rgba(0, 0, 0, 0.5)',
            borderColor: 'rgba(0, 0, 0, 1)',
           color:'white'


        },
      });
      const classes = useStyles();
    
    // console.log(data)
    return (
        <div className='blog'>
        <Grid container>
            <Grid xs={12}>

        <Grid container>
        {
            data.map((item)=>(

                
                <Grid sm={3} xs={6} className='mg-top-10'>
                  <Link to={{pathname:`/student/singleblog/${item.id}`,data:item}}>

           
<div className={item.image ? `${classes.overlay} blog__card` : 'blog__card'} style={!(item.image) ? {background:`${randomColor()}`,color:'black'} :{background:`url(${item.image})`}}>
    <h2>
        {item.title}    
    </h2>
</div>
<div className='flex align-item-center fs-12 mg-top-10 '>
    <Avatar src={item.author.profile_image} alt='user profile' style={{height:20,width:15}} className='mg-right-15'/>
    {item.author.first_name} {' '} {item.author.last_name}
</div>

</Link>
</Grid>
            ))
        }
            {/* <Grid xs={3}>

           
                    <div className='blog__card'>
                        <h2>
                            5 book needs
                        </h2>
                    </div>
                    <div className='flex align-item-center fs-12 mg-top-10 '>
                        <Avatar src='c' alt='user profile' style={{height:20,width:20}} className='mg-right-15'/>
                        Jenny
                    </div>
                
            </Grid> */}
        </Grid>
            </Grid>
        </Grid>
        </div>
    )
}


const randomColor=()=>{
    const random = Math.floor(Math.random() * color.length);
    return color[random]
}