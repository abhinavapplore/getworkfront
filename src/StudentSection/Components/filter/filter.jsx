import React, { useState, useEffect } from "react";
import {
  Grid,
  Divider,
  Paper,
  Modal,
  Button,
  MenuItem,
  Select,
} from "@material-ui/core";
import "./filter.css";
import Slider from "@material-ui/core/Slider";
import CloseIcon from "@material-ui/icons/Close";

import { httpRequest } from "../../../utils/httpRequest";
import baseUrl from "../../../common/CONSTANT";
import { value } from "jsonpath";
import kFormatter from "../../../utils/ZeroToK";
export default function Filter({
  filterData,
  setJobData,
  setSingleJobData,
  fullView,
}) {
  const [open, handleClose] = useState(false);

  // console.log(filterData)

  const initialState = {
    ctc_max: filterData.ctc_max,
    ctc_min: 0,
    equity_max: 3.5,
    equity_min: 0,
    job_title: [],
    job_type: [],
    job_type_name: [],
    location: [],
    company_id: [],
    company_name: [],
    temp: "",
    skills: [],
    salary_payment_type: "PER ANNUM",
    filter: false,
  };
  const outerInitialState = {
    ctc_max: filterData.ctc_max,
    ctc_min: 0,
    equity_max: 3.5,
    equity_min: 0,
    job_title: [],
    job_type: [],
    job_type_name: [],
    location: [],
    company_id: [],
    company_name: [],
    temp: "",
    skills: [],
    salary_payment_type: "PER ANNUM",
    filter: false,
  };
  const [state, setState] = useState(initialState);
  const [outerState, setOuterState] = useState(outerInitialState);

  const handleClick = () => {
    const token = localStorage.getItem("gw_token");
    const data = JSON.parse(localStorage.getItem("user_details"));
    let temp = "";

    for (let key in state) {
      if (state[key].length !== 0) {
        temp += "&" + key + "=" + state[key];

        //   console.log(temp);
      }
    }
    // console.log(temp)
    GetData(
      baseUrl.niyukti.BASE_URL,
      `job/student/job/?user_id=${window.btoa(
        data.id
      )}${temp}&college_id=${window.btoa(
        data.student_user_details.education[0].college
      )}`,
      { headers: token },
      setJobData
    );
    setOuterState(state);
  };

  const GetData = async (baseUrl, endPoint, body, updateState) => {
    let res = await httpRequest(baseUrl, endPoint, body);

    if (res.data.results.length) {
      setSingleJobData(res.data.results[0]);
      updateState(res.data.results);
    } else {
      updateState([]);
      setSingleJobData(null);
    }
    handleClose(!open);
  };

  const GetDataOut = async (baseUrl, endPoint, body, updateState) => {
    let res = await httpRequest(baseUrl, endPoint, body);

    if (res.data.results.length) {
      setSingleJobData(res.data.results[0]);
      updateState(res.data.results);
    } else {
      updateState([]);
      setSingleJobData(null);
    }
  };

  const callback = (value) => {
    // console.log(value)
    handleClose(value);
  };
  const resetAll = () => {
    const token = localStorage.getItem("gw_token");
    const data = JSON.parse(localStorage.getItem("user_details"));
    setState(initialState);
    GetData(
      baseUrl.niyukti.BASE_URL,
      `job/student/job/?user_id=${window.btoa(
        data.id
      )}&college_id=${window.btoa(
        data.student_user_details.education[0].college
      )}`,
      { headers: token },
      setJobData
    );

    setTimeout(() => handleClose(!open), 3000);
  };
  const resetAllOut = () => {
    const token = localStorage.getItem("gw_token");
    const data = JSON.parse(localStorage.getItem("user_details"));
    setState(initialState);
    setOuterState(outerInitialState);

    GetDataOut(
      baseUrl.niyukti.BASE_URL,
      `job/student/job/?user_id=${window.btoa(
        data.id
      )}&college_id=${window.btoa(
        data.student_user_details.education[0].college
      )}`,
      { headers: token },
      setJobData
    );
  };

  return (
    <div className="filter mt-5">
      {outerState.filter ? (
        <>
          <Grid container>
            <Grid xs={12} className="mb-1">
              {outerState.job_title.length > 0
                ? outerState.job_title.map((item) => (
                    <span className="filter__tag">
                      {item}{" "}
                      {/* <CloseIcon onClick={()=>deleteStateData('job_title',item)}/> */}
                    </span>
                  ))
                : null}
              {outerState.job_type_name.length > 0
                ? outerState.job_type_name.map((item) => (
                    <span className="filter__tag">
                      {item}{" "}
                      {/* <CloseIcon onClick={()=>deleteStateData('job_type',item)}/> */}
                    </span>
                  ))
                : null}
              {outerState.location.length > 0
                ? outerState.location.map((item) => (
                    <span className="filter__tag">
                      {item}{" "}
                      {/* <CloseIcon onClick={()=>deleteStateData('location',item)}/> */}
                    </span>
                  ))
                : null}
              {outerState.skills.length > 0
                ? outerState.skills.map((item) => (
                    <span className="filter__tag">
                      {item}{" "}
                      {/* <CloseIcon onClick={()=>deleteStateData('skills',item)}/> */}
                    </span>
                  ))
                : null}
              {outerState.company_name.length > 0
                ? outerState.company_name.map((item) => (
                    <span className="filter__tag">
                      {item}{" "}
                      {/* <CloseIcon onClick={()=>deleteStateData('skills',item)}/> */}
                    </span>
                  ))
                : null}

              {!outerState.filter && <div style={{ height: 50 }}> </div>}
            </Grid>
          </Grid>
          <Divider />
        </>
      ) : (
        ""
      )}
      <Grid container>
        <Grid xs={10}>
          <div
            className="fs-20 fw-700 text-black text-center"
            onClick={() => handleClose(true)}
          >
            Filters
          </div>
        </Grid>
        <Grid xs={2}>
          {outerState.filter && (
            <Button
              variant="outlined"
              color="primary"
              style={{ float: "right", marginRight: 15, marginTop: 5 }}
              className="fw-700"
              onClick={resetAllOut}
            >
              Clear All
            </Button>
          )}
        </Grid>
      </Grid>
      {/* <Modal
        open={open}
        onClose={()=>handleClose(!open)}
        
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        test
      </Modal> */}
      <Modal
        className="Modal-filter"
        open={open}
        onClose={() => handleClose(!open)}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <Grid container justify="center">
          <Grid xs={fullView ? 12 : 9}>
            {filterData !== null && (
              <FilterModal
                fullView={fullView}
                outerState={outerState}
                setOuterState={setOuterState}
                initialState={initialState}
                resetAll={resetAll}
                handleClose={callback}
                handleClick={handleClick}
                state={state}
                setState={setState}
                data={filterData}
                handleClosey={callback}
              />
            )}
          </Grid>
        </Grid>
      </Modal>
    </div>
  );
}

const FilterModal = ({
  data,
  state,
  fullView,
  handleClick,
  handleClose,
  setState,
  resetAll,
  initialState,
}) => {
  let allIDCompany = {};

  // console.log(data)

  data.companies.map((obj) => {
    allIDCompany[obj.company_id] = obj.company_name;
  });
  let allIdJobType = {};
  data.job_types.map((obj) => {
    allIdJobType[obj.job_type_id] = obj.job_type_name;
  });
  // console.log(allIdJobType)
  // console.log(allID)

  // const[value,setvlaue]=useState(0)
  const [valueeq, setvlaueeq] = useState([data.equity_min, data.equity_max]);
  // const [companies,ctc_max,ctc_min,equity_max,equity_min,job_titles,job_types,locations,skills]=data
  const [ctcValue, setCtcValue] = useState([data.ctc_min, data.ctc_max]);
  const handleChangeCtc = (event, newValue) => {
    // setCtcValue(newValue)
    setCtcValue(newValue);
    console.log(newValue[1]);

    setState({ ...state, ctc_min: newValue[0], ctc_max: newValue[1] });
    // setState({...state,ctc_max:newValue[0]})
  };
  const handleChange = (event, newValue) => {
    setvlaueeq(newValue);

    console.log(newValue);

    setState({ ...state, equity_min: newValue[0], equity_max: newValue[1] });
    // setState({...state,equity_min:newValue[0]})
  };

  const deleteStateData = (key, value) => {
    let tempState = state;

    let index = tempState[key].findIndex((item) => item === value);
    if (index > -1) {
      tempState[key].splice(index, 1);
    }

    setState({ ...tempState });
  };

  const updateState = (key, value) => {
    setState({ ...state, filter: true });
    let tempState = state;
    // console.log(tempState[key].includes(value))
    if (!tempState[key].includes(value)) {
      tempState[key].push(value);
    }
    tempState.filter = true;

    setState({ ...tempState });
  };

  const updateStateCompany = (key, value) => {
    updateState("company_name", allIDCompany[value]);
    updateState("company_id", value);

    // let temp=data.companies.filter((obj))
  };
  const updateStateJobType = (key, value) => {
    // console.log(key,value)
    // console.log(allIdJobType)
    // console.log(allIdJobType[value])
    updateState("job_type_name", allIdJobType[value]);
    updateState("job_type", value);

    // let temp=data.companies.filter((obj))
  };

  return (
    <div className="filter-modal pd-10">
      <Paper
        className="scroll filter-modal-height"
        justify="center"
        style={
          fullView
            ? { overflowY: "scroll", height: "auto", paddingBottom: "10px" }
            : { textAlign: "center" }
        }
      >
        <Grid container className="pd-10">
          <Grid xs={fullView ? 12 : 9}>
            {state.job_title.length > 0
              ? state.job_title.map((item) => (
                  <span className="filter__tag">
                    {item}{" "}
                    <CloseIcon
                      onClick={() => deleteStateData("job_title", item)}
                    />
                  </span>
                ))
              : null}
            {state.job_type_name.length > 0
              ? state.job_type_name.map((item) => (
                  <span className="filter__tag">
                    {item}{" "}
                    <CloseIcon
                      onClick={() => deleteStateData("job_type_name", item)}
                    />
                  </span>
                ))
              : null}
            {state.location.length > 0
              ? state.location.map((item) => (
                  <span className="filter__tag">
                    {item}{" "}
                    <CloseIcon
                      onClick={() => deleteStateData("location", item)}
                    />
                  </span>
                ))
              : null}
            {state.skills.length > 0
              ? state.skills.map((item) => (
                  <span className="filter__tag">
                    {item}{" "}
                    <CloseIcon
                      onClick={() => deleteStateData("skills", item)}
                    />
                  </span>
                ))
              : null}
            {state.company_name.length > 0
              ? state.company_name.map((item) => (
                  <span className="filter__tag">
                    {item}{" "}
                    <CloseIcon
                      onClick={() => deleteStateData("company_name", item)}
                    />
                  </span>
                ))
              : null}
          </Grid>
        </Grid>
        <Divider />
        <Grid container>
          <Grid xs={10}>
            <div className="fs-16 fw-700 text-black pd-10">Compensation</div>
          </Grid>
          {/* <Grid xs={2}>
                    <div className='fs-16 fw-300 text-blue pd-10' onClick={resetAll}>
                            Clear all
                        </div>
                    </Grid> */}
        </Grid>
        <Grid container justify="space-evenly" className="pd-10">
          <Grid sm={5} xs={11}>
            <div className="filter__salary">
              <p className="fs-16 fw-700 text-black">Salary(per year)</p>
              <span className="fs-12 fw-300 text-gray">
                INR 0-{kFormatter(data.ctc_max)}
              </span>
              <div>
                <Slider
                  value={ctcValue}
                  valueLabelDisplay="auto"
                  onChange={handleChangeCtc}
                  min={data.ctc_min}
                  max={data.ctc_max}
                  aria-labelledby="range-slider"
                />
              </div>
            </div>
          </Grid>
          <Grid sm={5} xs={11}>
            <div className="filter__salary">
              <p className="fs-16 fw-700 text-black">Equity range</p>
              <span className="fs-12 fw-300 text-gray">0.5%-3.5%</span>
              <div>
                <Slider
                  value={valueeq}
                  valueLabelDisplay="auto"
                  onChange={handleChange}
                  min={data.equity_min}
                  step={0.01}
                  max={data.equity_max}
                  aria-labelledby="range-slider"
                />
              </div>
            </div>
          </Grid>
        </Grid>
        <Divider />
        <Grid container>
          <Grid xs={12}>
            <div>
              <h3 className="fs-16 fw-700 text-black pd-10">
                Area of interest
              </h3>
            </div>
          </Grid>
        </Grid>
        <Grid container>
          <Grid sm={5} xs={11}>
            <div className="flex justify-space-even pd-10">
              <p className="fs-13 fw-700 text-black">Positon</p>
              <DropDown
                data={data.job_titles}
                value={state.job_title}
                handleChange={(event) =>
                  updateState("job_title", event.target.value)
                }
              />
            </div>
            <div className="flex justify-space-even pd-10">
              <p className="fs-13 fw-700 text-black">Type</p>
              {/* <DropDown data={data.job_types} value={state.job_type} handleChange={(event)=>updateState('job_type',event.target.value)}/> */}
              <DropDownJobType
                data={data.job_types}
                value={state.job_type}
                handleChange={(event) =>
                  updateStateJobType("job_type_id", event.target.value)
                }
              />
            </div>
          </Grid>
          <Grid sm={5} xs={11}>
            <div>
              <h3 className="fs-16 fw-700 text-black"></h3>
            </div>
            <div className="flex justify-space-even pd-10">
              <p className="fs-13 fw-700 text-black">Skills</p>
              <DropDown
                data={data.skills}
                handleChange={(event) =>
                  updateState("skills", event.target.value)
                }
              />
            </div>
            <div className="flex justify-space-even pd-10">
              <p className="fs-13 fw-700 text-black">Company</p>
              <DropDownCompany
                data={data.companies}
                value={state.company_id}
                handleChange={(event) =>
                  updateStateCompany("company_id", event.target.value)
                }
              />
            </div>
          </Grid>
        </Grid>
        <Divider />
        <Grid container>
          <Grid sm={5} xs={11}>
            <div className="">
              <h3 className="fs-16 fw-700 text-black pd-10">Location</h3>
            </div>
            <div className="flex justify-space-even pd-10">
              <p className="fs-13 fw-700 text-black">Area</p>
              <DropDown
                data={data.locations}
                value={state.location}
                handleChange={(event) =>
                  updateState("location", event.target.value)
                }
              />
            </div>
          </Grid>
        </Grid>
        <Divider />
        <Grid container>
          <Grid xs={12} className="pd-10 flex" justify="flex-end">
            <Button
              variant="outlined"
              color="primary"
              style={{ float: "right", marginRight: 15 }}
              className="fw-700"
              onClick={() => {
                setState(initialState);
                handleClose(false);
              }}
            >
              Close
            </Button>
            <Button
              variant="outlined"
              color="primary"
              style={{ float: "right", marginRight: 15 }}
              className="fw-700"
              onClick={resetAll}
            >
              Clear All
            </Button>
            <Button
              variant="contained"
              color="primary"
              style={{ backgroundColor: "#3282C4", float: "right" }}
              onClick={handleClick}
              className="fw-700"
            >
              View Results
            </Button>
          </Grid>
        </Grid>
      </Paper>
    </div>
  );
};

export function DropDown({ data, handleChange, value }) {
  // const[value2,setVlaue]=useState(value)

  return (
    <Select
      labelId="demo-simple-select-label"
      id="demo-simple-select"
      style={{ width: 150, background: "white", height: "40px" }}
      variant="outlined"
      value={value}
      onChange={handleChange}
    >
      {data.map((item) => (
        <MenuItem value={item}>{item}</MenuItem>
      ))}
    </Select>
  );
}

export function DropDownCompany({ data, handleChange, value }) {
  // const[value2,setVlaue]=useState(value)

  return (
    <Select
      labelId="demo-simple-select-label"
      id="demo-simple-select"
      style={{ width: 150, background: "white", height: "40px" }}
      variant="outlined"
      value={value}
      onChange={handleChange}
    >
      {data.map((item) => (
        <MenuItem value={item.company_id}>{item.company_name}</MenuItem>
      ))}
    </Select>
  );
}

export function DropDownJobType({ data, handleChange, value }) {
  // const[value2,setVlaue]=useState(value)
  //   console.log(data)
  return (
    <Select
      labelId="demo-simple-select-label"
      id="demo-simple-select"
      style={{ width: 150, background: "white", height: "40px" }}
      variant="outlined"
      value={value}
      onChange={handleChange}
    >
      {data.map((item) => (
        <MenuItem value={item.job_type_id}>{item.job_type_name}</MenuItem>
      ))}
    </Select>
  );
}
