import React, { useState } from "react";
import {
  Grid,
  CardContent,
  Card,
  Avatar,
  Modal,
  Button,
} from "@material-ui/core";
import kFormatter from "../../../utils/ZeroToK";
import ViewJob from "../viewJob/ViewJob";
import JobIcon from "../../../assets/svg/Job.svg";
import MoneyIcon from "../../../assets/svg/bx_bx-money.svg";
import { BreakpointProvider, Breakpoint } from "react-socks";
import BookmarkIcon from "@material-ui/icons/Bookmark";
import BookmarkBorderIcon from "@material-ui/icons/BookmarkBorder";
const AvatarStyle = {
  height: 60,
  width: 50,
  borderRadius: 5,
};

export default function SmallCard({
  data,
  setSingleJobData,
  selectedJobData,
  viewjob,
  apply,
}) {
  const [open, handleClose] = useState(false);
  const [jobData, setJobData] = useState([]);

  const openJob = (item) => {
    setJobData(item);
    handleClose(!open);
  };
  const callback = (value) => {
    handleClose(value);
    // do something with value in parent component, like save to state
  };

  return data.length
    ? data.map((item) => (
        <BreakpointProvider>
          <Breakpoint large up>
            <div className="small-card" onClick={() => openJob(item)}>
              <Grid container>
                <Grid xs={12}>
                  <Card
                    style={{ borderLeft: "3px solid #E55935" }}
                    onClick={() => setSingleJobData(item)}
                  >
                    <CardContent
                      className={
                        selectedJobData.job_id === item.job_id
                          ? "click-bg"
                          : " bye"
                      }
                    >
                      <Grid container>
                        <Grid xs={12}>
                          <Grid container className="job-card__top-header">
                            <Grid sm={3} xs={2}>
                              <Avatar
                                src={item.company?.company_logo}
                                alt="company-picture"
                                style={AvatarStyle}
                              />
                            </Grid>
                            <Grid xs={9} className="flex justify-space-between">
                              <div className="flex flex-col job-card__top-header__company-detail relative">
                                <h4
                                  className="fs-12"
                                  onClick={() => openJob(item)}
                                >
                                  {item.job_title}
                                </h4>

                                <p className="fs-12 mg-0">
                                  {item.company?.company_name}
                                </p>
                                <p className="fs-12 mg-0">
                                  {item.company?.company_location}
                                </p>
                              </div>
                              <div>
                                <div className=" flex align-item-center">
                                  <SaveIcon
                                    jobId={item.job_id}
                                    isSave={item.is_saved}
                                  />
                                </div>
                              </div>
                            </Grid>
                          </Grid>
                        </Grid>

                        <Grid container className="mg-top-10">
                          <Grid xs={12} className="fs-12 job-card__job-details">
                            <div>
                              <img
                                src={MoneyIcon}
                                style={{ width: 10, marginRight: 5 }}
                              />
                              <span className="mg-right-5 text-green">
                                {kFormatter(item.ctc_min)} -{" "}
                                {kFormatter(item.ctc_max)}{" "}
                                {item.salary_payment_type.toLowerCase()}
                              </span>
                              {"•"}{" "}
                              <img
                                src={JobIcon}
                                style={{ width: 10, marginRight: 5 }}
                              />
                              <span className="mg-right-15 text-green">
                                {item.employment_type_name}
                              </span>
                              <span className="mg-right-15">
                                {/* 1.5%-2% */}
                              </span>
                            </div>

                            {/* {
                                           interview && (<> <ApplyButton jobId={item.job_id}/></>)
                                       }
        
                                       {
                                           reject && (<><RejectButton jobId={item.job_id}/> <ApplyButton jobId={item.job_id}/></>)
                                       } */}
                          </Grid>
                        </Grid>
                        {/* {
                                        all &&(
                                            <Grid container>
                                       <Grid xs={12}>
                                       <div className='flex justify-end'>

                                       <div className='mg-right-15 mg-top-10 flex align-item-center'>

                                            <SaveIcon jobId={item.job_id} /> 
                                            </div>
                                            <div className='mg-right-15 mg-top-10 flex align-item-center'>

                                        
                                       <HiddenIcon jobId={item.job_id}/>
                                            </div>
                                       </div>

                                       </Grid>
                                   </Grid>
                                        )
                                    } */}
                      </Grid>
                    </CardContent>
                  </Card>
                </Grid>
              </Grid>
            </div>
          </Breakpoint>
          <Breakpoint medium down>
            <div
              className="small-card mg-bottom-10"
              onClick={() => openJob(item)}
            >
              <Grid container>
                <Grid xs={12}>
                  <Card
                    style={{ borderLeft: "3px solid #E55935" }}
                    onClick={() => setSingleJobData(item)}
                  >
                    <CardContent
                      className={
                        selectedJobData.job_id === item.job_id
                          ? "click-bg"
                          : " bye"
                      }
                    >
                      <Grid container>
                        <Grid xs={12}>
                          <Grid
                            container
                            justify="space-around"
                            className="job-card__top-header"
                          >
                            <Grid sm={3} xs={2}>
                              <Avatar
                                src={item.company?.company_logo}
                                alt="company-picture"
                                style={AvatarStyle}
                              />
                            </Grid>
                            <Grid xs={9} className="flex justify-space-between">
                              <div className="flex flex-col job-card__top-header__company-detail relative">
                                <h4
                                  className="fs-12"
                                  onClick={() => openJob(item)}
                                >
                                  {item.job_title}
                                </h4>

                                <p className="fs-12 mg-0">
                                  {item.company?.company_name}
                                </p>
                                <p className="fs-12 mg-0">
                                  {item.company?.company_location}
                                </p>
                              </div>
                              <div>
                                <div className="mg-right-15 mg-top-10 flex align-item-center">
                                  <SaveIcon
                                    jobId={item.job_id}
                                    isSave={item.is_saved}
                                  />
                                </div>
                              </div>
                            </Grid>
                          </Grid>
                        </Grid>

                        <Grid container className="mg-top-10">
                          <Grid xs={12} className="fs-12 job-card__job-details">
                            <div className="flex flex-col">
                              <span className="mg-right-15">
                                {item.employment_type_name}
                              </span>
                              <span className="mg-right-15 text-green">
                                {kFormatter(item.ctc_min)} -{" "}
                                {kFormatter(item.ctc_max)} per{" "}
                                {item.salary_payment_type.toLowerCase()}
                              </span>
                              <span className="mg-right-15">
                                {/* 1.5%-2% */}
                              </span>
                            </div>
                            {apply && (
                              <>
                                {" "}
                                <ApplyButton
                                  jobId={data.job_id}
                                  reject={false}
                                />
                              </>
                            )}

                            {/* {
                                   interview && (<> <ApplyButton jobId={item.job_id}/></>)
                               }

                               {
                                   reject && (<><RejectButton jobId={item.job_id}/> <ApplyButton jobId={item.job_id}/></>)
                               } */}
                          </Grid>
                        </Grid>

                        {/* {
                                all &&(
                                    <Grid container>
                               <Grid xs={12}>
                               <div className='flex justify-end'>

                               <div className='mg-right-15 mg-top-10 flex align-item-center'>

                                    <SaveIcon jobId={item.job_id} /> 
                                    </div>
                                    <div className='mg-right-15 mg-top-10 flex align-item-center'>

                                
                               <HiddenIcon jobId={item.job_id}/>
                                    </div>
                               </div>

                               </Grid>
                           </Grid>
                                )
                            } */}
                      </Grid>
                    </CardContent>
                  </Card>
                </Grid>
              </Grid>
              <Modal
                open={open}
                onClose={() => handleClose(!open)}
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
              >
                <Grid container justify="center">
                  <Grid xs={12} sm={12}>
                    <ViewJob
                      data={jobData}
                      apply={true}
                      open={true}
                      handleClosey={callback}
                    />
                  </Grid>
                </Grid>
              </Modal>
            </div>
          </Breakpoint>
        </BreakpointProvider>
      ))
    : "";
}

const ApplyButton = ({ jobId, reject }) => {
  const [done, setDone] = useState(false);

  const data = JSON.parse(localStorage.getItem("user_details"));

  const handelActive = (jobId) => {
    fetch("http://54.162.60.38/job/student/apply/", {
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      body: `job_id=${jobId}&user_id=${data.id}&round=1&status=1&feedback=1`,
    })
      .then((res) => res.json())
      .then((data) => {
        alert(data.data.message);

        setDone(true);
      });
  };
  return (
    <Button
      variant="contained"
      style={{ backgroundColor: "#3282C4", float: "right" }}
      className="text-white fw-700"
      disabled={done}
      onClick={() => handelActive(jobId)}
    >
      {reject ? (done ? "Accepted" : "Accept") : done ? "Applied" : "Apply"}
      {/* {
              done ? 'Applied' : 'Apply'
          } */}
    </Button>
  );
};

const SaveIcon = ({ jobId, isSave }) => {
  const data = JSON.parse(localStorage.getItem("user_details"));
  const [done, setDone] = useState(isSave);
  const handelActive = (jobId) => {
    if (done) {
      fetch("http://54.162.60.38/job/student/status/", {
        method: "POST",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        body: `job_id=${jobId}&user_id=${data.id}&status=unsave`,
      })
        .then((res) => res.json())
        .then((data) => {
          alert(data.data.message);
          setDone(!done);
          // updateData(jobId)
        });
    } else {
      fetch("http://54.162.60.38/job/student/status/", {
        method: "POST",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        body: `job_id=${jobId}&user_id=${data.id}&status=saved`,
      })
        .then((res) => res.json())
        .then((data) => {
          alert(data.data.message);
          setDone(!done);
          // updateData(jobId)
        });
    }
  };
  return (
    <>
      {done ? (
        <>
          {" "}
          <BookmarkIcon
            className="pointer text-blue"
            onClick={(event) => {
              event.stopPropagation();
              handelActive(jobId);
            }}
          />
          <span className="fs-12 text-blue"></span>
        </>
      ) : (
        <>
          {" "}
          <BookmarkBorderIcon
            className="pointer"
            onClick={(event) => {
              event.stopPropagation();
              handelActive(jobId);
            }}
          />
          <span className="fs-12 text-blue"></span>
        </>
      )}
    </>
  );
};
