import React from 'react'
import { Link } from 'react-router-dom'
import { Paper, Avatar } from '@material-ui/core'

export default function Index() {
    const data=JSON.parse(localStorage.getItem('user_details'))
    return (
        <div>
              <Link to='/student/profile'>

<div>
<Paper className='top-des__profile-card'>

  <Avatar src={data.profile_picture} alt='profile picture' className='my-2' style={{height:75,width:75}}/>
    <h3 className='fs-20 mb-2 overflow-hidden'>
      {data.first_name} {' '} {data.last_name}
    </h3>
    <p className='fs-14 text-center mb-0 overflow-hidden'>
    {data.student_user_details.education[0].degree}
      
     
    </p>
    <p className='fs-14 text-center mb-0'>
    from     
    </p>
   
    <p className='fs-12 text-center overflow-hidden'>
    {data.student_user_details.education[0].college_name}
    </p>
</Paper>
</div>
</Link>
        </div>
    )
}
