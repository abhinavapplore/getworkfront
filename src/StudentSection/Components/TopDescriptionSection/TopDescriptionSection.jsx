import React from 'react'
import {Grid, Paper, Avatar } from '@material-ui/core'
import './topDescription.css'
import  { Breakpoint, BreakpointProvider } from 'react-socks';
import { Link } from 'react-router-dom';

export default function TopDescriptionSection() {
  const data=JSON.parse(localStorage.getItem('user_details'))
  
    return (
      
<BreakpointProvider>
  <Breakpoint large up>

      <div className='top-des'>

       <Grid container justify='center'>
        
          <Grid xs={12} style={{marginTop:'80px'}}>

              <div className='top-des__introduction' >

              <p className='text-dark' style={{fontSize:'24px'}}>Online special <span className='text-green fw-700'>Content</span> and <span className='text-green fw-700'>Jobs</span> for you</p>
              <h3 style={{color:'#E55934',fontSize:'48px',fontWeight:'700'}}>
              What makes you different
              </h3>
              <h3 style={{color:'#E55934',fontSize:'48px',fontWeight:'700'}}>
             makes you strong
              </h3>

              </div>
          </Grid>
       </Grid>
      </div>
  </Breakpoint>
  <Breakpoint medium down>

<div className='top-des'>

 <Grid container>
   
    <Grid xs={12}>

        <div className='top-des__introduction' >

        <p className='fs-24 text-dark'>Online special <span className='text-green fw-700 text-center'>Content</span> and  <br/><span className='text-green fw-700'>Jobs</span> for you</p>
        <h3 className='fs-16 fw-700' style={{color:'#E55934'}}>
              What makes you <br/> different makes you strong
              </h3>

        </div>
    </Grid>
 </Grid>
</div>
</Breakpoint>
</BreakpointProvider>

    )
}
