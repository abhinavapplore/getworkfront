import React, { useState } from 'react'
import { Avatar, Grid, Modal } from '@material-ui/core'
import { useLocation } from "react-router-dom"
import { Link, NavLink } from 'react-router-dom'
import './connection.css'
import PublicPorfile from '../PublicProfile/Index'
export default function ConnectionCard({data}) {
    const[open,handleClose]=useState(false);
    const[value,setValue]=useState('')
    const loc = useLocation().pathname;
    let show = 0;
    console.log(loc);
    if (loc == "/student/dashboard") {
        data = data.slice(0,4);
        show = 1;
    }            
    const  callback = (value) => {
        handleClose(value)
       // do something with value in parent component, like save to state
   }
    // console.log(data)
    return (
        <div className='connection'>
        <Grid container>
            <Grid xs={12}>

        <Grid container>
        {   
            data.map((item)=>(
                
                <Grid sm={3} xs={6} className='mg-top-10' onClick={()=>{setValue(item.id);handleClose(!open)}}>
                <div className='connection__card'>

                    <Avatar src={item.profile_picture} alt='profile'/>

                    <div className='fs-16 fw-700 mg-top-10 text-white'>
                    {item.first_name} {item.last_name}
                    </div>
                    <div className='fs-10 text-gray'>
                        student
                    </div>
                    <div className='fs-16 text-white'>
                        {item.degree}
                    </div>
                </div>
            </Grid>
            ))
        }
            
        </Grid>
            </Grid>
        </Grid>
        {show ? (<Link to='/student/connection' className="connection_link">View All Connections <i class="fa fa-arrow-right"></i></Link>): ''}

        
        <Modal
        open={open}
        onClose={()=>handleClose(!open)}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
      <Grid container justify='center'>
          <Grid xs={10}>
          <PublicPorfile data={null} userId={value} handleClosey={callback} />

          </Grid>
      </Grid>
      </Modal>
        </div>
    )
}
