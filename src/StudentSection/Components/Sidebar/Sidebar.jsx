import React from 'react'
import { Link, NavLink } from 'react-router-dom'
import WorkOutlineIcon from '@material-ui/icons/WorkOutline';
import TocIcon from '@material-ui/icons/Toc';
import CallIcon from '@material-ui/icons/Call';
import PeopleAltIcon from '@material-ui/icons/PeopleAlt';
import SmallProileCard from '../../Components/SmallProfileCard/Index'
import HomeIcon from '../../../assets/svg/Home.svg'
import JobIcon from '../../../assets/svg/Job.svg'
import TelephoneIcon from '../../../assets/svg/bx_bx-phone-call (1).svg'
import ContentIcon from '../../../assets/svg/bx_bx-book-content.svg'
import ConnectionIcon from '../../../assets/svg/feather_users.svg'
import {
    Accordion,
    AccordionItem,
    AccordionItemButton,
    AccordionItemHeading,
    AccordionItemPanel,
} from 'react-accessible-accordion';

export default function Sidebar() {
    return (
        <div className='text-left fs-20' >
            <SmallProileCard />
            {/* <Link to='/student/open' className='fs-20 flex align-item-center'><img src={HomeIcon}/>Home</Link>
       <Link to='/student/open' className='fs-20 flex align-item-center mg-top-20'><img src={JobIcon}/>Jobs</Link>
         <div className='mg-left-30'>
         <Link to='/student/open' className='fs-20'>Open Jobs</Link>
         <Link to='/student/applied' className='fs-20'>Applied</Link>
         <Link to='/student/saved' className='fs-20'>Saved</Link>
         <Link to='/student/closed' className='fs-20'>Closed</Link>
         <Link to='/student/hidden' className='fs-20'>Hidden</Link>
         </div>

         <Link to='/student/interview' className='fs-20 flex align-item-center'><img src={TelephoneIcon}/>Interview</Link>
         <div className='mg-left-30'>
         <Link to='/student/invited' className='fs-20'>Invited</Link>
         <Link to='/student/scheduled' className='fs-20'>Scheduled</Link>
  
         </div>
         <Link to='/student/blog' className='fs-20 flex align-item-center'><img src={ContentIcon}/>Content</Link>
         <Link to='/student/connection' className='fs-20 flex align-item-center'><img src={ConnectionIcon}/>Connections</Link>
  */}
            <Accordion preExpanded={['b','c']} allowZeroExpanded style={{ padding: '2px 8px 6px 16px' }}>
                <AccordionItem uuid="a">
                    <AccordionItemHeading className='mg-top-20 fs-16'>
                        <AccordionItemButton>
                            <NavLink activeClassName='link-active' to='/student/dashboard' className='fs-20 flex align-item-center'><img src={HomeIcon} className='mr-2 pointer' />Home</NavLink>
                        </AccordionItemButton>
                    </AccordionItemHeading>
                </AccordionItem>
                <AccordionItem uuid="b">
                    <AccordionItemHeading className='active mg-top-20 fs-20 pointer'>
                        <AccordionItemButton>
                            <img src={JobIcon} className='mr-2 pointer' style={{color:'#000 !important'}} /> Jobs
                        </AccordionItemButton>
                    </AccordionItemHeading>
                    <AccordionItemPanel className='mg-left-20 mg-top-10 fs-16'>
                        <NavLink activeClassName='link-active' to='/student/open' className='fs-20'>Open Jobs</NavLink>
                    </AccordionItemPanel>
                    <AccordionItemPanel className='mg-left-20 mg-top-10 fs-16'>
                        <NavLink activeClassName='link-active' to='/student/applied' className='fs-20'>Applied</NavLink>
                    </AccordionItemPanel>
                    <AccordionItemPanel className='mg-left-20 mg-top-10 fs-16'>
                        <NavLink activeClassName='link-active' to='/student/saved' className='fs-20'>Saved</NavLink>
                    </AccordionItemPanel>
                    <AccordionItemPanel className='mg-left-20 mg-top-10 fs-16'>
                        <NavLink activeClassName='link-active' to='/student/closed' className='fs-20'>Closed</NavLink>
                    </AccordionItemPanel>
                    <AccordionItemPanel className='mg-left-20 mg-top-10 fs-16'>
                        <NavLink activeClassName='link-active' to='/student/hidden' className='fs-20'>Hidden</NavLink>
                    </AccordionItemPanel>
                </AccordionItem>
                <AccordionItem uuid="c">
                    <AccordionItemHeading className='mg-top-20 fs-20 pointer'>
                        <AccordionItemButton>
                            <img src={TelephoneIcon} className='mr-2 pointer' /> Interview
                    </AccordionItemButton>
                    </AccordionItemHeading>
                    <AccordionItemPanel className='mg-left-20 mg-top-10 fs-16'>
                        <NavLink activeClassName='link-active' to='/student/invited' className='fs-20'>Invited</NavLink>
                    </AccordionItemPanel>
                    <AccordionItemPanel className='mg-left-20 mg-top-10 fs-16'>
                        <NavLink activeClassName='link-active' to='/student/scheduled' className='fs-20'>Scheduled</NavLink>
                    </AccordionItemPanel>
                </AccordionItem>
                <AccordionItem uuid="d">
                    <AccordionItemHeading className='mg-top-20 fs-20'>
                        <AccordionItemButton>
                            <NavLink activeClassName='link-active' to='/student/blog' className='fs-20 flex align-item-center'><img src={ContentIcon} className='mr-2 pointer' />Content</NavLink>
                        </AccordionItemButton>
                    </AccordionItemHeading>
                </AccordionItem>
                <AccordionItem uuid="e">
                    <AccordionItemHeading className='mg-top-20 fs-20'>
                        <AccordionItemButton>
                            <NavLink activeClassName='link-active' to='/student/connection' className='fs-20 flex align-item-center'><img src={ConnectionIcon} className='mr-2 pointer' />Connections</NavLink>

                        </AccordionItemButton>
                    </AccordionItemHeading>
                </AccordionItem>
            </Accordion>
        </div>
    )
}
