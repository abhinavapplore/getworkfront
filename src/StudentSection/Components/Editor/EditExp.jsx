import React, { useState, useEffect } from 'react'
import { Grid, Card, CardContent, Select, MenuItem, TextField, Divider, Button } from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close';
import baseUrl from '../../../common/CONSTANT'
import {httpRequestPost, httpRequest} from '../../../utils/httpRequest';

const empType= [
    {
        "id": 1,
        "job_type": "Full Time"
    },
    {
        "id": 2,
        "job_type": "Part Time"
    }
]
export default function EditExp({handleClosey}) {
    const [state, setState] = React.useState({});
    const [dropState, setDropState] = React.useState({});

    const handleChange = event => {
      const {
        target: { name, value }
      } = event;
      console.log(name,value)
      setState({...state, [name]: value });
    };

    const handleDrop=event=>{
        const {
            target: { name, value}
          } = event;
          console.log(event.target)
          setDropState({...dropState, [name]: value});
    }
  
   
  
    const[companyList,setCompanyList]=useState([])
    useEffect(()=>{
        GetData(baseUrl.pravesh.BASE_URL,'api/company/company_list',null,setCompanyList)
    },[])
    const GetData=async(baseUrl,endPoint,header,updateState)=>{

        let res = await httpRequest(baseUrl,endPoint,header)
        console.log(res.data)
        updateState(res.data.all_companies)
  
      }
    return (
        <div className='edit-eduction'>
              <Grid container justify='center'>
                <Grid xs={6}>
                <Card>
                    <CardContent style={{height:'100vh',overflowY:'scroll'}}>
                    <Grid container style={{borderBottom:'1px solid black'}}>
                        <Grid xs={12} className='flex justify-space-between'>

                    <h5 className='text-left'>

                    Edit Experience
                       

                    </h5>
                    
                    <span className='float-right pointer' onClick={()=>handleClosey(false)} >
                        <CloseIcon/>
                    </span> 
                        </Grid>
                    </Grid>

                    <Grid container className=''>
                        <Grid xs={6} className='mg-top-10'>
                            <h5 className='mg-top-10 fs-16' >Title*</h5>
                        </Grid>
                    </Grid>
                    <Grid container>
                        <Grid xs={6} className='fs-16'>
                        

                        <TextField
          id="filled-select-currency-native"
          onChange={handleChange}
          name='title'
          size="small"
          SelectProps={{
            native: true,
          }}
         
          variant="filled"/>
       
                            
                        
                        
                        </Grid>
                    </Grid>
                    <Grid container className='mg-top-10 '>
                        <Grid xs={6} className=' '>
                            <h5  className='mg-top-10 fs-16'>Employment type *</h5>
                        </Grid>
                    </Grid>
                    <Grid container>
                        <Grid xs={6}>
                        

                        <TextField
         
          select
          name='empType'
          size="small"
          onChange={handleDrop}
          SelectProps={{
            native: true,
          }}
          helperText="Please select your employment type"
          variant="filled"
        >
       {
           empType.map((item)=><option value={item.id}>{item.job_type}</option>)
       }
          
       </TextField>
                      
                        
                        </Grid>
                    </Grid>

                    <Grid container className=''>
                        <Grid xs={6} className='mg-top-10 '>
                            <h5 className='mg-top-10 fs-16'>Company*</h5>
                        </Grid>
                    </Grid>
                    <Grid container>
                        <Grid xs={6}>
                       
                       {
                           companyList.length &&(

                            <TextField
        
          select
          name='companyName'
          size="small"
          onChange={handleDrop}
          SelectProps={{
            native: true,
          }}
          helperText="Please select your company name"
          variant="filled"
        >

            {
                companyList.map((item)=>
                    <option value={item.id}>{item.company}</option>
                )
            }
            <option id={0}>other</option>
        
          
       </TextField>

                           )
                       } 
                       {/* <TextField size="small" fullWidth id="filled-basic" label="Filled" variant="filled" />
                       */}
                        
                        </Grid>
                    </Grid>
                    <Grid container className=' '>
                        <Grid xs={6} className='mg-top-10 '>
                        <h5 className='mg-top-10 fs-16'>Location*</h5>
                        </Grid>
                    </Grid>
                    <Grid container>
                        <Grid xs={6}>
                        <TextField size="small" name='location' onChange={handleChange} fullWidth id="filled-basic" label="Filled" variant="filled" />
                        </Grid>
                    </Grid>
                    <Grid container>
                    <Grid xs={3}>
                            <h5 className='mg-top-10 fs-16'>Start date *</h5>
                        </Grid>
                        <Grid xs={3}>
                            <h5 className='mg-top-10 fs-16'>End date *</h5>
                        </Grid>
                    </Grid>
                    <Grid container>
                    <Grid xs={3}>
                           
                    <TextField
          id="filled-select-currency-native"
          onChange={handleChange}
          name='startDate'
          size="small"
          SelectProps={{
            native: true,
          }}
         
          variant="filled"/>
                        </Grid>
                        <Grid xs={3}>
                        <TextField
          id="filled-select-currency-native"
          name='endDate'
          onChange={handleChange}
          size="small"
          SelectProps={{
            native: true,
          }}
         
          variant="filled"/>
                        </Grid>
                    </Grid>
                    <Grid container className=''>
                        <Grid xs={6} className='mg-top-10 '>
                        <h5 className='mg-top-10 fs-16'>Description*</h5>
                        </Grid>
                    </Grid>
                    <Grid container>
                        <Grid xs={6}>
                        <TextField name='description' onChange={handleChange} size="small" fullWidth id="filled-basic" label="Filled" variant="filled" multiline />
                        </Grid>
                    </Grid>
                    <Grid container className='mg-top-10'>
                        <Grid xs={6} className='mg-top-10'>
                            <ApplyButton state={state} dropState={dropState}/>
                        </Grid>
                    </Grid>
                   

                    
                    </CardContent>
                </Card>
                </Grid>
                
            </Grid>
        </div>
    )
}

const ApplyButton=({state,dropState})=>{
    
    const [done,setDone]=useState(false)
    
    const handelActive=()=>{
        
        const token=localStorage.getItem('gw_token');
            fetch('http://praveshtest.getwork.org/api/company/work_experience/',{
                method: 'POST',
                headers: {
                  'Content-Type': 'application/x-www-form-urlencoded',
                  'Authorization': `Token ${token}`
                  
                },
                body: `company_id=${dropState.companyName}&company_name=${null}&company_website=https://www.youtube.com/&job_type_id=${dropState.empType}&start_date=${state.startDate}&end_date=${state.endDate}&job_designation=${state.title}&job_description=${state.description}&skills=${null}`
              }).then((res)=>res.json()).then((data)=>{
                  alert(data.data.message)
            
               
            })
              
        
        }
    return(
        <Button variant="contained" style={{backgroundColor:'#3282C4'}} className='text-white fw-700' disabled={done} onClick={()=>handelActive()}>
         Save
        </Button>
    )

    
}