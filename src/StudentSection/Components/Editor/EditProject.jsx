import React, { useState } from 'react'
import { Grid, Card, CardContent, TextField, Button } from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close';

export default function EditProject({handleClosey}) {
    return (
        <div className='edit-project'>
        <Grid container justify='center'>
          <Grid xs={6}>
          <Card>
              <CardContent>
              <Grid container style={{borderBottom:'1px solid black'}}>
                        <Grid xs={12} className='flex justify-space-between'>

                    <h5 className='text-left'>

                    Edit Project
                       

                    </h5>
                    
                    <span className='float-right pointer' onClick={()=>handleClosey(false)} >
                        <CloseIcon/>
                    </span> 
                        </Grid>
                    </Grid>

                    <Grid container className='mg-top-10 '>
                        <Grid xs={6} className=' '>
                            <h5  className='mg-top-10 fs-16'>Project Name *</h5>
                        </Grid>
                    </Grid>
            
           

            
              <Grid container>
                  <Grid xs={6}>
                  <TextField size='small' fullWidth id="filled-basic" label="Filled" variant="filled" />
                
                  
                  </Grid>
              </Grid>
              <Grid container className='mg-top-10 '>
                        <Grid xs={6} className=' '>
                            <h5  className='mg-top-10 fs-16'>Link *</h5>
                        </Grid>
                    </Grid>
              <Grid container>
                  <Grid xs={6}>
                  <TextField fullWidth id="filled-basic" label="Filled" variant="filled" />
                  </Grid>
              </Grid>
              <Grid container className='mg-top-10 '>
                        <Grid xs={6} className=' '>
                            <h5  className='mg-top-10 fs-16'>Description*</h5>
                        </Grid>
                    </Grid>
              <Grid container>
                  <Grid xs={6}>
                  <TextField fullWidth size='small' id="filled-basic" label="Filled" variant="filled" />
                  </Grid>
              </Grid>

              <Grid container className='mg-top-10'>
                        <Grid xs={6} className='mg-top-10'>
                            <ApplyButton/>
                        </Grid>
                    </Grid>
             

              
              </CardContent>
          </Card>
          </Grid>
          
      </Grid>
  </div>
    )
}

const ApplyButton=({jobId})=>{
    
    const [done,setDone]=useState(false)
    
    const handelActive=(jobId)=>{
        
       
            fetch('http://54.162.60.38/job/student/apply/',{
                method: 'POST',
                headers: {
                  'Content-Type': 'application/x-www-form-urlencoded'
                  
                },
                body: `job_id=${jobId}&user_id=2&round=1&status=1&feedback=1`
              }).then((res)=>res.json()).then((data)=>{
                  alert(data.data.message)
            
               
            })
              
        
        }
    return(
        <Button variant="contained" style={{backgroundColor:'#3282C4'}} className='text-white fw-700' disabled={done} onClick={()=>handelActive(jobId)}>
         Save
        </Button>
    )

    
}