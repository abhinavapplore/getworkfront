import React from 'react'
import { Grid, CardContent, Card, Select, MenuItem, InputLabel, TextField } from '@material-ui/core'

export default function Editor({ data, handleClosey }) {
    // console.log(handleClosey(false))
    const MakeItem = function (X) {
        return <MenuItem value={X}>{X}</MenuItem>;
    };
    return (
        data === null ? '' : (<div className='editor'>
            <Grid container>
                <Grid xs={12}>
                    <Card>
                        <CardContent>
                            <Grid container>
                                <Grid xs={12} className='flex justify-space-between'>

                                    <h4 className='text-left'>

                                        Edit {' '}
                                        {
                                            data.name
                                        }

                                    </h4>
                                    <span className='float-right' onClick={() => handleClosey(false)} >
                                        Close
                    </span>
                                </Grid>
                            </Grid>
                            {
                                data.field.map((item) => (
                                    <Grid container justify='center' className='mg-top-20'>
                                        <Grid xs={5}>

                                            {
                                                item.type === 'text' && (<>
                                                    <h6 className='text-left'>
                                                        {item.name}
                                                    </h6>
                                                    <TextField fullWidth id="filled-basic" label="Filled" variant="filled" />
                                                </>
                                                )
                                            }
                                            {
                                                item.type === 'drop' && (<>
                                                    <h6 className='text-left'>
                                                        {item.name}
                                                    </h6>
                                                    <Select
                                                        labelId="demo-simple-select-label"
                                                        id="demo-simple-select"
                                                        value={item.option[0]}

                                                    >
                                                        {
                                                            item.option.map(MakeItem)
                                                        }
                                                    </Select>
                                                </>
                                                )
                                            }
                                        </Grid>
                                    </Grid>
                                ))
                            }


                        </CardContent>
                    </Card>
                </Grid>

            </Grid>
        </div>)
    )
}

const ApplyButton = ({ jobId }) => {
    const loading = Loading();
    const [done, setDone] = useState(false)

    const handelActive = (jobId) => {

        loading.changeLoading(true)
        fetch('http://54.162.60.38/job/student/apply/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'

            },
            body: `job_id=${jobId}&user_id=2&round=1&status=1&feedback=1`
        }).then((res) => res.json()).then((data) => {
            alert(data.data.message)
            loading.changeLoading(false)
            setDone(true)
        })


    }
    return (
        <Button variant="contained" style={{ backgroundColor: '#3282C4' }} className='text-white fw-700' disabled={done} onClick={() => handelActive(jobId)}>
            {
                done ? 'Applied' : 'Apply'
            }
        </Button>
    )


}