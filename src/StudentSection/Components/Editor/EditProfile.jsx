import React, { useState } from 'react'
import { Grid, Card, CardContent, TextField, Button } from '@material-ui/core'

export default function EditProfile({handleClosey}) {
    return (
        <div className='edit-profile'>
        <Grid container>
          <Grid xs={12}>
          <Card>
              <CardContent>
              <Grid container>
                  <Grid xs={12} className='flex justify-space-between'>

              <h4 className='text-left'>

              Edit Profile
                 

              </h4>
              <span className='float-right' onClick={()=>handleClosey(false)} >
                  Close
              </span> 
                  </Grid>
              </Grid>

              <Grid container>
                  <Grid xs={6}>
                      <h3>Full Name*</h3>
                  </Grid>
              </Grid>
            
           

            
              <Grid container>
                  <Grid xs={6}>
                  <TextField fullWidth id="filled-basic" label="Filled" variant="filled" />
                
                  
                  </Grid>
              </Grid>
              <Grid container>
                  <Grid xs={6}>
                     Current Company
                  </Grid>
              </Grid>
              <Grid container>
                  <Grid xs={6}>
                  <TextField fullWidth id="filled-basic" label="Filled" variant="filled" />
                  </Grid>
              </Grid>
              <Grid container>
                  <Grid xs={6}>
                     Current College
                  </Grid>
              </Grid>
              <Grid container>
                  <Grid xs={6}>
                  <TextField fullWidth id="filled-basic" label="Filled" variant="filled" />
                  </Grid>
              </Grid>
              <Grid container>
                  <Grid xs={6}>
                     Upload Photo
                  </Grid>
              </Grid>
              <Grid container>
                  <Grid xs={6}>
                  <input type='file'/>
                  </Grid>
              </Grid>
             

              
              </CardContent>
          </Card>
          </Grid>
          
      </Grid>
  </div>
    )
}


const ApplyButton=({jobId})=>{
    
    const [done,setDone]=useState(false)
    
    const handelActive=(jobId)=>{
        
       
            fetch('http://54.162.60.38/job/student/apply/',{
                method: 'POST',
                headers: {
                  'Content-Type': 'application/x-www-form-urlencoded'
                  
                },
                body: `job_id=${jobId}&user_id=2&round=1&status=1&feedback=1`
              }).then((res)=>res.json()).then((data)=>{
                  alert(data.data.message)
            
               
            })
              
        
        }
    return(
        <Button variant="contained" style={{backgroundColor:'#3282C4'}} className='text-white fw-700' disabled={done} onClick={()=>handelActive(jobId)}>
         Save
        </Button>
    )

    
}