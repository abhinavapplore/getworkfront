import React, {useEffect, useState} from 'react'
import { Grid, Card, CardContent, Select, MenuItem, TextField, Button } from '@material-ui/core'
import baseUrl from '../../../common/CONSTANT'
import {httpRequest} from '../../../utils/httpRequest';
import CloseIcon from '@material-ui/icons/Close';
export default function EditEduction({handleClosey}) {

   
    
    const [collegeName,setCollegeName]=useState([])
    const [degreeName,setDegreeName]=useState([])
    const [gradeName,setGradeName]=useState([])
    const [courseName,setCourseName]=useState([])
    const [description,setDescription]=useState([])
    const [selectedCollegeName,setselectedCollegeName]=useState('none')
    const [state, setState] = React.useState({});
    const [dropState, setDropState] = React.useState({});

    const handleChange = event => {
      const {
        target: { name, value }
      } = event;
      console.log(name,value)
      setState({...state, [name]: value });
    };

    const handleDrop=event=>{
        const {
            target: { name, value}
          } = event;
          console.log(event.target)
          setDropState({...dropState, [name]: value});
    }
    useEffect(()=>{
        const token=localStorage.getItem('gw_token');

        GetData(baseUrl.pravesh.BASE_URL,'api/education/college',{headers:token},setCollegeName)
        // GetData(baseUrl.pravesh.BASE_URL,'api/education/degree',{headers:'ed296db75cb7d9533f1120ea41970ad535394154'},setDegreeName)
        // GetData(baseUrl.pravesh.BASE_URL,'api/education/specialization',{headers:'ed296db75cb7d9533f1120ea41970ad535394154'},setCourseName)
        

    },[])
    useEffect(()=>{

         
        // GetData(baseUrl.pravesh.BASE_URL,'api/education/degree',{headers:'ed296db75cb7d9533f1120ea41970ad535394154'},setDegreeName)

    },[collegeName])
    useEffect(()=>{

         
         // GetData(baseUrl.pravesh.BASE_URL,'api/education/specialization',{headers:'ed296db75cb7d9533f1120ea41970ad535394154'},setCourseName)
        
    },[degreeName])
    const GetData=async(baseUrl,endPoint,body,updateState)=>{
console.log(baseUrl,endPoint,body)
        let res = await httpRequest(baseUrl,endPoint,body)
        // console.log(res)
         updateState(res)

         console.log(collegeName)

  
      }

      const MakeItem = function(X) {
        //   console.log(X.name)
        return <option value={X.id}>{X.name}</option>;
    };
    
    return (
        <div className='edit-eduction'>
              <Grid container justify='center'>
                <Grid xs={6}>
                <Card>
                    <CardContent>
                    <Grid container style={{borderBottom:'1px solid black'}}>
                        <Grid xs={12} className='flex justify-space-between'>

                    <h5 className='text-left'>

                    Edit Education
                       

                    </h5>
                    
                    <span className='float-right pointer' onClick={()=>handleClosey(false)} >
                        <CloseIcon/>
                    </span> 
                        </Grid>
                    </Grid>
                    <Grid container className='mg-top-10 '>
                        <Grid xs={6} className=' '>
                            <h5  className='mg-top-10 fs-16'>School or College *</h5>
                        </Grid>
                    </Grid>
                    <Grid container>
                        <Grid xs={6}>
                        {
                            collegeName.length &&(

                                <TextField
          id="filled-select-currency-native"
          select
          name='collegeName'
          onChange={handleDrop}
          size="small"
          SelectProps={{
            native: true,
          }}
         
          variant="filled"
        >
        <MakeItem>
            none
        </MakeItem>
        {
            collegeName.map(MakeItem)
        }
          
        </TextField>
                            )
                        }
                        
                        </Grid>
                    </Grid>
                    <Grid container className='mg-top-10 '>
                        <Grid xs={6} className=' '>
                            <h5  className='mg-top-10 fs-16'>Degree *</h5>
                        </Grid>
                    </Grid>
                    <Grid container>
                        <Grid xs={6}>
                        {
                            degreeName.length &&(

                                <TextField
          id="filled-select-currency-native"
          select
          
          size="small"
          SelectProps={{
            native: true,
          }}
         
          variant="filled"
        >
        <MakeItem>
            none
        </MakeItem>
        {
            degreeName.map(MakeItem)
        }
          
        </TextField>
                            )
                        }
                        
                        </Grid>
                    </Grid>

                     <Grid container className='mg-top-10 '>
                        <Grid xs={6} className=' '>
                            <h5  className='mg-top-10 fs-16'>Field of study*</h5>
                        </Grid>
                    </Grid>
                    <Grid container>
                        <Grid xs={6}>
                        {
                            courseName.length &&(

                                <TextField
          id="filled-select-currency-native"
          select
          
          size="small"
          SelectProps={{
            native: true,
          }}
         
          variant="filled"
        >
        <MakeItem>
            none
        </MakeItem>
        {
            courseName.map(MakeItem)
        }
          
        </TextField>
                            )
                        }
                        
                        </Grid>
                    </Grid>
                    <Grid container className='mg-top-10 '>
                        <Grid xs={6} className=' '>
                            <h5  className='mg-top-10 fs-16'>Description*</h5>
                        </Grid>
                    </Grid>
                    <Grid container>
                        <Grid xs={6}>
                        <TextField multiline fullWidth id="filled-basic" size='small' label="Filled" variant="filled" />
                        </Grid>
                    </Grid>
                    <Grid container className='mg-top-10'>
                        <Grid xs={6} className='mg-top-10'>
                            <ApplyButton/>
                        </Grid>
                    </Grid>
                   

                    
                    </CardContent>
                </Card>
                </Grid>
                
            </Grid>
        </div>
    )
}


const ApplyButton=({jobId})=>{
    
    const [done,setDone]=useState(false)
    
    const handelActive=(jobId)=>{
        
       
            fetch('http://54.162.60.38/job/student/apply/',{
                method: 'POST',
                headers: {
                  'Content-Type': 'application/x-www-form-urlencoded'
                  
                },
                body: `job_id=${jobId}&user_id=2&round=1&status=1&feedback=1`
              }).then((res)=>res.json()).then((data)=>{
                  alert(data.data.message)
            
               
            })
              
        
        }
    return(
        <Button variant="contained" style={{backgroundColor:'#3282C4'}} className='text-white fw-700' disabled={done} onClick={()=>handelActive(jobId)}>
         Save
        </Button>
    )

    
}