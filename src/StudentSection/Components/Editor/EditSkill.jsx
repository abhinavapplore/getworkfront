
const ApplyButton=({jobId})=>{
    const loading=Loading();
    const [done,setDone]=useState(false)
    
    const handelActive=(jobId)=>{
        
        loading.changeLoading(true)
            fetch('http://54.162.60.38/job/student/apply/',{
                method: 'POST',
                headers: {
                  'Content-Type': 'application/x-www-form-urlencoded'
                  
                },
                body: `job_id=${jobId}&user_id=2&round=1&status=1&feedback=1`
              }).then((res)=>res.json()).then((data)=>{
                  alert(data.data.message)
                  loading.changeLoading(false)
                  setDone(true)
            })
              
        
        }
    return(
        <Button variant="contained" style={{backgroundColor:'#3282C4'}} className='text-white fw-700' disabled={done} onClick={()=>handelActive(jobId)}>
          {
              done ? 'Applied' : 'Apply'
          }
        </Button>
    )

    
}