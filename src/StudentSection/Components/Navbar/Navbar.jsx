import React, { useState } from 'react'
import { IconButton, Button, AppBar, Toolbar, Menu, MenuItem, Avatar } from '@material-ui/core'
import Logo from '../../../assets/images/gw_sidebar_icon.png'
import RingNotification from '../../../assets/icon/ri_notification-2-line.png';
import ProfileIcon from '../../../assets/icon/gg_profile.png';
import UserRequest from '../../../assets/icon/user request.png';
import MessageIcon from '../../../assets/icon/bx_bx-chat.png';
import HomeIcon from '../../../assets/svg/Home.svg'
import { Link } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from "react-router-dom";
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import './navbar.css'
import WorkOutlineIcon from '@material-ui/icons/WorkOutline';
import TocIcon from '@material-ui/icons/Toc';
import CallIcon from '@material-ui/icons/Call';
import EditIcon from '@material-ui/icons/Edit';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import PeopleAltIcon from '@material-ui/icons/PeopleAlt';
import { BreakpointProvider, Breakpoint } from 'react-socks';
import { EndPointPrefix } from '../../../constants/constants';
import axios from 'axios';
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  img: {
    width: 40
  }
}));


export default function Navbar() {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [open, setOpen] = useState(false);
  const [loading, setLoading] = useState(false)
  let history = useHistory();
  const data = JSON.parse(localStorage.getItem('user_details'))

  const profilePic = data.profile_picture
  const handleProfileMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleMenuClose = () => {
    setAnchorEl(null);

  };
  const logout = () => {
    console.log('inside logout')
    setLoading(true);
    axios.get(EndPointPrefix + '/logout/', {
      headers: {
        "Authorization": 'Token ' + localStorage.getItem('gw_token')
      }
    })
      .then(res => {
        setLoading(false);
        console.log('logout!', res);
        if (res.data.success) {

          localStorage.clear();

          history.push('/login')

        }
      })
      .catch(err => {
        console.log(err);
      })

  }
  function openNav() {
    document.getElementById("mySidebar").style.width = "250px";
    // document.getElementById("main").style.marginLeft = "250px";
  }

  function closeNav() {
    document.getElementById("mySidebar").style.width = "0";
    // document.getElementById("main").style.marginLeft= "0";
  }
  const isMenuOpen = Boolean(anchorEl);
  const classes = useStyles();
  const menuId = 'primary-search-account-menu';
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={menuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem > <Link to='/student/publicprofile' target='_blank'> <AccountCircleIcon className='mg-right-15' /> View Profile</Link></MenuItem>
      <MenuItem onClick={() => history.push('/student/profile')}><EditIcon className='mg-right-15' />Edit Profile</MenuItem>
      <MenuItem onClick={logout}><ExitToAppIcon className='mg-right-15' />Logout</MenuItem>
    </Menu>
  );
  return (
    <BreakpointProvider>
      <Breakpoint large up>

        <div className='navbar'>

          <AppBar position="fixed" color='transparent' style={{ boxShadow: 'none', background: '#E5E5E5', minHeight: 40 }}>
            <Toolbar >

              <Link to='/student/dashboard'>


                <img src={Logo} style={{ width: '142px' }} alt='' className={classes.img} />

              </Link>
              {/* <Typography variant="h6" className={classes.title}>
      News
    </Typography> */}


              <div>
                <IconButton>
                  <Avatar src={MessageIcon} style={{ height: 30, width: 30 }} />
                </IconButton>
                <IconButton>
                  <Avatar src={RingNotification} style={{ height: 30, width: 30 }} />
                </IconButton>

                <IconButton>
                  <Avatar src={UserRequest} style={{ height: 30, width: 30 }} />
                </IconButton>
                {/* <IconButton>
             <Avatar src={ProfileIcon} style={{height:30,width:30}}/>
            </IconButton> */}
                <IconButton
                  edge="end"
                  aria-label="account of current user"
                  aria-controls={menuId}
                  aria-haspopup="true"
                  onClick={handleProfileMenuOpen}
                  color="inherit"
                  style={{ float: 'right' }}
                >
                  <Avatar src={ProfileIcon} style={{ height: 30, width: 30 }} />
                </IconButton>
              </div>
            </Toolbar>
          </AppBar>
          {renderMenu}

        </div>
      </Breakpoint>
      <Breakpoint medium down>

        <div className='navbar'>

          <AppBar position="fixed" color='transparent' style={{ boxShadow: 'none', background: '#E5E5E5', minHeight: 40 }}>
            <Toolbar style={{ padding: 0 }}>

              <div id="mySidebar" class="sidebar-mobo">
                <a href="#" class="closebtn" onClick={closeNav}>×</a>
                <Link activeClassName='link-active' to='/student/dashboard' className='fs-20 flex align-item-center'><img src={HomeIcon} className='ml-1 pointer' />Home</Link>
                <Link to='/student/open' className='fs-20 flex align-item-center'><WorkOutlineIcon />Jobs</Link>
                <div className='mg-left-30 flex flex-col'>
                  <Link to='/student/open' className='fs-20 mt-1'>Open Jobs</Link>
                  <Link to='/student/applied' className='fs-20 mt-1'>Applied</Link>
                  <Link to='/student/saved' className='fs-20 mt-1'>Saved</Link>
                  <Link to='/student/closed' className='fs-20 mt-1'>Closed</Link>
                  <Link to='/student/hidden' className='fs-20 mt-1'>Hidden</Link>
                </div>

                <Link to='/student/interview' className='fs-20 flex align-item-center'><CallIcon />Interview</Link>
                <div className='mg-left-30 flex flex-col'>
                  <Link to='/student/invited' className='fs-20 mt-1'>Invited</Link>
                  <Link to='/student/scheduled' className='fs-20 mt-1'>Scheduled</Link>

                </div>
                <Link to='/student/blog' className='fs-20 flex align-item-center'><TocIcon />Content</Link>
                <Link to='/student/connection' className='fs-20 flex align-item-center'><PeopleAltIcon />Connections</Link>

              </div>
              <button class="openbtn" onClick={() => openNav()}>☰</button>
              <Link to='/student/dashboard'>


                <img src={Logo} alt='' className={classes.img} />

              </Link>
              {/* <Typography variant="h6" className={classes.title}>
News
</Typography> */}


              <div>
                <IconButton
                  edge="end"
                  aria-label="account of current user"
                  aria-controls={menuId}
                  aria-haspopup="true"
                  onClick={handleProfileMenuOpen}
                  color="inherit"
                  style={{ float: 'right' }}
                >
                  <Avatar src={ProfileIcon} style={{ height: 50, width: 50 }} />

                </IconButton>
              </div>
            </Toolbar>
          </AppBar>
          {renderMenu}

        </div>
      </Breakpoint>
    </BreakpointProvider>
  )
}
