import React, { Children } from 'react'
import Sidebar from '../Components/Sidebar/Sidebar'
import './layout.css'
import { Breakpoint, BreakpointProvider } from 'react-socks';
import Navbar from '../Components/Navbar/Navbar';
import { Container, Grid } from '@material-ui/core';
import CssBaseline from '@material-ui/core/CssBaseline';


export default function Layout({ children }) {

    return (
        <BreakpointProvider>
            <Breakpoint large up>
                {/* <Navbar/> */}
                <div class="container">
                    <div className='row'>
                        <Navbar />
                    </div>
                    <div class="row">
                        <aside class="col-2 sidedesktop  offset-md-0 fixed-top mt-5 scroll" id="left" >

                            <Sidebar />

                        </aside>

                        <main class="col offset-md-0 main-right h-100">

                            <div class="row">
                                <div class="col-12">
                                    {children}
                                </div>
                            </div>
                        </main>
                    </div>
                </div>

                {/*         
        <div className='layout'>
            <div class="row">
                <div class="col-sm-2 col-lg-2 col-md-2 col-xl-2">
                    <div className="sidenav">
                        <Sidebar/>
                        </div>
                </div>
            </div>
            <div class="col-sm-9 col-lg-9 col-md-9 col-xl-9">'
                <div className="main">
                    
  
                </div> 
            </div>
     
       
            


        </div> */}



            </Breakpoint>
            <Breakpoint medium down>
                <Navbar />
                <div className="main-mobo">
                    {children}

                </div>
            </Breakpoint>
        </BreakpointProvider>
    )
}
