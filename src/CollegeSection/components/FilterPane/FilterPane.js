import React,{useState, useEffect} from 'react'
import Skeleton from 'react-loading-skeleton';
import close_icon from '../../../assets/images/icons/close-icon.png'

const bg={
    background: '#F2F2F2'
}

const mainHRStyles={
    width: '100vw',
    marginLeft: '-30px'
}

export const FilterPane = ({show,
    college,setCollege,allColleges,selectedJobColleges,setSelectedJobColleges,
    location,setLocation,allLocations,selectedJobLocations,setSelectedJobLocations,
    degree,setDegree,allDegrees,selectedJobDegrees,setSelectedJobDegrees,
    skill,setSkill,allSkills,selectedJobSkills,setSelectedJobSkills,
    workEx,setWorkEx,allWorkEx,selectedJobWorkEx,setSelectedJobWorkEx,
    passoutYear,setPassoutYear,allPassoutYears,selectedJobPassoutYears,setSelectedJobPassoutYears
}) => {
     //right panel variables for storing all filter values
     const [jobSkills,setJobSkills]=useState([])
     const [jobDegrees,setJobDegrees]=useState([])
     const [jobColleges,setJobColleges]=useState(allColleges)
     const [jobLocations,setJobLocations]=useState(allLocations)
     const [jobWorkEx,setJobWorkEx]=useState(allWorkEx)
     const [jobPassoutYear,setJobPassoutYear]=useState([])
 
     //
     const [currentJobSkill,setCurrentJobSkill]=useState('All')
     const [currentJobDegree,setCurrentJobDegree]=useState('All')
     const [currentJobCollege,setCurrentJobCollege]=useState(college)
     const [currentJobLocation,setCurrentJobLocation]=useState('All')
     const [currentWorkEx,setCurrentWorkEx]=useState('All')
     const [currentPassoutYear,setCurrentPassoutYear]=useState('All')
 

     //const [selectedPassoutYear,setSelectedPassoutYear]=useState([])

    return (
                   
            <div className="card gw-card" style={{height:'90vh',overflowY:'scroll',overflowX:'hidden',width: '100%',top:'90px'}}>
                    {
                        1 ? 
                        <div className="card-body">
                            <div className="row my-0 justify-content-center px-3">
                                <p className="fs-18 mb-0"><b>Filter</b></p>
                            </div>
                            <hr style={mainHRStyles}/>
                            <div className="my-4 text-left">
                                <p className="fs-16 mb-1 fw-500">Location</p>
                                {selectedJobLocations ? 
                                    selectedJobLocations.map(jobLocation=>(<p className="fs-16 gray-2 my-1">{jobLocation}<span onClick={()=>{setSelectedJobLocations(selectedJobLocations.filter(joblocation=>joblocation!==jobLocation))}} className="close-icon cp"><img src={close_icon} alt=""/></span></p>)) : <Skeleton/>}
                                    <div className="dropdown my-2">
                            <div style={bg} className="dropdown-sort dropdown-toggle-custom" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style={{whiteSpace:'nowrap',overflow:'hidden',textOverflow:'ellipsis'}}>
                                {location}
                            </div>
                            <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                {
                                    allLocations && allLocations.map(job_location=>{
                                        return(
                                            <button onClick={()=>{
                                                setLocation(job_location)
                                                if(job_location!=='All' && !selectedJobLocations.includes(job_location))
                                                setSelectedJobLocations([...selectedJobLocations,job_location])
                                                }}  className="dropdown-item fs-18 cp" value={job_location} ><span className="text-truncate d-inline-block" style={{maxWidth:"175px"}}>{job_location}</span></button>
                                        )   
                                    })
                                }
                              
                            </div>
                        </div> 

                            </div>

                           
                            <div class="dropdown-divider"></div>
                            <div className="my-4 text-left">
                            <p className="fs-16 mb-1 fw-500">Passout year</p>

                               {selectedJobPassoutYears ? selectedJobPassoutYears.map(year=>(<p className="fs-16 gray-2 my-1">{year}<span onClick={()=>{setSelectedJobPassoutYears(selectedJobPassoutYears.filter(Year=>Year!==year))}} className="close-icon cp"><img src={close_icon} alt=""/></span></p>)) : <Skeleton/>}
                               <div className="dropdown my-2">
                           <div style={bg} className="dropdown-sort dropdown-toggle-custom" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style={{whiteSpace:'nowrap',overflow:'hidden',textOverflow:'ellipsis'}}>
                               {passoutYear}
                           </div>
                           <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                               {
                                   allPassoutYears && allPassoutYears.map(year=>{
                                       return(
                                           <button onClick={()=>{
                                               setPassoutYear(year)
                                               if(year!=='All' && !selectedJobPassoutYears.includes(year))
                                               setSelectedJobPassoutYears([...selectedJobPassoutYears,year])
                                               }}  className="dropdown-item fs-18 cp" value={year} >{year}</button>
                                       )   
                                   })
                               }
                             
                           </div>
                       </div> 
                           </div>

                            <div class="dropdown-divider"></div>
                            {/* <div className="my-4 text-left">
                                <p className="fs-16 mb-1 fw-500">Colleges</p>
                               
                                { selectedJobColleges ? selectedJobColleges.map(college=>{
                                        return(
                                            <p className="fs-16 gray-2 my-1">{college}<span className="close-icon cp" onClick={()=>{setSelectedJobColleges(selectedJobColleges.filter(jobcollege=>jobcollege!==college))}}><img src={close_icon} alt=""/></span></p>
                                        )
                                    }) : 
                                    <></>
                                }
                                    <div className="dropdown my-2">
                            <div style={bg} className="dropdown-sort dropdown-toggle-custom" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style={{whiteSpace:'nowrap',overflow:'hidden',textOverflow:'ellipsis'}}>
                                {college}
                            </div>
                            <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                {
                                    allColleges && allColleges.map(job_college=>{
                                        return(
                                            <button onClick={()=>{
                                                setCollege(job_college)
                                                if(job_college!=='All' && !selectedJobColleges.includes(job_college))
                                                setSelectedJobColleges([...selectedJobColleges,job_college])
                                                }}  className="dropdown-item fs-18 cp" value={job_college} ><span className="d-inline-block text-truncate" style={{maxWidth:"175px"}}>{job_college}</span></button>
                                        )   
                                    })
                                }
                              
                            </div>
                        </div> 
                                
                            </div>
                            <div class="dropdown-divider"></div> */}

                                       <div className="mt-2 mb-4 text-left ">

                                <p className="fs-16 mb-0 fw-500 mt-4">Work Experience</p>
                                { 
                                    selectedJobWorkEx && selectedJobWorkEx.map((job_workex)=> {
                                        return( <p className="fs-16 gray-2 my-2"> {job_workex} <span onClick={()=>{setSelectedJobWorkEx(selectedJobWorkEx.filter(workex=>workex!==job_workex))}} className="close-icon cp"><img src={close_icon} alt=""/></span></p>)
                                    })
                                }
                               
                                <div className="dropdown my-2">
                            <div style={bg} className="dropdown-sort dropdown-toggle-custom" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style={{whiteSpace:'nowrap',overflow:'hidden',textOverflow:'ellipsis'}}>
                                {workEx}
                            </div>
                            <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                {
                                    allWorkEx && allWorkEx.map(job_workex=>{
                                        return(
                                            <button onClick={()=>{
                                                setWorkEx(job_workex)
                                                if(job_workex!=='All' && !selectedJobWorkEx.includes(job_workex))
                                                setSelectedJobWorkEx([...selectedJobWorkEx,job_workex])
                                                }
                                            }  
                                            className="dropdown-item fs-18 cp" value={job_workex} >{job_workex}</button>
                                        )   
                                    })
                                }
                              
                            </div>
                        </div> 
                            </div>

                            <div class="dropdown-divider"></div>
                          


                                                

                        </div>
                        :
                        <>
                            <div className="justify-content-center my-5 py-5 fs-18 gray-3">
                                <strong>Select a profile to filter applicants</strong>
                            </div>
                        </>
                    }

                    
            </div>
    )
}
