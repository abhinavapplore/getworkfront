import React,{useState,useEffect} from 'react';
import { BrowserRouter as Router,Link, useHistory, useLocation} from 'react-router-dom'; 
import axios from 'axios';
import WorkOutlineIcon from '@material-ui/icons/WorkOutline';
import TocIcon from '@material-ui/icons/Toc';
import CallIcon from '@material-ui/icons/Call';
import EditIcon from '@material-ui/icons/Edit';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import PeopleAltIcon from '@material-ui/icons/PeopleAlt';

import './navbar.css'
import { EndPointPrefix} from '../../../constants/constants';


const StudentNavbar=({name,dp})=>{
  const [loading,setLoading]=useState(false)
  const [studentName,setStudentName]=useState(name);
  const [profileImg,setProfileImage]=useState(dp);
  useEffect(()=>{
    console.log('name: ',name);
  },[name])

  const history=useHistory();
  const loc = useLocation();
  console.log('asdfadsfadsf');
  console.log(loc.pathname);
  let pageTitle = '';
  switch(loc.pathname) {
    case '/company/track': 
      pageTitle = 'Track';
      break;
    case '/company/applicant': 
      pageTitle = 'Applicant';
      break;
    case '/company/interview': 
      pageTitle = 'Interview';
      break;
    default: pageTitle = 'Applicant';
  }

  const Logout=()=>{
    console.log('inside logout')
    setLoading(true);
        axios.get(EndPointPrefix+'/logout/',{
            headers:{
                "Authorization":'Token '+localStorage.getItem('gw_token')
            }
        })
            .then(res=>{
              setLoading(false);
                console.log('logout!',res);
                if(res.data.success){
                  
                  localStorage.clear();
                  history.push('/login')   

                }
                //setLoading(false)
            })
            .catch(err=>{
                console.log(err);
            })

  }
  function openNav() {
    document.getElementById("mySidebar").style.width = "250px";
    // document.getElementById("main").style.marginLeft = "250px";
  }
  
  function closeNav() {
    document.getElementById("mySidebar").style.width = "0";
    // document.getElementById("main").style.marginLeft= "0";
  }

    return(
        <>
        <nav className="navbar small-nav navbar-expand navbar-light bg-white topbar mb-4 fixed-top shadow-main navbar-main" style={{ paddingLeft: "20px"}}>
  {/* Sidebar Toggle (Topbar) */}
  <button id="sidebarToggleTop" className="btn btn-link d-md-none rounded-circle mr-3" onClick={openNav}>
    <i className="fa fa-bars" />
  </button>

  <div className="row navbar-heading" style={{marginLeft: "20px"}}>
    <h4><b>{pageTitle}</b></h4>
  </div>

  <ul className="navbar-nav ml-auto mr-md-0 mr-3">

    <div className="topbar-divider d-none d-sm-block" />
    <li className="nav-item dropdown no-arrow">
      <a className="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        
        <span className="mr-2 d-none d-lg-inline  fs-16 fw-500">{studentName}</span>
        <img className="img-profile rounded-circle" src={profileImg ? profileImg :"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAwFBMVEX/8+0AQWr/9u//+PEAP2kAOGQAOmUANmP/+/MAPWcAQmoAMWD/9/EAMWHp5OIANGIALV4AKVzX2Nr37+vs5+XQ0NMAK10AJVoAIVcAJVmos76/xMoAP2vFyMyxucJHco46WXkuUnR7kKNBXHtlfpUASnKDl6ikrbjf3d1XdY+YpbMAHFc2YYFRborT1tmUqbg5aohuhJkAHlgqUHQAE1GQm6omX4Fbf5m/ytGJn69oiJ9WcIl4h5srWXswUXQZU3jrmELOAAALTklEQVR4nO2deV8aPRDH2Vx7H5BFcRdWQY6FLii1VtFa3/+7enaLFvBij4Rk+/DtP1ZbP45JZiaTyS+NxpEjR44cOXLkUABgQQi1NelHFhD9E7EEQECduBfNxufXf/6Mr6Je7NAG/DfMhCCOZsvrkWuaJiEqVglJP7JHyXIWxRYU/eNVBEDND6YXiooJUnZAiGDVuBgGvlbjkQSNODrvePiNcdt2Yq9zHsWNetoIrMXsycOfWvcKtp9mixo6HqD5yz7W99qXoePfD75WMxthPLbdz2fnu9nq2uPYEv1DFwDQ6MnMbd7aRvsporUZRugMdVLMwBSiD+N6xI50AL+r+Sfo1jCq32sxjMAffBEe9tiIB770JkKalBrAVxMvqOQzFfSM/RHwK7DRk3oUteimuIvZhdxEmmgzPke7R1UNTE1E99KaCKK3+XUpkB5JOlFheJMvS9uHfhNK6W6Ab1SfomuIIWPQAM4FKwNTEy8c+UykQ5eZgYqiLqlog96iXRVMtffgXknmUGHrB1MDFeVHSypvA/yE3SJcQxKpvI01URkbmC7FiUSDCNoVsu3PQLgtzyBqU/ZDmA7iVBpnAxcdDgYqSmchyyDSOWs3s4bMJQmKMOJiX4ohSX5Kf7HMZrZRf0kxiKBtczJQUWwp3Kk2YJuvbWMOZHCn0GYfC19BrgSFcBg1DW4WKs1IvK+Bw2rFta/BQ+GDCJxLfpM0naaXwrfCMOpyNFBRuuKn6S2vYLjGvRVtoM91GWYL0RdrIIhHPJdhuhBHsdiFyDOhWeMKPseAAW8LbcGuBj7wdTTpGA7EWqhd89kabsDXYlNTOGJzVPE5+kjwLPU4G5guRLEW+h7fYJHiCd0Fg9YBxlBsPYp+57h1WtMRm9T4Hd4WGoItdPhUSrcRPobcLTwVaiGIm9wttFsiPQ1w+PtST+wshbwTbwXZYrM2rcs74iNFcOb9k3fmTX4KHsM7vkUMRVHvBO8tVvxK+mvMmeC9RcTbmXqCT9hAm/csxULDYRbyn/lugfUnwbW2f79e2mjc8mjD2KAKr3nDQOd6MqMHos8tQPuGq4W/xXec0DlPV6NL0HACBzxrwqLrwRng/oxfIcM4uxc+SRsNi2NBEXnCD7kbWfLNL16ITrvXwPCEm4VNKdq+gPPEy5vqT8L7FP5AB7ymqTsWHysy+LVjIPGNGGt4BX0Zwv0aXht9eyXJEKZbqG9cLPwmfOP0F+2OR+bmPsgQDNeANpdefSnaZ1+gQw43Soay+JkMGDI3UJo29hfoI/N7T48yJN0b4D3rg8TOvVRDmLUOsa25qYIbhd4DeqW1Ij4CEfmEB9jeK3HluEuyA2gxLLqhvuBa/odYM5tVwcbwZnI50hcsldUWQydSGtiAbUbOBpG2ZJHiFcqodKpKsrV/D3AuWMxTXUZBhRfggkUD0clC0jmaoY2rH3p7K9mymR38x6pL0Z3KugjXgPiy2iaDXIo+1d4HbJ9UCRmoKWug2KBNquyjTgOpF+Eaa1JaQgLhiZzJzBvouGzMaEob6jMAfFUgBXRY7jSqOXyVTQTySe8CEAej10gNyp3VkPGrgbA9CuSS3gVWa/Zkq3/bl1ITi0/Uk8GrgVb8pHpPs5Y04wg0Zy2oi6d/ZxldnhZzN+hssPnPU5xJ7/aXjhTSu6DRfvBevKc5/ftpOEFFsnCibHnR6bq1GqknD23xc9Vq3Xb/Ljqjudmbg1DJX31TR+HGktuNhIHavW2JjR/QGvS3t72IbIYCtpO8abiXbDIZa7ItvojU/kCgbjv0A2S+WW6nq42JdNzNM4wYrTZGWOPT3a8isxv4YmwEjd6j+X6tNSebqK1F87e/gfeY862zbDp574R1+7EnQtEc0oHxUdTbybyAc/X9S1lv5HautnTZ4OTDUo9qjA8uvgtoaH82PGfjjaJzGkkGffczr0rc/sDXtr7p4OyTX4Rph4eViYbx8gtF+ea2onOaDdwm9kfrkdhJ6im3/qX/RZ6g4+UB1b6Bdt//cv9Aptu+ATScKPFOVB0hBWXvPmRPP6hN7zp0tqMd9Kdf+iX19/2hEgDoDLw90Vy1d0J1mpXT+8Hj5Y3R1YmOjJvLx8E93cmu08ThZE8yi5oD5yDDCBfz/de5dGX2Zt1ASJ1eGAWTIArbThpJd74K6EzZnwPZ80OU4WDUz7NxQPr8nfsDAEIre1AHvJ1tmj/PJbKs9rl3SaXu7jRnvqniWc5IDf0Zzpnh6ZsEnZOB8TB/QRSZ8/DdaH3wPRvhvIBcmDfkWYyDcVLoLBsbw8Ue/we09rCYUD1OOIaN3reixVB8et7zP93JAuj3zk+LNgCQb9xOwAOjxMELxtNJ66O3qwBstCbTvAtwG90IuNinXenlTpYwuZgGDtQyH/oCTP/mBNMLUq6BQ9c5KEUDuuqULmYj4jWVu1nYjmPHceK4Hc7ulDOvvAo/6qyYu1Q6rtgvg7DpEeM5SZJng3hm1eNizLq0qj2wefYAZVkbk+9E2PZmlqkO8qbJNPaveN9JLwNZMbNPmzCZoqxBhJVHtQLWmvms+BExKTWCniHjCGYgJg8Lgbjy8z/8IDfV03BAC5SuDw/uVnaodGly17qqgGFWfcwEBjKPYAaudtkbhhylrNmA7Eo9/ZS7LGJ19FGFecpTbp0dFYTbgfSLcA0OSvpT4FTs4DoUJCnZrlmPOZpRcp6C1jeZI+E2xrdSrf3064MSqcBlWjZhxF2KjR0Ildhl+PP6DGE6iPPCN2r5i3ixxSt8ZKM91yNSvKI/F3Sn7O8T8qbofUWLs4QXewq+ZgIW/fo40jUFFaWsGW+5fPa4he677WmLkBI8LRAwQItJ5f2woG6B1A0G9QqGa7wC9Qye0k/8wOf5QyKsUUq6AXXzuxr/dP/3k5D82uaAyTXCw9PMHRFhUJfN/S52blcDr+roShXFvMptIXeBZz6YuXWl/gcW/vuztJYpTZGkBiw+aSiXnLP8+ydaTwub+UuKWg2OnN6jP+fPvPm/F8cDs4DmMOhxf3qEPcgtoGAHnKRetcQMMndyG5iaeFu/QXRvi9RpQKt+tbaLYq01cFy3xM0cFyzr05rVvFGn6PkaDKTvM9kGmcUv09BpnapRZaRAwYLrsw5sQTdlHomAET+tddaclBPK1MZ1WYreuGTPEF3WY59ol5aRBPRXHVIbe1m+xxTQofyB35z6FZpogTOVfaKeDKsYmN01l9vdIHtVWW4BrgrJsBwWgpgInUbS9rJjI2RgX3YROZH0zsz76+IlyS7LE9n6FA1ivL3yX8XERmvE/xH1QqCT5xYr8/4Awaz/pQzLYUFuf2KxlleCrSGRZKoauLvkIR8FaJg0uT4FmA9EThNeYjVQiy6RYLeKMJpHGj/JAegHc1vgXDWwPeesiwUsJ0o6TB8JyE9X7SShz12/DUDtfth3Dz9ZidtftjnOz20ssLhN3MMWqrA73xGV4g1oOL2pfXKYgURd4tnnPefAEorA0mj02CfMXgv4DB3r/ceAakLkE2GjFSyfzdKywftBrvm8DFoNccqJANI4vOs27S/U28qiY/Os+xDGVISi4K6RGoxXw8sRVpktS0RUbFwOr2Ioi54wgJYTTgaJYlePIgibtnE9noQOkMS6F4AFqNMKx+dKs2mqJQRMMtkT1Ws2lfNV2HKoJXpufshaO8i/Dx4e5xcjJZ1qKiF7TEWIEJz+O+XmYj59CBb+Hy0i0ZbsIbWz4bd60eR2sJwno65q26ZqmirWdZzx54PsE7bt4u4ouR4ObidRL/Yb8tu2BchWEaWOE7faURiMV6uH8+uf52t+Xt89rFbjIIzardhxKE1/KzmU3eRkrXuVoe2w/twfbSzRP+KRI0eOHDnyP+A/U5z9W6OIC2EAAAAASUVORK5CYII="} />
      </a>
      <div className="dropdown-menu bd-none dropdown-menu-right shadow-main animated--grow-in" aria-labelledby="userDropdown">
        <a className="dropdown-item list-item-main" href="/student/profile" >
          <i className="fas fa-user fa-sm fa-fw mr-2 " />
          Profile
        </a>
        <a className="dropdown-item list-item-main" href="#">
          <i className="fas fa-cogs fa-sm fa-fw mr-2 " />
          Settings
        </a>
        <a className="dropdown-item list-item-main" href="#">
          <i className="fas fa-list fa-sm fa-fw mr-2 " />
          Activity Log
        </a>
        <div className="dropdown-divider" />
        <span onClick={Logout} className="dropdown-item list-item-main">
          <i className="fas fa-sign-out-alt fa-sm fa-fw mr-2 " />
          Logout
        </span>
      </div>
    </li>
  </ul>
</nav>

{/* <div id="mySidebar" class="sidebar">
<a href="#" class="closebtn" onClick={closeNav}>×</a>
<Link to='/student/open' className='fs-20 flex align-item-center'><WorkOutlineIcon/>Jobs</Link>
         <div className='mg-left-30'>
         <Link to='/student/open' className='fs-20'>Open Jobs</Link>
         <Link to='/student/applied' className='fs-20'>Applied</Link>
         <Link to='/student/saved' className='fs-20'>Saved</Link>
         <Link to='/student/closed' className='fs-20'>Closed</Link>
         <Link to='/student/hidden' className='fs-20'>Hidden</Link>
         </div>

         <Link to='/student/interview' className='fs-20 flex align-item-center'><CallIcon/>Interview</Link>
         <div className='mg-left-30'>
         <Link to='/student/invited' className='fs-20'>Invited</Link>
         <Link to='/student/scheduled' className='fs-20'>Scheduled</Link>
  
         </div>
         <Link to='/student/blog' className='fs-20 flex align-item-center'><TocIcon/>Content</Link>
         <Link to='/student/connection' className='fs-20 flex align-item-center'><PeopleAltIcon/>Connections</Link>
 
</div> */}


        </>
    )
}

export default StudentNavbar;