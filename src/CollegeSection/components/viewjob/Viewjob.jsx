import React,{useState,useEffect} from 'react';
import {useLocation} from 'react-router-dom';
import Axios from 'axios';
import { Grid, Card, CardContent, Divider, Button } from '@material-ui/core';
import './viewjob.css'
import httpRequest from '../../../utils/httpRequest';
import CloseIcon from '@material-ui/icons/Close';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import WorkOutlineIcon from '@material-ui/icons/WorkOutline';
import HourglassEmptyIcon from '@material-ui/icons/HourglassEmpty';
import RoomIcon from '@material-ui/icons/Room';
const JobViewStyles={
  Page:{
    height:'auto',
    background: '#E5E5E5'
  },
  Logo:{
    height:'100px',
    width:'100px'
  }

}
const ViewJob=({data,handleClosey,open,apply,reject})=>{
  // console.log(data)
  const userType=localStorage.getItem('user_type');
  const location=useLocation();
  const [isReject,setIsReject]=useState(false)
  const [jobData,setJobData]=useState()
  console.log(jobData);
  useEffect(()=>{
  
    setJobData(data)
  
   

    

  },[jobData])
 

  return(
    <>
    
    {
      data!==null ?(
        <Grid container justify='center' onClick={()=>handleClosey(false)}>
        <Grid xs={open ? 12 :9} >
          <Card style={{height:'100vh',overflowY:'scroll'}} onClick={()=>handleClosey(true)}>
            <CardContent style={{paddingTop:75}} onClick={()=>handleClosey(true)}>
            <span className='float-right pointer' onClick={()=>handleClosey(false)} >
            <CloseIcon/>
                    </span> 
              <Grid container>
              <Grid xs={12}>
                <Grid container>

                <Grid xs={12} sm={10}>

                <h4>
                  <b>
                    {data.job.job_role_name}
                  </b>
                </h4>

                </Grid>
                <Grid container>
                  <Grid xs={12} sm={10}>
                    <div className='flex flex-wrap'>
                    {
                      data.job.eligibility_criteria.skills.map((item)=>(
                        <span className='viewjob_tag'>
                        {item.skill_name}
                      </span>
                      ))
                    }
                   
                    </div>
                  </Grid>
                </Grid>

                <Grid container className='mg-top-10'>
                <Grid xs={6} sm={5} className='mg-top-10'>
                <b>
                <h6 className='viewjob_heading flex align-item-center'>Compensation</h6>
                </b>
                <div className='flex align-item-center viewjob_data'>
                ₹ {data.job.ctc_max} - {data.job.ctc_min} {data.job.salary_payment_type}
                </div>
                    </Grid>
                    <Grid xs={6} sm={4} className='mg-top-10'>
                    <b>
                    <h6 className='viewjob_heading'>Equity range</h6>
                    </b>
                    <div className='viewjob_data'> {data.job.equity_min}% - {data.job.equity_max}%</div>
                    </Grid>
                <Divider/>
                </Grid>
                <Grid container className='mg-top-10'>
                <Grid xs={12} sm={5}>
                <b>

                    <h6 className='viewjob_heading flex align-item-center'> <RoomIcon/> Office location</h6>
                </b>
                    <div className='viewjob_data'>
                      {
                        data.job.job_location[0].city
                      }
                    </div>
                  </Grid>
                  <Grid xs={12} sm={3} className='mg-top-10'>
                  <b>
                        <div className='flex align-item-center'>
                            <WorkOutlineIcon/>
                    <h6 className='mg-0 viewjob_heading'>Job type</h6>
                        </div>
                  </b>
                    <div className='mg-left-10 viewjob_data'>
                      {
                        data.job.job_type_name
                      }
                    </div>
                  </Grid>
                 
                </Grid>
                <Grid container>
                    <Grid xs={12} sm={5} className='mg-top-10'>
                    <b>
                        <div className='flex align-item-center'>
                            <HourglassEmptyIcon/>
                      <h6 className='mg-0 viewjob_heading'>Employment type</h6>
                        </div>
                    </b>
                    <div className='mg-left-10 viewjob_data'>

                      {
                        data.job.employment_type_name
                      }
                    </div>
                    </Grid>
                    <Grid xs={12} sm={5} className='mg-top-10'>
                    {
                      data.job.job_duration_end !==null && (
                       <>
                       <b>
                       <h6 className='viewjob_heading'>Duration</h6>
                       </b> 
                    <div className='viewjob_data'>
                    {
                        data.job.job_duration_start
                      } {' '}
                      -
                      {' '}
                      {
                        data.job.job_duration_end
                      }
                    </div>
                    </>
                      )
                    }
                  </Grid>
                   
                </Grid>
                <Grid container justify='flex-end'>
                  <Grid xs={6} className='flex justify-end'>
                  {
                                           reject && (<><RejectButton isReject={isReject} id={data.id} companyId={data.job.company[0].company_id} jobId={data.job.job_id}/> <AcceptButton setIsReject={setIsReject} companyId={data.job.company[0].company_id} id={data.id} reject={reject} jobId={data.job.job_id}/></>)
                                       }
                  </Grid>
                </Grid>
                <Grid container>
                  <Grid x={12} className='mg-top-10'>
                  <b>

                  <h6 className='viewjob_heading'>Description</h6>
                  </b>
                    <div className='viewjob_data'>
                      {
                        data.job.job_description
                      }
                    </div>
                  </Grid>
                </Grid>
                <Grid container>
                  <Grid xs={12} className='mg-top-10'>
                  <b>

                  <h6 className='viewjob_heading'>Preferences</h6>
                  </b>
                    <div>
                      {



                      }
                    </div>
                  </Grid>
                </Grid>
                <Grid container>
                    <Grid xs={12} className='mg-top-10'>
                    <b>

                        <h6 className='viewjob_heading'>
                          About {data?.job.company[0]?.company_name}
                        </h6>
                    </b>

                        <div className='flex flex-wrap'>
                        <span className='viewjob_tag'>
                        {data?.job.company[0]?.industry_name}
                      </span>
                        </div>
                        <div className='mg-top-10 viewjob_data '>
                          {
                            data.job.job_description
                          }
                        </div>
                    </Grid>
                </Grid>
                <Grid container>
                  <Grid xs={12} className='mg-top-10'>
                    <div>
                      <span className='fs-16 fw-700'>Company size :</span> <span>
                       {data?.job.company[0]?.company_size}
                      </span>
                    </div>
                    <div>
                      <span className='fs-16 fw-700'>
                        check out us @ 
                      </span>
                      <span className='viewjob_data'>
                        {data?.job.company[0]?.company_website}
                      </span>
                    </div>
                  </Grid>
                </Grid>
                <Grid container>
                  <Grid xs={12} className='mg-top-10'>

                    {
                      data.job.documents!==undefined && (
                        <div>
                  <b>

                    <h6 className='viewjob_heading'>Documents required</h6>
                  </b>

                        <div className='viewjob_data'>

                        {
                          data.job.documents.cover_letter==="required" &&( <>  • {' '} Cover letter</>)
                        }
                        </div>

                      
                      <div className='viewjob_data'>
                    
                        {
                          data.job.documents.resume==="required" &&( <>  • {' '} Resume</>)
                        }
                      </div>
                        
                     
                     
                      <div className='viewjob_data'>
                 

                        {
                          data.job.documents.transcipt==="required" && ( <>  • {' '} Transcipt</>)
                        }
                      </div>
                        </div>
                      )
                    }
                       
                    
                    
                  </Grid>
                </Grid>
                <Grid container>
                  <Grid xs={12} className='flex' justify='flex-end'>
                  {
                                           apply && (<>  <HiddenBtn  jobId={data.job.job_id} reject={false}/>  <ApplyButton jobId={data.job.job_id} reject={false}/></>)
                                       }
                  </Grid>
                </Grid>
                </Grid>
              </Grid>
                
                
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
      )
      :''
    }
 
    </>
  )
}

export default ViewJob;

const ApplyButton=({jobId,reject})=>{
  
  const [done,setDone]=useState(false)
 
  const data=JSON.parse(localStorage.getItem('user_details'))
     
  const handelActive=(jobId)=>{
      
      
          fetch('http://54.162.60.38/job/student/apply/',{
              method: 'POST',
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
                
              },
              body: `job_id=${jobId}&user_id=${data.job.id}&round=1&status=1&feedback=1`
            }).then((res)=>res.json()).then((data)=>{
                alert(data.job.data.job.message)
                
                setDone(true)
          })
            
      
      }
  return(
      <Button variant="contained" style={{backgroundColor:'#3282C4',float:'right'}} className='text-white fw-700' disabled={done} onClick={()=>handelActive(jobId)}>
       {
           reject ? (
              done ? 'Accepted' : 'Accept'
           ):
           (
              done ? 'Applied' : 'Apply'
           )
       } 
       {/* {
            done ? 'Applied' : 'Apply'
        } */}
      </Button>
  )

  
}


const AcceptButton=({id,reject,setIsReject,companyId,jobId})=>{
  console.log(reject)
  const data=JSON.parse(localStorage.getItem('user_details'))

  const [done,setDone]=useState(false)
  const handelActive=(jobId)=>{

      
   
          fetch('http://54.162.60.38/job/college/invite/?college_id=MQ==&job_status=TkVX',{
              method: 'POST',
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
                
              },
              body: `id=${id}&job_id=${jobId}&college_id=1&company_id=${companyId}&is_approved=${true}&is_rejected=${false}&user_id=31`
            }).then((res)=>res.json()).then((data)=>{
                alert(data.data.message)
                
                setDone(true)
                setIsReject(true)
          })
            
      
      }
  return(
      <Button variant="contained" style={{backgroundColor:'#3282C4'}} className='text-white fw-700' disabled={done} onClick={(event)=>{event.stopPropagation();handelActive(jobId)}}>
       {
           reject ? (
              done ? 'Accepted' : 'Accept'
           ):
           (
              done ? 'Applied' : 'Apply'
           )
       } 
       {/* {
            done ? 'Applied' : 'Apply'
        } */}
      </Button>
      
  )

  
}


const RejectButton=({id,jobId,isReject,companyId})=>{
  const data=JSON.parse(localStorage.getItem('user_details'))
 
  const [done,setDone]=useState(false)
  const handelActive=(jobId)=>{
      
     
          fetch('http://54.162.60.38/job/college/invite/?college_id=MQ==&job_status=TkVX',{
              method: 'POST',
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
                
              },
              body: `id=${id}&job_id=${jobId}&college_id=1&company_id=${companyId}&is_approved=${false}&is_rejected=${true}&user_id=31`
            }).then((res)=>res.json()).then((data)=>{
                alert(data.data.message)
               
                setDone(true)
          })
            
      
      }
  return(
      
          !isReject && (

      <Button variant="outlined" color="primary" className='fw-700 mr-1' disabled={done} onClick={(event)=>{event.stopPropagation();handelActive(jobId)}}>
       {
            done ? 'Rejected' : 'Reject'
        }
      </Button>
          )
      
  )

  
}



const HiddenBtn=({jobId,hideStatus})=>{
  const [done,setDone]=useState(hideStatus)
  const handelActive=(jobId)=>{
      const data=JSON.parse(localStorage.getItem('user_details'))

      if(hideStatus){
          fetch('http://54.162.60.38/job/student/status/',{
              method: 'POST',
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
                
              },
              body: `job_id=${jobId}&user_id=${data.job.id}&status=unhide`
            }).then((res)=>res.json()).then((data)=>{
                alert(data.job.data.job.message)
                setDone(false)
  
          })
      }else{

          fetch('http://54.162.60.38/job/student/status/',{
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
            
          },
          body: `job_id=${jobId}&user_id=${data.job.id}&status=hidden`
        }).then((res)=>res.json()).then((data)=>{
            alert(data.job.data.job.message)
            setDone(true)

      })

      }
   
      

   
      
  
  }

  return (
      <>

    
      {
          done ? (<> <Button className='mg-right-15' style={{marginRight:15}} disabled={true} variant='outlined'>Hidden</Button></>) : (<><Button variant='outlined' style={{marginRight:15}}  className='pointer text-blue mg-right-15' onClick={(event)=>{event.stopPropagation();handelActive(jobId)}}><span className='fs-12 text-blue'>hide</span></Button></>)
      }
  
      </>
      
  )
}