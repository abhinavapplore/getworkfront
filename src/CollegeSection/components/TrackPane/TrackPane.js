import React, { useState } from "react";
import "./Panes.css";
// const PaneStyles={
//     background: '#fff',
//     boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
//     width:'87vw',
//     height:'60px',
//     top: '60px',
//     left:'20px',
//     zIndex:'1'
// }
const TrackPane = ({
	profiles,
	jobProfile,
	setJobProfile,
	setShow,
	search,
	setSearch,
}) => {
	const [selected, setSelected] = useState("All Jobs");
	const handleSelect = (e) => {
		console.log("inside change: ", e.target.value);
		setSelected(e.target.value);
		setShow(e.target.value);
	};
	return (
		<>
			<nav
				className="job-pane fs-14 navbar fixed-top small-nav navbar-expand navbar-light topbar shadow-main ml-auto"
				style={{ paddingLeft: "140px", width: "100%" }}
			>
				<div className="row fs-18 mx-5">
					{/* Separate this as a component */}
					<p className="my-auto ml-5 pl-3">Profile</p>
					<div className="dropdown ml-4">
						<div
							className="dropdown-sort dropdown-toggle-custom"
							type="button"
							id="dropdownMenuButton"
							data-toggle="dropdown"
							aria-haspopup="true"
							aria-expanded="false"
						>
							<span
								className="fs-15 d-inline-block text-truncate"
								style={{ maxWidth: "150px" }}
							>
								{jobProfile}
							</span>
						</div>
						<div
							className="dropdown-menu"
							style={{ height: "50vh", overflowY: "scroll" }}
							aria-labelledby="dropdownMenuButton"
						>
							{profiles &&
								profiles.length > 0 &&
								profiles.map((profile) => {
									return (
										<button
											key={profile}
											onClick={(e) => {
												setJobProfile(e.target.value);
											}}
											className="dropdown-item cp"
											value={profile}
										>
											<span
												className="d-inline-block text-truncate"
												style={{ maxWidth: "150px" }}
											></span>
											{profile}
										</button>
									);
								})}
						</div>
					</div>
				</div>
				{/* Separate this as a component */}
				<div className="gw-input-container fs-18 ml-auto">
					<i class="fas fa-search"></i>
					<input
						onChange={(e) => {
							setSearch(e.target.value);
						}}
						type="text"
						className="ml-5 gw-input input-secondary input-small mr-3"
						value={search}
						placeholder="Search by name..."
					></input>
				</div>
			</nav>
		</>
	);
};

export default TrackPane;
