import React, { useState, useEffect, useRef } from "react";
import { Grid, Avatar, Paper, Button } from "@material-ui/core";
import Layout from "../../layout/Layout";
import BookmarkBorderIcon from "@material-ui/icons/BookmarkBorder";
import BookmarkIcon from "@material-ui/icons/Bookmark";
import "./compnaycard.css";
import EmployeIcon from "../../../assets/icon/newicon.svg";
import VerifiedIcon from "../../../assets/svg/ic_baseline-verified.svg";
import BookmarkIconSvg from "../../../assets/icon/bookmark.svg";
import ArrowdownIcon from "../../../assets/icon/arrowdown.svg";
const sideLine = { borderRight: "1px solid #BDBDBD" };

export default function CompanyCard( {data, invitation=false}) {
    const [open, setOpen] = React.useState(false);
    const handleClose=()=>{
        setOpen(!open)
	}
	const [inviteClick, setInviteClick] = React.useState(false);
	const done = useRef([]);

	useEffect(() => {
		done.current = done.current.slice(0, data.length);
	 }, [data.length]);
	 

	return data.map((item, index) => (
		<div className="college-company-card" style={{ minWidth: "80vw" }}>
			<div className="row">
				<div className="col-lg-12">
					<div className="mt-3">
						<div className="card mb-3">
							<div className="card-body">
								<div className="row">
									<div className="col-md-2 col-12 d-flex flex-row bd-highlight p-1">
										<img
											src="http://www.bigpicture.com/wp-content/uploads/2014/10/placeholder.jpg"
											className="img-fluid"
										/>
									</div>
									<div className="col-md-10">
										<div className="row">
											<div className="col-md-6 d-flex flex-col  ">
												<div className="d-flex flex-row align-items-center">
													<img
														src={item.logo}
														alt="Avatar"
														className="box-avatar"
													></img>
													<div className="d-flex flex-col ml-1" style={{paddingLeft:'5%'}}>
														<h5 className="card-title d-flex flex-row bd-highlight   mb-0">
															{item.company_name}
														</h5>
														<div className="flex justify-space-between align-item-center company-detail-line__company-detail-line">
															<div className="fs-12 text-gray">
																{" "}
																<img src={EmployeIcon} />
																{item.size}
															</div>{" "}
															{" • "}{" "}
															<div className="fs-12">{item.industry}</div>
															{" • "}{" "}
															<div className="fs-12">
																<img src={VerifiedIcon} />
																<span style={{color:'#E55934',fontSize:'16px',fontWeight:'700',fontFamily:'open-sans',paddingTop:'1%'}}>Verified</span>
															</div>
														</div>
													</div>
													
													

												</div>
											</div>
											<div className="col-md-6 col-12 d-flex align-item-center mt-2  justify-end">
											{ invitation && (
													<div className='col-md-4 col-12 d-flex align-item-center mt-2  justify-end'>
                <Button variant="contained" disabled={done.current[index]}  style={{backgroundColor:'#3282C4'}} data-toggle="modal" data-target={`#${item.company_user.company_id}`} className='text-white fw-700 w-50'>

                            {
                                done.current[index] ? 'Invited' : 'Invite'
                            }
                </Button>
                    {/* <SaveIcon/> */}
                </div>
						)}
											<img className="mr-4" src={BookmarkIconSvg} />
											<img src={ArrowdownIcon} />
												{/* <Button
													variant="contained"
													style={{ backgroundColor: "#3282C4" }}
													data-toggle="modal"
													data-target="#invitemodal"
													className="text-white fw-700 w-50"
												>
													Invite
												</Button> */}
												{/* <SaveIcon/> */}
											</div>
										</div>
										<hr className="my-2" />
										<div className="row ml-1 justify-flex-start">
											<div className="col-md-5 p-2 rounded bg-f2">
												<div className="row">
													<p className="fs-16 fw-700 ml-3">Details</p>
												</div>
												<div className="flex">
													<div className="flex flex-col pd-10" style={sideLine}>
														<h4 className="fs-14 fw-700">{item.city}</h4>
														<span className="fs-12 fw-300 text-left">HQ</span>
													</div>
													<div className="flex flex-col pd-10" style={sideLine}>
														<h4 className="fs-14 fw-700">
															{item.year_founded}
														</h4>
														<span className="fs-12 fw-300 text-left">
															Founded
														</span>
													</div>
													<div className="flex flex-col pd-10">
														<h4 className="fs-14 fw-700">{item.city}</h4>
														<span className="fs-12 fw-300 text-left">HQ</span>
													</div>
												</div>
											</div>
											<div className="col-md-6  des-left-10-mobo-left-zero mobo-top-10 p-2 rounded bg-f2">
												<div className="row">
													<p className="fs-16 fw-700 ml-3">Hiring Details</p>
												</div>
												<div className="flex">
													<div className="flex flex-col pd-10" style={sideLine}>
														<h4 className="fs-14 fw-700">
															{item.company_user.first_name}
														</h4>
														<span className="fs-12 fw-300 text-left">
															Contact
														</span>
													</div>
													<div className="flex flex-col pd-10" style={sideLine}>
														<h4 className="fs-14 fw-700">
															{item.company_user.hiring_total}
														</h4>
														<span className="fs-12 fw-300 text-left">
															Past Hiring
														</span>
													</div>
													<div className="flex flex-col pd-10" style={sideLine}>
														<h4 className="fs-14 fw-700">
															{item.company_user.hiring_now}
														</h4>
														<span className="fs-12 fw-300 text-left">
															Hiring Now
														</span>
													</div>
													<div className="flex flex-col pd-10">
														<h4 className="fs-14 fw-700"></h4>
														<span className="fs-12 fw-300 text-left">
															Preference of hiring
														</span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<OtherCompanyModal onClose={handleClose}/>
			{/* <InviteCompanyModal
				companyId={item.company_user.company_id}
				companyName={""}
				hrName={item.company_user.first_name}
				hrEmail={""}
			/> */}
			<InviteCompanyModal companyId={item.company_user.company_id} setInviteClick={setInviteClick} inviteClick={inviteClick} setDone={done} index={index} companyName={item.company_name} hrName={item.company_user.first_name}  hrEmail={null} />
		</div>
	));
}

const InviteButton=({companyId,hrName,companyName,hrEmail,note,setDone,inviteClick,setInviteClick, index})=>{

    console.log("ksdfklerflmlker", setDone);
    const[doneText,setDoneText]=useState(false)
   
    // console.log(reject)
    const handelInvite=(jobId)=>{
    
        
      
            fetch('http://praveshtest.getwork.org/api/college/invite/',{
                method: 'POST',
                headers: {
                  'Content-Type': 'application/json',
                  'Authorization':'Token 2cedd84e1320c1ddfeae772f649d1bd2a7f511a1'
                  
                },
                body:JSON.stringify({company_id:companyId,company_name:companyName,hr_name:hrName,hr_email:hrEmail,note:note})
              }).then((res)=>res.json()).then((data)=>{
                  alert(data.data.message)
				  setDone.current[index] = true;
				  setInviteClick(!inviteClick)
                  setDoneText(true)
            })
        }
    return(
    <Button variant="contained" style={{backgroundColor:'#3282C4'}} data-toggle="modal" data-target={`#${companyId}`} className='text-white fw-700'  onClick={(event)=>{event.stopPropagation();handelInvite()}}>
      Invite
         
        </Button>
    )

    
}


const SaveIcon=({jobId,updateData,isSave})=>{
 
    const data=JSON.parse(localStorage.getItem('user_details'))
    const [done,setDone]=useState(isSave)
    const handelActive=(jobId)=>{
        
        if(done){
            fetch('http://54.162.60.38/job/student/status/',{
                method: 'POST',
                headers: {
                  'Content-Type': 'application/x-www-form-urlencoded'

                  
                },
                body: `job_id=${jobId}&user_id=${data.id}&status=unsave`
              }).then((res)=>res.json()).then((data)=>{
                  alert(data.data.message)
                setDone(!done)
                // updateData(jobId)
            })
        }
        else{
            fetch('http://54.162.60.38/job/student/status/',{
                method: 'POST',
                headers: {
                  'Content-Type': 'application/x-www-form-urlencoded'
                  
                },
                body: `job_id=${jobId}&user_id=${data.id}&status=saved`
              }).then((res)=>res.json()).then((data)=>{
                  alert(data.data.message)
                setDone(!done)
                // updateData(jobId)
            })
        }
     
           
              
        
        }
    return (
        <>
       {done ? <>  <BookmarkIcon className='pointer text-blue mg-right-5' onClick={(event)=>{event.stopPropagation();handelActive(jobId)}}/><span className='fs-12 text-blue'></span></>:<>  <BookmarkBorderIcon className='pointer text-blue mg-right-5' onClick={(event)=>{event.stopPropagation();handelActive(jobId)}}/><span className='fs-12 text-blue'></span></>}
        </>
        
    )
}

const OtherCompanyModal=()=>{
	
	const handleSubmit=async()=>{
		if(!companyName || !hrName || !hrEmail){
				alert("Please Enter Valid Details");
				return;
		}
        fetch('http://praveshtest.getwork.org/api/college/invite/',{
                method: 'POST',
                headers: {
                  'Content-Type': 'application/json',
                  'Authorization':'Token 2cedd84e1320c1ddfeae772f649d1bd2a7f511a1'
                  
                },
                body:JSON.stringify({company_id:null,company_name:companyName,hr_name:hrName,hr_email:hrEmail,note:null})
			  }).then((res)=>
				  
			  	res.json()).then((data)=>{
                alert(data.data.message)
                //   setDone(true)
            })
              
        // console.log(res.data.results)
        // updateState(res.data.results)
    }
    const [companyName,setCompanyName]=useState('')
    const[hrName,setHrName]=useState('')
    const[hrEmail,setHrEmail]=useState('')
    const[note,setNote]=useState(null)
    return(
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Invite other company</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div className='row'>
            <div className='col-md-12'>
            Connect with companies easily which are not on Getwork
            </div>
        </div>
        <div className='row'>
        <div className='col-md-5 d-flex justify-content-end'>
                <p className='fw-700 fs-16'>Compnay Name</p>
            </div>
            <div className='col-md-6'>
               <input type='text' onChange={(e)=>setCompanyName(e.target.value)}/>
            </div>
        </div>
        <div className='row'>
        <div className='col-md-5 d-flex justify-content-end'>
                <p className='fw-700 fs-16'>HR Name</p>
            </div>
            <div className='col-md-6'>
               <input type='text' onChange={(e)=>setHrName(e.target.value)}/>
            </div>
        </div>
        <div className='row'>
        <div className='col-md-5 d-flex justify-content-end'>
                <p className='fw-700 fs-16'>HR Email id</p>
            </div>
            <div className='col-md-6'>
               <input type='email' onChange={(e)=>setHrEmail(e.target.value)}/>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onClick={handleSubmit}>Done</button>
      </div>
    </div>
  </div>
</div>
    )
}


const InviteCompanyModal=({companyId,hrName,companyName,hrEmail,inviteClick,setInviteClick,setDone, index})=>{
    const[openNote,setOpenNote]=useState(false)
	const[note,setNote]=useState(null)
    return(
        <div class="modal fade" id={companyId} tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">You can customize the invitation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div className='row'>
            <div className='col-md-12'>
				Your invitation to <b>{companyName}</b>  is on its way. You can add a note to personalize your invitation.
            </div>
        </div>
        {
            openNote&&(
                <div className='row'>
        <div className='col-md-12 d-flex justify-content-end'>
        <textarea class="form-control" onChange={(e)=>setNote(e.target.value)} aria-label="With textarea"></textarea>
            </div>
           
        </div>
            )
        }
       
       
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-outline-primary" onClick={()=>setOpenNote(!openNote)}>{openNote ? 'Close Note' : 'Add a Note'}</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
       <InviteButton companyId={companyId} setInviteClick={setInviteClick} inviteClick={inviteClick}  setDone={setDone} index={index} hrName={hrName} companyName={companyName} hrEmail={hrEmail} note={note}/>
      </div>
    </div>
  </div>
</div>
    )
}
