import React from 'react'
import { Link, NavLink } from 'react-router-dom'
import WorkOutlineIcon from '@material-ui/icons/WorkOutline';
import TocIcon from '@material-ui/icons/Toc';
import CallIcon from '@material-ui/icons/Call';
import PeopleAltIcon from '@material-ui/icons/PeopleAlt';
// import SmallProileCard from '../../Components/SmallProfileCard/Index'
import HomeIcon from '../../../assets/svg/Home.svg'
import JobIcon from '../../../assets/svg/Job.svg'
import TelephoneIcon from '../../../assets/svg/bx_bx-phone-call (1).svg'
import ContentIcon from '../../../assets/svg/bx_bx-book-content.svg'
import ConnectionIcon from '../../../assets/svg/feather_users.svg'
import MainIcon from '../../../assets/img/getwork-logo.png'
import './sidebar.css'
import {
  Accordion,
  AccordionItem,
  AccordionItemButton,
  AccordionItemHeading,
  AccordionItemPanel,
} from 'react-accessible-accordion';
import { Avatar } from '@material-ui/core';
export default function Sidebar() {
    return (
       <div className='text-left fs-20 sidebar-college'>
       {/* <SmallProileCard/> */}
       {/* <Link to='/student/open' className='fs-20 flex align-item-center'><img src={HomeIcon}/>Home</Link>
       <Link to='/student/open' className='fs-20 flex align-item-center mg-top-20'><img src={JobIcon}/>Jobs</Link>
         <div className='mg-left-30'>
         <Link to='/student/open' className='fs-20'>Open Jobs</Link>
         <Link to='/student/applied' className='fs-20'>Applied</Link>
         <Link to='/student/saved' className='fs-20'>Saved</Link>
         <Link to='/student/closed' className='fs-20'>Closed</Link>
         <Link to='/student/hidden' className='fs-20'>Hidden</Link>
         </div>

         <Link to='/student/interview' className='fs-20 flex align-item-center'><img src={TelephoneIcon}/>Interview</Link>
         <div className='mg-left-30'>
         <Link to='/student/invited' className='fs-20'>Invited</Link>
         <Link to='/student/scheduled' className='fs-20'>Scheduled</Link>
  
         </div>
         <Link to='/student/blog' className='fs-20 flex align-item-center'><img src={ContentIcon}/>Content</Link>
         <Link to='/student/connection' className='fs-20 flex align-item-center'><img src={ConnectionIcon}/>Connections</Link>
  */}
  <div className='flex justify-space-between align-item-center'>
      <img src={MainIcon} style={{width:75,height:75}}/>

      <Avatar src='' alt='hello' style={{borderRadius:0,width:50,height:50}}/>
  </div>
  <Accordion preExpanded={['d','e','f']} style={{padding:'2px 8px 6px 16px',marginTop:'33%'}}>

  <AccordionItem uuid="a">
            <AccordionItemHeading className='mg-top-20 fs-16'>
                <AccordionItemButton>
                <NavLink activeClassName="link-active" to='/college/open-job' className='fs-20 flex align-item-center'>Home</NavLink>
  
                </AccordionItemButton>
            </AccordionItemHeading>
          
            
        </AccordionItem>
        <AccordionItem uuid="b">
            <AccordionItemHeading className='mg-top-20 fs-16'>
                <AccordionItemButton>
                <NavLink activeClassName="link-active" to='/company/post-job' className='fs-20 flex align-item-center'>Create Job</NavLink>
  
                </AccordionItemButton>
            </AccordionItemHeading>
          
            
        </AccordionItem>
        <AccordionItem uuid="c">
            <AccordionItemHeading className='mg-top-20 fs-16'>
                <AccordionItemButton>
                <NavLink activeClassName="link-active" to='/college/invite' className='fs-20 flex align-item-center'>Invite Companies</NavLink>
  
                </AccordionItemButton>
            </AccordionItemHeading>
          
            
        </AccordionItem>
        
  <AccordionItem uuid="d">
            <AccordionItemHeading className='mg-top-20 fs-16'>
                <AccordionItemButton>
               
                   {/* <img src={JobIcon}/>  */}
                   Jobs
                </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel className='mg-left-20 mg-top-10 fs-16'>
            <NavLink activeClassName="link-active" to='/college/open-job' className='fs-20'>Open Jobs</NavLink>
           
            </AccordionItemPanel>
           
            <AccordionItemPanel className='mg-left-20 mg-top-10 fs-16'>
              <NavLink activeClassName="link-active" to='/college/close-job' className='fs-20'>Closed Jobs</NavLink>
            </AccordionItemPanel>
            <AccordionItemPanel className='mg-left-20 mg-top-10 fs-16'>
              <NavLink activeClassName="link-active" to='/college/new-job' className='fs-20'>New Jobs</NavLink>
            </AccordionItemPanel>
         
        </AccordionItem>
        <AccordionItem>
            <AccordionItemHeading className='mg-top-20 fs-16'>
                <AccordionItemButton>
                <NavLink activeClassName="link-active" to='/college/track' className='fs-20 flex align-item-center'>Application status</NavLink>
  
                </AccordionItemButton>
            </AccordionItemHeading>
          
            
        </AccordionItem>

        <AccordionItem uuid="e">
            <AccordionItemHeading className='mg-top-20 fs-16'>
                <AccordionItemButton>
                  Notices
                </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel className='mg-left-20 mg-top-10 fs-16'>
            <NavLink activeClassName="link-active" to='/student/invited' className='fs-20'>Inbox</NavLink>
            </AccordionItemPanel>
            <AccordionItemPanel className='mg-left-20 mg-top-10 fs-16'>
            <NavLink activeClassName="link-active" to='/student/scheduled' className='fs-20'>Outbox</NavLink>
            </AccordionItemPanel>
            
        </AccordionItem>
        <AccordionItem uuid="f">
            <AccordionItemHeading className='mg-top-20 fs-16'>
                <AccordionItemButton>
                  Connections
                </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel className='mg-left-20 mg-top-10 fs-16'>
            <NavLink activeClassName="link-active" to='/college/company-connection' className='fs-20'>Companies</NavLink>
            </AccordionItemPanel>
            <AccordionItemPanel className='mg-left-20 mg-top-10 fs-16'>
            <NavLink activeClassName="link-active" to='/student/scheduled' className='fs-20'>Students</NavLink>
            </AccordionItemPanel>
            
        </AccordionItem>
        <AccordionItem uuid="g">
            <AccordionItemHeading className='mg-top-20 fs-16'>
                <AccordionItemButton>
                <NavLink activeClassName="link-active" to='/student/blog' className='fs-20 flex align-item-center'>Reports</NavLink>
         
                </AccordionItemButton>
            </AccordionItemHeading>
           
            
        </AccordionItem>
      

  
</Accordion>

       </div>
    )
}