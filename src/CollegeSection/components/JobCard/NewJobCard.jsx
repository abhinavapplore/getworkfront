import React, { useState } from "react";
import cx from "classnames";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import { useLocation } from "react-router-dom"
import { Link } from 'react-router-dom'
import styles from "./newjobcard.scss";
import kFormater from '../../../utils/ZeroToK'
import { Avatar, Button, Modal, Grid } from "@material-ui/core";
import ViewJob from "../viewjob/Viewjob";
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import BookmarkBorderIcon from '@material-ui/icons/BookmarkBorder';
import BookmarkIcon from '@material-ui/icons/Bookmark';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import { NavLink } from "react-router-dom";



const NewJobCard= ({data,applied,interview,reject,fullView,all,hideStatus,saveJob}) => {
    console.log(data)
//   const {
//     Icon,
//     role,
//     company,
//     location,
//     position,
//     compensation,
//     growth,
//     action,
//     duration,
//   } = props;
//   console.log(styles)
const[open,handleClose]=useState(false);
const[jobData,setJobData]=useState([])
const [isReject,setIsReject]=useState(false)

const openJob=(item)=>{
    //    console.log(item)

   setJobData(item)
   handleClose(!open)
  
   }
   const  callback = (value) => {
    handleClose(value)
   // do something with value in parent component, like save to state
}
  

  return (
    <div>{
      data.length?(
            data.map((item)=>
         
                    (

                    <div className='new-job-card' key={item.job.job_id}>
              
                  <div className='container college-cont' style={{borderLeft:'5px solid #E55935'}} onClick={()=>openJob(item)}>
                    <div className='container__header'>
                    <Avatar src={item.job.company[0]?.company_logo} style={{width:50,height:50,borderRadius:0}} alt='company-picture' />
                      <div className='container__header__names'>
                        <div className='container__header__role position-relative'>
                          <h3 className='fs-20'> {item.job.job_title}</h3>
                          {
                                          applied && <div className='absolute top-0 right-0 fs-16 fw-700 text-green' style={{right:'4rem'}}>{item.job.applicant_status_name}
                                          <ChevronRightIcon />
                                          </div>
                                      }  
  
                        </div>
                        <div className='container__header__company'>
                          <span className='container__header__cname'> {item.job.company[0]?.company_name}</span>
                          <br />
                          <span className='container__header__location'>
                          {item.job.company[0]?.company_location}&nbsp;&middot;&nbsp;{item.job.company[0]?.company_size} employees
                          </span>
                        </div>
                      </div>
                    </div>
                    <div className='container__body'>
                      <div className='container__body__info'>
                        <span className='fs-16' >{item.job.employment_type_name}</span>&nbsp;&middot;&nbsp;
                        <span className='orange fs-16'> {kFormater(item.job.ctc_min) } - {kFormater(item.job.ctc_max)} {' '} {item.job.salary_payment_type.toLowerCase()}</span>
                        &nbsp;&middot;&nbsp;
                        <span className='fs-16'> {item.job.equity_min}% - {item.job.equity_max}%</span>
                      </div>
                      <div>

                      {
                                           interview && (<> <ApplyButton jobId={item.job.job_id} interviewId={item.job.id} reject={reject}/></>)
                                       }
        
                                       {/* {
                                           reject && (<><RejectButton isReject={isReject} id={item.id} companyId={item.job.company[0].company_id} jobId={item.job.job_id}/> <AcceptButton setIsReject={setIsReject} companyId={item.job.company[0].company_id} id={item.id} reject={reject} jobId={item.job.job_id}/></>)
                                       } */}
                                       {
                                         reject &&  ( <RejectAccept id={item.id} companyId={item.job.company[0].company_id} jobId={item.job.job_id} reject={reject}/> )
                                       }
                                       {/* {
                                           item.job.interview.length ? ( <RejectAccept interviewId={item.job.interview[0]?.interview_id} jobId={item.job.job_id} reject={reject}/> ):<div className='fs-16 text-black-50'>invited for interview, details will be posted soon</div>
                                       } */}
                                       {/* {
                                         !item.job.is_accepted ?
(<>
                                        { item.job.interview.length ? ( <RejectAccept interviewId={item.job.interview[0]?.interview_id} jobId={item.job.job_id} reject={reject}/> ):<div className='fs-16 text-black-50'>invited for interview, details will be posted soon</div>
                              }   </>
                                         ):null
                                       } */}
                                       {/* {
                                         reject && 
                                         item.job.interview.length ? ( <RejectAccept interviewId={item.job.interview[0]?.interview_id} jobId={item.job.job_id} reject={reject}/> ):<div className='fs-16 text-black-50'>invited for interview, details will be posted soon</div>
                                 
                                         
                                        
                                       } */}
                      </div>
                     
                    </div>
                    <div className='container__footer flex justify-content-between'>
                      {/* <p>
                        <span className='orange'>{action}</span>
                        &nbsp;&middot;&nbsp;
                        <span className='container__header__location'>{duration}</span>
                      </p> */}
                      <div className='w-50'>
                     {item?.interview?.length ?( <div className='container__body__info'>
                        <span className='fs-12' >{item.job.interview[0]?.location?.city}</span>&nbsp;&middot;&nbsp;
                        <span className='orange fs-12'> {item.job.interview[0]?.start_date} - {item.job.interview[0]?.end_date} {' '} </span>
                        {/* &nbsp;&middot;&nbsp;
                        <span className='fs-16'> 1.5%-2% </span> */}
                      </div>):null}
                      </div>
                      <div className='w-50'>

                      {
                                        all &&(
                                           <div className='flex flex-row justify-content-end'>
                                             {
                                              <SaveIcon jobId={item.job.job_id} isSave={item.job.is_saved} /> 
                                          
                                             }
                                             {
                                              <HiddenIcon jobId={item.job.job_id} hideStatus={hideStatus}/>
                                    
                                             }
                                           </div>
                                        )
                                    }


                      {
                                        saveJob &&(
                                           <div className='flex flex-row justify-content-end'>
                                             {
                                              <SaveIcon jobId={item.job.job_id} isSave={item.job.is_saved} /> 
                                          
                                             }
                                             
                                           </div>
                                        )
                                    }
                      </div>
                    </div>
                  </div>

                  
                    </div>

                    )
            )
      ):null
      }
      <Modal
        open={open}
        onClose={()=>handleClose(!open)}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
      <Grid container justify='flex-end'>
                  <Grid xs={12} sm={10}>

        <ViewJob data={jobData} apply={interview} reject={reject} open={fullView} handleClosey={callback}/>
                  </Grid>
                  </Grid>
         

        
      </Modal>
</div>

      
  );
};

export default NewJobCard;



const ApplyButton=({jobId,reject})=>{
    
    
    const [done,setDone]=useState(false)
    // console.log(reject)
    const handelActive=(jobId)=>{
    
        const data=JSON.parse(localStorage.getItem('user_details'))
       
            fetch('http://54.162.60.38/job/student/apply/',{
                method: 'POST',
                headers: {
                  'Content-Type': 'application/x-www-form-urlencoded'
                  
                },
                body: `job_id=${jobId}&user_id=${data.id}&round=1&status=1&feedback=1`
              }).then((res)=>res.json()).then((data)=>{
                  alert(data.data.message)
                
                  setDone(true)
            })
              
        
        }
    return(
    <Button variant="contained" style={{backgroundColor:'#3282C4'}} className='text-white fw-700' disabled={done} onClick={(event)=>{event.stopPropagation();handelActive(jobId)}}>
         {
             reject ? (
                done ? 'Accepted' : 'Accept'
             ):
             (
                done ? 'Applied' : 'Apply'
             )
         } 
         {/* {
              done ? 'Applied' : 'Apply'
          } */}
        </Button>
    )

    
}





const AcceptButton=({id,reject,setIsReject,companyId,jobId})=>{
  // console.log(reject)
  const data=JSON.parse(localStorage.getItem('user_details'))

  const [done,setDone]=useState(false)
  const handelActive=(jobId)=>{

      
   
          fetch('http://54.162.60.38/job/college/invite/?college_id=MQ==&job_status=TkVX',{
              method: 'POST',
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
                
              },
              body: `id=${id}&job_id=${jobId}&college_id=1&company_id=${companyId}&is_approved=${true}&is_rejected=${false}&user_id=31`
            }).then((res)=>res.json()).then((data)=>{
                alert(data.data.message)
                
                setDone(true)
                setIsReject(true)
          })
            
      
      }
  return(
      <Button variant="contained" style={{backgroundColor:'#3282C4'}} className='text-white fw-700' disabled={done} onClick={(event)=>{event.stopPropagation();handelActive(jobId)}}>
       {
           reject ? (
              done ? 'Accepted' : 'Accept'
           ):
           (
              done ? 'Applied' : 'Apply'
           )
       } 
       {/* {
            done ? 'Applied' : 'Apply'
        } */}
      </Button>
      
  )

  
}


const RejectButton=({id,jobId,isReject,companyId})=>{
  const data=JSON.parse(localStorage.getItem('user_details'))
 
  const [done,setDone]=useState(false)
  const handelActive=(jobId)=>{
      
     
          fetch('http://54.162.60.38/job/college/invite/?college_id=MQ==&job_status=TkVX',{
              method: 'POST',
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
                
              },
              body: `id=${id}&job_id=${jobId}&college_id=1&company_id=${companyId}&is_approved=${false}&is_rejected=${true}&user_id=31`
            }).then((res)=>res.json()).then((data)=>{
                alert(data.data.message)
               
                setDone(true)
          })
            
      
      }
  return(
      
          !isReject && (

      <Button variant="outlined" color="primary" className='fw-700 mr-1' disabled={done} onClick={(event)=>{event.stopPropagation();handelActive(jobId)}}>
       {
            done ? 'Rejected' : 'Reject'
        }
      </Button>
          )
      
  )

  
}

const SaveIcon=({jobId,updateData,isSave})=>{
 
    const data=JSON.parse(localStorage.getItem('user_details'))
    const [done,setDone]=useState(isSave)
    const handelActive=(jobId)=>{
        
        if(done){
            fetch('http://54.162.60.38/job/student/status/',{
                method: 'POST',
                headers: {
                  'Content-Type': 'application/x-www-form-urlencoded'
                  
                },
                body: `job_id=${jobId}&user_id=${data.id}&status=unsave`
              }).then((res)=>res.json()).then((data)=>{
                  alert(data.data.message)
                setDone(!done)
                // updateData(jobId)
            })
        }
        else{
            fetch('http://54.162.60.38/job/student/status/',{
                method: 'POST',
                headers: {
                  'Content-Type': 'application/x-www-form-urlencoded'
                  
                },
                body: `job_id=${jobId}&user_id=${data.id}&status=saved`
              }).then((res)=>res.json()).then((data)=>{
                  alert(data.data.message)
                setDone(!done)
                // updateData(jobId)
            })
        }
     
           
              
        
        }
    return (
        <>
       {done ? <> <div className='flex align-item-center'> <BookmarkIcon className='pointer text-blue mg-right-5' onClick={(event)=>{event.stopPropagation();handelActive(jobId)}}/><span className='fs-12 text-blue mr-2'>Saved</span></div></>:<> <div className='flex align-item-center'> <BookmarkBorderIcon className='pointer text-blue mg-right-5' onClick={(event)=>{event.stopPropagation();handelActive(jobId)}}/><span className='fs-12 text-blue mr-2'>Save</span></div></>}
        </>
        
    )
}
const HiddenIcon=({jobId,hideStatus})=>{
  console.log(hideStatus)
    const [done,setDone]=useState(hideStatus)
    const handelActive=(jobId)=>{
        const data=JSON.parse(localStorage.getItem('user_details'))

        if(hideStatus){
            fetch('http://54.162.60.38/job/student/status/',{
                method: 'POST',
                headers: {
                  'Content-Type': 'application/x-www-form-urlencoded'
                  
                },
                body: `job_id=${jobId}&user_id=${data.id}&status=unhide`
              }).then((res)=>res.json()).then((data)=>{
                  alert(data.data.message)
                  setDone(false)
    
            })
        }else{

            fetch('http://54.162.60.38/job/student/status/',{
            method: 'POST',
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
              
            },
            body: `job_id=${jobId}&user_id=${data.id}&status=hidden`
          }).then((res)=>res.json()).then((data)=>{
              alert(data.data.message)
              setDone(true)

        })

        }
     
        

     
        
    
    }

    return (
        <>

      
        {
            done ? (<><div className='flex align-item-center'><VisibilityOffIcon className='pointer text-blue mg-right-5' onClick={(event)=>{event.stopPropagation();handelActive(jobId)}}/><span className='fs-12 text-blue mr-2'>hidden</span></div>  </>) : (<><div className='flex align-item-center'><VisibilityIcon className='pointer text-blue mg-right-5' onClick={(event)=>{event.stopPropagation();handelActive(jobId)}}/><span className='fs-12 text-blue mr-2'>hide</span></div></>)
        }
    
        </>
        
    )
}



const RejectAccept=({jobId,reject,interviewId,companyId,id})=>{
  console.log(jobId)
  const[isReject,setIsReject]=useState(false)
  return(

<>
     {
          !isReject && (

          <RejectButton  jobId={jobId}/>
      )
      }
       <AcceptButton setIsReject={setIsReject} companyId={companyId} id={id} interviewId={interviewId}  reject={reject} jobId={jobId}/>
   </>
  )
}