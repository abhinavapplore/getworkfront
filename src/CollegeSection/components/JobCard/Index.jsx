import React, { useState } from 'react'
import { Card, CardContent, Grid, Avatar, Button, Modal } from '@material-ui/core'
import './jobcard.css'
import kFormater from '../../../utils/ZeroToK'

import BookmarkBorderIcon from '@material-ui/icons/BookmarkBorder';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import ViewJob from '../viewjob/Viewjob'
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import BookmarkIcon from '@material-ui/icons/Bookmark';
import VisibilityIcon from '@material-ui/icons/Visibility';

const AvatarStyle={
    height:60,
    width:50,
    borderRadius:5
}


export default function JobsCard({handleClosey,data,interview,all,reject,applied,updateData,hideStatus,sideArrow,fullView}) {
//    console.log(data)
   const[open,handleClose]=useState(false);
    const[jobData,setJobData]=useState([])
    const [isReject,setIsReject]=useState(false)

  
   const openJob=(item)=>{
    //    console.log(item)

   setJobData(item)
   handleClose(!open)
  
   }
   const  callback = (value) => {
    handleClose(value)
   // do something with value in parent component, like save to state
}
  


     
    return (

        data!==null ?(

            data.map((item)=>
            (   
                <div className='job-card' key={item.job.job_id} >
                <div className='row'>
                    <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <div className='card'>
                            <div className='card-body'>
                                <div className='row'>
                                    <div className='col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12'>
                                        <div className='row'>
                                            <div className='col-2 col-sm-1 col-md-1 col-lg-1 col-xl-1'>
                                                <img src={item.job.company[0].company_logo} className='box-avatar'/>
                                            </div>
                                            <div className='col-10 col-sm-10 col-md-10 col-lg-10 col-xl-10'>
                                                {
                                                    applied && <div className='absolute fs-16 fw-700 text-green' style={{right:0}}>{item.applicant_status_name}</div>
                                                }  
                
                                                <h4 className='fs-16' onClick={()=>openJob(item)}>
                                                    {item.job.job_title}
                
                
                                                </h4>
                
                                                <p className='fs-12 mg-0'>
                                                    {item.job.company[0].company_name}
                                                </p>
                                                <p className='fs-12 mg-0'>
                                                {item.job.job_location[0].city}
                                                </p>
                                                
                                            </div>
                                        </div>
                
                                        <div className='row'>
                                            <div className='col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 job-card__job-details fs-12' >
                                                <div>
                        
                                                    <span className='mg-right-15'>
                                                        {item.job.employment_type_name} {' • '}
                                                    </span>
                                                    <span className='mg-right-15'>
                                                       {kFormater(item.job.ctc_min) } - {kFormater(item.job.ctc_max)} {item.job.salary_payment_type.toLowerCase()}
                                                    </span>
                                                    <span className='mg-right-15'>
                                                        {/* 1.5%-2% */}
                                                    </span>
                                                 </div>
                                                 <div className='flex justify-center-end'>

                                                 {/* {
                                                    interview && (<> <ApplyButton jobId={item.job.job_id} reject={reject}/></>)
                                                } */}
                 
                                                {
                                                    reject && (<><RejectButton companyId={item.job.company[0].company_id} id={item.id} isReject={isReject} jobId={item.job.job_id}/> <AcceptButton setIsReject={setIsReject} companyId={item.job.company[0].company_id} id={item.id}  reject={reject} jobId={item.job.job_id}/></>)
                                                }
                                                 </div>
                                            </div>
                                        </div>
                                        {
                                            all &&(
                                                <Grid container>
                                           <Grid xs={12}>
                                           <div className='flex justify-end'>
                
                                           <div className='mg-right-15 mg-top-10 flex align-item-center'>
                
                                                <SaveIcon jobId={item.job.job_id} isSave={item.job.is_saved} updateData={updateData} /> 
                                                </div>
                                                <div className='mg-right-15 mg-top-10 flex align-item-center'>
                
                                            
                                           <HiddenIcon jobId={item.job.job_id} hideStatus={hideStatus}/>
                                                </div>
                                           </div>
                
                                           </Grid>
                                       </Grid>
                                            )
                                        }
                
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Modal
                open={open}
                onClose={()=>handleClose(!open)}
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
              >
              <Grid container justify='flex-end'>
                  <Grid xs={12} sm={10}>
                 
                <ViewJob data={jobData} apply={interview} open={fullView} handleClosey={callback}/>
            
                  </Grid>
              </Grid>
              </Modal>
            </div>

                
            ))
        ) : (
            <div className='text-gray fs-20 fw-700 mg-top-20'>No Jobs</div>
        )


    )
}


const ApplyButton=({jobId,reject})=>{
  
    const [done,setDone]=useState(false)
    // console.log(reject)
    const handelActive=(jobId)=>{
    
        const data=JSON.parse(localStorage.getItem('user_details'))
       
            fetch('http://54.162.60.38/job/student/apply/',{
                method: 'POST',
                headers: {
                  'Content-Type': 'application/x-www-form-urlencoded'
                  
                },
                body: `job_id=${jobId}&user_id=MQ==&round=1&status=1&feedback=1`
              }).then((res)=>res.json()).then((data)=>{
                  alert(data.data.message)
                 
                  
                  setDone(true)
            })
              
        
        }
    return(
    <Button variant="contained" style={{backgroundColor:'#3282C4'}} className='text-white fw-700' disabled={done} onClick={(event)=>{event.stopPropagation();handelActive(jobId)}}>
         {
             reject ? (
                done ? 'Accepted' : 'Accept'
             ):
             (
                done ? 'Applied' : 'Apply'
             )
         } 
         {/* {
              done ? 'Applied' : 'Apply'
          } */}
        </Button>
    )

    
}



const AcceptButton=({id,reject,setIsReject,companyId,jobId})=>{
    console.log(reject)
    const data=JSON.parse(localStorage.getItem('user_details'))

    const [done,setDone]=useState(false)
    const handelActive=(jobId)=>{
 
        
     
            fetch('http://54.162.60.38/job/college/invite/?college_id=MQ==&job_status=TkVX',{
                method: 'POST',
                headers: {
                  'Content-Type': 'application/x-www-form-urlencoded'
                  
                },
                body: `id=${id}&job_id=${jobId}&college_id=1&company_id=${companyId}&is_approved=${true}&is_rejected=${false}&user_id=31`
              }).then((res)=>res.json()).then((data)=>{
                  alert(data.data.message)
                  
                  setDone(true)
                  setIsReject(true)
            })
              
        
        }
    return(
        <Button variant="contained" style={{backgroundColor:'#3282C4'}} className='text-white fw-700' disabled={done} onClick={(event)=>{event.stopPropagation();handelActive(jobId)}}>
         {
             reject ? (
                done ? 'Accepted' : 'Accept'
             ):
             (
                done ? 'Applied' : 'Apply'
             )
         } 
         {/* {
              done ? 'Applied' : 'Apply'
          } */}
        </Button>
        
    )

    
}

const RejectButton=({id,jobId,isReject,companyId})=>{
    const data=JSON.parse(localStorage.getItem('user_details'))
   
    const [done,setDone]=useState(false)
    const handelActive=(jobId)=>{
        
       
            fetch('http://54.162.60.38/job/college/invite/?college_id=MQ==&job_status=TkVX',{
                method: 'POST',
                headers: {
                  'Content-Type': 'application/x-www-form-urlencoded'
                  
                },
                body: `id=${id}&job_id=${jobId}&college_id=1&company_id=${companyId}&is_approved=${false}&is_rejected=${true}&user_id=31`
              }).then((res)=>res.json()).then((data)=>{
                  alert(data.data.message)
                 
                  setDone(true)
            })
              
        
        }
    return(
        
            !isReject && (

        <Button variant="outlined" color="primary" className='fw-700 mr-1' disabled={done} onClick={(event)=>{event.stopPropagation();handelActive(jobId)}}>
         {
              done ? 'Rejected' : 'Reject'
          }
        </Button>
            )
        
    )

    
}

const SaveIcon=({jobId,updateData,isSave})=>{
 
    const data=JSON.parse(localStorage.getItem('user_details'))
    const [done,setDone]=useState(isSave)
    const handelActive=(jobId)=>{
        
        if(done){
            fetch('http://54.162.60.38/job/student/status/',{
                method: 'POST',
                headers: {
                  'Content-Type': 'application/x-www-form-urlencoded'
                  
                },
                body: `job_id=${jobId}&user_id=${data.id}&status=unsave`
              }).then((res)=>res.json()).then((data)=>{
                  alert(data.data.message)
                setDone(!done)
                // updateData(jobId)
            })
        }
        else{
            fetch('http://54.162.60.38/job/student/status/',{
                method: 'POST',
                headers: {
                  'Content-Type': 'application/x-www-form-urlencoded'
                  
                },
                body: `job_id=${jobId}&user_id=${data.id}&status=saved`
              }).then((res)=>res.json()).then((data)=>{
                  alert(data.data.message)
                setDone(!done)
                // updateData(jobId)
            })
        }
     
           
              
        
        }
    return (
        <>
       {done ? <>  <BookmarkIcon className='pointer text-blue mg-right-5' onClick={(event)=>{event.stopPropagation();handelActive(jobId)}}/><span className='fs-12 text-blue'>Saved</span></>:<>  <BookmarkBorderIcon className='pointer text-blue mg-right-5' onClick={(event)=>{event.stopPropagation();handelActive(jobId)}}/><span className='fs-12 text-blue'>Save</span></>}
        </>
        
    )
}
const HiddenIcon=({jobId,hideStatus})=>{
    const [done,setDone]=useState(hideStatus)
    const handelActive=(jobId)=>{
        const data=JSON.parse(localStorage.getItem('user_details'))

        if(hideStatus){
            fetch('http://54.162.60.38/job/student/status/',{
                method: 'POST',
                headers: {
                  'Content-Type': 'application/x-www-form-urlencoded'
                  
                },
                body: `job_id=${jobId}&user_id=${data.id}&status=unhide`
              }).then((res)=>res.json()).then((data)=>{
                  alert(data.data.message)
                  setDone(false)
    
            })
        }else{

            fetch('http://54.162.60.38/job/student/status/',{
            method: 'POST',
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
              
            },
            body: `job_id=${jobId}&user_id=${data.id}&status=hidden`
          }).then((res)=>res.json()).then((data)=>{
              alert(data.data.message)
              setDone(true)

        })

        }
     
        

     
        
    
    }

    return (
        <>

      
        {
            done ? (<>  <VisibilityOffIcon className='pointer text-blue mg-right-5' onClick={(event)=>{event.stopPropagation();handelActive(jobId)}}/><span className='fs-12 text-blue'>hidden</span></>) : (<><VisibilityIcon className='pointer text-blue mg-right-5' onClick={(event)=>{event.stopPropagation();handelActive(jobId)}}/><span className='fs-12 text-blue'>hide</span></>)
        }
    
        </>
        
    )
}




// data!==null ?(

//     data.map((item)=>
//     (   
//                <div className='job-card mg-top-20' key={item.job.job_id}>
//         <Grid container>
//             <Grid  sm={12} xs={12}>

//             <Card style={{borderLeft:'3px solid #E55935'}} onClick={()=>openJob(item)} >
//                 <CardContent className='relative'>
//                         {
//                                 sideArrow && <div className='absolute pointer' style={{right:10}}> <ArrowForwardIosIcon onClick={()=>openJob(item)}/> </div>
                
//                         }     <Grid container>
//                         <Grid xs={12}>
//                             <Grid container className='job-card__top-header'>
//                                 <Grid sm={1} xs={2}>
//                                         <Avatar src={item.job.company[0].company_logo} alt='company-picture' style={AvatarStyle} />

//                                 </Grid>
//                                 <Grid xs={10} className='flex flex-col job-card__top-header__company-detail relative'>
//                               {
//                                   applied && <div className='absolute fs-16 fw-700 text-green' style={{right:0}}>{item.applicant_status_name}</div>
//                               }  
//                                     <h4 className='fs-16' onClick={()=>openJob(item)}>
//                                         {item.job.job_title}


//                                     </h4>

//                                     <p className='fs-12 mg-0'>
//                                         {item.job.company[0].company_name}
//                                     </p>
//                                     <p className='fs-12 mg-0'>
//                                     {item.job.company[0].company_location}
//                                     </p>

//                                 </Grid>
//                             </Grid>
//                         </Grid>
                     
//                            <Grid container className='mg-top-10'>
//                                <Grid xs={12} className='fs-12 job-card__job-details'>
//                                <div>

//                                   <span className='mg-right-15'>
//                                       {item.job.employment_type_name}
//                                   </span>
//                                   <span className='mg-right-15'>
//                                      {kFormater(item.job.ctc_min) } - {kFormater(item.job.ctc_max)} per {item.job.salary_payment_type.toLowerCase()}
//                                   </span>
//                                   <span className='mg-right-15'>
//                                       {/* 1.5%-2% */}
//                                   </span>
//                                </div>

//                                {
//                                    interview && (<> <ApplyButton jobId={item.job.job_id} reject={reject}/></>)
//                                }

//                                {
//                                    reject && (<><RejectButton companyId={item.job.company[0].company_id} id={item.id} isReject={isReject} jobId={item.job.job_id}/> <AcceptButton setIsReject={setIsReject} companyId={item.job.company[0].company_id} id={item.id}  reject={reject} jobId={item.job.job_id}/></>)
//                                }

                                 
//                                </Grid>
//                            </Grid>
//                             {
//                                 all &&(
//                                     <Grid container>
//                                <Grid xs={12}>
//                                <div className='flex justify-end'>

//                                <div className='mg-right-15 mg-top-10 flex align-item-center'>

//                                     <SaveIcon jobId={item.job.job_id} isSave={item.job.is_saved} updateData={updateData} /> 
//                                     </div>
//                                     <div className='mg-right-15 mg-top-10 flex align-item-center'>

                                
//                                <HiddenIcon jobId={item.job.job_id} hideStatus={hideStatus}/>
//                                     </div>
//                                </div>

//                                </Grid>
//                            </Grid>
//                                 )
//                             }
                          
                      
//                     </Grid>
//                 </CardContent>
//             </Card>
//             </Grid>
//         </Grid>
//         <Modal
// open={open}
// onClose={()=>handleClose(!open)}
// aria-labelledby="simple-modal-title"
// aria-describedby="simple-modal-description"
// >
// <Grid container justify='flex-end'>
//   <Grid xs={12} sm={10}>
 
// <ViewJob data={jobData} apply={interview} open={fullView} handleClosey={callback}/>

//   </Grid>
// </Grid>
// </Modal>
//         </div>

        
//     ))
// ) : (
//     <div className='text-gray fs-20 fw-700 mg-top-20'>No Jobs</div>
// )