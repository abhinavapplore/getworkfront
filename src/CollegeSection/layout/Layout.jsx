import React from 'react'
// import Sidebar from '../Components/Sidebar/Sidebar'
// import './layout.css'
import  { Breakpoint, BreakpointProvider } from 'react-socks';
import Navbar from '../components/NavBar/Navbar';
import { Container, Grid } from '@material-ui/core';
import CssBaseline from '@material-ui/core/CssBaseline';
// import Sidebar from '../../bundles/common/components/UI/Sidebar';
import SideBar from '../components/SideBar/SideBar'


export default function Layout({children}) {
  
    return (<>
        <BreakpointProvider>
        <Breakpoint large up>

          <Grid container>
            <Grid xs={12}>

               <Navbar/>
            </Grid>
          </Grid>
          <Grid container>
            <Grid xs={2}>

            <SideBar/>
            </Grid>
            <Grid xs={10 }>

            <div className="mg-top-20 comapnyconc" style={{marginTop:50,paddingRight:10}}>
                    {children}

                 </div>
            </Grid>
          </Grid>
        </Breakpoint>
        <Breakpoint medium down>
                <Navbar/>
                 <div className="mt-5">
                    {children}

                 </div>
        </Breakpoint>
      </BreakpointProvider>
      {/* <Sidebar userType='COMPANY'/> */}
      </>
    )
}
