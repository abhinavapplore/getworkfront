/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react'
import { BreakpointProvider, Breakpoint } from 'react-socks'
import Layout from '../../layout/Layout'
import { Grid } from '@material-ui/core'
import JobsCard from '../../components/JobCard/Index'
import baseUrl from '../../../common/CONSTANT'
import {httpRequest} from '../../../utils/httpRequest'
import NewJobCard from '../../components/JobCard/NewJobCard'

export default function NewJob() {

    const [jobData,setJobData]=useState([])

    
    const [newURL, setNewURL] = useState("");
  const [newEndPoint, setNewEndPoint] = useState("");
  const [end, setEnd] = useState(false);

    useEffect(()=>{
       
        GetData(baseUrl.niyukti.BASE_URL,`job/college/invite/?college_id=MQ==&job_status=TkVX`,null,setJobData)
        

    },[])

    const GetData=async(baseUrl,endPoint,body,updateState)=>{
       
                let res = await httpRequest(baseUrl,endPoint,body)
                if (res.data.next === null) {
                    setEnd(true);
                  } else {
                    setNewURL(res.data.next.slice(0, 20));
                    setNewEndPoint(res.data.next.slice(20));
                  }
                  updateState(jobData.concat([...res.data.results]))
               
              }

              const handleScroll = (event) => {
                let e = event.nativeEvent;
                if (
                  e.target.scrollTop + 10 >=
                  e.target.scrollHeight - e.target.clientHeight
                ) {
                  if (end !== true) {
                    const token = localStorage.getItem("gw_token");
                    GetData(newURL, newEndPoint, { headers: token }, setJobData);
                  }
                }
              };

    return (
        <div className='new-job'>
        <Layout>
            <div className='text-left'>
                
                <div className='row'>
                    <div className='col-12 col-sm-10 col-md-10 col-lg-10 col-xl-10'>
                    <div className='fs-16 fw-700 text-left'>
            {/* {
                jobData.length
            } {' '} New Jobs */}
        </div>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-12'>
                    <div id="myid" className="scrollY2"  style={{ marginTop: "10px"}} onScroll={handleScroll}>

                         <NewJobCard data={jobData} fullView={false} reject={true} all={false}  /> 
                         </div>
                    </div>
                </div>
                    
                </div>
          
        </Layout>
    </div>
    )
}
