/* eslint-disable */
import React, { useState, useEffect, useRef } from "react";
import Layout from "../../layout/Layout";
import CompanyCard from "../../components/companyCard/CompanyCard";
import baseUrl from "../../../common/CONSTANT";
import { httpRequest } from "../../../utils/httpRequest";

export default function Invite() {
	const [jobData, setJobData] = useState([]);
	const step = useRef(1);
	const [apiCall, setApiCall] = useState("connected");
	const [newURL, setNewURL] = useState("");
	const [newEndPoint, setNewEndPoint] = useState("");
	const [end, setEnd] = useState(false);

	useEffect(() => {
		GetData(
			baseUrl.pravesh.BASE_URL,
			"api/college/invite/?state=" + apiCall,
			{ headers: "330fc17905eabbb1935ad6e67fff73cd618503ee" },
			setJobData
		);
		setEnd(false);
	}, [apiCall]);
	const GetData = async (baseUrl, endPoint, body, updateState) => {
		console.log(baseUrl + endPoint);
		let res = await httpRequest(baseUrl, endPoint, body);
		console.log(res.data.next);
		if (res.data.next === null) {
			setEnd(true);
		} else {
			setNewURL(res.data.next.slice(0, 31));
			setNewEndPoint(res.data.next.slice(31));
		}
		updateState(jobData.concat([...res.data.results]));
	};

	const handleScroll = (event) => {
		let e = event.nativeEvent;
		if (
			e.target.scrollTop + 10 >=
			e.target.scrollHeight - e.target.clientHeight
		) {
			if (end !== true) {
				GetData(
					newURL,
					newEndPoint,
					{ headers: "330fc17905eabbb1935ad6e67fff73cd618503ee" },
					setJobData
				);
			}
		}
	};

	return (
		<div className="company-invite  mt-0">
			<Layout>
				<div
					className="row track-tabs"
					style={{ position: "relative", top: "-40px", width: "100vw" }}
				>
					<ul
						className="nav nav-tabs pt-3"
						id="myTab"
						role="tablist"
						style={{ background: "#fff" }}
					>
						<li
							className="nav-item mt-3 ml-5 mr-3 mx-xs-0"
							onClick={() => {
								step.current = 1;
								setApiCall("connected");
								setJobData([]);
							}}
						>
							<a
								className="nav-link active"
								id="home-tab"
								data-toggle="tab"
								href="#applicants"
								role="tab"
								aria-controls="home"
								aria-selected="true"
								style={{ color: "#000" }}
							>
								Connected
							</a>
						</li>
						{/* <li
							className="nav-item mt-3 mr-3"
							onClick={() => {
								step.current = 2;
								setApiCall("connected");
								setJobData([]);
							}}
						>
							<a
								className="nav-link"
								id="profile-tab"
								data-toggle="tab"
								href="#review"
								role="tab"
								aria-controls="profile"
								aria-selected="false"
								style={{ color: "#000" }}
							>
								Connected
							</a>
						</li> */}
						<li
							className="nav-item mt-3 mr-3"
							onClick={() => {
								step.current = 3;
								setApiCall("pending");
								setJobData([]);
							}}
						>
							<a
								className="nav-link"
								id="contact-tab"
								data-toggle="tab"
								href="#shortlisted"
								role="tab"
								aria-controls="contact"
								aria-selected="false"
								style={{ color: "#000" }}
							>
								Pending
							</a>
						</li>
						<li
							className="nav-item mt-3 mr-3"
							onClick={() => {
								step.current = 4;
								setApiCall("rejected");
								setJobData([]);
							}}
						>
							<a
								className="nav-link"
								id="contact-tab"
								data-toggle="tab"
								href="#interviewed"
								role="tab"
								aria-controls="contact"
								aria-selected="false"
								style={{ color: "#000" }}
							>
								Rejected
							</a>
						</li>
					</ul>
				</div>
				<div id="myflex">
				<div className="row">
					<div
						id="myid"
						className="scrollY3"
						style={{ minWidth: "90vw" }}
						onScroll={handleScroll}
					>
						<div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
							{jobData.length ? <CompanyCard data={jobData} /> : null}
						</div>
					</div>
				</div>
				</div>
				
			</Layout>
		</div>
	);
}
