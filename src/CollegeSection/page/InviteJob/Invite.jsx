/* eslint-disable */
import React, { useState, useEffect } from "react";
import Layout from "../../layout/Layout";
import CompanyCard from "../../components/companyCard/CompanyCard";
import baseUrl from "../../../common/CONSTANT";
import { httpRequest } from "../../../utils/httpRequest";

export default function Invite() {
	const [jobData, setJobData] = useState([]);
	const [search, setSearch] = useState("");
	const games = ["1", "2", "3", "4", "5"];
	const [game, setGame] = useState("");
	const [newURL, setNewURL] = useState("");
	const [newEndPoint, setNewEndPoint] = useState("");
	const [end, setEnd] = useState(false);

	useEffect(() => {
		GetData(
			baseUrl.pravesh.BASE_URL,
			"api/college/invite/?state=invite",
			{ headers: "330fc17905eabbb1935ad6e67fff73cd618503ee" },
			setJobData
		);
	}, []);
	const GetData = async (baseUrl, endPoint, body, updateState) => {
		let res = await httpRequest(baseUrl, endPoint, body);
		console.log(res.data.results);
		if (res.data.next === null) {
			setEnd(true);
		} else {
			setNewURL(res.data.next.slice(0, 31));
			setNewEndPoint(res.data.next.slice(31));
		}
		updateState(jobData.concat([...res.data.results]));
	};

	const handleScroll = (event) => {
		let e = event.nativeEvent;
		if (
			e.target.scrollTop + 10 >=
			e.target.scrollHeight - e.target.clientHeight
		) {
			if (end !== true) {
				GetData(
					newURL,
					newEndPoint,
					{ headers: "330fc17905eabbb1935ad6e67fff73cd618503ee" },
					setJobData
				);
			}
		}
	};

	return (
		<div className="company-invite  mt-0">
			<Layout>
				<nav
					className="job-pane fs-14 navbar fixed-top small-nav navbar-expand navbar-light topbar ml-auto"
					style={{ paddingLeft: "40vw", width: "100%", height: "90px" }}
				>
					<div className="gw-input-container fs-18 ml-auto">
						<i class="fas fa-search"></i>
						<input
							onChange={(e) => {
								setSearch(e.target.value);
							}}
							type="text"
							className="ml-5 gw-input input-secondary1 input-small mr-3"
							value={search}
							placeholder="Search"
						></input>
					</div>
				</nav>
				<nav
					className="job-pane fs-14 navbar fixed-top small-nav navbar-expand navbar-light topbar ml-auto"
					style={{
						paddingLeft: "40vw",
						width: "100%",
						marginTop: "90px",
						borderTop: "1px solid grey",
					}}
				>
					<div className="row fs-18 mx-5">
						<h3 style={{ marginLeft: "10vw" }}>Filters</h3>
						<div className="dropdown ml-4">
							<div
								className="dd dropdown-toggle-custom"
								type="button"
								id="dropdownMenuButton"
								data-toggle="dropdown"
								aria-haspopup="true"
								aria-expanded="false"
							>
								<span
									className="fs-15 d-inline-block text-truncate"
									style={{ maxWidth: "150px" }}
								>
									{game}
								</span>
							</div>
							<div
								className="dropdown-menu"
								style={{ height: "50vh", overflowY: "scroll" }}
								aria-labelledby="dropdownMenuButton"
							>
								{games &&
									games.length > 0 &&
									games.map((g) => {
										return (
											<button
												key={g}
												onClick={(e) => {
													setGame(e.target.value);
												}}
												className="dropdown-item cp"
											>
												<span
													className="d-inline-block text-truncate"
													style={{ maxWidth: "150px" }}
												></span>
												{g}
											</button>
										);
									})}
							</div>
						</div>
					</div>
				</nav>
				<div
					className="row track-tabs"
					style={{ position: "relative", top: "130px", width: "98vw" }}
				>
					<div style={{ paddingLeft: "30vw", marginTop: "20px" }}>
						<div
							style={{
								width: "480px",
								marginRight: "12px",
							}}
						>
							If you can’t find the company you are looking for, click on{" "}
							<b>invite other company</b>
						</div>
					</div>
					<div className="col-12 col-sm-3 col-md-3 col-lg-3 col-xl-3 d-flex justify-center">
						<div>
							<button
								style={{ background: "white", marginTop: "20px" }}
								className="btn btn-outline-primary"
								data-toggle="modal"
								data-target="#exampleModal"
							>
								Invite other company
							</button>
						</div>
					</div>
				</div>
				<div className="row comapnyconcinvite">
					<div
						className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12"
						style={{ marginTop: "150px" }}
					>
						<div
							id="myid"
							className="scrollY4"
							onScroll={handleScroll}
							style={{ minWidth: "89vw" }}
						>
							<div>
								<h5>Recommendations for you</h5>
							</div>
							{jobData.length ? <CompanyCard data={jobData} invitation={true} /> : null}
						</div>
					</div>
				</div>
			</Layout>
		</div>
	);
}
