/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react'
import baseUrl from '../../../common/CONSTANT'
import {httpRequest} from '../../../utils/httpRequest'
import Layout from '../../layout/Layout'
import { Grid } from '@material-ui/core'
import JobsCard from '../../components/JobCard/Index'
import NewJobCard from '../../components/JobCard/NewJobCard'
export default function OpenJob() {
    const [jobData,setJobData]=useState([])
    const [singleJobData,setSingleJobData]=useState(null)
    const [filter,setFiltersData]=useState(null)
    const data=JSON.parse(localStorage.getItem('user_details'))

    const [newURL, setNewURL] = useState("");
  const [newEndPoint, setNewEndPoint] = useState("");
  const [end, setEnd] = useState(false);
    useEffect(()=>{
        const token=localStorage.getItem('gw_token');
        GetData(baseUrl.niyukti.BASE_URL,`job/college/invite/?college_id=MQ==&job_status=T1BFTg==`,null,setJobData)
        

    },[])
    const GetData=async(baseUrl,endPoint,body,updateState)=>{
        // console.log(baseUrl,endPoint,body)
                let res = await httpRequest(baseUrl,endPoint,body)
                if (res.data.next === null) {
                    setEnd(true);
                  } else {
                    setNewURL(res.data.next.slice(0, 20));
                    setNewEndPoint(res.data.next.slice(20));
                  }
                updateState(jobData.concat([...res.data.results]))
               
              }

              useEffect(()=>{},[jobData])
              const handleScroll = (event) => {
                let e = event.nativeEvent;
                if (
                  e.target.scrollTop + 10 >=
                  e.target.scrollHeight - e.target.clientHeight
                ) {
                  if (end !== true) {
                    const token = localStorage.getItem("gw_token");
                    GetData(newURL, newEndPoint, { headers: token }, setJobData);
                  }
                }
              };
    return (
        <div className='college-open-job'>

        <Layout>
                    <div className='open-job text-left'>
        
                    
        
                    <Grid container className='mg-top-20'>
                        <Grid xs={12}>
                        <div className='fs-16 fw-700 text-left mx-auto open-job-college' style={{maxWidth: '1140px'}}>
                            {
                                jobData.length
                            } {' '} open jobs
                        </div>
                        </Grid>
                    </Grid>
                            <Grid container>
                            <Grid xs={12}>
                            <div id="myid" className="scrollY2"  style={{ marginTop: "10px"}} onScroll={handleScroll}>

                            <NewJobCard data={jobData}   interview={false} reject={false}/> 
                            </div>
                            </Grid>
                            </Grid>
        
                        
                    </div>
        
                </Layout>
                    
                </div>
    )
}
