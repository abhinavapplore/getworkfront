/* eslint-disable */
import React,{ useState,useEffect, useRef } from 'react'
import TrackPane from '../../components/TrackPane/TrackPane';
import './Track.css';
import dot_icon from '../../../assets/images/icons/dot-icons.png';
import applicant_pic from '../../../assets/images/applicant-pic.png';
import { FilterPane } from '../../components/FilterPane/FilterPane';
import Axios from 'axios';
import { environment as env } from '../../../environments/environment';
import {StatusMap} from '../../../constants/constants'
import {useAlert} from 'react-alert'
import Layout from '../../layout/Layout'

const Track = () => {

    const alert=useAlert()
    // const companyID=JSON.parse(localStorage.getItem('company')).company;

    //keep track of which tab applicant is at
    const step=useRef(1)

    //state that stores complete data from backend
    const [allData,setAllData]=useState()

    //intial data state variables
    const [allApplicants,setAllApplicants]=useState([])
    const [appliedApplicants,setAppliedApplicants]=useState([])
    const [profileApplicants,setProfileApplicants]=useState([])
    const [reviewedApplicants,setReviewedApplicants]=useState([])
    const [shortlistedApplicants,setShortistedApplicants]=useState([])
    const [interviewedApplicants,setInterviewedApplicants]=useState([])
    const [rejectedApplicants,setRejectedApplicants]=useState([])
    const [hiredApplicants,setHiredApplicants]=useState([])

    //state variables for updating status
    const [selectedApplicants,setSelectedApplicants]=useState({
        Applied:[],
        Reviewed:[],
        Shortlisted:[],
        Interviewed:[],
        Rejected:[],
        Hired:[]
    })

    //state variables for filtered applicants
    const [search,setSearch]=useState('')
    const [allFilteredApplicants,setAllFilteredApplicants]=useState([])
    const [filteredAppliedApplicants,setFilteredAppliedApplicants]=useState([])
    const [filteredProfileApplicants,setFilteredProfileApplicants]=useState([])
    const [filteredReviewedApplicants,setFilteredReviewedApplicants]=useState([])
    const [filteredShortlistedApplicants,setFilteredShortistedApplicants]=useState([])
    const [filteredInterviewedApplicants,setFilteredInterviewedApplicants]=useState([])
    const [filteredRejectedApplicants,setFilteredRejectedApplicants]=useState([])
    const [filteredHiredApplicants,setFilteredHiredApplicants]=useState([])

    //state variables for storing filters data
    const [allJobProfiles,setAllJobProfiles]=useState()
    const [allColleges,setAllColleges]=useState([])
    const [allLocations,setAllLocations]=useState([])
    const [allDegrees,setAllDegrees]=useState([])
    const [allPassoutYears,setAllPassoutYears]=useState([])
    const [allSkills,setAllSkills]=useState([])
    const [allWorkEx,setAllWorkEx]=useState([])
    //currently selected filter
    const [jobProfile,setJobProfile]=useState('All')
    const [college,setCollege]=useState('All')
    const [location,setLocation]=useState('All')
    const [degree,setDegree]=useState('All')
    const [passoutYear,setPassoutYear]=useState('All')
    const [skill,setSkill]=useState('All')
    const [workEx,setWorkEx]=useState('All')
    //all selected filter parameters
    const [selectedColleges,setSelectedColleges]=useState([])
    const [selectedLocations,setSelectedLocations]=useState([])
    const [selectedDegrees,setSelectedDegrees]=useState([])
    const [selectedPassoutYears,setSelectedPassoutYears]=useState([])
    const [selectedSkills,setSelectedSkills]=useState([])
    const [selectedWorkEx,setSelectedWorkEx]=useState([])

    const getAllApplicants=()=>{
        Axios.get(env.niyuktiBaseUrl+`college/applicants/?college_id=MQ==`)
            .then(res=>{
                // console.log(res)
                if(res.data.success){
                    setAllData(res.data.data.results)
                    console.log(res.data.data)
                    let all=[],profiles=[],applied=[],reviewed=[],shortlisted=[],interviewed=[],rejected=[],hired=[];
                    res.data.data.results.forEach(data=>{
                        profiles.push(data.job_title)
                        data.applicants.forEach(applicant=>{
                            all.push(applicant)
                            switch(applicant.status_name){
                                    case "Applied":         applied.push(applicant) 
                                                            break;
                                    case "Review":    reviewed.push(applicant)
                                                            break;
                                    case "Shortlisted" :    shortlisted.push(applicant)
                                                            break;
                                    case "Interview" :    interviewed.push(applicant)
                                                            break;
                                    case "Rejected"    :    rejected.push(applicant)
                                                            break;
                                    case "Hired"       :    hired.push(applicant)
                                                            break;
                                         default       :    break;
                            }
                        })
                    })
                    console.log(applied,reviewed,shortlisted,interviewed,rejected,hired)
                    //console.log()
                    setAllJobProfiles(['All',...profiles])
                    setAllApplicants(all)
                    setAppliedApplicants(applied)
                    setFilteredAppliedApplicants(applied)
                    setReviewedApplicants(reviewed)
                    setFilteredReviewedApplicants(reviewed)
                    setShortistedApplicants(shortlisted)
                    setFilteredShortistedApplicants(shortlisted)
                    setInterviewedApplicants(interviewed)
                    setFilteredInterviewedApplicants(interviewed)
                    setRejectedApplicants(rejected)
                    setFilteredRejectedApplicants(rejected)
                    setHiredApplicants(hired)
                    setFilteredHiredApplicants(hired)
                }
            })
            .catch(err=>{
                console.log(err)
            })
    }

    useEffect(()=>{
        getAllApplicants()
    },[])

    const filter=(arr)=>{
        arr=arr.filter(applicant=>{
            let fullname=applicant.first_name+applicant.last_name;
            let flag=true;
            if(jobProfile!=="All" && applicant.job_title!==jobProfile)
            flag=false;
            if(selectedColleges.length>0 && !selectedColleges.includes(applicant.education[0].college_name))
            flag=false;
            if(selectedLocations.length>0 && !selectedLocations.includes(applicant.current_city))
            flag=false;
            if(selectedWorkEx.length>0 && !selectedWorkEx.includes(applicant.work_ex))
            flag=false;
            if(selectedPassoutYears.length>0){
                //console.log('checking: ',selectedPassoutYears,applicant.education[0].end_date.split('-')[0])
                if(applicant.education[0].end_date==="null") 
                flag=false;
                else if(applicant.education[0].end_date && !selectedPassoutYears.includes(applicant.education[0].end_date.split('-')[0])){
                console.log('false for applicant ,',selectedPassoutYears,applicant,applicant.education[0].end_date)
                flag=false
                }
                
            } 
        return flag;
        })   
    return arr;    
    }

    const FilterHelper=()=>{
        let tempApplied=appliedApplicants,tempReviewed=reviewedApplicants,tempShortlisted=shortlistedApplicants,
        tempInterviewed=interviewedApplicants,tempRejected=rejectedApplicants,tempHired=hiredApplicants;

        tempApplied=filter(tempApplied)
        tempReviewed=filter(tempReviewed)
        tempShortlisted=filter(shortlistedApplicants)
        tempInterviewed=filter(interviewedApplicants)
        tempRejected=filter(tempRejected)
        tempHired=filter(tempHired)

        setFilteredAppliedApplicants(tempApplied)
        setFilteredReviewedApplicants(tempReviewed)
        setFilteredShortistedApplicants(tempShortlisted)
        setFilteredInterviewedApplicants(tempInterviewed)
        setFilteredRejectedApplicants(tempRejected)
        setFilteredHiredApplicants(tempHired)
    }

    const setApplicantFilters=()=>{
        let job={}
        allData.forEach(data=>{
           
           if(data.job_title===jobProfile){
            job=data;
            return;
           }
        })
        // console.log(job)
       
        // console.log(job)
        // console.log(...job.filter.colleges)
        // console.log(...job.filter.current_city)
        // setAllColleges(['All',...job.filter.colleges])
        setAllLocations(['All',...job.filter.current_city])
        setAllDegrees(['All',...job.filter.degree])
        setAllPassoutYears(['All',...job.filter.end_date])
        setAllSkills(['All',...job.filter.skills])
        setAllWorkEx(['All',...job.filter.work_experience])
    }

    useEffect(()=>{
        if(jobProfile==="All"){
            setFilteredAppliedApplicants(appliedApplicants)
            setFilteredReviewedApplicants(reviewedApplicants)
            setFilteredShortistedApplicants(shortlistedApplicants)
            setFilteredInterviewedApplicants(interviewedApplicants)
            setFilteredRejectedApplicants(rejectedApplicants)
            setFilteredHiredApplicants(hiredApplicants)
        }
        else if(jobProfile && jobProfile!=='All'){
         FilterHelper();
         setApplicantFilters()
        }
    },[jobProfile,search,selectedColleges,selectedLocations,selectedPassoutYears,selectedWorkEx])

    const ToggleAllApplicants=(status)=>{
        console.log('inside toggle all selected')
        let applicants=[],result=[];
        
        switch(status){
            case "Applied"      :  applicants=filteredAppliedApplicants;        break;
            case "Reviewed"     :  applicants=filteredReviewedApplicants;       break;
            case "Shortlisted"  :  applicants=filteredShortlistedApplicants;    break;
            case "Interviewed"  :  applicants=filteredInterviewedApplicants;    break;
            case "Rejected"     :  applicants=filteredRejectedApplicants;       break;
            case "Hired"        :  applicants=filteredHiredApplicants;          break;
            default: break
        }
       
        console.log(applicants)

        if(applicants.every(applicant=>selectedApplicants[status].includes(applicant.applicant_id))){
            result=[]
            console.log('empty array ',result)
        }
        else{
            result=applicants.map(applicant=>applicant.applicant_id)
            console.log('populate array ',result)
        }
     setSelectedApplicants({
         ...selectedApplicants,
         [status]:result
     })

    }

    const handleCheck=(status,applicant_id)=>{
        let temp=selectedApplicants[status]
        if(selectedApplicants[status].includes(applicant_id)){
            console.log('applicant selected, remove it',temp,applicant_id)
            //applicant selected, remove it
            temp=temp.filter(id=>id!==applicant_id)
            
        }else{
            console.log('applicant not present, add it',temp,applicant_id)
            //applicant not present, add it
            temp.push(applicant_id)
        }
        setSelectedApplicants({
            ...selectedApplicants,
            [status]:temp
        })
    }

    const UpdateStatus=(from, to)=>{
        
        let applicantIds=[],data=[],fromData=[],fromFiltered=[],toData=[],toFiltered=[]
        if(from==="Under Review"){
        console.log('here',from,to)
        console.log(selectedApplicants['Reviewed'])
        applicantIds=selectedApplicants['Reviewed']
        console.log(applicantIds)
        fromData=reviewedApplicants;
        fromFiltered=filteredReviewedApplicants;
        }
        else{
        //console.log('here #2')
        applicantIds=selectedApplicants[from]
            switch(from){
                case "Applied": fromData=appliedApplicants;fromFiltered=filteredAppliedApplicants; break;
                case "Shortlisted": fromData=shortlistedApplicants;fromFiltered=filteredShortlistedApplicants; break;
                case "Interviewed": fromData=interviewedApplicants;fromFiltered=filteredInterviewedApplicants; break;
                default : break;
            }
        }

        if(to=="Under Review"){
        toData=reviewedApplicants;
        toFiltered=filteredReviewedApplicants;
        }
        else{
        //applicantIds=selectedApplicants[from]
            switch(to){
                case "Rejected": toData=rejectedApplicants;toFiltered=filteredRejectedApplicants; break;
                case "Shortlisted": toData=shortlistedApplicants;toFiltered=filteredShortlistedApplicants; break;
                case "Interviewed": toData=interviewedApplicants;toFiltered=filteredInterviewedApplicants; break;
                case "Hired"   : toData=hiredApplicants;toFiltered=filteredHiredApplicants; break;
                default : break;
            }
        }

        for(let i=0; i<applicantIds.length; i++){
            data.push({
                "id":applicantIds[i],
                "round":2,
                "status":StatusMap[to],
                
            })
        }
        Axios.post(env.niyuktiBaseUrl+'company/status_update/',{
            "student_data":data
        })
            .then(res=>{
                console.log(res)
                if(res.data.success){
                    alert.success(res.data.data.message)
                    let temp=fromData.filter(data=>applicantIds.includes(data.applicant_id))
                    fromData=fromData.filter(data=>!applicantIds.includes(data.applicant_id))
                    fromFiltered=fromFiltered.filter(data=>!applicantIds.includes(data.applicant_id))
                    console.log(temp)
                    toData=[...toData,...temp]
                    toFiltered=[...toFiltered,...temp]
                    if(from=="Applied"){
                        setAppliedApplicants(fromData)
                        setFilteredAppliedApplicants(fromFiltered)
                        if(to=="Rejected"){
                            setRejectedApplicants(toData)
                            setFilteredRejectedApplicants(toFiltered)
                        }
                        else
                            setReviewedApplicants(toData)
                            setFilteredReviewedApplicants(toFiltered)
                    }
                    else if(from=="Under Review"){
                        setReviewedApplicants(fromData)
                        setFilteredReviewedApplicants(fromFiltered)
                        if(to=="Rejected"){
                            setRejectedApplicants(toData)
                            setFilteredRejectedApplicants(toFiltered)
                        }
                        else
                            setShortistedApplicants(toData)
                            setFilteredShortistedApplicants(toFiltered)
                    }
                    else if(from=="Shortlisted"){
                        setShortistedApplicants(fromData)
                        setFilteredShortistedApplicants(fromFiltered)
                        if(to=="Rejected"){
                            setRejectedApplicants(toData)
                            setFilteredRejectedApplicants(toFiltered)
                        }
                        else
                            setInterviewedApplicants(toData)
                            setFilteredInterviewedApplicants(toFiltered)
                    }
                    else if(from=="Interviewed"){
                        setInterviewedApplicants(fromData)
                        setFilteredInterviewedApplicants(fromFiltered)
                        if(to=="Rejected"){
                            setRejectedApplicants(toData)
                            setFilteredRejectedApplicants(toFiltered)
                        }
                        else
                            setHiredApplicants(toData)
                            setFilteredHiredApplicants(toFiltered)
                    }
                }
                else
                alert.error(res.data.error)
            })
            .catch(err=>{
                console.log(err)
            })
    }

    return (
        <div>
        <Layout>

           <TrackPane profiles={allJobProfiles} jobProfile={jobProfile} setJobProfile={setJobProfile} search={search} setSearch={setSearch}/>
            <div className="row">
                <div className="col-md-9 col-12 p-0">
                <div className="track-tabs">
                        <div>
                            <ul className="nav nav-tabs pt-3" id="myTab" role="tablist" style={{background:'#fff'}}>
                                <li className="nav-item mt-3 ml-5 mr-3 mx-xs-0" onClick={()=>{step.current=1}}>
                                <a className="nav-link active" id="home-tab" data-toggle="tab" href="#applicants" role="tab" aria-controls="home" aria-selected="true">Applicants</a>
                                </li>
                                <li  className="nav-item mt-3 mr-3" onClick={()=>{step.current=2}}>
                                <a className="nav-link" id="profile-tab" data-toggle="tab" href="#review" role="tab" aria-controls="profile" aria-selected="false">Review</a>
                                </li>
                                <li  className="nav-item mt-3 mr-3" onClick={()=>{step.current=3}}>
                                <a className="nav-link" id="contact-tab" data-toggle="tab" href="#shortlisted" role="tab" aria-controls="contact" aria-selected="false">Shortlisted</a>
                                </li>
                                <li className="nav-item mt-3 mr-3" onClick={()=>{step.current=4}}>
                                <a className="nav-link" id="contact-tab" data-toggle="tab" href="#interviewed" role="tab" aria-controls="contact" aria-selected="false">Interviewed</a>
                                </li>
                                <li  className="nav-item mt-3 mr-3" onClick={()=>{step.current=5}}>
                                <a className="nav-link" id="contact-tab" data-toggle="tab" href="#rejected" role="tab" aria-controls="contact" aria-selected="false">Rejected</a>
                                </li>
                                <li className="nav-item mt-3 mr-3" onClick={()=>{step.current=6}}>
                                <a className="nav-link" id="contact-tab" data-toggle="tab" href="#hired" role="tab" aria-controls="contact" aria-selected="false">Hired</a>
                                </li>
                            </ul>
                            <div className="tab-content" id="myTabContent">
                                
                                <div key={step} className="tab-pane fade show active" id="applicants" role="tabpanel" aria-labelledby="home-tab">
                                    <div className="card applicant-box mt-3">
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col-md-6 col-4 text-left">
                                                    <div className="form-check ml-3">
                                                        <input type="checkbox" onChange={()=>{ToggleAllApplicants('Applied')}} checked={filteredAppliedApplicants.every(applicant=>selectedApplicants['Applied'].includes(applicant.applicant_id))} className="form-check-input" id="exampleCheck1" />
                                                    </div>
                                                </div>
                                                <div className="col-md-3 col-4 text-left">
                                                
                                                  <p className="fs-18 mb-0 cp" onClick={()=>{UpdateStatus("Applied","Under Review")}}><strong>Move to next step</strong></p>
                                                </div>
                                                <div className="col-md-3 col-4 text-left">
                                                    <p className="fs-18 mb-0 cp" onClick={()=>{UpdateStatus("Applied","Rejected")}}><strong>Reject</strong></p>
                                                </div>
                                            </div>
                                            <hr/>
                                            {
                                                filteredAppliedApplicants ?
                                                (
                                                    filteredAppliedApplicants.length>0 ?
                                                    (
                                                        filteredAppliedApplicants.map((applicant)=>{
                                                            return(
                                                                <div className="row mx-auto" key={applicant.applicant_id}>
                                                                    <div className="card applicant-card">
                                                                        <div className="card-body">
                                                                            <div className="row">
                                                                                <div className="col-1 text-left">
                                                                                    <div className="form-check">
                                                                                        <input type="checkbox" onChange={()=>{handleCheck('Applied',applicant.applicant_id)}} checked={selectedApplicants.Applied.includes(applicant.applicant_id)} className="form-check-input" id="exampleCheck1" />
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-11 text-left">
                                                                                    <p className="fs-14 mb-0">{applicant.job_title}</p>
                                                                                </div>
                                                                            </div>
                                                                            <hr/>
                                                                            <div className="row">
                                                                                <div className="col-1 text-left"><img src={applicant.profile_picture} alt=""/></div>
                                                                                <div className="col-11 text-left pl-5">
                                                                                    <div className="row">
                                                                                        <div className="col-6 text-left">
                                                                                            <p className="mb-1 fs-14 gray-2">{applicant.first_name} {applicant.last_name}</p>
                                                                                            <p className="mb-1 fs-14"><strong>{applicant.education[0].degree} - Mechanical . Percentage - {applicant.education[0].percentage}</strong></p>
                                                                                            <p className="mb-1 fs-14"><strong>{applicant.education[0].college_name}</strong></p>
                                                                                        
                                                                                        </div>
                                                                                        <div className="col-6">
                                                                                            <div className="row">
                                                                                            {
                                                                                                applicant.skill && applicant.skill.map(skill=>{
                                                                                                    return(
                                                                                                        <div className="applicant-skill-badge mr-2" key={skill.skill_id}>
                                                                                                            <span>{skill.skill_name}</span>
                                                                                                        </div>
                                                                                                    )
                                                                                                })
                                                                                            }
                                                                                            </div>
                                                                                            

                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="row pl-3">
                                                                                        <p className="mb-1 fs-14 gray-3">
                                                                                                Product designer and full stack developer with interests in immersive computing and XR, political ventures and emerging technologies. Able to take ideas from concept to wireframe to prototype to production.
                                                                                            </p>

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            )
                                                        })

                                                    )
                                                    :
                                                    <>No Data</>
                                                ):
                                                <>Loading...</>

                                            }

                                        </div>
                                    </div>
                                </div>
                                <div key={step} className="tab-pane fade" id="review" role="tabpanel" aria-labelledby="profile-tab">
                                <div className="card applicant-box mt-3">
                                        <div className="card-body">
                                            <div className="row">
                                                {/* <div className="col-md-6 col-4 text-left">
                                                    <div className="form-check ml-3">
                                                        <input type="checkbox" onChange={()=>{ToggleAllApplicants('Reviewed')}} checked={filteredReviewedApplicants.every(applicant=>selectedApplicants['Reviewed'].includes(applicant.applicant_id))} className="form-check-input" id="exampleCheck1" />
                                                    </div>
                                                </div> */}
                                                {/* <div className="col-md-3 col-4 text-left">
                                                    <p className="fs-18 mb-0 cp" onClick={()=>{UpdateStatus("Under Review","Shortlisted")}}><strong>Move to next step</strong></p>
                                                </div>
                                                <div className="col-md-3 col-4 text-left">
                                                    <p className="fs-18 mb-0 cp" onClick={()=>{UpdateStatus("Under Review","Rejected")}}><strong>Reject</strong></p>
                                                </div> */}
                                            </div>
                                            <hr className='mt-5'/>
                                            {
                                                filteredReviewedApplicants ?
                                                (
                                                    filteredReviewedApplicants.length>0 ?
                                                    (
                                                        filteredReviewedApplicants.map((applicant)=>{
                                                            return(
                                                                <div className="row mx-auto" key={applicant.applicant_id}>
                                                                    <div className="card applicant-card">
                                                                        <div className="card-body">
                                                                            <div className="row">
                                                                                {/* <div className="col-1 text-left">
                                                                                    <div className="form-check">
                                                                                        <input type="checkbox" onChange={()=>{handleCheck('Reviewed',applicant.applicant_id)}} checked={selectedApplicants.Reviewed.includes(applicant.applicant_id)} className="form-check-input" id="exampleCheck1" />
                                                                                    </div>
                                                                                </div> */}
                                                                                <div className="col-11 text-left">
                                                                                    <p className="fs-14 mb-0">{applicant.job_title}</p>
                                                                                </div>
                                                                            </div>
                                                                            <hr/>
                                                                            <div className="row">
                                                                                <div className="col-1 text-left"><img src={applicant.profile_picture} alt=""/></div>
                                                                                <div className="col-11 text-left pl-5">
                                                                                    <div className="row">
                                                                                        <div className="col-6 text-left">
                                                                                            <p className="mb-1 fs-14 gray-2">{applicant.first_name} {applicant.last_name}</p>
                                                                                            <p className="mb-1 fs-14"><strong>{applicant.education[0].degree} - Mechanical . Percentage - {applicant.education[0].percentage}</strong></p>
                                                                                            <p className="mb-1 fs-14"><strong>{applicant.education[0].college_name}</strong></p>
                                                                                        
                                                                                        </div>
                                                                                        <div className="col-6">
                                                                                            <div className="row">
                                                                                            {
                                                                                                applicant.skill && applicant.skill.map(skill=>{
                                                                                                    return(
                                                                                                        <div className="applicant-skill-badge mr-2" key={skill.skill_id}>
                                                                                                            <span>{skill.skill_name}</span>
                                                                                                        </div>
                                                                                                    )
                                                                                                })
                                                                                            }
                                                                                            </div>
                                                                                            

                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="row pl-3">
                                                                                        <p className="mb-1 fs-14 gray-3">
                                                                                                Product designer and full stack developer with interests in immersive computing and XR, political ventures and emerging technologies. Able to take ideas from concept to wireframe to prototype to production.
                                                                                            </p>

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            )
                                                        })

                                                    )
                                                    :
                                                    <>No Data</>
                                                ):
                                                <>Loading...</>

                                            }
                                        </div>
                                    </div>
                                </div>
                                <div key={step} className="tab-pane fade" id="shortlisted" role="tabpanel" aria-labelledby="contact-tab">
                                <div className="card applicant-box mt-3">
                                        <div className="card-body">
                                            <div className="row">
                                                {/* <div className="col-md-6 col-4 text-left">
                                                    <div className="form-check ml-3">
                                                        <input type="checkbox" onChange={()=>{ToggleAllApplicants('Shortlisted')}} checked={filteredShortlistedApplicants.every(applicant=>selectedApplicants['Shortlisted'].includes(applicant.applicant_id))} className="form-check-input" id="exampleCheck1" />
                                                    </div>
                                                </div> */}
                                                {/* <div className="col-md-3 col-4 text-left">
                                                    <p className="fs-18 mb-0 cp" onClick={()=>{UpdateStatus("Shortlisted","Interviewed")}}><strong>Move to next step</strong></p>
                                                </div>
                                                <div className="col-md-3 col-4 text-left">
                                                    <p className="fs-18 mb-0 cp" onClick={()=>{UpdateStatus("Shortlisted","Rejected")}}><strong>Reject</strong></p>
                                                </div> */}
                                            </div>
                                            <hr className='mt-5'/>
                                            {
                                                filteredShortlistedApplicants ?
                                                (
                                                    filteredShortlistedApplicants.length>0 ?
                                                    (
                                                        filteredShortlistedApplicants.map((applicant)=>{
                                                            return(
                                                                <div className="row mx-auto" key={applicant.applicant_id}>
                                                                    <div className="card applicant-card">
                                                                        <div className="card-body">
                                                                            <div className="row">
                                                                                {/* <div className="col-1 text-left">
                                                                                    <div className="form-check">
                                                                                        <input type="checkbox" onChange={()=>{handleCheck('Shortlisted',applicant.applicant_id)}} checked={selectedApplicants.Shortlisted.includes(applicant.applicant_id)} className="form-check-input" id="exampleCheck1" />
                                                                                    </div>
                                                                                </div> */}
                                                                                <div className="col-11 text-left">
                                                                                    <p className="fs-14 mb-0">{applicant.job_title}</p>
                                                                                </div>
                                                                            </div>
                                                                            <hr/>
                                                                            <div className="row">
                                                                                <div className="col-1 text-left"><img src={applicant.profile_picture} alt=""/></div>
                                                                                <div className="col-11 text-left pl-5">
                                                                                    <div className="row">
                                                                                        <div className="col-6 text-left">
                                                                                            <p className="mb-1 fs-14 gray-2">{applicant.first_name} {applicant.last_name}</p>
                                                                                            <p className="mb-1 fs-14"><strong>{applicant.education[0].degree} - Mechanical . Percentage - {applicant.education[0].percentage}</strong></p>
                                                                                            <p className="mb-1 fs-14"><strong>{applicant.education[0].college_name}</strong></p>
                                                                                        
                                                                                        </div>
                                                                                        <div className="col-6">
                                                                                            <div className="row">
                                                                                            {
                                                                                                applicant.skill && applicant.skill.map(skill=>{
                                                                                                    return(
                                                                                                        <div className="applicant-skill-badge mr-2" key={skill.skill_id}>
                                                                                                            <span>{skill.skill_name}</span>
                                                                                                        </div>
                                                                                                    )
                                                                                                })
                                                                                            }
                                                                                            </div>
                                                                                            

                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="row pl-3">
                                                                                        <p className="mb-1 fs-14 gray-3">
                                                                                                Product designer and full stack developer with interests in immersive computing and XR, political ventures and emerging technologies. Able to take ideas from concept to wireframe to prototype to production.
                                                                                            </p>

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            )
                                                        })

                                                    )
                                                    :
                                                    <>No Data</>
                                                ):
                                                <>Loading...</>

                                            }
                                        </div>
                                    </div>
                                </div>
                                <div key={step} className="tab-pane fade" id="interviewed" role="tabpanel" aria-labelledby="contact-tab">
                                <div className="card applicant-box mt-3">
                                        <div className="card-body">
                                            <div className="row">
                                                {/* <div className="col-md-6 col-4 text-left">
                                                    <div className="form-check ml-3">
                                                        <input type="checkbox" onChange={()=>{ToggleAllApplicants('Interviewed')}} checked={filteredInterviewedApplicants.every(applicant=>selectedApplicants['Interviewed'].includes(applicant.applicant_id))} className="form-check-input" id="exampleCheck1" />
                                                    </div>
                                                </div> */}
                                                {/* <div className="col-md-3 col-4 text-left">
                                                    <p className="fs-18 mb-0 cp" onClick={()=>{UpdateStatus("Interviewed","Hired")}}><strong>Move to next step</strong></p>
                                                </div>
                                                <div className="col-md-3 col-4 text-left">
                                                    <p className="fs-18 mb-0 cp" onClick={()=>{UpdateStatus("Interviewed","Rejected")}}><strong>Reject</strong></p>
                                                </div> */}
                                            </div>
                                            <hr className='mt-5'/>
                                            {
                                                filteredInterviewedApplicants ?
                                                (
                                                    filteredInterviewedApplicants.length>0 ?
                                                    (
                                                        filteredInterviewedApplicants.map((applicant)=>{
                                                            return(
                                                                <div className="row mx-auto" key={applicant.applicant_id}>
                                                                    <div className="card applicant-card">
                                                                        <div className="card-body">
                                                                            <div className="row">
                                                                                {/* <div className="col-1 text-left">
                                                                                    <div className="form-check">
                                                                                        <input type="checkbox" onChange={()=>{handleCheck('Interviewed',applicant.applicant_id)}} checked={selectedApplicants.Interviewed.includes(applicant.applicant_id)} className="form-check-input" id="exampleCheck1" />
                                                                                    </div>
                                                                                </div> */}
                                                                                <div className="col-11 text-left">
                                                                                    <p className="fs-14 mb-0">{applicant.job_title}</p>
                                                                                </div>
                                                                            </div>
                                                                            <hr/>
                                                                            <div className="row">
                                                                                <div className="col-1 text-left"><img src={applicant.profile_picture} alt=""/></div>
                                                                                <div className="col-11 text-left pl-5">
                                                                                    <div className="row">
                                                                                        <div className="col-6 text-left">
                                                                                            <p className="mb-1 fs-14 gray-2">{applicant.first_name} {applicant.last_name}</p>
                                                                                            <p className="mb-1 fs-14"><strong>{applicant.education[0].degree} - Mechanical . Percentage - {applicant.education[0].percentage}</strong></p>
                                                                                            <p className="mb-1 fs-14"><strong>{applicant.education[0].college_name}</strong></p>
                                                                                        
                                                                                        </div>
                                                                                        <div className="col-6">
                                                                                            <div className="row">
                                                                                            {
                                                                                                applicant.skill && applicant.skill.map(skill=>{
                                                                                                    return(
                                                                                                        <div className="applicant-skill-badge mr-2" key={skill.skill_id}>
                                                                                                            <span>{skill.skill_name}</span>
                                                                                                        </div>
                                                                                                    )
                                                                                                })
                                                                                            }
                                                                                            </div>
                                                                                            

                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="row pl-3">
                                                                                        <p className="mb-1 fs-14 gray-3">
                                                                                                Product designer and full stack developer with interests in immersive computing and XR, political ventures and emerging technologies. Able to take ideas from concept to wireframe to prototype to production.
                                                                                            </p>

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            )
                                                        })

                                                    )
                                                    :
                                                    <>No Data</>
                                                ):
                                                <>Loading...</>

                                            }
                                        </div>
                                    </div>
                                </div>
                                <div key={step} className="tab-pane fade" id="rejected" role="tabpanel" aria-labelledby="contact-tab">
                                <div className="card applicant-box mt-3">
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col-md-6 col-4 text-left">
                                                    <div className="form-check ml-3 d-none">
                                                        <input type="checkbox" onChange={()=>{ToggleAllApplicants('Rejected')}} checked={filteredRejectedApplicants.every(applicant=>selectedApplicants['Rejected'].includes(applicant.applicant_id))} className="form-check-input" id="exampleCheck1" />
                                                    </div>
                                                </div>
                                                {/* <div className="col-md-3 col-4 text-left">
                                                    <p className="fs-18 mb-0 cp d-none"><strong>Move to next step</strong></p>
                                                </div>
                                                <div className="col-md-3 col-4 text-left">
                                                    <p className="fs-18 mb-0 cp d-none"><strong>Reject</strong></p>
                                                </div> */}
                                            </div>
                                            <hr className='mt-5'/>

                                                        {
                                                filteredRejectedApplicants ?
                                                (
                                                    filteredRejectedApplicants.length>0 ?
                                                    (
                                                        filteredRejectedApplicants.map((applicant)=>{
                                                            return(
                                                                <div className="row mx-auto" key={applicant.applicant_id}>
                                                                    <div className="card applicant-card">
                                                                        <div className="card-body">
                                                                            <div className="row">
                                                                                <div className="col-1 text-left">
                                                                                    <div className="form-check d-none">
                                                                                        <input type="checkbox" onChange={()=>{handleCheck('Rejected',applicant.applicant_id)}} checked={selectedApplicants.Rejected.includes(applicant.applicant_id)} className="form-check-input" id="exampleCheck1" />
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-11 text-left">
                                                                                    <p className="fs-14 mb-0">{applicant.job_title}</p>
                                                                                </div>
                                                                            </div>
                                                                            <hr/>
                                                                            <div className="row">
                                                                                <div className="col-1 text-left"><img src={applicant.profile_picture} alt=""/></div>
                                                                                <div className="col-11 text-left pl-5">
                                                                                    <div className="row">
                                                                                        <div className="col-6 text-left">
                                                                                            <p className="mb-1 fs-14 gray-2">{applicant.first_name} {applicant.last_name}</p>
                                                                                            <p className="mb-1 fs-14"><strong>{applicant.education[0].degree} - Mechanical . Percentage - {applicant.education[0].percentage}</strong></p>
                                                                                            <p className="mb-1 fs-14"><strong>{applicant.education[0].college_name}</strong></p>
                                                                                        
                                                                                        </div>
                                                                                        <div className="col-6">
                                                                                            <div className="row">
                                                                                            {
                                                                                                applicant.skill && applicant.skill.map(skill=>{
                                                                                                    return(
                                                                                                        <div className="applicant-skill-badge mr-2" key={skill.skill_id}>
                                                                                                            <span>{skill.skill_name}</span>
                                                                                                        </div>
                                                                                                    )
                                                                                                })
                                                                                            }
                                                                                            </div>
                                                                                            

                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="row pl-3">
                                                                                        <p className="mb-1 fs-14 gray-3">
                                                                                                Product designer and full stack developer with interests in immersive computing and XR, political ventures and emerging technologies. Able to take ideas from concept to wireframe to prototype to production.
                                                                                            </p>

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            )
                                                        })

                                                    )
                                                    :
                                                    <>No Data</>
                                                ):
                                                <>Loading...</>

                                            }
                                        </div>
                                    </div>
                                </div>
                                <div key={step} className="tab-pane fade" id="hired" role="tabpanel" aria-labelledby="contact-tab">
                                <div className="card applicant-box mt-3">
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col-md-6 col-4 text-left">
                                                    <div className="form-check ml-3 d-none">
                                                        <input type="checkbox d-none" onChange={()=>{ToggleAllApplicants('Hired')}} checked={filteredHiredApplicants.every(applicant=>selectedApplicants['Hired'].includes(applicant.applicant_id))} className="form-check-input" id="exampleCheck1" />
                                                    </div>
                                                </div>
                                                {/* <div className="col-md-3 col-4 text-left">
                                                    <p className="fs-18 mb-0 cp d-none"><strong>Move to next step</strong></p>
                                                </div>
                                                <div className="col-md-3 col-4 text-left">
                                                    <p className="fs-18 mb-0 cp d-none"><strong>Reject</strong></p>
                                                </div> */}
                                            </div>
                                            <hr className='mt-5'/>
                                            {
                                                filteredHiredApplicants ?
                                                (
                                                    filteredHiredApplicants.length>0 ?
                                                    (
                                                        filteredHiredApplicants.map((applicant)=>{
                                                            return(
                                                                <div className="row mx-auto" key={applicant.applicant_id}>
                                                                    <div className="card applicant-card">
                                                                        <div className="card-body">
                                                                            <div className="row">
                                                                                <div className="col-1 text-left">
                                                                                    <div className="form-check  d-none">
                                                                                        <input type="checkbox" onChange={()=>{handleCheck('Hired',applicant.applicant_id)}} checked={selectedApplicants.Hired.includes(applicant.applicant_id)} className="form-check-input" id="exampleCheck1" />
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-11 text-left">
                                                                                    <p className="fs-14 mb-0">{applicant.job_title}</p>
                                                                                </div>
                                                                            </div>
                                                                            <hr/>
                                                                            <div className="row">
                                                                                <div className="col-1 text-left"><img src={applicant.profile_picture} alt=""/></div>
                                                                                <div className="col-11 text-left pl-5">
                                                                                    <div className="row">
                                                                                        <div className="col-6 text-left">
                                                                                            <p className="mb-1 fs-14 gray-2">{applicant.first_name} {applicant.last_name}</p>
                                                                                            <p className="mb-1 fs-14"><strong>{applicant.education[0].degree} - Mechanical . Percentage - {applicant.education[0].percentage}</strong></p>
                                                                                            <p className="mb-1 fs-14"><strong>{applicant.education[0].college_name}</strong></p>
                                                                                        
                                                                                        </div>
                                                                                        <div className="col-6">
                                                                                            <div className="row">
                                                                                            {
                                                                                                applicant.skill && applicant.skill.map(skill=>{
                                                                                                    return(
                                                                                                        <div className="applicant-skill-badge mr-2" key={skill.skill_id}>
                                                                                                            <span>{skill.skill_name}</span>
                                                                                                        </div>
                                                                                                    )
                                                                                                })
                                                                                            }
                                                                                            </div>
                                                                                            

                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="row pl-3">
                                                                                        <p className="mb-1 fs-14 gray-3">
                                                                                                Product designer and full stack developer with interests in immersive computing and XR, political ventures and emerging technologies. Able to take ideas from concept to wireframe to prototype to production.
                                                                                            </p>

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            )
                                                        })

                                                    )
                                                    :
                                                    <>No Data</>
                                                ):
                                                <>Loading...</>

                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>

                        
                    </div>
                </div>
                <div className="col-md-3 col-12 mx-md-0 mx-5 mx-sm-0">
                    
                    <FilterPane show={jobProfile==="All" ? false : true}
                    //college
                    college={college} setCollege={setCollege} allColleges={allColleges} selectedJobColleges={selectedColleges} setSelectedJobColleges={setSelectedColleges}
                    //location
                    location={location} setLocation={setLocation} allLocations={allLocations} selectedJobLocations={selectedLocations} setSelectedJobLocations={setSelectedLocations}
                    //degrees
                    degree={degree} setDegree={setDegree} allDegrees={allDegrees} selectedJobDegrees={selectedDegrees} setSelectedJobDegrees={setSelectedDegrees}
                    //skills
                    skill={skill}  setSkill={setSkill}  allSkills={allSkills} selectedJobSkills={selectedSkills} setSelectedJobSkills={selectedSkills}
                    //work experience
                    workEx={workEx} setWorkEx={setWorkEx} allWorkEx={allWorkEx} selectedJobWorkEx={selectedWorkEx} setSelectedJobWorkEx={setSelectedWorkEx}
                    //pass out year
                    passoutYear={passoutYear} setPassoutYear={setPassoutYear} allPassoutYears={allPassoutYears} selectedJobPassoutYears={selectedPassoutYears} setSelectedJobPassoutYears={setSelectedPassoutYears}
                    />
                </div>
            </div>
        </Layout>


    </div>
    )
}


export default Track